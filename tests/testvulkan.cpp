#include "testvulkan.h"
#include "app/compoundpolyhedron.h"
#include "app/transformsolid.h"

#define WIDTH 800
#define HEIGHT 800

void TestVulkan::initTestCase() {
  window = new VulkanWindow();
  window->init(WIDTH, HEIGHT, "Polygonal");
  vulkan->createInstance(window->getRequiredExtensions());
  vulkan->m_graphics->m_surface = window->createSurface(vulkan->m_instance.get());
  if (!vulkan->pickPhysicalDevice())
    throw std::runtime_error(vulkan->getErrorMessage());
  if (!vulkan->createLogicalDevice())
    throw std::runtime_error(vulkan->getErrorMessage());
  if (!vulkan->m_graphics->createStdCommandPool())
    throw std::runtime_error(vulkan->m_graphics->getErrorMessage());
}

void TestVulkan::initTestSolidFill() {
  Config config("/tmp/test.json");

  config.writeFile();
  pt::ptree root = config.readFile();
  vulkan->cleanup();
  vulkan->getConfig(root);
  vulkan->setMousePos(0, 0);
  vulkan->m_leftPanel->m_type = vulkan->m_leftPanel->m_nextType;
  vulkan->buildSwapChain(false, WIDTH, HEIGHT);
  QVERIFY(vulkan->m_leftPanel->m_solidFillPipeline);
  QVERIFY(vulkan->m_graphics->createSyncObjects());
  vulkan->m_leftPanel->initUBO();
}

void TestVulkan::initTestDrawLine() {
  Config config("/tmp/test.json");

  config.writeFile();
  pt::ptree root = config.readFile();
  vulkan->cleanup();
  vulkan->getConfig(root);
  vulkan->setMousePos(0, 0);
  vulkan->m_leftPanel->m_type = vulkan->m_leftPanel->m_nextType;
  vulkan->m_leftPanel->setDrawLines(true);
  vulkan->buildSwapChain(false, WIDTH, HEIGHT);
  QVERIFY(vulkan->m_leftPanel->m_solidLinePipeline);
  QVERIFY(vulkan->m_graphics->createSyncObjects());
  vulkan->m_leftPanel->initUBO();
}

void TestVulkan::cleanupTestCase() {
  vulkan->cleanup();
  delete window;
}

void TestVulkan::testCubeFill() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createCube();
  testUpdateObjectFill(solid);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createPlatonic2Compound(solid));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
}

void TestVulkan::testCubeLines() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createCube();
  testUpdateObjectLines(solid);
}
void TestVulkan::testCube2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createCube2Compound());
  testUpdateObjectFill();
}

void TestVulkan::testCube3Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createCube3Compound());
  testUpdateObjectFill();
}

void TestVulkan::testCube4Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createCube4Compound());
  testUpdateObjectFill();
}

void TestVulkan::testCube5Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createCube5Compound());
  testUpdateObjectFill();
}

void TestVulkan::testCuboctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createCuboctahedron(0, 1);
  testUpdateObjectFill(solid);
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testUpdateObjectFill();
}

void TestVulkan::testDodecahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createDodecahedron(0);
  testUpdateObjectFill(solid);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createPlatonic2Compound(solid));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testUpdateObjectFill();
}

void TestVulkan::testDodecahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createDodecahedron2Compound());
  testUpdateObjectFill();
}

void TestVulkan::testIcosahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createIcosahedron(0);
  testUpdateObjectFill(solid);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createPlatonic2Compound(solid));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testUpdateObjectFill();
}

void TestVulkan::testIcosahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createIcosahedron2Compound());
  testUpdateObjectFill();
}

void TestVulkan::testOctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createOctahedron(0);
  testUpdateObjectFill(solid);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createPlatonic2Compound(solid));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
}

void TestVulkan::testOctahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createOctahedron2Compound());
  testUpdateObjectFill();
}

void TestVulkan::testTetrahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTetrahedron();
  testUpdateObjectFill(solid);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  m_solid.reset(c->createPlatonic2Compound(solid));
  testUpdateObjectFill();
  m_solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
  testUpdateObjectFill();
}

size_t TestVulkan::getNumberOfVertices(const std::unique_ptr<Polyhedron>& solid) {
  size_t result = 0;
  const auto mapCorners = solid->getNumberOfFacesByCorners();
  for (const auto& pair : mapCorners) {
    result += pair.first * pair.second;
  }
  return result;
}

size_t TestVulkan::getNumberOfIndices(const std::unique_ptr<Polyhedron>& solid) {
  size_t result = 0;
  const auto mapCorners = solid->getNumberOfFacesByCorners();
  for (const auto& pair : mapCorners) {
    result += 3 * (pair.first - 2) * pair.second;
  }
  return result;
}

void TestVulkan::testUpdateObjectFill(std::unique_ptr<Polyhedron>& solid) {
  solid->fillNormals();
  auto& pipeline = vulkan->m_leftPanel->m_solidFillPipeline;
  pipeline->clearAll();
  pipeline->fillBuffers(solid);
  const auto numberOfIndicesExpected = getNumberOfIndices(solid);
  const auto numberOfVerticesExpected = getNumberOfVertices(solid);
  const auto numberOfIndicesFound = pipeline->getNumberOfIndices();
  QVERIFY2(numberOfIndicesFound == numberOfIndicesExpected, QTest::toString(QStringLiteral("%1: Should have %2 indices. Found %3").arg(solid->getName().c_str()).arg(numberOfIndicesExpected).arg(numberOfIndicesFound)));
  const auto numberOfVerticesFound = pipeline->getNumberOfVertices();
  QVERIFY2(numberOfVerticesFound == numberOfVerticesExpected, QTest::toString(QStringLiteral("%1: Should have %2 vertices. Found %3").arg(solid->getName().c_str()).arg(numberOfVerticesExpected).arg(numberOfVerticesFound)));
}

void TestVulkan::testUpdateObjectLines(std::unique_ptr<Polyhedron>& solid) {
  solid->fillNormals();
  vulkan->m_leftPanel->m_solidLinePipeline->clearAll();
  vulkan->m_leftPanel->m_solidLinePipeline->fillBuffers(solid);
  const auto numberOfVerticesExpected = solid->getNumberOfVertices();
  const auto numberOfVerticesFound = vulkan->m_leftPanel->m_solidLinePipeline->getNumberOfVertices();
  QVERIFY2(numberOfVerticesFound == numberOfVerticesExpected, QTest::toString(QStringLiteral("%1: Should have %2 vertices. Found %3").arg(solid->getName().c_str()).arg(numberOfVerticesExpected).arg(numberOfVerticesFound)));
}

glm::mat4 TestVulkan::testMVP() {
  const auto size = vulkan->m_graphics->getExtent2D();

  glm::mat4 view = glm::lookAt(vulkan->m_leftPanel->getEye(), glm::dvec3(0.0, 0.0, 0.0), glm::dvec3(0.0, 0.0, 1.0));
  glm::mat4 proj = glm::perspectiveFov(M_PI_4f32 / 2.0f, static_cast<float>(size.width), static_cast<float>(size.height), 0.1f, 100.0f);
  // Vulkan clip space has inverted Y and half Z.
  static glm::mat4 clip(1.0f,  0.0f, 0.0f, 0.0f,
                       0.0f, -1.0f, 0.0f, 0.0f,
                       0.0f,  0.0f, 0.5f, 0.0f,
                       0.0f,  0.0f, 0.5f, 1.0f);
  return clip * proj * view;
}
