#include "testsolids.h"
#include "app/dualpolyhedron.h"
#include "app/dualcompound.h"
#include "app/expandedpolyhedron.h"
#include "app/regularpolyhedron.h"
#include "app/truncatedpolyhedron.h"
#include "app/zonohedron.h"
#include "app/compoundpolyhedron.h"
#include "app/transformsolid.h"
#include "app/morphdual.h"
#include "app/morphregular.h"

#define WIDTH 800
#define HEIGHT 600

void TestSolids::initTestCase() {
}

void TestSolids::cleanupTestCase() {
}

void TestSolids::testCube() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createCube();
  testMorphDual(solid);
  testMorphRegular(solid);
  testDualCompound(solid);
  testPlatonicSolid(solid, 4, 3);
  testSolidDual(solid, ObjectType::CUBE, ObjectType::OCTAHEDRON, 8, 6);
  testSolidExpand(solid, ObjectType::RHOMBI_CUBOCTAHEDRON);
  testSolidRegular(solid, ObjectType::CUBOCTAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_CUBE, ObjectType::TRIAKIS_OCTAHEDRON);
  testSolidZono(solid, ObjectType::RHOMBIC_DODECAHEDRON, 14, 12, true);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> compound(c->createPlatonic2Compound(solid));
  QVERIFY(compound);
  testSolid(compound, ObjectType::CUBE_2_COMPOUND, 26, 40);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 32),
                                                 std::make_pair(4, 8)};
  testFacesSameNumberOfCorners(compound, mapCornersExpected);
}

void TestSolids::testCube2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createCube2Compound());
  QVERIFY(solid);
  testSolid(solid, ObjectType::CUBE_2_COMPOUND, 20, 36);
}

void TestSolids::testCube3Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createCube3Compound());
  QVERIFY(solid);
  testSolid(solid, ObjectType::CUBE_3_COMPOUND, 86, 120);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 72),
                                                 std::make_pair(4, 48)};
  testFacesSameNumberOfCorners(solid, mapCornersExpected);
}

void TestSolids::testCube4Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createCube4Compound());
  QVERIFY(solid);
  testSolid(solid, ObjectType::CUBE_4_COMPOUND, 98, 96);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 48),
                                                 std::make_pair(4, 24),
                                                 std::make_pair(6, 24)};
  testFacesSameNumberOfCorners(solid, mapCornersExpected);
}

void TestSolids::testCube5Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createCube5Compound());
  QVERIFY(solid);
  testSolid(solid, ObjectType::CUBE_5_COMPOUND, 182, 360);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 360)};
  testFacesSameNumberOfCorners(solid, mapCornersExpected);
}

void TestSolids::testCuboctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createCuboctahedron(0, 1);
  testMorphDual(solid);
  testMorphRegular(solid);

  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 8),
                                                 std::make_pair(4, 6)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::CUBOCTAHEDRON, ObjectType::RHOMBIC_DODECAHEDRON, 12, 14);
  testSolidRegular(solid, ObjectType::RHOMBI_CUBOCTAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_CUBOCTAHEDRON, ObjectType::DISDYAKIS_DODECAHEDRON);
  testSolidZono(solid, ObjectType::TRUNCATED_OCTAHEDRON, 24, 14, true);
  std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION));
  testSolid(stellation, 26, 48);
}

void TestSolids::testCuboctahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createCuboctahedron2Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 36),
                                         std::make_pair(12, 2)};
  testFacesSameNumberOfCorners(solid, mapCorners);
  testSolid(solid, ObjectType::CUBOCTAHEDRON_2_COMPOUND, 30, 38);
}

void TestSolids::testDodecahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createDodecahedron(0);
  testMorphDual(solid);
  testMorphRegular(solid);

  testDualCompound(solid);
  testPlatonicSolid(solid, 5, 3);
  testSolidDual(solid, ObjectType::DODECAHEDRON, ObjectType::ICOSAHEDRON, 20, 12);
  testSolidExpand(solid, ObjectType::RHOMBICOSIDODECAHEDRON);
  testSolidRegular(solid, ObjectType::ICOSIDODECAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_DODECAHEDRON, ObjectType::TRIAKIS_ICOSAHEDRON);
  std::unique_ptr<Polyhedron> stellation1(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testSolid(stellation1, ObjectType::SMALL_STELLATED_DODECAHEDRON, 32, 60);
  std::unique_ptr<Polyhedron> stellation2(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
  testSolid(stellation2, ObjectType::GREAT_DODECAHEDRON, 32, 60);
  std::unique_ptr<Polyhedron> stellation3(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 3));
  testSolid(stellation3, ObjectType::GREAT_STELLATED_DODECAHEDRON, 32, 60);
  std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> compound(c->createPlatonic2Compound(solid));
  QVERIFY(compound);
  testSolid(compound, ObjectType::DODECAHEDRON_2_COMPOUND, 38, 72);
}
/*
void TestSolids::testDodecahedron2Compound() {
  CompoundPolyhedron c(colorList);
  std::unique_ptr<Polyhedron> solid(c.createDodecahedron2Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 72)};
  testFacesSameNumberOfCorners(*solid.get(), mapCorners);
  testSolid(*solid.get(), ObjectType::DODECAHEDRON_2_COMPOUND, 38, 72);
}
*/
void TestSolids::testIcosidodecahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createIcosidodecahedron(0, 1);
  testMorphDual(solid);
  testMorphRegular(solid);

  testSolidDual(solid, ObjectType::ICOSIDODECAHEDRON, ObjectType::RHOMBIC_TRIACONTAHEDRON, 30, 32);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 20),
                                         std::make_pair(5, 12)};
  testArchimedeanSolid(solid, mapCorners);
  std::unique_ptr<Polyhedron> stellation1(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testSolid(stellation1, 62, 120);
  std::unique_ptr<Polyhedron> stellation2(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
  testSolid(stellation2, 122, 240);
}

void TestSolids::testIcosahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createIcosahedron(0);
  testMorphDual(solid);
  testMorphRegular(solid);

  testDualCompound(solid);
  testPlatonicSolid(solid, 3, 5);
  testSolidDual(solid, ObjectType::ICOSAHEDRON, ObjectType::DODECAHEDRON, 12, 20);
  testSolidExpand(solid, ObjectType::RHOMBICOSIDODECAHEDRON);
  testSolidRegular(solid, ObjectType::ICOSIDODECAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_ICOSAHEDRON, ObjectType::PENTAKIS_DODECAHEDRON);
  std::unique_ptr<Polyhedron> stellation1(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testSolid(stellation1, ObjectType::SMALL_TRIAMBIC_ICOSAHEDRON, 32, 60);
  std::unique_ptr<Polyhedron> stellation2(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
  testSolid(stellation2, ObjectType::OCTAHEDRON_5_COMPOUND, 62, 120);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> compound(c->createPlatonic2Compound(solid));
  QVERIFY(compound);
  testSolid(compound, ObjectType::ICOSAHEDRON_2_COMPOUND, 78, 80);
}

void TestSolids::testIcosahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createIcosahedron2Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 72),
                                         std::make_pair(12, 8)};
  testFacesSameNumberOfCorners(solid, mapCorners);
  testSolid(solid, ObjectType::ICOSAHEDRON_2_COMPOUND, 78, 80);
}

void TestSolids::testOctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createOctahedron(0);
  testMorphDual(solid);
  testMorphRegular(solid);

  testDualCompound(solid);
  testPlatonicSolid(solid, 3, 4);
  testSolidDual(solid, ObjectType::OCTAHEDRON, ObjectType::CUBE, 6, 8);
  testSolidExpand(solid, ObjectType::RHOMBI_CUBOCTAHEDRON);
  testSolidRegular(solid, ObjectType::CUBOCTAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_OCTAHEDRON, ObjectType::TETRAKIS_HEXAHEDRON);
  testSolidZono(solid, ObjectType::CUBE, 8, 6, true);
  std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION));
  testStellatedOctahedron(stellation);
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> compound(c->createPlatonic2Compound(solid));
  QVERIFY(compound);
  testSolid(compound, ObjectType::OCTAHEDRON_2_COMPOUND, 30, 48);
}

void TestSolids::testOctahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createOctahedron2Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 36),
                                         std::make_pair(12, 2)};
  testSolid(solid, ObjectType::OCTAHEDRON_2_COMPOUND, 30, 38, mapCorners);
}

void TestSolids::testOctahedron3Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createOctahedron3Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 48),
                                         std::make_pair(4, 24)};
  testSolid(solid, ObjectType::OCTAHEDRON_3_COMPOUND, 50, 72, mapCorners);
}

void TestSolids::testOctahedron4Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createOctahedron4Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 72),
                                         std::make_pair(4, 72)};
  testSolid(solid, ObjectType::OCTAHEDRON_4_COMPOUND, 110, 144, mapCorners);
}

void TestSolids::testPentagonalBipyramid() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createPentagonalBipyramid();
  testMorphDual(solid);
  testMorphRegular(solid);

  testEdgesSameLength(solid);
  testFacesSameNumberOfCorners(solid, 3);
  testSolidDual(solid, ObjectType::PENTAGONAL_BIPYRAMID, ObjectType::PENTAGONAL_PRISM, 7, 10);
}

void TestSolids::testRhombicDodecahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createRhombicDodecahedron();
  testMorphDual(solid);
  testMorphRegular(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(4, 12)};
  testFacesSameNumberOfCorners(solid, mapCorners);
  testSolidDual(solid, ObjectType::RHOMBIC_DODECAHEDRON, ObjectType::CUBOCTAHEDRON, 14, 12);
  testSolidRegular(solid, ObjectType::RHOMBI_CUBOCTAHEDRON);
  std::unique_ptr<Polyhedron> stellation1(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
  testSolid(stellation1, ObjectType::RHOMBIC_DODECAHEDRON_FIRST_STELLATION, 26, 48);
  std::unique_ptr<Polyhedron> stellation2(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
  mapCorners = {std::make_pair(3, 48),
                std::make_pair(4, 24)};
  testSolid(stellation2, ObjectType::RHOMBIC_DODECAHEDRON_SECOND_STELLATION, 50, 72, mapCorners);
}

void TestSolids::testRhombicDodecahedron2Compound() {
  auto c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> solid(c->createRhombicDodecahedron2Compound());
  QVERIFY(solid);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 24),
                                         std::make_pair(6, 6)};
  testSolid(solid, ObjectType::RHOMBIC_DODECAHEDRON_2_COMPOUND, 26, 30, mapCorners);
}

void TestSolids::testSnubCube() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createSnubCube();
  testMorphDual(solid);
  testMorphRegular(solid);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 32),
                                                 std::make_pair(4, 6)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::SNUB_CUBE, ObjectType::PENTAGONAL_ICOSITETRAHEDRON, 24, 38);
}

void TestSolids::testStellatedOctahedron(const std::unique_ptr<Polyhedron>& solid) {
  testSolid(solid, ObjectType::STELLATED_OCTAHEDRON, 14, 24);
  std::map<size_t, size_t> mapCorners = {std::make_pair(3, 24)};
  testFacesSameNumberOfCorners(solid, mapCorners);
}

void TestSolids::testTetrahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTetrahedron();
  testMorphDual(solid);
  testMorphRegular(solid);
  testDualCompound(solid);
  testPlatonicSolid(solid, 3, 3);
  testSolidDual(solid, ObjectType::TETRAHEDRON, ObjectType::TETRAHEDRON, 4, 4);
  testSolidExpand(solid, ObjectType::CUBOCTAHEDRON);
  testSolidRegular(solid, ObjectType::OCTAHEDRON);
  testSolidTruncatedDual(solid, ObjectType::TRUNCATED_TETRAHEDRON, ObjectType::TRIAKIS_TETRAHEDRON);
  testSolidZono(solid, ObjectType::RHOMBIC_DODECAHEDRON, 14, 12, true);
  std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(colorList);
  std::unique_ptr<Polyhedron> compound(c->createPlatonic2Compound(solid));
  QVERIFY(compound);
  testStellatedOctahedron(compound);
}

void TestSolids::testTetrakishexahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTetrakisHexahedron();
  testMorphDual(solid);
  testSolidDual(solid, ObjectType::TETRAKIS_HEXAHEDRON, ObjectType::TRUNCATED_OCTAHEDRON, 14, 24);
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 72)};
    testSolid(stellation, ObjectType::DODECAHEDRON_2_COMPOUND, 38, 72, mapCornersExpected);
  }
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 144)};
    testSolid(stellation, ObjectType::TETRAKIS_HEXAHEDRON_SECOND_STELLATION, 74, 144, mapCornersExpected);
  }
}

void TestSolids::testTriakisOctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTriakisOctahedron();
  testMorphDual(solid);
  testSolidDual(solid, ObjectType::TRIAKIS_OCTAHEDRON, ObjectType::TRUNCATED_CUBE, 14, 24);
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 72)};
    testSolid(stellation, ObjectType::TRIAKIS_OCTAHEDRON_FIRST_STELLATION, 38, 72, mapCornersExpected);
  }
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 96), std::make_pair(4, 24)};
    testSolid(stellation, ObjectType::TRIAKIS_OCTAHEDRON_SECOND_STELLATION, 74, 120, mapCornersExpected);
  }
}
void TestSolids::testTriakisTetrahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTriakisTetrahedron();
  testMorphDual(solid);
  testSolidDual(solid, ObjectType::TRIAKIS_TETRAHEDRON, ObjectType::TRUNCATED_TETRAHEDRON, 8, 12);
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1));
    testSolid(stellation, ObjectType::TRIAKIS_TETRAHEDRON_FIRST_STELLATION, 20, 36);
  }
  {
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 48),
                                                   std::make_pair(4, 12)};
    testSolid(stellation, ObjectType::TRIAKIS_TETRAHEDRON_SECOND_STELLATION, 38, 60, mapCornersExpected);
  }
}

void TestSolids::testTriangularBipyramid() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTriangularBipyramid();
  testMorphDual(solid);
  testMorphRegular(solid);
  testEdgesSameLength(solid);
  testFacesSameNumberOfCorners(solid, 3);
  testSolidDual(solid, ObjectType::TRIANGULAR_BIPYRAMID, ObjectType::TRIANGULAR_PRISM, 5, 6);
  testSolidZono(solid, ObjectType::HEXAGONAL_PRISM, 12, 8, false);
}

void TestSolids::testTruncatedCube() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTruncatedCube();
  testMorphDual(solid);
  testMorphRegular(solid);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 8),
                                                 std::make_pair(8, 6)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::TRUNCATED_CUBE, ObjectType::TRIAKIS_OCTAHEDRON, 24, 14);
}

void TestSolids::testTruncatedDodecahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTruncatedDodecahedron();
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 20),
                                                 std::make_pair(10, 12)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::TRUNCATED_DODECAHEDRON, ObjectType::TRIAKIS_ICOSAHEDRON, 60, 32);
  {
    std::unique_ptr<Polyhedron> dual(TransformSolid::transform(solid, PolyhedronTransformation::DUAL, 1));
    std::unique_ptr<Polyhedron> stellation(TransformSolid::transform(dual, PolyhedronTransformation::STELLATION, 1));
    std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 180)};
    testSolid(stellation, ObjectType::TRIAKIS_ICOSAHEDRON_FIRST_STELLATION, 92, 180, mapCornersExpected);
  }
}

void TestSolids::testTruncatedOctahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTruncatedOctahedron();
  testMorphDual(solid);
  testMorphRegular(solid);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(4, 6),
                                                 std::make_pair(6, 8)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::TRUNCATED_OCTAHEDRON, ObjectType::TETRAKIS_HEXAHEDRON, 24, 14);
}

void TestSolids::testTruncatedTetrahedron() {
  auto solid = std::make_unique<Polyhedron>(colorList, true);
  solid->createTruncatedTetrahedron();
  testMorphDual(solid);
  testMorphRegular(solid);
  std::map<size_t, size_t> mapCornersExpected = {std::make_pair(3, 4),
                                                 std::make_pair(6, 4)};
  testArchimedeanSolid(solid, mapCornersExpected);
  testSolidDual(solid, ObjectType::TRUNCATED_TETRAHEDRON, ObjectType::TRIAKIS_TETRAHEDRON, 12, 8);
}

//PRIVATE

void TestSolids::testMorphDual(const std::unique_ptr<Polyhedron>& solid) {
  auto morph = std::make_unique<MorphDual>(solid);
  const auto solidType = morph->getSolidType();
  const auto solidName = std::move(Polyhedron::getName(solidType).c_str());
  const auto nFacesExpected = 2 * (solid->getNumberOfFaces() + solid->getNumberOfVertices()) - 2;
  const auto nFacesCreated = morph->getNumberOfFaces();
  QVERIFY2(nFacesCreated == nFacesExpected, QTest::toString(QStringLiteral("Dual morph of solid %1 should have %2 faces. Found %3").arg(solidName).arg(nFacesExpected).arg(nFacesCreated)));
  const auto nStartVerticesExpected = solid->getNumberOfVertices();
  const auto nStartVerticesCreated = morph->getNumberOfVertices(0);
  QVERIFY2(nStartVerticesCreated == nStartVerticesExpected, QTest::toString(QStringLiteral("Dual morph of solid %1 should have %2 start vertices. Found %3").arg(solidName).arg(nStartVerticesExpected).arg(nStartVerticesCreated)));
  const auto nSolidVertices = solid->getNumberOfVertices();
  size_t nEndVerticesExpected = 0;
  for (size_t iVertex = 0; iVertex < nSolidVertices; iVertex++) {
    for (const auto& edge : solid->getEdges()) {
      if (edge.iVertexBegin == iVertex || edge.iVertexEnd == iVertex)
        nEndVerticesExpected++;
    }
  }
  nEndVerticesExpected *= 2;
  const auto nEndVerticesCreated = morph->getNumberOfVertices(1);
  QVERIFY2(nEndVerticesCreated == nEndVerticesExpected, QTest::toString(QStringLiteral("Dual morph of solid %1 should have %2 end vertices. Found %3").arg(solidName).arg(nEndVerticesExpected).arg(nEndVerticesCreated)));
}

void TestSolids::testMorphRegular(const std::unique_ptr<Polyhedron>& solid) {
  auto morph = std::make_unique<MorphRegular>(solid);
  const auto solidType = morph->getSolidType();
  const auto solidName = std::move(Polyhedron::getName(solidType).c_str());
  const auto nFacesExpected = solid->getNumberOfFaces() + solid->getNumberOfVertices();
  const auto nFacesCreated = morph->getNumberOfFaces();
  QVERIFY2(nFacesCreated == nFacesExpected, QTest::toString(QStringLiteral("Regular morph of solid %1 should have %2 faces. Found %3").arg(solidName).arg(nFacesExpected).arg(nFacesCreated)));
  const auto nStartVerticesExpected = solid->getNumberOfVertices();
  const auto nStartVerticesCreated = morph->getNumberOfVertices(0);
  QVERIFY2(nStartVerticesCreated == nStartVerticesExpected, QTest::toString(QStringLiteral("Regular morph of solid %1 should have %2 start vertices. Found %3").arg(solidName).arg(nStartVerticesExpected).arg(nStartVerticesCreated)));
  const auto nEndVerticesExpected = getNumberOfVertices(solid);
  const auto nEndVerticesCreated = morph->getNumberOfVertices(1);
  QVERIFY2(nEndVerticesCreated == nEndVerticesExpected, QTest::toString(QStringLiteral("Regular morph of solid %1 should have %2 end vertices. Found %3").arg(solidName).arg(nEndVerticesExpected).arg(nEndVerticesCreated)));
}

void TestSolids::testArchimedeanSolid(const std::unique_ptr<Polyhedron>& solid, const std::map<size_t, size_t>& mapCorners) {
  testEdgesSameLength(solid);
  testFacesSameNumberOfCorners(solid, mapCorners);
  testFacesSameAngle(solid);
}

void TestSolids::testDualCompound(const std::unique_ptr<Polyhedron>& solid) {
  const auto originalType = solid->getType();
  const auto nSolidVertices = solid->getNumberOfVertices();
  const auto nSolidFaces = solid->getNumberOfFaces();
  auto d = std::make_unique<DualCompound>(solid);
  if (!d->m_target)
    QFAIL("Could not create a dual compound");
  const auto nDualVerticesCreated = d->m_target->getNumberOfVertices();
  const auto nDualVerticesExpected = 2 * nSolidVertices + 2 * nSolidFaces - 2;
  size_t nDualFacesExpected = 0;
  for (const auto& face : solid->getFaces()) {
    nDualFacesExpected += face.getCorners() * 2;
  }
  const auto nFacesCreated = d->m_target->getNumberOfFaces();
  // Should be all triangles
  testFacesSameNumberOfCorners(d->m_target, 3);
  QVERIFY2(nDualVerticesCreated == nDualVerticesExpected, QTest::toString(QStringLiteral("Dual compound of %1: Should have %2 vertices. Found %3").arg(Polyhedron::getName(originalType).c_str()).arg(nDualVerticesExpected).arg(nDualVerticesCreated)));
  QVERIFY2(nFacesCreated == nDualFacesExpected, QTest::toString(QStringLiteral("Dual compound of %1: Should have %2 faces. Found %3").arg(Polyhedron::getName(originalType).c_str()).arg(nDualFacesExpected).arg(nFacesCreated)));
}

void TestSolids::testEdgesSameLength(const std::unique_ptr<Polyhedron>& solid) {
  const auto minLength = solid->getMinimumLengthEdge();
  const auto maxLength = solid->getMaximumLengthEdge();
  QVERIFY2((maxLength - minLength) / maxLength < 0.0001, QTest::toString(QStringLiteral("%1: Edges should be of the same length").arg(solid->getName().c_str())));
}

void TestSolids::testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, size_t numberOfCorners) {
  const auto& faces = solid->getFaces();
  for (const auto& face : faces) {
    const auto corners = face.getCorners();
    QVERIFY2(corners == numberOfCorners, QTest::toString(QStringLiteral("%1: Should have %2 corners. Found %3").arg(solid->getName().c_str()).arg(numberOfCorners).arg(corners)));
  }
}

void TestSolids::testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, const std::vector<size_t> numberOfCorners) {
  const auto& faces = solid->getFaces();
  for (const auto& face : faces) {
    const auto corners = face.getCorners();
    QVERIFY2(contains(numberOfCorners, corners), QTest::toString(QStringLiteral("Wrong number of corners. Found %1").arg(corners)));
  }
}
// mapCornersExpected : Map with pairs <X corners, number of faces with X corners>
void TestSolids::testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, const std::map<size_t, size_t> mapCornersExpected) {
  const auto mapCornersFound = solid->getNumberOfFacesByCorners();
  for (const auto& pair : mapCornersFound) {
    const auto it = mapCornersExpected.find(pair.first);
    QVERIFY2(it != mapCornersExpected.end(), QTest::toString(QStringLiteral("%1: Found a face with %2 corners. None expected").arg(solid->getName().c_str()).arg(pair.first)));
    QVERIFY2(it->second == pair.second, QTest::toString(QStringLiteral("%1: Found faces with %2 corners. Expected %3 faces. Found %4 faces").arg(solid->getName().c_str()).arg(pair.first).arg(it->second).arg(pair.second)));
  }
}
// numberOfCorners : Number of corners for each face
// nVertexFaces : Number of faces at each vertex
void TestSolids::testPlatonicSolid(const std::unique_ptr<Polyhedron>& solid, size_t numberOfCorners, size_t nVertexFaces) {
  testEdgesSameLength(solid);
  testFacesSameNumberOfCorners(solid, numberOfCorners);
  testVertexFaces(solid, nVertexFaces);
  testSameAngle(solid);
}
// Test whether all angles of each face of a solid are the same
void TestSolids::testFacesSameAngle(const std::unique_ptr<Polyhedron>& solid) {
  const auto& faces = solid->getFaces();
  const auto& vertices = solid->getVertices();
  for (const auto& face : faces) {
    auto dotpMin = SNAN;
    auto dotpMax = SNAN;
    const auto nCorners = face.getCorners();
    for (size_t i = 0; i < nCorners; ++i) {
      const auto previousVertex = vertices[face[(i - 1 + nCorners) % nCorners]];
      const auto currentVertex = vertices[face[i]];
      const auto nextVertex = vertices[face[(i + 1) % nCorners]];
      const auto dotp = glm::dot(glm::normalize(previousVertex - currentVertex), glm::normalize(nextVertex - currentVertex));
      if (isnan(dotpMax)) {
        dotpMax = dotp;
        dotpMin = dotp;
      } else {
        if (dotp < dotpMin)
          dotpMin = dotp;
        if (dotp > dotpMax)
          dotpMax = dotp;
      }
    }
    QVERIFY2(qFuzzyIsNull(dotpMax - dotpMin), "Angles of a face should be the same");
  }
}
// Test whether all angles of a solid are the same
void TestSolids::testSameAngle(const std::unique_ptr<Polyhedron>& solid) {
  const auto& faces = solid->getFaces();
  const auto& vertices = solid->getVertices();
  auto dotpMin = SNAN;
  auto dotpMax = SNAN;
  for (const auto& face : faces) {
    const auto nCorners = face.getCorners();
    for (size_t i = 0; i < nCorners; ++i) {
      const auto previousVertex = vertices[face[(i - 1 + nCorners) % nCorners]];
      const auto currentVertex = vertices[face[i]];
      const auto nextVertex = vertices[face[(i + 1) % nCorners]];
      const auto dotp = glm::dot(glm::normalize(previousVertex - currentVertex), glm::normalize(nextVertex - currentVertex));
      if (isnan(dotpMax)) {
        dotpMax = dotp;
        dotpMin = dotp;
      } else {
        if (dotp < dotpMin)
          dotpMin = dotp;
        if (dotp > dotpMax)
          dotpMax = dotp;
      }
    }
  }
  QVERIFY2(qFuzzyIsNull(dotpMax - dotpMin), "All angles should be the same");
}

void TestSolids::testSolid(const std::unique_ptr<Polyhedron>& solid, size_t numberOfVertices, size_t numberOfFaces, size_t numberOfEdges) {
  const auto currentType = solid->getType();
  const auto nVertices = solid->getNumberOfVertices();
  QVERIFY2(nVertices == numberOfVertices, QTest::toString(QStringLiteral("%1: Should have %2 vertices. Found %3").arg(Polyhedron::getName(currentType).c_str()).arg(numberOfVertices).arg(nVertices)));
  const auto nFaces = solid->getNumberOfFaces();
  QVERIFY2(nFaces == numberOfFaces, QTest::toString(QStringLiteral("%1: Should have %2 faces. Found %3").arg(Polyhedron::getName(currentType).c_str()).arg(numberOfFaces).arg(nFaces)));
  const auto nEdges = solid->getNumberOfEdges();
  QVERIFY2(nEdges == numberOfEdges, QTest::toString(QStringLiteral("%1: Should have %2 edges. Found %3").arg(Polyhedron::getName(currentType).c_str()).arg(numberOfEdges).arg(nEdges)));
  testSolidVertices(solid);
}

void TestSolids::testSolid(const std::unique_ptr<Polyhedron>& solid, const ObjectType expectedType, size_t numberOfVertices, size_t numberOfFaces) {
  const auto numberOfEdges = numberOfVertices + numberOfFaces - 2;
  testSolid(solid, numberOfVertices, numberOfFaces, numberOfEdges);
  const auto currentType = solid->getType();
  QVERIFY2(currentType == expectedType, QTest::toString(QStringLiteral("Found type %1. Expected %2").arg(Polyhedron::getName(currentType).c_str()).arg(Polyhedron::getName(expectedType).c_str())));
}

void TestSolids::testSolid(const std::unique_ptr<Polyhedron>& solid, const ObjectType expectedType, size_t numberOfVertices, size_t numberOfFaces, const std::map<size_t, size_t> mapCornersExpected) {
  testSolid(solid, expectedType, numberOfVertices, numberOfFaces);
  testFacesSameNumberOfCorners(solid, mapCornersExpected);
}

void TestSolids::testSolidDual(const std::unique_ptr<Polyhedron>& solid, ObjectType originalType, ObjectType dualType, size_t numberOfVertices, size_t numberOfFaces) {
  testSolid(solid, originalType, numberOfVertices, numberOfFaces);
  const auto d = std::make_unique<DualPolyhedron>(solid);
  QVERIFY(d->m_target);
  const auto actualDualType = d->m_target->getType();
  QVERIFY2(actualDualType == dualType, QTest::toString(QStringLiteral("Found type %1. Expected %2").arg(Polyhedron::getName(actualDualType).c_str()).arg(Polyhedron::getName(dualType).c_str())));
  testSolid(d->m_target, dualType, numberOfFaces, numberOfVertices);
  if (d->m_target->m_isPlatonic) {
    testEdgesSameLength(d->m_target);
    testSameAngle(d->m_target);
  }
}

void TestSolids::testSolidExpand(const std::unique_ptr<Polyhedron>& solid, ObjectType expandType) {
  const auto nOriginalVertices = solid->getNumberOfVertices();
  const auto nOriginalFaces = solid->getNumberOfFaces();
  const auto nOriginalEdges = solid->getNumberOfEdges();
  const auto& originalEdges = solid->getEdges();
  // Count the number of vertices of the expanded solid
  auto vertexEdges = std::vector<ushort>(nOriginalVertices);
  for (const auto& edge : originalEdges) {
    vertexEdges[edge.iVertexBegin]++;
    vertexEdges[edge.iVertexEnd]++;
  }
  ushort nVertices = 0;
  for (const auto iVertexEdge : vertexEdges) {
    nVertices += iVertexEdge;
  }
  const auto e = std::make_unique<ExpandedPolyhedron>(solid);
  QVERIFY(e->m_target);
  testSolid(e->m_target, expandType, nVertices, nOriginalVertices + nOriginalFaces + nOriginalEdges);
}

void TestSolids::testSolidRegular(const std::unique_ptr<Polyhedron>& solid, ObjectType regularType) {
  const auto nOriginalVertices = solid->getNumberOfVertices();
  const auto nOriginalFaces = solid->getNumberOfFaces();
  const auto r = std::make_unique<RegularPolyhedron>(solid);
  QVERIFY(r->m_target);
  testSolid(r->m_target, regularType, nOriginalVertices + nOriginalFaces - 2, nOriginalVertices + nOriginalFaces);
}

void TestSolids::testSolidTruncatedDual(const std::unique_ptr<Polyhedron>& solid, ObjectType truncatedType, ObjectType dualType) {
  const auto nOriginalVertices = solid->getNumberOfVertices();
  const auto nOriginalFaces = solid->getNumberOfFaces();
  const auto t = std::make_unique<TruncatedPolyhedron>(solid);
  QVERIFY(t->m_target);
  testSolidDual(t->m_target, truncatedType, dualType, (nOriginalVertices + nOriginalFaces - 2) * 2, nOriginalVertices + nOriginalFaces);
}

void TestSolids::testSolidVertices(const std::unique_ptr<Polyhedron>& solid) {
  const auto& faces = solid->getFaces();
  const auto nVertices = solid->getNumberOfVertices();
  for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
    ushort count = 0;
    for (const auto& face : faces) {
      if (face.contains(static_cast<uint16_t>(iVertex)))
        count++;
    }
    QVERIFY(count > 2);
  }
}

void TestSolids::testSolidZono(const std::unique_ptr<Polyhedron>& solid, ObjectType zonoType, ushort numberOfVertices, ushort numberOfFaces, bool checkLength) {
  auto z = std::make_unique<Zonohedron>(solid);
  QVERIFY(z->m_target);
  testSolid(z->m_target, zonoType, numberOfVertices, numberOfFaces);
  if (checkLength)
    testEdgesSameLength(z->m_target);
  testZonoFaces(z->m_target);
}

void TestSolids::testVertexFaces(const std::unique_ptr<Polyhedron>& solid, const size_t nVertexFaces) {
  const auto nOriginalVertices = solid->getNumberOfVertices();
  const auto& originalEdges = solid->getEdges();
  // Count the number of vertices of the expanded solid
  auto vertexEdges = std::vector<ushort>(nOriginalVertices);
  for (const auto& edge : originalEdges) {
    vertexEdges[edge.iVertexBegin]++;
    vertexEdges[edge.iVertexEnd]++;
  }
  for (const auto iFaces : vertexEdges) {
    QVERIFY2(iFaces == nVertexFaces, QTest::toString(QStringLiteral("Found %1 faces. Expected %2").arg(iFaces).arg(nVertexFaces)));
  }
}

void TestSolids::testZonoFaces(const std::unique_ptr<Polyhedron>& solid) {
  const auto& faces = solid->getFaces();
  for (const auto& face : faces) {
    const auto corners = face.getCorners();
    QVERIFY2(corners % 2 == 0, QTest::toString(QStringLiteral("Zonohedron should even number of corners. Found %1").arg(corners)));
  }
}

size_t TestSolids::getNumberOfVertices(const std::unique_ptr<Polyhedron>& solid) {
  size_t result = 0;
  const auto mapCorners = solid->getNumberOfFacesByCorners();
  for (const auto& pair : mapCorners) {
    result += pair.first * pair.second;
  }
  return result;
}

/*

void TestMain::testStellatedOctahedron() {
  mainWindow->changeToOctahedron();
  focusWidget->operatorStellation();
  const auto &solid = focusWidget->getCurrentObject(true);
  testEdgesSameLength(solid);
  testFacesSameNumberOfCorners(solid, 3);
  testSolid(solid, ObjectType::STELLATED_OCTAHEDRON, 14, 24);
}

void TestMain::testTruncatedIcosahedron() {
  mainWindow->changeToTruncatedIcosahedron();
  const auto &solid = focusWidget->getOriginalPolyhedron();
  testArchimedeanSolid(solid, {5,6});
  testSolidDual(solid, ObjectType::TRUNCATED_ICOSAHEDRON, ObjectType::PENTAKIS_DODECAHEDRON, 60, 32);
}

void TestMain::testDualCompound(const PolyHedron &originalSolid) {
  const auto originalType = originalSolid.getType();
  const auto numberOfVertices = originalSolid.getVerticesSize();
  const auto numberOfFaces = originalSolid.getFacesSize();
  focusWidget->operatorDualCompound();
  const auto solid = focusWidget->getCurrentObject(false);
  const auto nVerticesCreated = solid.getVerticesSize();
  const auto nVerticesExpected = 2 * numberOfVertices + 2 * numberOfFaces - 2;
  ushort nFacesExpected = 0;
  for (const auto &face : originalSolid.getFaces()) {
    nFacesExpected += face.getCorners() * 2;
  }
  const auto nFacesCreated = solid.getFacesSize();
  // Should be all triangles
  testFacesSameNumberOfCorners(solid, 3);
  focusWidget->undoLastOperator();
  QVERIFY2(nVerticesCreated == nVerticesExpected, QTest::toString(QStringLiteral("Dual compound of %1: Should have %2 vertices. Found %3").arg(PolyHedron::getNameByType(originalType)).arg(nVerticesExpected).arg(nVerticesCreated)));
  QVERIFY2(nFacesCreated == nFacesExpected, QTest::toString(QStringLiteral("Dual compound of %1: Should have %2 faces. Found %3").arg(PolyHedron::getNameByType(originalType)).arg(nFacesExpected).arg(nFacesCreated)));
}

void TestMain::testFacesSameNumberOfCorners(const PolyHedron &solid, const QVector<int> numberOfCorners) {
  const auto &faces = solid.getFaces();
  for (const auto &face : faces) {
    const auto corners = face.getCorners();
    QVERIFY2(numberOfCorners.contains(corners), QTest::toString(QStringLiteral("Wrong number of corners. Found %1").arg(corners)));
  }
}

void TestMain::testSettings(const ObjectData &originalObject, const QString originalFile, const QString nextFile, const ObjectOperator action) {
  // Test loading the original solid
  ObjectData object;
  ViewData view;
  mainWindow->loadSettings(originalFile, object, view);
  QVERIFY2(object.objectType == originalObject.objectType, QTest::toString(QStringLiteral("Loaded original type %1. Expected %2").arg(PolyHedron::getNameByType(object.objectType)).arg(PolyHedron::getNameByType(originalObject.objectType))));
  QVERIFY(object.objectOperators.size() == originalObject.objectOperators.size());
  mainWindow->loadSettings(nextFile, object, view);
  QVERIFY2(object.objectType == originalObject.objectType, QTest::toString(QStringLiteral("Loaded type %1. Expected %2").arg(PolyHedron::getNameByType(object.objectType)).arg(PolyHedron::getNameByType(originalObject.objectType))));
  QVERIFY(object.objectOperators.last() == uint(action));
}
*/
//QTEST_MAIN(TestSolids)

