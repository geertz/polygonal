QT += testlib
QT -= gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    testvulkan.cpp \
    testsolids.cpp \
    main.cpp

LIBS += -L../src -lmyapp
LIBS += -lglfw -lvulkan


INCLUDEPATH += $$PWD/../src

HEADERS += \
    testvulkan.h \
    testsolids.h

copydata.commands = $(COPY_DIR) $$PWD/../src/shaders $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

DISTFILES +=
