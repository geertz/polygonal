#pragma once

#pragma GCC diagnostic ignored "-Wdeprecated-copy"

#include <QtTest/QtTest>
#include "app/polyhedron.h"
#include "app/morphbase.h"

class TestSolids: public QObject {
  Q_OBJECT

private slots:
  void initTestCase();
  void testCube();
  void testCube2Compound();
  void testCube3Compound();
  void testCube4Compound();
  void testCube5Compound();
  void testCuboctahedron();
  void testCuboctahedron2Compound();
  void testDodecahedron();
//  void testDodecahedron2Compound();
  void testIcosahedron();
  void testIcosahedron2Compound();
  void testIcosidodecahedron();
  void testOctahedron();
  void testOctahedron2Compound();
  void testOctahedron3Compound();
  void testOctahedron4Compound();
  void testPentagonalBipyramid();
  void testRhombicDodecahedron();
  void testRhombicDodecahedron2Compound();
  void testSnubCube();
  void testTetrahedron();
  void testTetrakishexahedron();
  void testTriakisOctahedron();
  void testTriakisTetrahedron();
  void testTriangularBipyramid();
  void testTruncatedCube();
  void testTruncatedDodecahedron();
  void testTruncatedOctahedron();
  void testTruncatedTetrahedron();
  /*
  void testStellatedOctahedron();
  void testTruncatedIcosahedron();*/
  void cleanupTestCase();
private:
  std::vector<glm::vec3> colorList = {
    {1.0f, 0.0, 0.0f}, // red
    {0.0f, 1.0, 0.0f}, // green
    {0.0f, 0.0, 1.0f}, // blue
    {1.0f, 1.0, 0.0f} // yellow
  };
  void testArchimedeanSolid(const std::unique_ptr<Polyhedron>& solid, const std::map<size_t, size_t>& mapCorners);
  void testDualCompound(const std::unique_ptr<Polyhedron>& solid);
  void testEdgesSameLength(const std::unique_ptr<Polyhedron>& solid);
  void testFacesSameAngle(const std::unique_ptr<Polyhedron>& solid);
  void testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, size_t numberOfCorners);
  void testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, const std::vector<size_t> numberOfCorners);
  void testFacesSameNumberOfCorners(const std::unique_ptr<Polyhedron>& solid, const std::map<size_t, size_t> mapCornersExpected);
  void testMorphDual(const std::unique_ptr<Polyhedron>& solid);
  void testMorphRegular(const std::unique_ptr<Polyhedron>& solid);
  void testPlatonicSolid(const std::unique_ptr<Polyhedron>& solid, size_t numberOfCorners, size_t nVertexFaces);
  void testSameAngle(const std::unique_ptr<Polyhedron>& solid);
  inline void testSolid(const std::unique_ptr<Polyhedron>& solid, size_t numberOfVertices, size_t numberOfFaces) {
    testSolid(solid, numberOfVertices, numberOfFaces, numberOfVertices + numberOfFaces - 2);
  }
  void testSolid(const std::unique_ptr<Polyhedron>& solid, const ObjectType expectedType, size_t numberOfVertices, size_t numberOfFaces);
  void testSolid(const std::unique_ptr<Polyhedron>& solid, size_t numberOfVertices, size_t numberOfFaces, size_t numberOfEdges);
  void testSolid(const std::unique_ptr<Polyhedron>& solid, const ObjectType expectedType, size_t numberOfVertices, size_t numberOfFaces, const std::map<size_t, size_t> mapCornersExpected);
  void testSolidDual(const std::unique_ptr<Polyhedron>& solid, ObjectType originalType, ObjectType dualType, size_t numberOfVertices, size_t numberOfFaces);
  void testSolidExpand(const std::unique_ptr<Polyhedron>& solid, ObjectType expandType);
  void testSolidRegular(const std::unique_ptr<Polyhedron>& solid, ObjectType regularType);
  void testSolidTruncatedDual(const std::unique_ptr<Polyhedron>& solid, ObjectType truncatedType, ObjectType dualType);
  void testSolidVertices(const std::unique_ptr<Polyhedron>& solid);
  void testSolidZono(const std::unique_ptr<Polyhedron>& solid, ObjectType zonoType, ushort numberOfVertices, ushort numberOfFaces, bool checkLength);
  void testVertexFaces(const std::unique_ptr<Polyhedron>& solid, const size_t nVertexFaces);
  void testZonoFaces(const std::unique_ptr<Polyhedron>& solid);
  void testStellatedOctahedron(const std::unique_ptr<Polyhedron>& solid);
  static size_t getNumberOfVertices(const std::unique_ptr<Polyhedron>& solid);
};

