#pragma once

#pragma GCC diagnostic ignored "-Wdeprecated-copy"

#include <QtTest/QtTest>
#include "app/vulkanwindow.h"
#include "app/vulkanlib.h"
#include "app/polyhedron.h"

class TestVulkan: public QObject {
  Q_OBJECT

private slots:
  void initTestCase();
  void initTestSolidFill();
  void testCubeFill();
  void testCube2Compound();
  void testCube3Compound();
  void testCube4Compound();
  void testCube5Compound();
  void testCuboctahedron();
  void testDodecahedron();
  void testDodecahedron2Compound();
  void testIcosahedron();
  void testIcosahedron2Compound();
  void testOctahedron();
  void testOctahedron2Compound();
  void testTetrahedron();
  //void cleanupVulkan();
  void initTestDrawLine();
  void testCubeLines();
  void cleanupTestCase();
private:
  VulkanWindow *window = nullptr;
  std::unique_ptr<VulkanLib> vulkan = std::make_unique<VulkanLib>();
  std::vector<glm::vec3> colorList{
    {1.0f, 0.0, 0.0f},
    {0.0f, 1.0, 0.0f},
    {0.0f, 0.0, 1.0f}
  };
  std::unique_ptr<Polyhedron> m_solid;

  glm::mat4 testMVP();
  void testUpdateObjectFill(std::unique_ptr<Polyhedron>& solid);
  void testUpdateObjectLines(std::unique_ptr<Polyhedron>& solid);
  inline void testUpdateObjectFill() {
    testUpdateObjectFill(m_solid);
  }
  static size_t getNumberOfIndices(const std::unique_ptr<Polyhedron>& solid);
  static size_t getNumberOfVertices(const std::unique_ptr<Polyhedron>& solid);
};
