#include <QtTest/QtTest>
#include "testsolids.h"
#include "testvulkan.h"

int main(int argc, char *argv[]) {
  // setup lambda
  int status = 0;
  auto runTest = [&status, argc, argv](QObject* obj) {
    status |= QTest::qExec(obj, argc, argv);
  };
  runTest(new TestSolids);
  runTest(new TestVulkan);
  return  status;
}
