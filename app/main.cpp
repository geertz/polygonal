#include <boost/property_tree/ptree.hpp>
#include "imgui/imgui.h"
#include "imgui/imgui_internal.h"

#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_vulkan.h"
#include "imgui_fd/ImGuiFileDialog.h"

#include <glm/glm.hpp>
#include <array>
#include <iostream>
#include <stdexcept>
#include <filesystem>
#include <sys/stat.h>
#include <thread>

#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "app/vulkanwindow.h"
#include "app/vulkanlib.h"
#include "app/config.h"
#include "app/polyhedron.h"
#include "app/morphdual.h"
#include "app/morphedge.h"
#include "app/morphexpanddual.h"
#include "app/morphregular.h"
#include "app/transformsolid.h"
#include "app/compoundpolyhedron.h"
#include "app/panel.h"

namespace pt = boost::property_tree;

enum class FrameSizeState {NONE, NEEDS_RESIZING, BUSY_RESIZING};

struct PanelSolid {
  std::vector<Panel*> panels;
  bool changedObject = false;
};

struct PanelTexture {
  Panel* panel = nullptr;
  bool   changed = false;
  std::string m_selectedFile;
  std::string m_errorMessage;
};

struct PanelComputeImage {
  std::vector<Panel *> panels;
  bool update = false; // Update an existing image?
  bool computed = false; // Has the thread finished the compute?
};

struct PanelFileDialog {
  Panel* panel = nullptr;
  bool showLoadDialog = false;
  bool showSaveDialog = false;
  bool isMesh         = false;
  bool isTexture      = false;
};

std::vector<std::thread>     g_threads;
std::vector<std::thread::id> g_finished;
// Texture update
PanelTexture                 g_panelTexture;
// Solid update
PanelSolid                   g_panelSolidList;

PanelComputeImage            g_panelComputeImage;
std::mutex                   g_compute_mutex;

void texture_thread() {
  assert(g_panelTexture.panel->m_solid);
  g_panelTexture.panel->updateTextureResource(g_panelTexture.m_selectedFile.c_str());
  g_panelTexture.changed = true;
  g_finished.emplace_back(std::this_thread::get_id());
}
// Update mesh or solid
void solid_update_thread(const std::vector<glm::vec3>& colorList) {
  assert(!g_panelSolidList.panels.empty());
  for (auto& panel : g_panelSolidList.panels) {
    panel->m_prepared = false;
    auto nextObjectType = panel->getNextObjectType();
    if (nextObjectType == ObjectType::NONE && panel->m_nextType == PanelType::MESH)
      panel->updateGltfModel();
    else
      panel->updateSolid(panel->getNextObjectType(), colorList);
  }
  // Signal to the main thread that the solid is updated
  g_panelSolidList.changedObject = true;
}
// Transform solids
void solid_transform_thread() {
  assert(!g_panelSolidList.panels.empty());
  for (auto &panel : g_panelSolidList.panels) {
    panel->resetSolid();
  }
  // Signal to the main thread that the solid is updated
  g_panelSolidList.changedObject = true;
}
// Compute image thread
void compute_image_thread() {
  std::lock_guard<std::mutex> guard(g_compute_mutex);
  for (auto& panel : g_panelComputeImage.panels) {
    if (panel->computeImage()) {
      if (g_panelComputeImage.update)
        panel->updateComputeTexture();
    }
  }
  g_panelComputeImage.computed = true;
}

class VulkanApplication {
public:

  void run() {
    Config config;
    int width, height;

    // If no config file then create a default one
    if (!config.fileExists())
      config.writeFile();
    // Read the config file
    pt::ptree root = config.readFile();
    // Get the colors to be used for the solids
    colorList = config.readObjectColors(root);
    // Read the size of the window
    Config::readWindowExtent(root, width, height);
    // Initialise the window
    initWindow(width, height);
    // Initialise the vulkan setup
    const auto error = initVulkan(root, static_cast<uint32_t>(width), static_cast<uint32_t>(height));
    if (!error.empty()) {
      m_vulkan->cleanup();
      delete m_window;
      throw std::runtime_error(error);
    }
    panelSettings.panel = nullptr;
    panelSettings.changedDrawLines  = false;
    panelSettings.changedMorph      = false;
    panelSettings.changedShowpoints = false;
    panelSettings.changedUseTexture = false;
    // Left panel
    addPanelToThreadList(m_vulkan->m_leftPanel);
    // Right panel
    addPanelToThreadList(m_vulkan->m_rightPanel);
    // Start the threads if any
    g_panelSolidList.changedObject = false;
    if (!g_panelSolidList.panels.empty())
      updateObject();
    if (!g_panelComputeImage.panels.empty())
      showImage();
    // Initialise ImGui
    initImGui();
    m_vulkan->m_leftPanel->initUBO();
    m_vulkan->m_rightPanel->initUBO();
    mainLoop();
    cleanup();
  }

private:
  VulkanWindow              *m_window = nullptr;
  std::unique_ptr<VulkanLib> m_vulkan = std::make_unique<VulkanLib>();
  FrameSizeState frameState = FrameSizeState::NONE;
  PanelSettings panelSettings;
  bool showOptions = false, showSolidInfo = false, showAppMessage = false, showPanelError = false;
  bool objectColorChanged = false, backgroundColorChanged = false;
  bool leftMouseButtonDown = false;
  bool computingImage = false;
  double m_mousePosX, m_mousePosY;
  // List of colors
  std::vector<glm::vec3> colorList;
  const char *pOptionsName = "popupOptions";
  const char *pAppMessage = "popupAppMessage";
  const char *pChooseFileDialog = "chooseFileDlgKey";
  const char *pSaveFileDialog = "saveFileDlgKey";
  std::string appMessage{};
  std::vector<ImVec4> imColors;
  vk::ClearColorValue backgroundColor;
  PanelFileDialog m_fileDialog;

  // Call-back method to handle key input
  void static key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    (void)(scancode);
    (void)(mods);
    auto app = reinterpret_cast<VulkanApplication *>(glfwGetWindowUserPointer(window));
    if (action == GLFW_PRESS) {
      switch (key) {
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GLFW_TRUE);
        break;
      case GLFW_KEY_R: {
        auto panel = app->getCurrentPanel();
        panel->toggleRotate();
      }
        break;
      case GLFW_KEY_F1:
        app->showSolidInfo = true;
        break;
      }
    }
  }
  // Call-back to handle mouse scrolls
  void static scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    // Only use the change in Y
    (void)(xoffset);
    VulkanApplication *h = reinterpret_cast<VulkanApplication *>(glfwGetWindowUserPointer(window));
    // Ignore scrolling when a file-dialog is shown
    if (h->m_fileDialog.showLoadDialog || h->m_fileDialog.showSaveDialog)
      return;
    const auto panel = h->m_vulkan->getPanel();
    if (panel->m_type == PanelType::IMAGE_COMP) {
      ComputeState state = ComputeState::INITIAL;
      if (yoffset < 0) {
        state = panel->zoomOut();
      } else if (yoffset > 0) {
        state = panel->zoomIn();
      }
      if (yoffset != 0 && state == ComputeState::INITIAL && !panel->updatingTexture()) {
        // Do the actual compute in a thread
        g_panelComputeImage.panels.emplace_back(panel);
        g_panelComputeImage.computed = false;
        g_panelComputeImage.update = true;
        g_threads.emplace_back(std::thread{compute_image_thread});
      }
    } else if (panel->m_type == PanelType::MESH || panel->m_type == PanelType::SOLID) {
      if (yoffset < 0) {
        panel->outwardEye();
      } else if (yoffset > 0) {
        panel->inwardEye();
      }
    }
  }

  static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
    (void)width;
    (void)height;
    auto app = reinterpret_cast<VulkanApplication*>(glfwGetWindowUserPointer(window));
    if (app->frameState == FrameSizeState::NONE)
      app->frameState = FrameSizeState::NEEDS_RESIZING;
  }
  // Call-back to handle mouse button events
  static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    (void)mods;
    auto app = reinterpret_cast<VulkanApplication*>(glfwGetWindowUserPointer(window));
    if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
      app->showOptions = true;
    } else {
      if (!app->isOptionPopupOpen() && button == GLFW_MOUSE_BUTTON_LEFT) {
        if (app->m_fileDialog.showSaveDialog)
          return;
        if (action == GLFW_PRESS) {
          app->leftMouseButtonDown = true;
          glfwGetCursorPos(window, &app->m_mousePosX, &app->m_mousePosY);
          app->m_vulkan->setMousePos(app->m_mousePosX, app->m_mousePosY);
          const auto &panel = app->m_vulkan->getPanel();
          app->setPanelTitle(panel);
        }
        if (action == GLFW_RELEASE) {
          if (app->leftMouseButtonDown) {
            double xpos, ypos;
            glfwGetCursorPos(window, &xpos, &ypos);
            auto dx = xpos - app->m_mousePosX;
            auto dy = ypos - app->m_mousePosY;
            if (abs(dx) > 5 || abs(dy) > 5) {
              // Move eye
              const auto panel = app->m_vulkan->getPanel();
              if (panel->m_type == PanelType::SOLID || panel->m_type == PanelType::MESH) {
                panel->addEyeAzimuthInclination(dx / 2000, dy / 2000);
              } else if (panel->m_type == PanelType::IMAGE_COMP) {
                auto state = panel->setXYoffsets(dx, dy);
                if (state == ComputeState::INITIAL) {
                  // Do the actual compute in a thread
                  g_panelComputeImage.panels.emplace_back(panel);
                  g_panelComputeImage.computed = false;
                  g_panelComputeImage.update = true;
                  g_threads.emplace_back(std::thread{compute_image_thread});
                }
              }
              app->m_mousePosX = xpos;
              app->m_mousePosY = ypos;
            }
          }
          app->leftMouseButtonDown = false;
        }
      }
    }
  }
  void addPanelToThreadList(const std::unique_ptr<Panel>& panel) {
    if (panel->m_nextType == PanelType::IMAGE_COMP) {
      // Setup the thread to compute a fractal image for this panel
      g_panelComputeImage.panels.emplace_back(panel.get());
    } else if (panel->m_nextType == PanelType::IMAGE_FILE) {
      panel->m_type = PanelType::IMAGE_FILE;
      if (!panel->createPipelines())
        throw std::runtime_error(panel->getErrorMessage());
      panel->m_prepared = true;
      panel->m_nextType = PanelType::NONE;
    } else if (panel->m_nextType == PanelType::MESH) {
      assert(!panel->m_meshFile.empty());
      g_panelSolidList.panels.emplace_back(panel.get());
    } else if (panel->m_nextType == PanelType::SOLID && panel->getNextObjectType() != ObjectType::NONE) {
      // Setup the thread to calculate the solid for this panel
      g_panelSolidList.panels.emplace_back(panel.get());
    }
  }
  // Initialise the window and set the call-backs
  void initWindow (int width, int height) {
    m_window = new VulkanWindow();
    if (!m_window->m_active)
      throw std::runtime_error("Failed to initiate GLFW");
    if (width > 0 and height > 0)
      m_window->init(width, height, "Polygonal");
    else
      m_window->initFullScreen("Polygonal");
    glfwSetWindowUserPointer(m_window->m_window, reinterpret_cast<void *>(this));
    glfwSetKeyCallback(m_window->m_window, VulkanApplication::key_callback);
    glfwSetScrollCallback(m_window->m_window, VulkanApplication::scroll_callback);
    glfwSetFramebufferSizeCallback(m_window->m_window, VulkanApplication::framebufferResizeCallback);
    // Needed for ImGui popup
    glfwSetMouseButtonCallback(m_window->m_window, VulkanApplication::mouse_button_callback);
  }
  // Initialise ImGui
  void initImGui() {
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForVulkan(m_window->m_window, true);
    if (!m_vulkan->initImGui()) {
      throw std::runtime_error(m_vulkan->getErrorMessage());
    }
  }
  // Initialise Vulkan
  std::string initVulkan(pt::ptree root, uint32_t width, uint32_t height) {
    if (!m_vulkan->createInstance(m_window->getRequiredExtensions()))
      return m_vulkan->getErrorMessage();

    m_vulkan->m_graphics->m_surface = m_window->createSurface(m_vulkan->m_instance.get());;
    if (!m_vulkan->pickPhysicalDevice())
      return m_vulkan->getErrorMessage();
    if (!m_vulkan->createLogicalDevice())
      return m_vulkan->getErrorMessage();
    // Created the device
    // Now load some settings
    m_vulkan->getConfig(root);
    backgroundColor = m_vulkan->m_graphics->getBackgroundColor();
    if (!m_vulkan->m_graphics->createStdCommandPool())
      return m_vulkan->m_graphics->getErrorMessage();
    if (!m_vulkan->buildSwapChain(false, width, height))
      return m_vulkan->getErrorMessage();
    if (!m_vulkan->m_graphics->createSyncObjects())
      return m_vulkan->m_graphics->getErrorMessage();
    return "";
  }

  bool isOptionPopupOpen() {
    const auto& g = *ImGui::GetCurrentContext();
    return g.OpenPopupStack.Size > g.BeginPopupStack.Size;
  }
  // Start a thread to update a solid
  void updateObject() {
    g_threads.emplace_back(std::thread{solid_update_thread, colorList});
  }
  // Get the panel using the mouse coordinates
  Panel* getCurrentPanel() {
    double mouseX, mouseY;
    glfwGetCursorPos(m_window->m_window, &mouseX, &mouseY);
    m_vulkan->setMousePos(mouseX, mouseY);
    return m_vulkan->getPanel();
  }

  void setPanelTitle(Panel* panel) {
    auto title = panel->buildWindowTitle();
    m_window->setTitle(title);
  }

  void setPanelTitle(Panel* panel, MorphType morphType) {
    auto title = panel->buildWindowTitle(morphType);
    m_window->setTitle(title);
  }

  void updateSolidColor() {
    if (m_vulkan->m_leftPanel->m_solid)
      m_vulkan->m_leftPanel->updateColor(colorList);
    if (m_vulkan->m_rightPanel->m_solid)
      m_vulkan->m_rightPanel->updateColor(colorList);
  }

  void showSolids(Panel *panel) {
    if (!ImGui::CollapsingHeader("Solids", true))
      return;
    ObjectType currentType = ObjectType::NONE;
    ObjectType nextObjectType = ObjectType::NONE;
    if (panel->m_solid)
      currentType = panel->m_solid->getType();
    if (ImGui::CollapsingHeader("Platonic", true)) {
      if (ImGui::Button("Tetrahedron")) {
        nextObjectType = ObjectType::TETRAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cube")) {
        nextObjectType = ObjectType::CUBE;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Octahedron")) {
        nextObjectType = ObjectType::OCTAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Dodecahedron")) {
        nextObjectType = ObjectType::DODECAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Icosahedron")) {
        nextObjectType = ObjectType::ICOSAHEDRON;
        ImGui::CloseCurrentPopup();
      }
    }
    if (ImGui::CollapsingHeader("Archimedian", true)) {
      if (ImGui::Button("Cuboctahedron")) {
        nextObjectType = ObjectType::CUBOCTAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Icosidodecahedron")) {
        nextObjectType = ObjectType::ICOSIDODECAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Rhombicuboctahedron")) {
        nextObjectType = ObjectType::RHOMBI_CUBOCTAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Snub cube")) {
        nextObjectType = ObjectType::SNUB_CUBE;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Truncated cube")) {
        nextObjectType = ObjectType::TRUNCATED_CUBE;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Truncated dodecahedron")) {
        nextObjectType = ObjectType::TRUNCATED_DODECAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Truncated octahedron")) {
        nextObjectType = ObjectType::TRUNCATED_OCTAHEDRON;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Truncated tetrahedron")) {
        nextObjectType = ObjectType::TRUNCATED_TETRAHEDRON;
        ImGui::CloseCurrentPopup();
      }
    }
    if (ImGui::CollapsingHeader("Compounds", true)) {
      if (ImGui::Button("Rhombic dodecahedron 2-compound")) {
        nextObjectType = ObjectType::RHOMBIC_DODECAHEDRON_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cube 2-compound")) {
        nextObjectType = ObjectType::CUBE_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cube 3-compound")) {
        nextObjectType = ObjectType::CUBE_3_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cube 4-compound")) {
        nextObjectType = ObjectType::CUBE_4_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cube 5-compound")) {
        nextObjectType = ObjectType::CUBE_5_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Cuboctahedron 2-compound")) {
        nextObjectType = ObjectType::CUBOCTAHEDRON_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Dodecahedron 2-compound")) {
        nextObjectType = ObjectType::DODECAHEDRON_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Icosahedron 2-compound")) {
        nextObjectType = ObjectType::ICOSAHEDRON_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Octahedron 2-compound")) {
        nextObjectType = ObjectType::OCTAHEDRON_2_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Octahedron 3-compound")) {
        nextObjectType = ObjectType::OCTAHEDRON_3_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Octahedron 4-compound")) {
        nextObjectType = ObjectType::OCTAHEDRON_4_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Tetrahedron 4-compound")) {
        nextObjectType = ObjectType::TETRAHEDRON_4_COMPOUND;
        ImGui::CloseCurrentPopup();
      }
    }
    if (ImGui::Button("Pentagonal bipyramid")) {
      nextObjectType = ObjectType::PENTAGONAL_BIPYRAMID;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Rhombic dodecahedron")) {
      nextObjectType = ObjectType::RHOMBIC_DODECAHEDRON;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Tetrakis hexahedron")) {
      nextObjectType = ObjectType::TETRAKIS_HEXAHEDRON;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Triakis octahedron")) {
      nextObjectType = ObjectType::TRIAKIS_OCTAHEDRON;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Triakis tetrahedron")) {
      nextObjectType = ObjectType::TRIAKIS_TETRAHEDRON;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Triangular bipyramid")) {
      nextObjectType = ObjectType::TRIANGULAR_BIPYRAMID;
      ImGui::CloseCurrentPopup();
    }
    if (nextObjectType != ObjectType::NONE && nextObjectType != currentType) {
      panel->setNextObjectType(nextObjectType);
      // Start a thread to load a solid
      assert(g_panelSolidList.panels.empty());
      g_panelSolidList.panels.emplace_back(panel);
      g_panelSolidList.changedObject = false;
      updateObject();
    }
  }

  void updateComputeThread(Panel *panel) {
    if (computingImage)
      return;
    // Update the compute image
    g_panelComputeImage.panels.emplace_back(panel);
    // When the thread is finished this flag will be set to <true>
    g_panelComputeImage.computed = false;
    // Update existing image; Not a new one
    g_panelComputeImage.update = true;
    // Launch the thread
    computingImage = true;
    g_threads.emplace_back(std::thread{compute_image_thread});
  }

  void showComputeSetup(Panel *panel) {
    if (panel->showComputeSetup()) {
      updateComputeThread(panel);
    }
  }

  void showObjectColors() {
    if (!ImGui::CollapsingHeader("Object colors"))
      return;
    if (imColors.empty()) {
      for (const auto &color : colorList) {
        ImVec4 imColor{};
        imColor.x = color.r;
        imColor.y = color.g;
        imColor.z = color.b;
        imColor.w = 1.0f;
        imColors.emplace_back(imColor);
      }
    }
    for (size_t iColor = 0; iColor < colorList.size(); iColor++) {
      ImVec4 color1 = {};
      color1.x = colorList.at(iColor).r;
      color1.y = colorList.at(iColor).g;
      color1.z = colorList.at(iColor).b;
      color1.w = 1.0f;
      ImGui::Separator();
      bool open_popup = ImGui::ColorButton("MyColor##3b", color1, 0);
      ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
      char buttonLabel[20];
      sprintf(buttonLabel, "Palette%d", int(iColor + 1));
      open_popup |= ImGui::Button(buttonLabel);
      char popupLabel[20];
      sprintf(popupLabel, "myPicker%d", int(iColor + 1));
      if (open_popup)
        ImGui::OpenPopup(popupLabel);
      if (ImGui::BeginPopup(popupLabel, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::ColorPicker3("##picker", reinterpret_cast<float *>(&imColors.at(iColor)), ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
        if (ImGui::Button("Save", ImVec2(80, 0))) {
          colorList.at(iColor).x = imColors.at(iColor).x;
          colorList.at(iColor).y = imColors.at(iColor).y;
          colorList.at(iColor).z = imColors.at(iColor).z;
          ImGui::CloseCurrentPopup();
          objectColorChanged = true;
          Config c{};
          c.writeObjectColors(colorList);
        }
        ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
        if (ImGui::Button("Cancel", ImVec2(80, 0))) {
          ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
      }
    }
    if (ImGui::Button("background color"))
      ImGui::OpenPopup("backColor");
    if (ImGui::BeginPopup("backColor", ImGuiWindowFlags_AlwaysAutoResize)) {
      ImGui::ColorPicker3("##picker", reinterpret_cast<float *>(&backgroundColor), ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
      if (ImGui::Button("Save", ImVec2(80, 0))) {
        backgroundColorChanged = true;
        ImGui::CloseCurrentPopup();
      }
      ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
      if (ImGui::Button("Cancel", ImVec2(80, 0))) {
        ImGui::CloseCurrentPopup();
      }
      ImGui::EndPopup();
    }
  }

  void showInfoPopup() {
    const auto& panel = getCurrentPanel();
    if (panel->m_type == PanelType::SOLID || panel->m_type == PanelType::IMAGE_COMP || panel->m_type == PanelType::MESH)
      panel->showInfo();
  }
  void showPanelErrorPopup() {
    const auto &panel = getCurrentPanel();
    panel->showError();
  }
  // Show a message
  void showMessagePopup(const std::string &message) {
    if (ImGui::IsPopupOpen(pAppMessage) && ImGui::BeginPopup(pAppMessage)) {
      ImGui::SetWindowPos(ImVec2(100, 100));
      ImGui::Text("%s", message.c_str());
      ImGui::EndPopup();
    }
  }
  // Show a fractal
  void showImage() {
    ComputeSetup setup;
    auto panels = std::move(g_panelComputeImage.panels);
    for (auto &panel : panels) {
      const auto fractalType = panel->getFractalType();
      if (panel->prepareCompute(fractalType.get(), setup))
        g_panelComputeImage.panels.push_back(panel);
    }
    // Start a thread to do the compute
    g_panelComputeImage.computed = false;
    g_panelComputeImage.update = false;
    g_threads.emplace_back(std::thread{compute_image_thread});
  }
  // Show a fractal of type <newFractalType> in panel <panel>
  void showFractal(Panel *panel, FractalType newFractalType) {
    auto oldFractalType = panel->getFractalType();
    if (panel->m_type == PanelType::IMAGE_COMP) {
      if (oldFractalType == newFractalType) {
        if (!panel->getUseTexture() || panel->m_textureFile.empty())
          panel->m_nextType = PanelType::SOLID;
        else
          panel->m_nextType = PanelType::IMAGE_FILE;
      }
    } else {
      panel->m_nextType = PanelType::IMAGE_COMP;
    }
    if (panel->m_nextType == PanelType::IMAGE_COMP || panel->m_nextType == PanelType::IMAGE_FILE) {
      // Show a fractal
      ComputeSetup setup;
      if (panel->prepareCompute(newFractalType, setup)) {
        // Start a thread to calculate the fractal
        g_panelComputeImage.panels.emplace_back(panel);
        g_panelComputeImage.computed = false;
        // Create a new image; Not an update of an existing one
        g_panelComputeImage.update = false;
        g_threads.emplace_back(std::thread{compute_image_thread});
      }
    } else {
      // Show a solid
      if (!panel->createPipelines())
        throw std::runtime_error(panel->getErrorMessage());
      panel->setNextObjectType(panel->m_solid->getType());
      // Load the solid
      g_panelSolidList.panels.emplace_back(panel);
      g_panelSolidList.changedObject = false;
      updateObject();
    }
  }

  void showActions(Panel *panel) {
    ImGui::Separator();
    if (ImGui::Button("Load texture file")) {
      igfd::ImGuiFileDialog::Instance()->OpenDialog(pChooseFileDialog, " Choose a texture file ", ".png\0.jpg\0\0", panel->m_textureFile.c_str(), 1);
      m_fileDialog.panel = panel;
      m_fileDialog.showLoadDialog = true;
      m_fileDialog.isMesh = false;
      m_fileDialog.isTexture = true;
      ImGui::CloseCurrentPopup();
    }
    if (ImGui::Button("Load mesh")) {
      igfd::ImGuiFileDialog::Instance()->OpenDialog(pChooseFileDialog, " Choose a mesh file", ".gltf\0", panel->m_meshFile.c_str(), 1);
      m_fileDialog.panel = panel;
      m_fileDialog.showLoadDialog = true;
      m_fileDialog.isMesh = true;
      m_fileDialog.isTexture = false;
      ImGui::CloseCurrentPopup();
    }
    if (panel->m_type != PanelType::MESH && panel->hasMesh() && ImGui::Button("Show mesh")) {
      panel->m_type = PanelType::MESH;
      if (!panel->createPipelines())
        throw std::runtime_error(panel->getErrorMessage());
      if (!m_vulkan->rebuildCommandBuffers(true))
        throw std::runtime_error(m_vulkan->getErrorMessage());
      ImGui::CloseCurrentPopup();
    }
    if (panel->m_type != PanelType::SOLID && ImGui::Button("Show polygon")) {
      if (panel->m_solid)
        panel->setNextObjectType(panel->m_solid->getType());
      else
        panel->setNextObjectType(ObjectType::TETRAHEDRON);
      // Load the solid
      g_panelSolidList.panels.emplace_back(panel);
      g_panelSolidList.changedObject = false;
      updateObject();
      ImGui::CloseCurrentPopup();
    }
    if (panel->m_type == PanelType::IMAGE_COMP && ImGui::Button("Save fractal image")) {
      auto homedir = std::string(getenv("HOME")) + "/.";
      igfd::ImGuiFileDialog::Instance()->OpenDialog(pSaveFileDialog, " Save the fractal", ".png\0\0", homedir, 1);
      m_fileDialog.panel = panel;
      m_fileDialog.showSaveDialog = true;
      ImGui::CloseCurrentPopup();
    }
    if (!panel->m_textureFile.empty() && ImGui::Button("Show image")) {
      if (!panel->setTypeImage()) {
        showPanelError = true;
      } else {
        if (panel->m_type == PanelType::SOLID)
          panel->fillSolidFillTexturePipelineBuffer();
        if (!m_vulkan->updateTextureResource(panel)) {
          showPanelError = true;
        }
      }
      ImGui::CloseCurrentPopup();
    }
    // Show fractal options
    const auto fractalType = panel->getFractalType();
    if ((panel->m_type != PanelType::IMAGE_COMP || fractalType == FractalType::JULIA) && ImGui::Button("Show mandelbrot fractal")) {
      showFractal(panel, FractalType::MANDELBROT);
      ImGui::CloseCurrentPopup();
    }
    if ((panel->m_type != PanelType::IMAGE_COMP || fractalType == FractalType::MANDELBROT) && ImGui::Button("Show julia fractal")) {
      showFractal(panel, FractalType::JULIA);
      ImGui::CloseCurrentPopup();
    }
    if (!m_vulkan->splitPanel(panel))
      showPanelError = true;
    panel->showSolidActions(panelSettings);
  }

  void showOptionsPopup() {
    if (ImGui::IsPopupOpen(pOptionsName) && ImGui::BeginPopup(pOptionsName)) {
      const auto &panel = getCurrentPanel();
      if (panel->m_type == PanelType::SOLID)
        showSolids(panel);
      if (panel->m_type == PanelType::SOLID || panel->m_type == PanelType::MESH) {
        panel->showControls();
      }
      showActions(panel);
      if (panelSettings.panel) {
        ImGui::EndPopup();
        return;
      }
      if (panel->m_type == PanelType::SOLID && panel->getMorphType() == MorphType::NONE) {
        panelSettings.drawLines = panel->getDrawLines();
        panelSettings.showPoints = panel->getShowPoints();
        panelSettings.useTexture = panel->getUseTexture();
        if (ImGui::Checkbox("Draw lines", &panelSettings.drawLines)) {
          panelSettings.panel = panel;
          // Can not use texture when drawing lines
          panelSettings.useTexture = false;
          panelSettings.changedDrawLines = true;
          panelSettings.changedUseTexture = true;
        }
        if (ImGui::Checkbox("Show points", &panelSettings.showPoints)) {
          panelSettings.panel = panel;
          panelSettings.changedShowpoints = true;
        }
        if (!panel->m_textureFile.empty() && ImGui::Checkbox("Use texture", &panelSettings.useTexture)) {
          panelSettings.panel = panel;
          panelSettings.changedUseTexture = true;
        }
      }
      showObjectColors();
      if (panel->m_type == PanelType::IMAGE_COMP)
        showComputeSetup(panel);
      ImGui::EndPopup();
    }
  }
  // Remove finished threads
  void cleanupThreads() {
    if (!g_finished.empty()) {
      for (auto it_thread = g_threads.begin(); it_thread != g_threads.end();) {
        auto it_finished = std::find(g_finished.begin(), g_finished.end(), it_thread->get_id());
        if (it_finished != g_finished.end()) {
          it_thread->join();
          g_finished.erase(it_finished);
          g_threads.erase(it_thread);
        } else
          it_thread++;
        if (g_finished.empty())
          break;
      }
    }
  }

  void mainLoop() {
    int width, height;
    igfd::ImGuiFileDialog::Instance()->SetFilterInfos(".jpg", ImVec4(0.0f, 1.0f, 0.5f, 0.9f), "[JPG]"); // add an text for a filter type
    while (!m_window->windowShouldClose()) {
      m_window->pollEvents();
      if (panelSettings.panel) {
        if (!panelSettings.changedDrawLines)
          panelSettings.drawLines = panelSettings.panel->getDrawLines();
        if (!panelSettings.changedMorph)
          panelSettings.morphType = panelSettings.panel->getMorphType();
        if (!panelSettings.changedShowpoints)
          panelSettings.showPoints = panelSettings.panel->getShowPoints();
        if (!panelSettings.changedUseTexture)
          panelSettings.useTexture = panelSettings.panel->getUseTexture();
        if (panelSettings.transform != PolyhedronTransformation::NONE) {
          // Transform the current solid
          auto &panel = panelSettings.panel;
          assert(panel->m_solid);
          if (panel->getMorphType() == MorphType::NONE || panelSettings.transform == PolyhedronTransformation::EXPAND || panelSettings.transform == PolyhedronTransformation::REGULAR || panelSettings.transform == PolyhedronTransformation::TRUNCATE) {
            const auto currentType = panel->m_solid->getType();
            if (panelSettings.transform == PolyhedronTransformation::DUAL && currentType != ObjectType::NONE &&
                Polyhedron::getDualType(currentType) == ObjectType::NONE) {
              showAppMessage = true;
              appMessage = "No dual available";
            } else {
              assert(g_panelSolidList.panels.empty());
              panel->setTransform(panelSettings.transform, panelSettings.actionCount);
              // Launch the thread to calculate the transformed solid
              g_panelSolidList.panels.emplace_back(panel);
              g_panelSolidList.changedObject = false;
              g_threads.emplace_back(std::thread{solid_transform_thread});
            }
          }
          panelSettings.transform = PolyhedronTransformation::NONE;
        } else {
          panelSettings.panel->fillSolidPipelineBuffers(false, panelSettings.drawLines, panelSettings.showPoints, panelSettings.useTexture, panelSettings.morphType);
          if (!m_vulkan->rebuildCommandBuffers(true)) {
            throw std::runtime_error(m_vulkan->getErrorMessage());
          }
          if (panelSettings.panel->m_solid)
            setPanelTitle(panelSettings.panel, panelSettings.morphType);
        }
        // Reset
        panelSettings.panel = nullptr;
        panelSettings.changedDrawLines  = false;
        panelSettings.changedMorph      = false;
        panelSettings.changedShowpoints = false;
        panelSettings.changedUseTexture = false;
      }
      if (g_panelSolidList.changedObject) {
        assert(!g_panelSolidList.panels.empty());
        // Thread loaded the solids or meshes
        for (auto& panel : g_panelSolidList.panels) {
          panel->m_canDraw = false;
          panel->m_type = panel->m_nextType;
          panel->m_nextType = PanelType::NONE;
          if (panel->m_type != PanelType::MESH) {
            if (panel->m_type != PanelType::SOLID)
            // Update panel type and create any missing pipelines
              panel->m_type = PanelType::SOLID;
            if (!panel->createPipelines())
              throw std::runtime_error(panel->getErrorMessage());
            // Update the panel
            if (!Polyhedron::canMorph(panel->m_solid->getType()) && panel->getMorphType() != MorphType::NONE) {
              // Stop morphing
              panel->setMorphType(MorphType::NONE);
            }
            if (panel->getMorphType() == MorphType::NONE) {
              panel->prepareDrawingSolid();
            }
            panel->fillSolidPipelineBuffers();
          }
          panel->m_prepared = true;
        }
        const auto& focusPanel = m_vulkan->getPanel();
        for (auto& panel : g_panelSolidList.panels) {
          if (panel == focusPanel) {
            setPanelTitle(focusPanel);
            break;
          }
        }
        if (!m_vulkan->rebuildCommandBuffers(true))
          throw std::runtime_error(m_vulkan->getErrorMessage());
        g_panelSolidList.panels.clear();
        g_panelSolidList.changedObject = false;
        cleanupThreads();
      }
      if (g_panelComputeImage.computed) {
        for (auto& panel : g_panelComputeImage.panels) {
          panel->m_type = panel->m_nextType;
          if (!g_panelComputeImage.update) {
            // Handle the compute image created by a thread
            if (!panel->createPipelines())
              throw std::runtime_error(panel->getErrorMessage());
          }
          panel->m_prepared = true;
        }
        const auto& focusPanel = m_vulkan->getPanel();
        for (auto& panel : g_panelComputeImage.panels) {
          if (panel == focusPanel) {
            setPanelTitle(focusPanel);
            break;
          }
        }
        if (!m_vulkan->rebuildCommandBuffers(true))
          throw std::runtime_error(m_vulkan->getErrorMessage());
        // Clean up
        g_panelComputeImage.panels.clear();
        g_panelComputeImage.computed = false;
        g_panelComputeImage.update   = false;
        cleanupThreads();
        computingImage = false;
      }
      if (m_vulkan->m_leftPanel->m_type == PanelType::IMAGE_FILE && m_vulkan->m_leftPanel->m_prepared && !m_vulkan->m_leftPanel->m_canDraw) {
        if (!m_vulkan->rebuildCommandBuffers(true))
          throw std::runtime_error(m_vulkan->getErrorMessage());
      }
      if (!m_vulkan->m_leftPanel->m_canDraw && !m_vulkan->m_rightPanel->m_canDraw)  {
        sleep(1);
        continue;
      }
      if (backgroundColorChanged) {
        backgroundColorChanged = false;
        m_vulkan->setBackgroundColor(backgroundColor);
      }
      if (objectColorChanged) {
        objectColorChanged = false;
        updateSolidColor();
      }
      if (g_panelTexture.changed) {
        if (g_panelTexture.m_errorMessage.empty()) {
          // Thread loaded the texture
          // Update the panel
          if (!m_vulkan->updateTextureResource(g_panelTexture.panel))
            throw std::runtime_error(m_vulkan->getErrorMessage());
          if (!g_panelTexture.m_selectedFile.empty()) {
            g_panelTexture.panel->m_textureFile = g_panelTexture.m_selectedFile;
            g_panelTexture.m_selectedFile.clear();
          }
        } else {
          showAppMessage = true;
          appMessage = std::move(g_panelTexture.m_errorMessage);
        }
        g_panelTexture.changed = false;
        cleanupThreads();
      }
      uint32_t imageIndex;
      auto result = m_vulkan->acquireNextImage(&imageIndex);
      if (result == vk::Result::eErrorOutOfDateKHR) {
        m_window->getFramebufferSize(&width, &height);
        if (!m_vulkan->recreateSwapChain(static_cast<uint32_t>(width), static_cast<uint32_t>(height)))
          throw std::runtime_error(m_vulkan->getErrorMessage());
        continue;
      } else if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR) {
        throw std::runtime_error("Failed to acquire swap chain image");
      }
      if (m_vulkan->m_leftPanel->m_type == PanelType::IMAGE_COMP &&
          m_vulkan->m_leftPanel->getSeedOscillation() &&
          m_vulkan->m_leftPanel->canRecompute()) {
        // Do the actual compute in a thread
        updateComputeThread(m_vulkan->m_leftPanel.get());
      }
      // ImGui
      ImGui_ImplGlfw_NewFrame();
      ImGui::NewFrame();
      if (m_fileDialog.showLoadDialog) {
        const auto size = m_vulkan->m_graphics->getExtent2D();
        if (igfd::ImGuiFileDialog::Instance()->FileDialog(pChooseFileDialog, ImGuiWindowFlags_NoCollapse, ImVec2(30, 30), ImVec2(size.width, size.height))) {
          if (igfd::ImGuiFileDialog::Instance()->IsOk) {
            if (m_fileDialog.isTexture) {
              g_panelTexture.panel = m_fileDialog.panel;
              g_panelTexture.m_selectedFile = igfd::ImGuiFileDialog::Instance()->GetFilepathName();
              g_threads.emplace_back(std::thread{texture_thread});
              m_fileDialog.isTexture = false;
            } else if (m_fileDialog.isMesh) {
              m_fileDialog.panel->m_nextType = PanelType::MESH;
              m_fileDialog.panel->m_meshFile = igfd::ImGuiFileDialog::Instance()->GetFilepathName();
              g_panelSolidList.panels.emplace_back(m_fileDialog.panel);
              g_panelSolidList.changedObject = false;
              updateObject();
            }
          }
          igfd::ImGuiFileDialog::Instance()->CloseDialog(pChooseFileDialog);
          m_fileDialog.panel = nullptr;
          m_fileDialog.showLoadDialog = false;
        }
      }
      if (m_fileDialog.showSaveDialog) {
        const auto size = m_vulkan->m_graphics->getExtent2D();
        if (igfd::ImGuiFileDialog::Instance()->FileDialog(pSaveFileDialog, ImGuiWindowFlags_NoCollapse, ImVec2(120, 60), ImVec2(size.width, size.height))) {
          if (igfd::ImGuiFileDialog::Instance()->IsOk) {
            auto file = igfd::ImGuiFileDialog::Instance()->GetFilepathName();
            m_fileDialog.panel->saveRenderedImage(file);
          }
          igfd::ImGuiFileDialog::Instance()->CloseDialog(pSaveFileDialog);
          m_fileDialog.panel = nullptr;
          m_fileDialog.showSaveDialog = false;
        }
      }

      if (showOptions && !ImGui::IsPopupOpen(pOptionsName)) {
        imColors.clear();
        ImGui::OpenPopup(pOptionsName);
        showOptions = false;
      }
      showOptionsPopup();
      // Solid info
      if (showSolidInfo && !ImGui::IsPopupOpen(pPanelInfoName)) {
        ImGui::OpenPopup(pPanelInfoName);
        showSolidInfo = false;
      }
      showInfoPopup();
      // App message
      if (showAppMessage && !ImGui::IsPopupOpen(pAppMessage)) {
        ImGui::OpenPopup(pAppMessage);
        showAppMessage = false;
      }
      showMessagePopup(appMessage);
      if (showPanelError && !ImGui::IsPopupOpen(pErrorName)) {
        ImGui::OpenPopup(pErrorName);
        showPanelError = false;
      }
      showPanelErrorPopup();
      // ImGui::ShowDemoWindow();
      ImGui::EndFrame();
      ImGui::Render();
      if (m_vulkan->m_leftPanel->m_canDraw || m_vulkan->m_rightPanel->m_canDraw) {
        result = m_vulkan->drawFrame(imageIndex);
        if (frameState == FrameSizeState::NEEDS_RESIZING) {
          const auto size = m_vulkan->m_graphics->getExtent2D();
          m_window->getFramebufferSize(&width, &height);
          if (size.width == static_cast<uint32_t>(height) && size.width == static_cast<uint32_t>(width))
            frameState = FrameSizeState::NONE;
        }
        if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR || frameState == FrameSizeState::NEEDS_RESIZING) {
          frameState = FrameSizeState::BUSY_RESIZING;
          m_window->getFramebufferSize(&width, &height);
          m_vulkan->recreateSwapChain(static_cast<uint32_t>(width), static_cast<uint32_t>(height));
        } else if (result != vk::Result::eSuccess) {
          throw std::runtime_error("Failed to present swap chain image!");
        }
      }
      m_vulkan->m_graphics->updateCurrentFrame();
      frameState = FrameSizeState::NONE;
    }
    m_vulkan->m_logicalDevice->waitIdle();
    m_vulkan->putConfig();
    m_window->getWindowSize(&width, &height);
    Config c;
    c.writeWindowSize(width, height);
  }

  void cleanup() {
    // ImGui
    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    m_vulkan->cleanup();
    delete m_window;
    for (auto &t : g_threads) {
      t.join();
    }
  }
};

int main() {
  try {
    VulkanApplication app;
    app.run();
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
