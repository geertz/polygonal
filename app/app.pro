QT += core

TEMPLATE = app

SOURCES += main.cpp

LIBS += -L../src -lmyapp -lglfw -lvulkan -ltinygltf
INCLUDEPATH += $$PWD/../src

copydata.commands = $(COPY_DIR) $$PWD/../src/shaders $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
