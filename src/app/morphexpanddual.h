#pragma once

#include "morphbase.h"
#include "expandedpolyhedron.h"

class MorphExpandDual : public MorphBase {
public:
  MorphExpandDual(const std::unique_ptr<Polyhedron>& origin);
private:
  void expandFaces(size_t iRow, Face& face, const ExpandedPolyhedron& expand, size_t iSourceFace, const std::vector<uint16_t>& someVertices);
};

