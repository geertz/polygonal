#include "solidlinepipeline.h"

SolidLinePipeline::SolidLinePipeline(GraphicsResource* pGraphics) : PipelineBase(pGraphics, "shaders/vert_solid_line.spv", "shaders/frag_object.spv", "Solid line pipeline") {
  if (!m_pipelineResource || hasError())
    return;
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(glm::vec3));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  if (!createPipeline(vk::PrimitiveTopology::eLineList, vk::PolygonMode::eLine, vertexInputInfo)) {
    setResourceError();
    return;
  }
  m_ubo.alpha = 1.0f;
  m_ubo.color = {1.0f, 1.0f, 1.0f};
  m_ubo.pointSize = 10.0f;
  if (!m_pipelineResource->createUniformBuffers(sizeof(UniformBufferObject)))
    setResourceError();
}

std::vector<vk::VertexInputAttributeDescription> SolidLinePipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, 0}
  };
  return attributeDescriptions;
}

void SolidLinePipeline::clearAll() {
  m_vertices.clear();
  m_indices.clear();
}
void SolidLinePipeline::createBuffers() {
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
}
// Copy Uniform buffer object to image <currentImage>
auto SolidLinePipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

void SolidLinePipeline::fillBuffers(const std::unique_ptr<Polyhedron>& solid) {
  clearAll();
  const auto &vertices = solid->getVertices();
  assert(!vertices.empty());
  m_vertices.insert(m_vertices.begin(), vertices.begin(), vertices.end());
  const auto& edges = solid->getEdges();
  assert(!edges.empty());
  for (const auto& edge : edges) {
    m_indices.push_back(static_cast<uint16_t>(edge.iVertexBegin));
    m_indices.push_back(static_cast<uint16_t>(edge.iVertexEnd));
  }
}
