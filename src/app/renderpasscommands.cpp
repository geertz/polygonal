#include "renderpasscommands.h"

RenderPassCommands::RenderPassCommands(const vk::CommandBuffer &aBuffer, vk::CommandBufferUsageFlags usage) {
  m_buffer = aBuffer;
  vk::CommandBufferBeginInfo info{};
  info.flags = usage;
  info.pInheritanceInfo = nullptr; // Optional
  try {
    m_buffer.begin(info);
  } catch (...) {
    m_errorMessage = "Failed to begin command buffer!";
  }
}

RenderPassCommands::~RenderPassCommands() {
  if (started)
    m_buffer.endRenderPass();
  m_buffer.end();
}

void RenderPassCommands::beginRenderPass(const vk::RenderPassBeginInfo &info) {
  m_buffer.beginRenderPass(info, vk::SubpassContents::eInline);
  started = true;
}

void RenderPassCommands::nextSubpass() {
  m_buffer.nextSubpass(vk::SubpassContents::eInline);
}
