#pragma once

#include "dualpolyhedron.h"

class DualCompound : public DualPolyhedron {
  struct EdgeInfo {
    size_t     iEdge;
    glm::dvec3 dualVertex;
  };
public:
  DualCompound(const std::unique_ptr<Polyhedron>&);
  DualCompound(const DualCompound&) = delete; // No copy needed
private:
  void addFaces(uint16_t iCornerVertex, uint16_t iCenterVertex,  const glm::dvec3& dualVertex1, const glm::dvec3& dualVertex2);
  void static crossingLines(const glm::dvec3& point1, const glm::dvec3 direction1, const glm::dvec3& point2, const glm::dvec3 direction2, glm::dvec3& pointCross1, glm::dvec3& pointCross2, double& t);
  void createFaces(const glm::dvec3& p1, std::vector<EdgeInfo>& originalFaceEdges, const std::vector<Edge>& originalEdges);
};
