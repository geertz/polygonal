#include "textureresource.h"
#include "singletimecommands.h"
#include <cassert>
#include <cmath>
#include <stb_image.h>
#include <iostream>

TextureResource::TextureResource(GraphicsResource* pGraphics, const char* filename, bool createMipmaps) : ImageResource(pGraphics->getDevice(), vk::Format::eR8G8B8A8Srgb) {
  m_graphics = pGraphics;
  m_mipLevels = 0;
  int texWidth, texHeight, texChannels;
  stbi_uc* pixels = stbi_load(filename, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
  if (!pixels) {
    fprintf(stderr, "Failed to load texture image: %s\n", stbi_failure_reason());
    m_errorMessage = "Failed to load texture image";
    return;
  }
  BufferResource stagingBufferResource(m_graphics->getDevice()->getDevice());
  VkDeviceSize imageSize = static_cast<VkDeviceSize>(texWidth * texHeight * 4);
  if (!m_graphics->createBuffer(imageSize, 1, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, &stagingBufferResource)) {
    m_errorMessage = m_graphics->getErrorMessage();
    return;
  }
  const auto error = stagingBufferResource.copyData(pixels, imageSize);
  if (!error.empty()) {
    m_errorMessage = std::move(error);
    return;
  }
  // Free image memory
  stbi_image_free(pixels);

  if (!copyBufferToImage(stagingBufferResource.m_buffer, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight)))
    return;

  if (createMipmaps) {
    if (!generateMipmaps()) {
      assert(!m_errorMessage.empty());
      return;
    }
  } else {
    if (!transitionImageLayout(vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal))
      return;
  }
  createTextureImageView();
}

TextureResource::TextureResource(GraphicsResource* pGraphics, tinygltf::Image& anImage, bool createMipmaps) : ImageResource(pGraphics->getDevice(), vk::Format::eR8G8B8A8Unorm) {
  m_graphics = pGraphics;
  m_mipLevels = 0;
  BufferResource stagingBufferResource(m_graphics->getDevice()->getDevice());
  const VkDeviceSize imageSize = anImage.image.size();
  if (!m_graphics->createBuffer(imageSize, 1, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, &stagingBufferResource)) {
    m_errorMessage = m_graphics->getErrorMessage();
    return;
  }
  unsigned char* buffer = nullptr;
  // We convert RGB-only images to RGBA, as most devices don't support RGB-formats in Vulkan
  if (anImage.component == 3) {
    buffer = new unsigned char[imageSize];
    unsigned char* rgba = buffer;
    auto rgb = &anImage.image[0];
    for (int i = 0; i < anImage.width * anImage.height; ++i) {
      memcpy(rgba, rgb, sizeof(unsigned char) * 3);
      rgba += 4;
      rgb += 3;
    }
    stagingBufferResource.copyData(buffer, imageSize);
    delete buffer;
  } else {
    stagingBufferResource.copyData(anImage.image.data(), imageSize);
  }
  if (!createMipmaps)
    m_mipLevels = 1;
  if (!copyBufferToImage(stagingBufferResource.m_buffer, anImage.width, anImage.height))
    return;
  if (createMipmaps) {
    if (!generateMipmaps()) {
      assert(!m_errorMessage.empty());
      return;
    }
  } else {
    if (!transitionImageLayout(vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal))
      return;
  }
  createTextureImageView();
}

TextureResource::TextureResource(GraphicsResource* pGraphics, std::unique_ptr<ComputeResource>& pCompute) : ImageResource(pGraphics->getDevice(), vk::Format::eR8G8B8A8Srgb) {
  assert(pCompute->m_status == ComputeState::FINISHED);
  m_graphics = pGraphics;
  m_mipLevels = 0;
  auto mappedMemory = pCompute->m_bufferResource->mapMemory();
  Pixel* pmappedMemory =  reinterpret_cast<Pixel *>(mappedMemory);
  std::vector<unsigned char> image;
  const auto width = pCompute->getWidth();
  const auto height = pCompute->getHeight();
  image.reserve(width * height * 4);
  for (uint32_t i = 0; i < width * height; i++) {
    image.push_back(u_char(255.0f * pmappedMemory[i].r));
    image.push_back(u_char(255.0f * pmappedMemory[i].g));
    image.push_back(u_char(255.0f * pmappedMemory[i].b));
    image.push_back(u_char(255.0f * pmappedMemory[i].a));
  }
  pCompute->m_bufferResource->unmapMemory();

  BufferResource stagingBufferResource(m_graphics->getDevice()->getDevice());
  auto imageSize = static_cast<VkDeviceSize>(width * height * 4);
  m_graphics->createBuffer(imageSize, 1, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, &stagingBufferResource);
  stagingBufferResource.copyData(image.data(), imageSize);
  if (!copyBufferToImage(stagingBufferResource.m_buffer, width, height))
    return;
  if (!generateMipmaps()) {
    assert(!m_errorMessage.empty());
    return;
  }
  createTextureImageView();
  // Reset to ComputeState::INITIAL
  pCompute->reset();
}

bool TextureResource::copyBufferToImage(const vk::UniqueBuffer& buffer, uint32_t width, uint32_t height) {
  vk::Extent2D extent{width, height};
  createTextureImage(extent);
  // Make image <m_image> ready for transfer
  if (!transitionImageLayout(vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal)) {
    return false;
  }
  // Copy buffer to image
  if (!m_graphics->copyBufferToImage(buffer, m_image, extent)) {
    m_errorMessage = m_graphics->getErrorMessage();
    return false;
  }
  return true;
}

auto TextureResource::createTextureImage(const vk::Extent2D anExtent) -> bool {
  if (m_mipLevels == 0)
    m_mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(anExtent.width, anExtent.height)))) + 1;
  m_extent = anExtent;
  return createImage(anExtent, m_mipLevels, vk::SampleCountFlagBits::e1, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal);
}

bool TextureResource::generateMipmaps() {
  const auto formatProperties = m_device->m_physicalDevice->getFormatProperties(m_format);
  if (!(formatProperties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear)) {
    m_errorMessage = "Texture image format does not support linear blitting!";
    return false;
  }
  SingleTimeCommands command(m_device->getDevice(), m_graphics->m_commandPool.get());

  vk::ImageMemoryBarrier barrier{};
  barrier.image = m_image;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;
  barrier.subresourceRange.levelCount = 1;
  int32_t mipWidth = m_extent.width;
  int32_t mipHeight = m_extent.height;
  auto &cmdBuffer = command.m_commandBuffer;

  vk::DependencyFlags dep;
  for (uint32_t i = 1; i < m_mipLevels; i++) {
    barrier.subresourceRange.baseMipLevel = i - 1;
    barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
    barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

    cmdBuffer->pipelineBarrier(
    {vk::PipelineStageFlagBits::eTransfer}, {vk::PipelineStageFlagBits::eTransfer},
          dep,
          0, nullptr,
          0, nullptr,
          1, &barrier
      );
    vk::ImageBlit blit{};
    blit.srcOffsets[0] = vk::Offset3D( 0, 0, 0 );
    blit.srcOffsets[1] = vk::Offset3D(mipWidth, mipHeight, 1 );
    blit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
    blit.srcSubresource.mipLevel = i - 1;
    blit.srcSubresource.baseArrayLayer = 0;
    blit.srcSubresource.layerCount = 1;
    blit.dstOffsets[0] = vk::Offset3D( 0, 0, 0 );
    blit.dstOffsets[1] = vk::Offset3D( mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 );
    blit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
    blit.dstSubresource.mipLevel = i;
    blit.dstSubresource.baseArrayLayer = 0;
    blit.dstSubresource.layerCount = 1;

    cmdBuffer->blitImage(
          m_image, vk::ImageLayout::eTransferSrcOptimal,
          m_image, vk::ImageLayout::eTransferDstOptimal,
          1, &blit,
          vk::Filter::eLinear);
    barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

    cmdBuffer->pipelineBarrier(
    {vk::PipelineStageFlagBits::eTransfer}, {vk::PipelineStageFlagBits::eFragmentShader},
          dep,
          0, nullptr,
          0, nullptr,
          1, &barrier
      );
    if (mipWidth > 1)
      mipWidth /= 2;
    if (mipHeight > 1)
      mipHeight /= 2;
  }
  barrier.subresourceRange.baseMipLevel = m_mipLevels - 1;
  barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
  barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
  barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
  barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

  cmdBuffer->pipelineBarrier(
  {vk::PipelineStageFlagBits::eTransfer}, {vk::PipelineStageFlagBits::eFragmentShader},
        dep,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );
  if (!command.submit(m_graphics->m_graphicsQueue2)) {
    m_errorMessage = command.m_errorMessage;
    return false;
  }
  return true;
}

auto TextureResource::transitionImageLayout(vk::ImageLayout oldLayout, vk::ImageLayout newLayout) -> bool {
  assert(m_graphics->m_commandPool);
  assert(m_image);
  SingleTimeCommands command(m_device->getDevice(), m_graphics->m_commandPool.get());
  vk::ImageMemoryBarrier barrier{};
  barrier.oldLayout = oldLayout;
  barrier.newLayout = newLayout;
  barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
  barrier.image = m_image;
  barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
  barrier.subresourceRange.baseMipLevel = 0;
  barrier.subresourceRange.levelCount = m_mipLevels;
  barrier.subresourceRange.baseArrayLayer = 0;
  barrier.subresourceRange.layerCount = 1;
  vk::PipelineStageFlags sourceStage;
  vk::PipelineStageFlags destinationStage;
  vk::DependencyFlags dep;

  if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal) {
    barrier.srcAccessMask = {};
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;

    sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
    destinationStage = vk::PipelineStageFlagBits::eTransfer;
  } else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

    sourceStage = vk::PipelineStageFlagBits::eTransfer;
    destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
  } else {
    m_errorMessage = "Unsupported layout transition!";
    return false;
  }

  command.m_commandBuffer->pipelineBarrier(
  sourceStage, destinationStage,
        dep,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );
  if (!command.submit(m_graphics->m_graphicsQueue2)) {
    m_errorMessage = std::move(command.m_errorMessage);
    return false;
  }
  return true;
}
