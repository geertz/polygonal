#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "meshpipelinebase.h"
#include "gltfmodel.h"

class MeshMaterialPipeline : public MeshPipelineBase {
public:
  explicit MeshMaterialPipeline(GraphicsResource* pGraphics);
  auto copyUBO(uint32_t iImage) -> bool;
  void fillBuffers(const std::unique_ptr<GltfModel>& model);
  inline void setEye(const glm::vec3& aValue) {
    m_ubo.eye = aValue;
  }
  void updateVertexBuffer();
private:
  // To be used with shader <mesh1.vert>
  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
  };
  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 projView;
    glm::vec3 eye;
  };

  UniformBufferObject  m_ubo;
  std::vector<Vertex>  m_vertices;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};

