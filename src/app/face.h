#pragma once

#include <algorithm>
#include <array>
#include <vector>
#include <glm/glm.hpp>

struct Plane {
  glm::dvec3 normal, point;
};

class Polygon {
public:
  Polygon();
  inline void push_back(const glm::dvec3 &v) {
    m_vertices.emplace_back(v);
  }
  inline void clear() {
    m_vertices.clear();
  }
  inline std::vector<glm::dvec3> getVertices() const {
    return m_vertices;
  }
  inline glm::dvec3 operator[] (const ushort i) const {
    return m_vertices[i];
  }
  inline uint64_t size() const {
    return m_vertices.size();
  }
  inline std::vector<glm::dvec3>::const_iterator cbegin() const {
    return std::cbegin(m_vertices);
  }
  inline std::vector<glm::dvec3>::const_iterator begin() const {
    return std::begin(m_vertices);
  }
  inline std::vector<glm::dvec3>::const_iterator cend() const {
    return std::cend(m_vertices);
  }
  inline std::vector<glm::dvec3>::const_iterator end() const {
    return std::end(m_vertices);
  }
  auto getPlane() const -> Plane;
  auto takeLast() -> glm::dvec3;
private:
  std::vector<glm::dvec3> m_vertices;
};

class Face {
private:
  uint16_t              m_colorIndex;
protected:
  std::vector<uint16_t> m_indices;

public:
  glm::dvec3 normal;
  bool       invalid = false;

  Face();
  explicit Face(const uint16_t aColorIndex);
  explicit Face(const std::vector<uint16_t> &someIndices);
  explicit Face(const std::vector<uint16_t> &someIndices, const uint16_t aColorIndex);
  bool areNeighbors(const Face &face) const;
  inline auto at (size_t i) const {
    return m_indices.at(i);
  }
  inline uint16_t& at (size_t i) {
    return m_indices.at(i);
  }
  inline auto begin() const -> std::vector<uint16_t>::const_iterator {
    return std::cbegin(m_indices);
  }
  inline auto end() const -> std::vector<uint16_t>::const_iterator {
    return std::cend(m_indices);
  }
  inline auto contains(uint16_t index) const -> bool {
    return std::find(m_indices.begin(), m_indices.end(), index) != m_indices.end();
  }
  auto indexOf(uint16_t index) const -> long;
  inline auto assign(size_t count, uint16_t iVertex) -> void {
    m_indices.assign(count, iVertex);
  }
  inline auto clear() -> void {
    m_indices.clear();
  }
  inline auto copyIndices() const -> std::vector<uint16_t> {
    return m_indices;
  }
  inline auto getColorIndex() const -> ushort {
    return m_colorIndex;
  }
  inline void setColorIndex (ushort aColorIndex) {
    m_colorIndex = aColorIndex;
  }
  auto calculateNormal(const std::vector<glm::dvec3> &vertices) const -> glm::dvec3;
  inline void setNormal(const std::vector<glm::dvec3> &vertices) {
    normal = calculateNormal(vertices);
  }
  auto getCenter(const std::vector<glm::dvec3> &someVertices) const -> glm::dvec3;
  inline size_t getCorners() const {
    return m_indices.size();
  }
  inline auto getIndices() const & -> std::vector<uint16_t> {
    return m_indices;
  }
  auto getPlane(const std::vector<glm::dvec3> &someVertices) const -> Plane;
  auto getPolygon(const std::vector<glm::dvec3> &someVertices) const -> Polygon;
  inline auto operator[] (size_t i) const -> uint16_t {
    return m_indices.at(i);
  }
  // get the vertices of this face with a solid
  auto getVertices(const std::vector<glm::dvec3> &) const -> std::vector<glm::dvec3>;
  inline auto push_back (uint16_t iVertex) -> void {
    m_indices.push_back(iVertex);
  }
  inline auto push_back_unique (uint16_t iVertex) -> void {
    assert(!contains(iVertex));
    m_indices.push_back(iVertex);
  }
  inline void remove(uint16_t index) {
    m_indices.erase(std::remove(std::begin(m_indices), std::end(m_indices), index), std::end(m_indices));
  }
  inline auto setNormal(const glm::dvec3 &aNormal) -> void {
    normal = glm::normalize(aNormal);
  }
};

