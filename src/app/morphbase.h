#pragma once

#include "polyhedron.h"

enum class MorphType {NONE, DUAL, EDGE, EXPAND, REGULAR};

static std::map<MorphType, std::string> morphTypes = {
  {MorphType::NONE, "NONE"},
  {MorphType::DUAL, "DUAL"},
  {MorphType::EDGE, "EDGE"},
  {MorphType::EXPAND, "EXPAND"},
  {MorphType::REGULAR, "REGULAR"}
};

class MorphBase {
private:
  void makeClosestNeighbors(Face &newFace, const std::vector<glm::dvec3> &, const Face &oldFace);
protected:
  ObjectType m_solidType;
  glm::dvec3 crossProduct0;
  std::array<std::vector<glm::dvec3>, 4> m_vertices;
  std::array<std::vector<Face>, 4>       m_faces;

  void appendFace(size_t i, const Face &face);
  void appendFaceList(std::vector<Face> &, const std::vector<glm::dvec3> &, const Face &);
  inline void appendFaceWithVertex(size_t i, Face &face, const glm::dvec3 &vertex) {
    Polyhedron::appendFaceWithVertex(face, vertex, m_vertices[i]);
  }
  inline void appendFaceWithUniqueVertex(size_t i, Face &face, const glm::dvec3 &vertex) {
    Polyhedron::appendFaceWithUniqueVertex(face, vertex, m_vertices[i]);
  }
public:
  std::vector<glm::vec3> m_colorList;

  MorphBase();
  virtual~MorphBase();
  MorphBase(const MorphBase&) = delete; // No copy needed
  void clear();
  inline const Face& getFace(size_t i, size_t jFace) const {
    assert(jFace < m_faces[i].size());
    return m_faces[i].at(jFace);
  }
  inline size_t getNumberOfFaces() const {
    assert(m_faces[0].size() == m_faces[1].size());
    return m_faces[0].size();
  }
  inline auto getNumberOfVertices(size_t i) const {
    return m_vertices[i].size();
  }
  inline auto getSolidType() const {
    return m_solidType;
  }
  inline const glm::dvec3& getVertex(size_t i, size_t jVertex) const {
    return m_vertices[i].at(jVertex);
  }
};
