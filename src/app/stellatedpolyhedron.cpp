#include "stellatedpolyhedron.h"

StellationFace::StellationFace(const glm::dvec3& aNormal) : Face() {
  normal = aNormal;
}

StellationFace::StellationFace(const Face& aFace) : Face() {
  for (const auto iVertex : aFace)
    push_back_unique(iVertex);
}

void StellationFace::appendNoPairs(ushort iVertex) {
  if (m_indices.empty() || m_indices.back() != iVertex)
    push_back_unique(iVertex);
}

void StellationFace::appendLine(const glm::dvec3& aPoint, const glm::dvec3& aDirection) {
  Line line;
  line.point = aPoint;
  line.direction = aDirection;
  m_lines.push_back(line);
}

StellatedPolyhedron::StellatedPolyhedron(const std::unique_ptr<Polyhedron>& source) {
  const auto& originalFaces = source->getFaces();
  m_stellationVertices = source->getVertices();
  for (const auto& face : originalFaces) {
    StellationFace stellationFace(face);
    stellationFace.setNormal(m_stellationVertices);
    m_stellationFaces.push_back(stellationFace);
  }
  calculateStellationDiagram();
}

void StellatedPolyhedron::createStellation(const std::unique_ptr<Polyhedron>& source) {
  auto newType = ObjectType::NONE;
  // The new source is the previous target
  // Use <m_source> for the first stellation
  const auto& currentSource = (m_target ? m_target : source);
  const auto sourceType = currentSource->getType();
  switch (sourceType) {
  case ObjectType::CUBE:
    if (currentSource->getIsDualCompound()) {
      newType = ObjectType::CUBOCTAHEDRON_SECOND_STELLATION;
    }
    break;
  case ObjectType::CUBOCTAHEDRON:
    // Dual compound of cube and octahedron
    newType = ObjectType::CUBE;
    break;
  case ObjectType::DODECAHEDRON:
    if (currentSource->getIsDualCompound())
      newType = ObjectType::ICOSIDODECAHEDRON_SECOND_STELLATION;
    else
      newType = ObjectType::SMALL_STELLATED_DODECAHEDRON;
    break;
  case ObjectType::DODECAHEDRON_2_COMPOUND:
    newType = ObjectType::TETRAKIS_HEXAHEDRON_SECOND_STELLATION;
    break;
  case ObjectType::GREAT_DODECAHEDRON:
    newType = ObjectType::GREAT_STELLATED_DODECAHEDRON;
    break;
  case ObjectType::ICOSAHEDRON:
    newType = ObjectType::SMALL_TRIAMBIC_ICOSAHEDRON;
    break;
  case ObjectType::ICOSIDODECAHEDRON:
    // Dual compound of dodecahedron and icosahedron
    newType = ObjectType::DODECAHEDRON;
    break;
  case ObjectType::OCTAHEDRON:
    newType = ObjectType::STELLATED_OCTAHEDRON;
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON:
    newType = ObjectType::RHOMBIC_DODECAHEDRON_FIRST_STELLATION;
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON_FIRST_STELLATION:
    newType = ObjectType::RHOMBIC_DODECAHEDRON_SECOND_STELLATION;
    break;
  case ObjectType::TETRAKIS_HEXAHEDRON:
    newType = ObjectType::DODECAHEDRON_2_COMPOUND;
    break;
  case ObjectType::TRIAKIS_ICOSAHEDRON:
    newType = ObjectType::TRIAKIS_ICOSAHEDRON_FIRST_STELLATION;
    break;
  case ObjectType::TRIAKIS_OCTAHEDRON:
    newType = ObjectType::TRIAKIS_OCTAHEDRON_FIRST_STELLATION;
    break;
  case ObjectType::TRIAKIS_OCTAHEDRON_FIRST_STELLATION:
    newType = ObjectType::TRIAKIS_OCTAHEDRON_SECOND_STELLATION;
    break;
  case ObjectType::TRIAKIS_TETRAHEDRON:
    newType = ObjectType::TRIAKIS_TETRAHEDRON_FIRST_STELLATION;
    break;
  case ObjectType::TRIAKIS_TETRAHEDRON_FIRST_STELLATION:
    newType = ObjectType::TRIAKIS_TETRAHEDRON_SECOND_STELLATION;
    break;
  case ObjectType::SMALL_STELLATED_DODECAHEDRON:
    newType = ObjectType::GREAT_DODECAHEDRON;
    break;
  case ObjectType::SMALL_TRIAMBIC_ICOSAHEDRON:
    newType = ObjectType::OCTAHEDRON_5_COMPOUND;
    break;
  default:
    newType = ObjectType::NONE;
    break;
  }
  std::unique_ptr<Polyhedron> target = std::make_unique<Polyhedron>(newType, source->getColorList(), true);
  if (sourceType == ObjectType::CUBOCTAHEDRON ||
      sourceType == ObjectType::ICOSIDODECAHEDRON)
    target->setDualCompound();
  const auto currentVertices = currentSource->getVertices();
  for (const auto vertex : currentVertices) {
    target->appendVertex(vertex);
  }
  createNextStellation2(currentSource, target);
  // Move <target> to <m_target>
  m_target.reset(target.release());
}

void StellatedPolyhedron::calculateStellationDiagram() {
  const auto nFaces = m_stellationFaces.size();
  for (size_t iFace1 = 0; iFace1 < nFaces; iFace1++) {
    auto& face1 = m_stellationFaces[iFace1];
    const auto& normal1 = face1.normal;
    const auto& vertex1 = m_stellationVertices[face1[0]];
    for (size_t iFace2 = 0; iFace2 < nFaces; iFace2++) {
      if (iFace2 == iFace1)
        continue;
      const auto& face2 = m_stellationFaces[iFace2];
      const auto& normal2 = face2.normal;
      const auto& point2 = m_stellationVertices[face2[0]];
      glm::dvec3 linePoint, lineDirection;
      auto result = Polyhedron::getPlanesIntersection(normal1, vertex1, normal2, point2, linePoint, lineDirection);
      if (result == 2)
        face1.appendLine(linePoint, glm::normalize(lineDirection));
    }
  }
}

auto StellatedPolyhedron::fillLines(const StellationFace& face, ushort iVertex1, ushort iVertex2) -> std::vector<StellatedPolyhedron::LineVertex> {
  std::vector<LineVertex> lines;

  const auto& vertex1 = m_stellationVertices.at(iVertex1);
  const auto& vertex2 = m_stellationVertices.at(iVertex2);
  for (size_t iLine = 0; iLine < face.m_lines.size(); iLine++) {
    const auto& line = face.m_lines.at(iLine);
    auto d1 = Polyhedron::getDistanceToLineDir(vertex1, line.point, line.direction);
    auto d2 = Polyhedron::getDistanceToLineDir(vertex2, line.point, line.direction);
    // Skip lines which include neither <vertex1> nor <vertex2>
    if (d1 > 0.001 && d2 > 0.001)
      continue;
    // Both vertices on <line>
    if (d1 < 0.001f && d2 < 0.001f)
      continue;
    // Line includes <vertex1> or <vertex2> but not both
    LineVertex lv;
    lv.iLine = iLine;
    if (d1 < 0.001f)
      lv.iVertex = iVertex1;
    else
      lv.iVertex = iVertex2;
    lines.push_back(lv);
  }
  return lines;
}

void StellatedPolyhedron::getNewVertexUsingLines(const StellationFace& face, const std::vector<LineVertex>& lines, ushort iElem1, ushort iDelta, short &iNewVertex, glm::dvec3& newVertex) {
  const auto faceCenter = face.getCenter(m_stellationVertices);
  const auto nCorners = face.getCorners();
  const auto iVertex1 = face[iElem1];
  const auto iVertex2 = face[(iElem1 + iDelta) % nCorners];
  const auto& vertex1 = m_stellationVertices[iVertex1];
  const auto& vertex2 = m_stellationVertices[iVertex2];
  const auto edgeDir = glm::normalize(vertex2 - vertex1);
  auto faceCenterDistance = Polyhedron::getDistanceToLineDir(faceCenter, vertex1, edgeDir);
  iNewVertex = -1;
  newVertex = glm::dvec3(0);
  const auto nLines = lines.size();
  assert(nLines >= 2);
  float dist = 0;
  for (size_t iLine1 = 0; iLine1 < nLines; iLine1++) {
    if (lines[iLine1].iVertex != iVertex1)
      continue;
    // Get line1 through iVertex1;
    const auto line1 = face.m_lines[lines[iLine1].iLine];
    for (size_t iLine2 = 0; iLine2 < nLines; iLine2++) {
      if (lines[iLine2].iVertex != iVertex2)
        continue;
      // Get line2 through iVertex2;
      const auto line2 = face.m_lines[lines[iLine2].iLine];
      const auto vIntersect = Polyhedron::getLinesIntersect(vertex1, line1.direction, vertex2, line2.direction);
      if (glm::length(vIntersect) < 0.001)
        continue;

      const auto iTempVertex = Polyhedron::indexOfVector(m_stellationVertices, vIntersect);
      const auto distance1 = Polyhedron::getDistanceToLineDir(vIntersect, vertex1, edgeDir);
      if (iTempVertex >= 0) {
        if (iDelta == 2)
          continue;
        if (nCorners < 5)
          continue;
        auto iElemNew = face.indexOf(iTempVertex);
        if ((iElemNew + iDelta) % nCorners == iElem1) {
          // Prevent creating the same face twice
          continue;
        }
        // iTempVertex is on original stellation face
        const auto edgeDir1 = glm::normalize(vIntersect - vertex1);
        if (Polyhedron::getDistanceToLineDir(faceCenter, vertex2, edgeDir1) > Polyhedron::getDistanceToLineDir(faceCenter, vIntersect, edgeDir1))
          continue;
      }
      // New vertex should not be closer to the center of the face
      else if (Polyhedron::getDistanceToLineDir(faceCenter, vIntersect, edgeDir) < faceCenterDistance)
        continue;
      if (glm::length(newVertex) < 0.0001 || distance1 < dist) {
        iNewVertex = iTempVertex;
        newVertex = vIntersect;
        dist = distance1;
      }
    }
  }
}
// Replaced by createNextStellation2
void StellatedPolyhedron::createNextStellation(const Polyhedron* source, std::unique_ptr<Polyhedron>& target) {
  const auto nColors = source->getColorList().size();
  // Loop over all stellation faces
  size_t iColor = 0;
  std::vector<StellationFace> nextStellationFaces;
  for (const auto& oldStellationFace : m_stellationFaces) {
    const auto nCorners = oldStellationFace.getCorners();
    StellationFace newStellationFace(oldStellationFace.normal);
    const auto faceCenter = oldStellationFace.getCenter(m_stellationVertices);
    // Each corner of a face
    for (size_t i1 = 0; i1 < nCorners; i1++) {
      bool skipOne = false;
      const auto i2 = (i1 + 1) % nCorners;
      const auto iVertex1 = oldStellationFace.at(i1);
      const auto iVertex2 = oldStellationFace.at(i2);
      const auto lines = fillLines(oldStellationFace, iVertex1, iVertex2);
      glm::dvec3 newVertex(0);
      short iNewVertex = -1;
      if (lines.size() >= 2)
        getNewVertexUsingLines(oldStellationFace, lines, i1, 1, iNewVertex, newVertex);
      const auto i3 = (i2 + 1) % nCorners;
      auto iVertex3 = oldStellationFace.at(i3);
      if (glm::length(newVertex) < 0.0001) {
        // Check if <iVertex2> is closer to the center of the face then either <iVertex1> or <iVertex3>
        const auto d1 = glm::distance(faceCenter, m_stellationVertices.at(iVertex1));
        const auto d2 = glm::distance(faceCenter, m_stellationVertices.at(iVertex2));
        const auto d3 = glm::distance(faceCenter, m_stellationVertices.at(iVertex3));
        if ((d2 < d1 && d2 < d3) && (i2 > 0)) {
          // Use i3 instead of i2 to determine new vertex
          const auto lines = fillLines(oldStellationFace, iVertex1, iVertex3);
          if (lines.size() >= 2)
            getNewVertexUsingLines(oldStellationFace, lines, i1, 2, iNewVertex, newVertex);
          if (glm::length(newVertex) < 0.0001)
            continue;
          /* FIXME
          if (i2 == 0) {
            continue;
          }
          */
          skipOne = true;
        } else
          // Avoid duplicate face
          continue;
      } else if (iNewVertex < 0) {
        // Test iVertex2 -> iVertex3 -> iNewVertex
        const auto& vertex2 = m_stellationVertices.at(iVertex2);
        const auto& vertex3 = m_stellationVertices.at(iVertex3);
        if (Polyhedron::getDistanceToLine(newVertex, vertex2, vertex3) < 0.0001) {
          const auto d2New = glm::distance(vertex2, newVertex);
          const auto d3New = glm::distance(vertex3, newVertex);
          if (d2New > d3New) {
            const auto d23 = glm::distance(vertex2, vertex3);
            if (d23 < d2New && d23 < d3New) {
              // Use i3 instead of i2 to determine new vertex
              const auto lines = fillLines(oldStellationFace, iVertex1, iVertex3);
              getNewVertexUsingLines(oldStellationFace, lines, i1, 2, iNewVertex, newVertex);
              skipOne = true;
            }
          }
        }
      }
      if (iNewVertex < 0)
        iNewVertex = target->indexOfVector(newVertex);
      auto vertexExists = (iNewVertex >= 0);
      if (iNewVertex < 0) {
        iNewVertex = target->getNumberOfVertices();
        target->appendVertex(newVertex);
      }
      Face newFace(iColor % nColors);
      newFace.push_back_unique(iVertex1);
      newFace.push_back_unique(iVertex2);
      if (skipOne)
        newFace.push_back_unique(iVertex3);
      newFace.push_back_unique(iNewVertex);
      assert(!target->faceExists(newFace));
      target->appendFace(newFace);

      const auto iNewVertexS = Polyhedron::indexOfVector(m_stellationVertices, newVertex);
      vertexExists = (iNewVertexS >= 0);
      // Update new stellation face
      if (!vertexExists || !oldStellationFace.contains(iNewVertex)) {
        newStellationFace.appendNoPairs(iVertex1);
        newStellationFace.push_back_unique(iNewVertex);
        if (skipOne)
          newStellationFace.push_back_unique(iVertex3);
        else if (i2 > 0)
          newStellationFace.push_back_unique(iVertex2);
      } else if (oldStellationFace.contains(iNewVertexS)) {
        const auto i3 = oldStellationFace.indexOf(iNewVertex);
        if ((i3 > (short)i2) || (i2 == nCorners - 1 && i3 == 0))
          // iNewVertex eclipses iVertex2
          newStellationFace.appendNoPairs(iVertex1);
      }
      if (skipOne)
        i1++;
    }
    newStellationFace.m_lines = oldStellationFace.m_lines;
    nextStellationFaces.push_back(newStellationFace);
    iColor++;
  }
  // Remove verices not in use by a face
  target->compress();
  m_stellationFaces.clear();
  m_stellationFaces = nextStellationFaces;
  m_stellationVertices.clear();
  m_stellationVertices = target->getVertices();
}

void StellatedPolyhedron::createNextStellation2(const std::unique_ptr<Polyhedron>& source, std::unique_ptr<Polyhedron>& target) {
  const auto nColors = source->getColorList().size();
  // Loop over all stellation faces
  size_t iColor = 0;
  std::vector<StellationFace> nextStellationFaces;
  for (const auto& oldStellationFace : m_stellationFaces) {
    const auto nCorners = oldStellationFace.getCorners();
    StellationFace newStellationFace(oldStellationFace.normal);
    const auto faceCenter = oldStellationFace.getCenter(m_stellationVertices);
    size_t iOffset = 0;
    for (size_t i1 = 0; i1 < nCorners; i1++) {
      const auto i2 = (i1 + 1) % nCorners;
      const auto i3 = (i1 + 2) % nCorners;
      const auto iVertex1 = oldStellationFace.at(i1);
      const auto iVertex2 = oldStellationFace.at(i2);
      const auto iVertex3 = oldStellationFace.at(i3);
      if (glm::distance(faceCenter, m_stellationVertices.at(iVertex1)) > glm::distance(faceCenter, m_stellationVertices.at(iVertex2)) ||
          glm::distance(faceCenter, m_stellationVertices.at(iVertex2)) > glm::distance(faceCenter, m_stellationVertices.at(iVertex3))) {
        iOffset = i1;
        break;
      }
    }
    // Each corner of a face
    for (size_t i0 = iOffset; i0 < nCorners + iOffset; i0++) {
      bool skipOne = false;
      auto i1 = i0 % nCorners;
      const auto i2 = (i1 + 1) % nCorners;
      const auto iVertex1 = oldStellationFace.at(i1);
      const auto iVertex2 = oldStellationFace.at(i2);
      const auto lines = fillLines(oldStellationFace, iVertex1, iVertex2);
      glm::dvec3 newVertex(0);
      short iNewVertex = -1;
      if (lines.size() >= 2)
        getNewVertexUsingLines(oldStellationFace, lines, i1, 1, iNewVertex, newVertex);
      const auto i3 = (i2 + 1) % nCorners;
      auto iVertex3 = oldStellationFace.at(i3);
      if (glm::length(newVertex) < 0.0001) {
        // Check if <iVertex2> is closer to the center of the face then either <iVertex1> or <iVertex3>
        const auto d1 = glm::distance(faceCenter, m_stellationVertices.at(iVertex1));
        const auto d2 = glm::distance(faceCenter, m_stellationVertices.at(iVertex2));
        const auto d3 = glm::distance(faceCenter, m_stellationVertices.at(iVertex3));
        if ((d2 < d1 && d2 < d3) && (i2 > 0)) {
          // Use i3 instead of i2 to determine new vertex
          const auto lines = fillLines(oldStellationFace, iVertex1, iVertex3);
          if (lines.size() >= 2)
            getNewVertexUsingLines(oldStellationFace, lines, i1, 2, iNewVertex, newVertex);
          if (glm::length(newVertex) < 0.0001)
            continue;
          skipOne = true;
        } else
          // Avoid duplicate face
          continue;
      } else if (iNewVertex < 0) {
        // Test iVertex2 -> iVertex3 -> iNewVertex
        const auto& vertex2 = m_stellationVertices.at(iVertex2);
        const auto& vertex3 = m_stellationVertices.at(iVertex3);
        if (Polyhedron::getDistanceToLine(newVertex, vertex2, vertex3) < 0.0001) {
          const auto d2New = glm::distance(vertex2, newVertex);
          const auto d3New = glm::distance(vertex3, newVertex);
          if (d2New > d3New) {
            const auto d23 = glm::distance(vertex2, vertex3);
            if (d23 < d2New && d23 < d3New) {
              // Use i3 instead of i2 to determine new vertex
              const auto lines = fillLines(oldStellationFace, iVertex1, iVertex3);
              getNewVertexUsingLines(oldStellationFace, lines, i1, 2, iNewVertex, newVertex);
              skipOne = true;
            }
          }
        }
      }
      if (iNewVertex < 0)
        iNewVertex = target->indexOfVector(newVertex);
      auto vertexExists = (iNewVertex >= 0);
      if (iNewVertex < 0) {
        iNewVertex = target->getNumberOfVertices();
        target->appendVertex(newVertex);
      }
      Face newFace(iColor % nColors);
      newFace.push_back_unique(iVertex1);
      newFace.push_back_unique(iVertex2);
      if (skipOne)
        newFace.push_back_unique(iVertex3);
      newFace.push_back_unique(iNewVertex);
      assert(!target->faceExists(newFace));
      target->appendFace(newFace);

      const auto iNewVertexS = Polyhedron::indexOfVector(m_stellationVertices, newVertex);
      vertexExists = (iNewVertexS >= 0);
      // Update new stellation face
      if (!vertexExists || !oldStellationFace.contains(iNewVertex)) {
        newStellationFace.appendNoPairs(iVertex1);
        newStellationFace.push_back_unique(iNewVertex);
        if (skipOne)
          newStellationFace.push_back_unique(iVertex3);
        else if (i2 > iOffset)
          newStellationFace.push_back_unique(iVertex2);
      } else if (oldStellationFace.contains(iNewVertexS)) {
        const auto i3 = oldStellationFace.indexOf(iNewVertex);
        if ((i3 > static_cast<short>(i2)) || (i2 == nCorners - 1 && i3 == static_cast<int>(iOffset)))
          // iNewVertex eclipses iVertex2
          newStellationFace.appendNoPairs(iVertex1);
      }
      if (skipOne)
        i1++;
    }
    newStellationFace.m_lines = oldStellationFace.m_lines;
    nextStellationFaces.push_back(newStellationFace);
    iColor++;
  }
  // Remove verices not in use by a face
  target->compress();
  m_stellationFaces.clear();
  m_stellationFaces = nextStellationFaces;
  m_stellationVertices.clear();
  m_stellationVertices = target->getVertices();
}
