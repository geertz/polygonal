#pragma once

#include <unordered_map>

#include "morphbase.h"

class MorphEdge : public MorphBase {
public:
  MorphEdge(const std::unique_ptr<Polyhedron>& origin);
private:
  std::vector<Edge>       m_originalEdges;
  std::vector<Face>       m_originalFaces;
  std::vector<glm::dvec3> m_originalVertices;
  std::vector<Plane>      m_endPlanes;
  std::vector<std::map<uint16_t, uint16_t>> m_faceVertexList;

  Face buildEndFace(size_t iEdge1, const std::vector<std::vector<size_t>>& vertexEdges);
  void buildFaces(const std::unique_ptr<Polyhedron>& origin, size_t iEdge1, const std::vector<std::vector<size_t>>& vertexEdges, uint16_t iColor);
  void createEndFaces(const std::unique_ptr<Polyhedron>& origin, double minEdgeDistance, std::vector<size_t>& faceVertices, uint16_t &lastColor);
  void fillEndPlanes(const std::vector<size_t>& faceVertices);
  void getDistanceCenterToEdge(double& minValue, double& maxValue);
  Plane getEdgePlane(const std::unique_ptr<Polyhedron>& origin, size_t iEdge) const;
  void getEdgesLines(const std::vector<size_t>& edgeList, size_t iEdge, std::vector<size_t> &edges, std::unordered_map<size_t, Line> &lines);
  std::vector<glm::dvec3> getEdgeVertices(const std::unique_ptr<Polyhedron>& origin, size_t iEdge, const std::vector<size_t> &edgeList);
  static int getIntersectIndex(const Line& line1, const Line& line2, std::vector<glm::dvec3>& someVertices);
  static void removeFromVector(std::vector<size_t>& vector, size_t aValue);
  glm::dvec3 calculateFaceNormal(const std::unique_ptr<Polyhedron>& origin, const Edge& edge);
  
};
