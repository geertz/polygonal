#include "regularpolyhedron.h"

// Conway notation : ambo
// ambo * ambo = expand
RegularPolyhedron::RegularPolyhedron(const std::unique_ptr<Polyhedron>& source) {
  ObjectType newType = Polyhedron::getRegularType(source->getType());
  m_target = std::make_unique<Polyhedron>(newType, source->getColorList(), true);
  m_target->m_isConvex = source->m_isConvex;
  const auto &originalVertices = source->getVertices();
  const auto &originalFaces = source->getFaces();
  auto indexList = std::vector<std::vector<uint16_t>>(originalVertices.size());
  // Create the new faces based on the original one with vertices in between the original ones
  // Fill the indexList to create the new faces at the corners
  for (const auto &originalFace : originalFaces) {
    const auto nCorners = originalFace.getCorners();
    Face newFace(0);
    for (size_t i = 0; i < nCorners; i++) {
      // Get a vertex in between two original vertices
      // Pairs: 0,1 1,2 ..., corners - 1,0
      const auto iOriginalVertex1 = originalFace.at(i);
      const auto iOriginalVertex2 = originalFace.at((i + 1) % nCorners);
      const auto newVertex = (originalVertices[iOriginalVertex1] + originalVertices[iOriginalVertex2]) / 2.0;
      const auto iNewVertex = m_target->appendFaceWithUniqueVertex(newFace, newVertex);
      // Check whether indexList[iOriginalVertex1] contains iNewVertex
      auto &list1 = indexList[iOriginalVertex1];
      if (!contains(list1, iNewVertex))
        list1.push_back(iNewVertex);
      // Check whether indexList[iOriginalVertex2] contains iNewVertex
      auto &list2 = indexList[iOriginalVertex2];
      if (!contains(list2, iNewVertex))
        list2.push_back(iNewVertex);
    }
     m_target->appendFace(newFace);
  }
  // Create faces at the position of the original vertices using <indexList>
  Polyhedron::createCornerfaces(source, m_target, indexList, 0, pairVertexFaceList);
  // Set color indices
  m_target->setColorFaces2();
}
