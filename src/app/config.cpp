#include <sys/stat.h>
#include <pwd.h>
#include <iostream>
#include "config.h"

Config::Config() {
  m_filename.append(getHomeDir());
  m_filename.append("/Polygonal.json");
}

Config::Config(std::string filename) {
  m_filename.append(filename);
}

bool Config::fileExists() {
  struct stat buffer;
  return (stat (m_filename.c_str(), &buffer) == 0);
}

char *Config::getHomeDir() {
  char *homedir = getenv("HOME");
  if ( homedir == nullptr) {
    homedir = getpwuid(getuid())->pw_dir;
  }
  return homedir;
}

auto Config::readColor(pt::ptree& root, const std::string& tag) -> glm::vec3 {
  glm::vec3 color{};
  int i = 0;
  const auto colorNode = root.get_child_optional(tag);
  glm::vec3 defColor = glm::vec3(0.5f, 0.5f, 0.5f);
  if (colorNode.has_value()) {
    for (const pt::ptree::value_type &cell : colorNode.value()) {
      if (i < 3) {
        color[i] = cell.second.get_value<float>();
        if (color[i] < 0.0f)
          color[i] = 0.0f;
        else if (color[i] > 1.0f)
          color[i] = 1.0f;
      }
      i++;
    }
    if (i < 3)
      color[2] = defColor[2];
    if (i < 2)
      color[1] = defColor[1];
    if (i < 1)
      color[0] = defColor[0];
    return color;
  } else
    return defColor;
}

auto Config::readFractalPower(pt::ptree& root) -> uint32_t {
  boost::optional<uint32_t> optPower = root.get_optional<uint32_t>(NODE_FRACTAL_POWER);
  auto result = optPower.value_or(2);
  if (result < 2)
    result = 2;
  if (result > 16)
    result = 2;
  return result;
}
// Read the first Julia seed
auto Config::readJuliaSeed1(boost::property_tree::ptree& root) -> glm::vec2 {
  glm::vec2 seed{};
  int i = 0;
  const auto node = root.get_child_optional(NODE_FRACTAL_SEED1);
  glm::vec2 defSeed = glm::vec2(0.0f, 0.7f);
  if (node.has_value()) {
    for (const pt::ptree::value_type& cell : node.value()) {
      if (i < 2) {
        seed[i] = cell.second.get_value<float>();
      }
      i++;
    }
    return seed;
  } else
    return defSeed;
}
// Read the second Julia seed
auto Config::readJuliaSeed2(boost::property_tree::ptree& root) -> glm::vec2 {
  glm::vec2 seed{};
  int i = 0;
  const auto node = root.get_child_optional(NODE_FRACTAL_SEED2);
  glm::vec2 defSeed = glm::vec2(0.0f, 0.7f);
  if (node.has_value()) {
    for (const pt::ptree::value_type& cell : node.value()) {
      if (i < 2) {
        seed[i] = cell.second.get_value<float>();
      }
      i++;
    }
    return seed;
  } else
    return defSeed;
}

std::vector<glm::vec3> Config::readObjectColors(pt::ptree& root) {
  std::vector<glm::vec3> result{};
  for (const pt::ptree::value_type &colorNode : root.get_child(NODE_OBJECT_COLORS)) {
    glm::vec3 color{};
    int i = 0;
    for (const pt::ptree::value_type &cell : colorNode.second) {
      if (i < 3)
        color[i] = cell.second.get_value<float>();
      i++;
    }
    result.emplace_back(color);
  }
  const auto nColors = result.size();
  if (nColors < 1)
    result.push_back({1.0f,0.0f,0.0f});
  if (nColors < 2)
    result.push_back({0.0f,1.0f,0.0f});
  if (nColors < 3)
    result.push_back({0.0f,0.0f,1.0f});
  if (nColors < 4)
    result.push_back({1.0f,1.0f,0.0f});
  if (nColors < 5)
    result.push_back({1.0f,0.0f,1.0f});
  return result;
}

void Config::readBackgroundColor(pt::ptree root, vk::ClearColorValue &aColor) {
  if (root.count(NODE_BACKGROUND_COLOR) == 1) {
    int i = 0;
    const auto colorNode = root.get_child(NODE_BACKGROUND_COLOR);
    for (const pt::ptree::value_type &cell : colorNode) {
      if (i < 3)
        aColor.float32[i] = cell.second.get_value<float>();
      i++;
    }
  } else {
    aColor.float32[0] = 0;
    aColor.float32[1] = 0;
    aColor.float32[2] = 0;
  }
}

void Config::readWindowExtent(pt::ptree root, int& width, int& height) {
  boost::optional<int> w = root.get_optional<int>(NODE_WINDOW_WIDTH);
  width = w.value_or(800);
  boost::optional<int> h = root.get_optional<int>(NODE_WINDOW_HEIGHT);
  height = h.value_or(600);
}

pt::ptree Config::readFile() const {
  pt::ptree root{};

  pt::read_json(m_filename, root);
  return root;
}

bool Config::writeFile() {
  pt::ptree root;

  pt::ptree objectColors = writeDefaultColors();
  root.add_child(NODE_OBJECT_COLORS, objectColors);
  try {
    pt::write_json(m_filename, root);
    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}

pt::ptree Config::writeDefaultColors() {
  pt::ptree objectColors;
  pt::ptree red, green, blue;

  pt::ptree color;
  // First color : red
  red.put_value(1.0f);
  color.push_back(std::make_pair("", red));
  green.put_value(0.0f);
  color.push_back(std::make_pair("", green));
  blue.put_value(0.0f);
  color.push_back(std::make_pair("", blue));
  objectColors.push_back(std::make_pair("", color));
  // Second color : green
  color.clear();
  red.put_value(0.0f);
  color.push_back(std::make_pair("", red));
  green.put_value(1.0f);
  color.push_back(std::make_pair("", green));
  blue.put_value(0.0f);
  color.push_back(std::make_pair("", blue));
  objectColors.push_back(std::make_pair("", color));
  // Third color : blue
  color.clear();
  red.put_value(0.0f);
  color.push_back(std::make_pair("", red));
  green.put_value(0.0f);
  color.push_back(std::make_pair("", green));
  blue.put_value(1.0f);
  color.push_back(std::make_pair("", blue));
  objectColors.push_back(std::make_pair("", color));
  // Fourth color : yellow
  color.clear();
  red.put_value(1.0f);
  color.push_back(std::make_pair("", red));
  green.put_value(1.0f);
  color.push_back(std::make_pair("", green));
  blue.put_value(0.0f);
  color.push_back(std::make_pair("", blue));
  objectColors.push_back(std::make_pair("", color));
  // Fifth color : purple
  color.clear();
  red.put_value(1.0f);
  color.push_back(std::make_pair("", red));
  green.put_value(0.0f);
  color.push_back(std::make_pair("", green));
  blue.put_value(1.0f);
  color.push_back(std::make_pair("", blue));
  objectColors.push_back(std::make_pair("", color));

  return objectColors;
}

void Config::putValue(pt::ptree& root, const std::string& tag, float aValue) {
  if (!isinf(aValue) && !isnan(aValue)) {
    pt::ptree node;
    node.put_value(aValue);
    root.put_child(tag, node);
  }
}

auto Config::writeColor(pt::ptree& root, const std::string& tag, const glm::vec3& aColor) -> bool {
  pt::ptree red, green, blue;
  pt::ptree pColor;

  try {
    // Red
    red.put_value(aColor[0]);
    pColor.push_back(std::make_pair("", red));
    // Green
    green.put_value(aColor[1]);
    pColor.push_back(std::make_pair("", green));
    // Blue
    blue.put_value(aColor[2]);
    pColor.push_back(std::make_pair("", blue));
    root.put_child(tag, pColor);

    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}
// Save the power of the fractal function z -> z^power
auto Config::writeFractalPower(pt::ptree& root, const uint32_t& aValue) -> bool {
  try {
    pt::ptree node;
    node.put_value(aValue);
    root.put_child(NODE_FRACTAL_POWER, node);
    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}

auto Config::writeJuliaSeed(pt::ptree& root, const glm::vec2& seed1, const glm::vec2& seed2) -> bool {
  pt::ptree x, y;
  pt::ptree pSeed;

  try {
    {
      // X
      x.put_value(seed1.x);
      pSeed.push_back(std::make_pair("", x));
      // Y
      y.put_value(seed1.y);
      pSeed.push_back(std::make_pair("", y));
      root.put_child(NODE_FRACTAL_SEED1, pSeed);
    }
    pSeed.clear();
    {
      // X
      x.put_value(seed2.x);
      pSeed.push_back(std::make_pair("", x));
      // Y
      y.put_value(seed2.y);
      pSeed.push_back(std::make_pair("", y));
      root.put_child(NODE_FRACTAL_SEED2, pSeed);
    }
    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}

auto Config::writeObjectColors(const std::vector<glm::vec3>& colorList) -> bool {
  pt::ptree objectColors;
  pt::ptree red, green, blue;
  pt::ptree pColor;

  for (const auto& color : colorList) {
    pColor.clear();
    // Red
    red.put_value(color.r);
    pColor.push_back(std::make_pair("", red));
    // Green
    green.put_value(color.g);
    pColor.push_back(std::make_pair("", green));
    // Blue
    blue.put_value(color.b);
    pColor.push_back(std::make_pair("", blue));
    objectColors.push_back(std::make_pair("", pColor));
  }
  try {
    pt::ptree root = readFile();
    root.put_child(NODE_OBJECT_COLORS, objectColors);
    pt::write_json(m_filename, root);
    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}

bool Config::writeVulkanLibSettings(pt::ptree& root, const vk::ClearColorValue& bgColor) {
  pt::ptree red, green, blue;
  pt::ptree pColor;

  try {
    // Red
    red.put_value(bgColor.float32[0]);
    pColor.push_back(std::make_pair("", red));
    // Green
    green.put_value(bgColor.float32[1]);
    pColor.push_back(std::make_pair("", green));
    // Blue
    blue.put_value(bgColor.float32[2]);
    pColor.push_back(std::make_pair("", blue));
    root.put_child(NODE_BACKGROUND_COLOR, pColor);

    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}

auto Config::writeWindowSize(int width, int height) const -> bool {
  pt::ptree node;

  try {
    pt::ptree root = readFile();
    node.put_value(width);
    root.put_child(NODE_WINDOW_WIDTH, node);
    node.put_value(height);
    root.put_child(NODE_WINDOW_HEIGHT, node);
    pt::write_json(m_filename, root);
    return true;
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return false;
  }
}
