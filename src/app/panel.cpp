#include <glm/gtc/matrix_transform.hpp>
#include <sys/stat.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include "imgui/imgui.h"

#include "panel.h"
#include "graphicspipelineresource.h"
#include "morphdual.h"
#include "morphedge.h"
#include "morphexpanddual.h"
#include "morphregular.h"
#include "polyhedron.h"
#include "compoundpolyhedron.h"

Panel::Panel(GraphicsResource *pGraphics) {
  m_graphics = pGraphics;
  m_type = PanelType::NONE;
  m_prepared = false;
}

auto Panel::addEyeAzimuthInclination(double deltaAzimuth, double deltaInclination) -> void {
  m_eyeAzimuth += deltaAzimuth;
  if (m_eyeAzimuth > M_PI) {
    m_eyeAzimuth = -M_PI + fmod(m_eyeAzimuth, M_PI);
  } else if (m_eyeAzimuth < -M_PI) {
    m_eyeAzimuth = M_PI + fmod(m_eyeAzimuth, M_PI);
  }

  m_eyeInclination += deltaInclination;
  if (m_eyeInclination > M_PI) {
    m_eyeInclination = M_PI - fmod(m_eyeInclination, M_PI);
    if (m_eyeAzimuth > 0)
      m_eyeAzimuth -= M_PI;
    else
      m_eyeAzimuth += M_PI;
  } else if (m_eyeInclination < 0.0) {
    m_eyeInclination = - fmod(m_eyeInclination, M_PI);
    if (m_eyeAzimuth > 0)
      m_eyeAzimuth -= M_PI;
    else
      m_eyeAzimuth += M_PI;
  }
  setEye();
}

void Panel::draw(const SwapchainImageResource& imageResource, size_t iImage) {
  imageResource.setViewport(m_viewport);
  if (m_type == PanelType::SOLID) {
    // Draw axis
    assert(m_axisPipeline);
    m_axisPipeline->bindCommandBuffer(imageResource, iImage);
    imageResource.draw(6, 1, 0, 0);
    switch (m_shader) {
    case ShaderType::MORPH:
      assert(m_morphPipeline);
      m_morphPipeline->draw(imageResource, iImage);
      break;
    case ShaderType::SOLID_FILL:
      assert(m_solidFillPipeline);
      m_solidFillPipeline->draw(imageResource, iImage);
      break;
    case ShaderType::SOLID_FILL_TEXTURE:
      assert(m_solidFillTexturePipeline);
      m_solidFillTexturePipeline->draw(imageResource, iImage);
      break;
    case ShaderType::SOLID_LINE:
      assert(m_solidLinePipeline);
      m_solidLinePipeline->draw(imageResource, iImage);
      break;
    default:
      break;
    }
    if (m_showPoints && (m_shader == ShaderType::SOLID_FILL || m_shader == ShaderType::SOLID_FILL_TEXTURE || m_shader == ShaderType::SOLID_LINE)) {
      assert(m_solidPointPipeline);
      m_solidPointPipeline->draw(imageResource, iImage);
    }
    m_canDraw = true;
  } else if (m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE) {
    assert(m_imagePipeline);
    m_imagePipeline->bindCommandBuffer(imageResource, iImage);
    imageResource.draw(6,1,0,0);
    m_canDraw = true;
  } else if (m_type == PanelType::MESH) {
    if (m_meshUsesSkin) {
      assert(m_meshTexSkinPipeline);
      m_meshTexSkinPipeline->draw(imageResource, iImage);
    } else if (m_meshUsesTexture) {
      assert(m_meshTexturePipeline);
      m_meshTexturePipeline->draw(imageResource, iImage);
    } else if (m_meshUsesMaterial) {
      assert(m_meshMaterialPipeline);
      m_meshMaterialPipeline->draw(imageResource, iImage);
    } else {
      assert(m_meshPipeline);
      m_meshPipeline->draw(imageResource, iImage);
    }
    m_canDraw = true;
  }
}

auto Panel::buildObjectMVP() const -> glm::mat4 {
  if (m_enableRotation) {
    const glm::mat4 model = glm::rotate(glm::dmat4(1.0), m_angleOffset + m_duration * M_PI_2 * m_rotationSpeed, m_rotationAxis);
    return buildObjectVP() * model;
  } else
    return buildObjectVP();
}

auto Panel::buildObjectVP() const -> glm::mat4 {
  assert(m_viewport.width > 0 && m_viewport.height > 0);
  glm::mat4 view = glm::lookAt(m_eye, glm::dvec3(0.0, 0.0, 0.0), glm::dvec3(0.0, 1.0, 0.0));
  glm::mat4 proj = glm::perspective(M_PIf32 / 3.0f, static_cast<float>(m_viewport.width) / static_cast<float>(m_viewport.height), 0.1f, 100.0f);
  // Vulkan clip space has inverted Y and half Z.
  static glm::mat4 clip(1.0f,  0.0f, 0.0f, 0.0f,
                        0.0f,  1.0f, 0.0f, 0.0f,
                        0.0f,  0.0f, 0.5f, 0.0f,
                        0.0f,  0.0f, 0.5f, 1.0f);
  if (m_flipY)
    clip[1][1] = -1.0f;
  return clip * proj * view;
}

auto Panel::buildWindowTitle(MorphType morphType) const -> std::string {
  std::string result = "";
  if (m_type == PanelType::IMAGE_COMP) {
    assert(m_computeResource);
    auto fractalType = m_computeResource->getFractalType();
    if (fractalType == FractalType::JULIA)
      result = "Julia fractal";
    else
      result = "Mandelbrot fractal";
  } else if (m_type == PanelType::IMAGE_FILE) {
    if (m_textureFile.empty())
      result = "No file";
    else
      result = "File " + m_textureFile;
  } else if (m_type == PanelType::MESH) {
    if (m_gltfModelNext)
      result = m_gltfModelNext->getName();
    else {
      assert(m_gltfModel);
      result = m_gltfModel->getName();
    }
  } else if (m_type == PanelType::SOLID && m_solid) {
    const auto solidType = m_solid->getType();
    switch (morphType) {
    case MorphType::DUAL: {
      if (solidType == ObjectType::NONE) {
        result = "Morph dual";
      } else {
        auto dualType = Polyhedron::getDualType(solidType);
        result = "Morph dual of " + Polyhedron::getName(solidType) + " and " + Polyhedron::getName(dualType);
      }
    }
      break;
    case MorphType::EDGE: {
      if (solidType == ObjectType::NONE) {
        result = "Morph edge";
      } else {
        result = "Morph edge of " + Polyhedron::getName(solidType);
      }
    }
      break;
    case MorphType::EXPAND: {
      if (solidType == ObjectType::NONE) {
        result = "Morph expand dual";
      } else {
        auto dualType = Polyhedron::getDualType(solidType);
        result = "Morph expand dual of " + Polyhedron::getName(solidType) + " and " + Polyhedron::getName(dualType);
      }
    }
      break;
    case MorphType::REGULAR: {
      if (solidType == ObjectType::NONE) {
        result = "Morph regular";
      } else {
        auto regularType = Polyhedron::getRegularType(solidType);
        result = "Morph regular of " + Polyhedron::getName(solidType) + " and " + Polyhedron::getName(regularType);
      }
    }
      break;
    case MorphType::NONE:
      result = m_solid->getName();
      break;
    default:
      throw std::runtime_error("Illegal morph option!");
      break;
    }
  }
  return result;
}

auto Panel::copyPipelineUBO(size_t iImage) -> bool {
  if (m_type == PanelType::MESH) {
    assert(m_gltfModel);
    if (m_meshUsesSkin) {
      assert(m_meshTexSkinPipeline);
      if (!m_meshTexSkinPipeline->copyUBO(iImage, m_gltfModel)) {
        m_errorMessage = m_meshTexSkinPipeline->getErrorMessage();
        return false;
      }
    } else if (m_meshUsesTexture) {
      assert(m_meshTexturePipeline);
      if (!m_meshTexturePipeline->copyUBO(iImage)) {
        m_errorMessage = m_meshTexturePipeline->getErrorMessage();
        return false;
      }
    } else if (m_meshUsesMaterial) {
      assert(m_meshMaterialPipeline);
      if (!m_meshMaterialPipeline->copyUBO(iImage)) {
        m_errorMessage = m_meshMaterialPipeline->getErrorMessage();
        return false;
      }
    } else {
      assert(m_meshPipeline);
      if (!m_meshPipeline->copyUBO(iImage)) {
        m_errorMessage = m_meshPipeline->getErrorMessage();
        return false;
      }
    }
  } else if (m_type == PanelType::SOLID) {
    switch (m_shader) {
    case ShaderType::MORPH:
      assert(m_morphPipeline);
      if (!m_morphPipeline->copyUBO(iImage)) {
        m_errorMessage = m_morphPipeline->getErrorMessage();
        return false;
      }
      break;
    case ShaderType::SOLID_FILL_TEXTURE:
      assert(m_solidFillTexturePipeline);
      if (!m_solidFillTexturePipeline->copyUBO(iImage)) {
        m_errorMessage = m_solidFillTexturePipeline->getErrorMessage();
        return false;
      }
      break;
    case ShaderType::SOLID_FILL:
      assert(m_solidFillPipeline);
      if (!m_solidFillPipeline->copyUBO(iImage)) {
        m_errorMessage = m_solidFillPipeline->getErrorMessage();
        return false;
      }
      break;
    default:
      break;
    }
    if (m_drawLines && m_solidLinePipeline) {
      if (!m_solidLinePipeline->copyUBO(iImage)) {
        m_errorMessage = m_solidLinePipeline->getErrorMessage();
        return false;
      }
    }
    if (m_showPoints && m_solidPointPipeline) {
      if (!m_solidPointPipeline->copyUBO(iImage)) {
        m_errorMessage = m_solidPointPipeline->getErrorMessage();
        return false;
      }
    }
  }
  return true;
}

auto Panel::copyPanel(const Panel& panel, bool right) -> bool {
  m_type = panel.m_type;
  // Color
  m_colorAlpha = panel.m_colorAlpha;
  m_colorAmbient = panel.m_colorAmbient;
  m_metallic = panel.m_metallic;
  // Eye
  m_eyeAzimuth = panel.m_eyeAzimuth;
  m_eyeDistance = panel.m_eyeDistance;
  m_eyeInclination = panel.m_eyeInclination;
  setEye();
  // Light
  m_lightAzimuth = panel.m_lightAzimuth;
  m_lightInclination = panel.m_lightInclination;
  // Rotation
  m_enableRotation          = panel.m_enableRotation;
  m_rotate                  = panel.m_rotate;
  m_rotationAxisAzimuth     = panel.m_rotationAxisAzimuth;
  m_rotationAxisInclination = panel.m_rotationAxisInclination;
  setRotationAxis();
  // Speed
  m_morphSpeed = panel.m_morphSpeed;
  m_rotationSpeed = panel.m_rotationSpeed;
  // Texture
  m_viewport = panel.m_viewport;
  // vertical split
  m_viewport.width /= 2.0;
  if (right)
    m_viewport.x = m_viewport.width;
  if (panel.m_juliaSetup)
    m_juliaSetup = panel.m_juliaSetup;
  if (panel.m_mandelbrotSetup)
    m_mandelbrotSetup = panel.m_mandelbrotSetup;
  if (m_type == PanelType::IMAGE_COMP) {
    assert(panel.m_computeResource);
    m_fractalType = panel.m_computeResource->getFractalType();
    ComputeSetup setup;
    setup.colorA = panel.m_computeResource->getColorA();
    setup.colorB = panel.m_computeResource->getColorB();
    setup.fractalPower = panel.m_computeResource->getFractalPower();
    setup.juliaSeed1 = panel.m_computeResource->getJuliaSeed1();
    setup.juliaSeed2 = panel.m_computeResource->getJuliaSeed2();
    setup.xOffset = panel.m_computeResource->getXoffetResized(m_viewport.width, m_viewport.height);
    setup.yOffset = panel.m_computeResource->getYoffset();
    setup.zoom = panel.m_computeResource->getZoom();
    m_nextType = m_type;
    if (!prepareCompute(setup) || !computeImage())
      return false;
  } else if (m_type == PanelType::MESH) {
    m_meshFile = panel.m_meshFile;
    updateGltfModel();
    m_prepared = true;
  } else if (!panel.m_textureFile.empty()) {
    m_textureFile = panel.m_textureFile;
    m_textureResource.reset(new TextureResource(m_graphics, m_textureFile.c_str(), true));
  }
  // Solid
  m_drawLines = panel.m_drawLines;
  m_showPoints = panel.m_showPoints;
  m_useTexture = panel.m_useTexture;
  if (m_type == PanelType::SOLID) {
    assert(panel.m_solid);
    m_nextObject = panel.m_solid->getType();
    m_transform = panel.m_transform;
    m_morph = panel.m_morph;
    updateSolid(panel.m_solid->getType(), panel.m_solid->getColorList());
  }
  if (!createPipelines())
    return false;
  fillSolidPipelineBuffers();
  return true;
}

auto Panel::clearPanel() -> void {
  if (m_solid) {
    m_solid.reset(nullptr);
  }
  // Leave the pipelines intact
  // Can reuse them when rectivating the panel
  m_type = PanelType::NONE;
  m_nextType = PanelType::NONE;
}
// submit the compute commands
auto Panel::computeImage() -> bool {
  assert(m_computeResource);
  if (!m_computeResource->submit(m_graphics->m_graphicsQueue2, m_duration)) {
    m_errorMessage = m_computeResource->getErrorMessage();
    m_computeResource.reset(nullptr);
    return false;
  }
  return true;
}

auto Panel::copyUniformBuffer(uint32_t iImage) -> bool {
  if (m_type == PanelType::SOLID && m_axisPipeline) {
    if (!m_axisPipeline->copyUBO(iImage))
      return false;
  }
  if (m_type == PanelType::MESH || m_type == PanelType::SOLID) {
    if (m_viewChanged) {
      updateUniformBuffer(true);
      m_viewChanged = false;
    } else if (m_rotate) {
      setDuration();
      updateUniformBuffer(iImage == 0);
    }
    if (!copyPipelineUBO(iImage))
      return false;
  } else if (m_type == PanelType::IMAGE_COMP) {
    assert(m_computeResource);
    setDuration();
  }
  return true;
}
// Create the pipeline to show an image
auto Panel::prepareImage() -> bool {
  assert(m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE);
  if (!m_imagePipeline) {
    m_imagePipeline = std::make_unique<ImagePipeline>(m_graphics);
    if (m_imagePipeline->hasError()) {
      m_errorMessage = m_imagePipeline->getErrorMessage();
      m_imagePipeline.reset(nullptr);
      return false;
    }
  }
  if (m_type == PanelType::IMAGE_COMP) {
    assert(m_computeResource);
    assert(m_computeResource->m_status == ComputeState::FINISHED || m_computeResource->m_status == ComputeState::INITIAL);
    auto reComputed = (m_computeResource->m_status == ComputeState::FINISHED);
    auto resComp = new TextureResource(m_graphics, m_computeResource);
    if (resComp->hasError()) {
      m_errorMessage = resComp->getErrorMessage();
      delete resComp;
      return false;
    }
    if (reComputed)
      updateTextureResource(resComp);
    else
      m_textureResource.reset(resComp);
  } else {
    auto resFile = new TextureResource(m_graphics, m_textureFile.c_str(), true);
    if (resFile->hasError()) {
      m_errorMessage = resFile->getErrorMessage();
      delete resFile;
      return false;
    }
    m_textureResource.reset(resFile);
  }
  m_imagePipeline->updateUniformBuffers(m_textureResource->m_imageView);
  m_imagePipeline->fillVertices(m_viewport, m_textureResource->getExtent());
  return true;
}
// Create the pipeline for a morphing solid
auto Panel::createMorphPipeline(MorphType morphType) -> bool {
  if (!m_morphPipeline) {
    m_morphPipeline = std::make_unique<MorphPipeline>(m_graphics, m_colorAlpha, m_colorAmbient);
    if (m_morphPipeline->hasError()) {
      m_errorMessage = m_morphPipeline->getErrorMessage();
      m_morphPipeline.reset(nullptr);
      return false;
    }
  }
  m_drawLines  = false;
  m_morph      = morphType;
  m_showPoints = false;
  m_useTexture = false;
  return true;
}

auto Panel::createObjectPipeline() -> bool {
  assert(m_type == PanelType::SOLID);
  const auto mvp = buildObjectMVP();
  if (!m_axisPipeline) {
    m_axisPipeline = std::make_unique<AxisPipeline>(m_graphics);
    if (m_axisPipeline->hasError()) {
      m_errorMessage = m_axisPipeline->getErrorMessage();
      m_axisPipeline.reset(nullptr);
      return false;
    }
  }
  m_axisPipeline->setMVP(mvp);
  if (m_morph != MorphType::NONE) {
    if (!createMorphPipeline(m_morph)) {
      return false;
    }
    m_morphPipeline->setModel(getModel());
    m_morphPipeline->setProjectionView(buildObjectVP());
  } else {
    if (m_drawLines) {
      if (!createSolidLinePipeline())
        return false;
      m_solidLinePipeline->setMVP(mvp);
    } else {
      if (m_useTexture && m_textureResource) {
        if (!updateTextureResource())
          return false;
        m_solidFillTexturePipeline->setModel(getModel());
        m_solidFillTexturePipeline->setProjectionView(buildObjectVP());
      } else {
        if (!createSolidFillPipeline())
          return false;
        m_solidFillPipeline->setModel(getModel());
        m_solidFillPipeline->setProjectionView(buildObjectVP());
      }
    }
    if (m_showPoints) {
      if (!createSolidPointPipeline())
        return false;
      m_solidPointPipeline->setMVP(mvp);
    }
  }
  return true;
}

auto Panel::createPipelineBuffers() -> bool {
  if (m_type == PanelType::SOLID && m_solid) {
    if (m_morph != MorphType::NONE) {
      assert(m_morphPipeline);
      m_morphPipeline->createBuffers();
      m_shader = ShaderType::MORPH;
    } else {
      if (m_useTexture) {
        m_solidFillTexturePipeline->createBuffers();
        m_shader = ShaderType::SOLID_FILL_TEXTURE;
      } else if (m_drawLines) {
        assert(m_solidLinePipeline);
        m_solidLinePipeline->createBuffers();
        m_shader = ShaderType::SOLID_LINE;
      } else {
        assert(m_solidFillPipeline);
        m_solidFillPipeline->updateVertexIndexBuffers();
        m_shader = ShaderType::SOLID_FILL;
      }
      if (m_showPoints && m_solidPointPipeline)
        m_solidPointPipeline->updateVertexBuffer();
    }
  } else if (m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE) {
    if (!m_imagePipeline) {
      if (!prepareImage())
        return false;
    } else {
      updateImageUniformBuffers();
      m_imagePipeline->fillVertices(m_viewport, m_textureResource->getExtent());
    }
    m_imagePipeline->updateVertexBuffer();
  } else if (m_type == PanelType::MESH) {
    if (m_meshUsesSkin) {
      assert(m_meshTexSkinPipeline);
      m_meshTexSkinPipeline->updateVertexBuffer();
    } else if (m_meshUsesTexture) {
      assert(m_meshTexturePipeline);
      m_meshTexturePipeline->updateVertexBuffer();
    } else if (m_meshUsesMaterial) {
      assert(m_meshMaterialPipeline);
      m_meshMaterialPipeline->updateVertexBuffer();
    } else {
      assert(m_meshPipeline);
      m_meshPipeline->updateVertexBuffer();
    }
  }
  m_viewChanged = true;
  return true;
}

auto Panel::createPipelines() -> bool {
  if (m_type == PanelType::SOLID) {
    if (!m_textureFile.empty()) {
      m_textureResource.reset(new TextureResource(m_graphics, m_textureFile.c_str(), true));
      if (!m_errorMessage.empty())
        return false;
      m_useTexture = true;
    }
    if (!createObjectPipeline())
      return false;
  } else if (m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE) {
    if (!prepareImage())
      return false;
    m_imagePipeline->updateVertexBuffer();
  } else if (m_type == PanelType::MESH) {
    // Pipeline created by method updateGltfModel
  }
  return true;
}

auto Panel::createSolidFillPipeline() -> bool {
  if (!m_solidFillPipeline) {
    m_solidFillPipeline = std::make_unique<SolidFillPipeline>(m_graphics, m_colorAlpha, m_colorAmbient);
    if (m_solidFillPipeline->hasError()) {
      m_errorMessage = m_solidFillPipeline->getErrorMessage();
      m_solidFillPipeline.reset(nullptr);
      return false;
    }
  }
  m_drawLines = false;
  m_morph = MorphType::NONE;
  m_useTexture = false;
  return true;
}

auto Panel::createSolidLinePipeline() -> bool {
  if (!m_solidLinePipeline) {
    m_solidLinePipeline = std::make_unique<SolidLinePipeline>(m_graphics);
    if (m_solidLinePipeline->hasError()) {
      m_errorMessage = m_solidLinePipeline->getErrorMessage();
      m_solidLinePipeline.reset(nullptr);
      return false;
    }
  }
  m_drawLines = true;
  m_useTexture = false;
  return true;
}

auto Panel::createSolidPointPipeline() -> bool {
  if (!m_solidPointPipeline) {
    m_solidPointPipeline = std::make_unique<SolidPointPipeline>(m_graphics);
    if (m_solidPointPipeline->hasError()) {
      m_errorMessage = m_solidPointPipeline->getErrorMessage();
      m_solidPointPipeline.reset(nullptr);
      return false;
    }
  }
  m_showPoints = true;
  return true;
}

void Panel::fillMeshPipelineBuffers() {
  // Activate the new GltfModel
  m_gltfModel = std::move(m_gltfModelNext);
  m_meshUsesMaterial = m_gltfModel->hasMaterials();
  m_meshUsesTexture = m_gltfModel->hasTextures();
  m_meshUsesSkin = m_gltfModel->hasSkins();
  if (m_meshUsesSkin) {
    assert(m_meshTexSkinPipeline);
    m_meshTexSkinPipeline->fillBuffers(m_gltfModel);
  } else if (m_meshUsesTexture) {
    assert(m_meshTexturePipeline);
    m_meshTexturePipeline->fillBuffers(m_gltfModel);
  } else if (m_meshUsesMaterial) {
    assert(m_meshMaterialPipeline);
    m_meshMaterialPipeline->fillBuffers(m_gltfModel);
  } else {
    assert(m_meshPipeline);
    m_meshPipeline->fillBuffers(m_gltfModel);
  }
}

auto Panel::fillSolidPipelineBuffers(bool changedObject, bool drawLines, bool showPoints, bool useTexture, MorphType morphType) -> bool {
  const auto oldMorphType = getMorphType();
  if (morphType != MorphType::NONE) {
    if (m_solid) {
      if (!createMorphPipeline(morphType))
        return false;
      updateMorphPipeline(morphType);
    } else if (morphType != oldMorphType)
      setMorphType(morphType);
  } else {
    bool changed = setShowPoints(showPoints);
    if ((changed || changedObject) && showPoints && m_solid) {
      if (!createSolidPointPipeline())
        return false;
      m_solidPointPipeline->fillBuffers(m_solid);
    }
    if (useTexture) {
      if (m_solid) {
        // Cannot call updateTextureResource when m_solidFillTexturePipeline is in use!
        if (!m_solidFillTexturePipeline || !m_useTexture)
          updateTextureResource();

        fillSolidFillTexturePipelineBuffer();
      } else
        m_useTexture = useTexture;
    } else if (drawLines) {
      if (!createSolidLinePipeline())
        return false;
      if (m_solid && m_solid->getNumberOfVertices() > 0)
        m_solidLinePipeline->fillBuffers(m_solid);
    } else {
      if (!createSolidFillPipeline()) {
        return false;
      }
      if (m_solid)
        fillSolidFillPipelineBuffer();
    }
  }
  return true;
}

auto Panel::freeTextures() -> void {
  if (m_textureResourceOld)
    m_textureResourceOld.reset(nullptr);
  if (m_type == PanelType::NONE)
    m_textureResource.reset(nullptr);
}

void Panel::getJuliaConfig(boost::optional<pt::ptree&> root) {
  ComputeSetup setup;
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_XOFFSET);
    setup.xOffset = x.value_or(0.0f);
  }
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_YOFFSET);
    setup.yOffset = x.value_or(0.0f);
  }
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_ZOOM);
    setup.zoom = x.value_or(2.34f);
    if (setup.zoom < 0.0001f)
      setup.zoom = 2.34f;
  }
  setup.colorA = Config::readColor(root.get(), NODE_FRACTAL_COLOR_A);
  setup.colorB = Config::readColor(root.get(), NODE_FRACTAL_COLOR_B);
  setup.fractalPower = Config::readFractalPower(root.get());
  setup.juliaSeed1 = Config::readJuliaSeed1(root.get());
  setup.juliaSeed2 = Config::readJuliaSeed2(root.get());
  m_juliaSetup = setup;
}

void Panel::getMandelbrotConfig(boost::optional<pt::ptree&> root) {
  ComputeSetup setup;
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_XOFFSET);
    setup.xOffset = x.value_or(0.0f);
  }
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_YOFFSET);
    setup.yOffset = x.value_or(0.0f);
  }
  {
    auto x = root->get_optional<float>(NODE_FRACTAL_ZOOM);
    setup.zoom = x.value_or(2.34f);
    if (setup.zoom < 0.0001f)
      setup.zoom = 2.34f;
  }
  setup.colorA = Config::readColor(root.get(), NODE_FRACTAL_COLOR_A);
  setup.colorB = Config::readColor(root.get(), NODE_FRACTAL_COLOR_B);
  setup.fractalPower = Config::readFractalPower(root.get());
  m_mandelbrotSetup = setup;
}

bool Panel::getConfig(pt::ptree parent, const char *tag, bool isDefault) {
  boost::optional<pt::ptree&> panel_root = parent.get_child_optional(tag);
  if (!panel_root) {
    setEye();
    setRotationAxis();
    if (isDefault) {
      m_nextType = PanelType::SOLID;
      m_nextObject = ObjectType::TETRAHEDRON;
    }
    return false;
  }
  {
    const auto x = panel_root->get_optional<float>(NODE_ALPHA);
    m_colorAlpha = x.value_or(0.7f);
    if (m_colorAlpha < 0.0f)
      m_colorAlpha = 0.0f;
    else if (m_colorAlpha > 1.0f)
      m_colorAlpha = 1.0f;
  }
  {
    const auto x = panel_root->get_optional<float>(NODE_AMBIENT);
    m_colorAmbient = x.value_or(0.2f);
    if (m_colorAmbient < 0.0f)
      m_colorAmbient = 0.0f;
    else if (m_colorAmbient > 1.0f)
      m_colorAmbient = 1.0f;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_EYE_AZIMUTH);
    m_eyeAzimuth = x.value_or(0.0);
    if (m_eyeAzimuth < -M_PI)
      m_eyeAzimuth = -M_PI;
    else if (m_eyeAzimuth > M_PI)
      m_eyeAzimuth = M_PI;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_EYE_DISTANCE);
    m_eyeDistance = x.value_or(5.0);
    if (m_eyeDistance < 0.0)
      m_eyeDistance = 0.0;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_EYE_INCLINATION);
    m_eyeInclination = x.value_or(1.0);
    if (m_eyeInclination < 0.0)
      m_eyeInclination = 0.0;
    else if (m_eyeInclination > M_PI)
      m_eyeInclination = M_PI;
  }
  setEye();
  {
    const auto x = panel_root->get_optional<double>(NODE_LIGHT_AZIMUTH);
    m_lightAzimuth = x.value_or(-1.5);
    if (m_lightAzimuth < -M_PI)
      m_lightAzimuth = -M_PI;
    else if (m_lightAzimuth > M_PI)
      m_lightAzimuth = M_PI;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_LIGHT_INCLINATION);
    m_lightInclination = x.value_or(0.0);
    if (m_lightInclination < 0.0)
      m_lightInclination = 0.0;
    else if (m_lightInclination > M_PI)
      m_lightInclination = M_PI;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_ROTATION_AZIMUTH);
    m_rotationAxisAzimuth = x.value_or(0.0);
    if (m_rotationAxisAzimuth < -M_PIf64)
      m_rotationAxisAzimuth = -M_PIf64;
    else if (m_rotationAxisAzimuth > M_PIf64)
      m_rotationAxisAzimuth = M_PIf64;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_ROTATION_INCLINATION);
    m_rotationAxisInclination = x.value_or(0.0);
    if (m_rotationAxisInclination < 0)
      m_rotationAxisInclination = 0.0;
    else if (m_rotationAxisInclination > M_PIf64)
      m_rotationAxisInclination = M_PIf64;
  }
  setRotationAxis();
  {
    auto x = panel_root->get_optional<bool>(NODE_PANEL_ENABLE_ROTATION);
    m_enableRotation = x.value_or(false);
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_SPEED_MORPH);
    m_morphSpeed = x.value_or(1.0);
    if (m_morphSpeed < 0.0)
      m_morphSpeed = 0.0;
    else if (m_morphSpeed > 1.0)
      m_morphSpeed = 1.0;
  }
  {
    const auto x = panel_root->get_optional<double>(NODE_SPEED_ROTATION);
    m_rotationSpeed = x.value_or(1.0);
    if (m_rotationSpeed < 0.0)
      m_rotationSpeed = 0.0;
    else if (m_rotationSpeed > 1.0)
      m_rotationSpeed = 1.0;
  }
  { // Load mesh file
    bool fileExists = false;
    boost::optional<std::string> fileName = panel_root->get_optional<std::string>(NODE_MESH_FILE);
    m_meshFile = fileName.value_or("");
    if (!m_meshFile.empty()) {
      struct stat buffer;
      fileExists = (stat (m_meshFile.c_str(), &buffer) == 0);
      if (!fileExists)
        m_meshFile.clear();
    }
  }
  { // Load texture file
    bool fileExists = false;
    boost::optional<std::string> fileName = panel_root->get_optional<std::string>(NODE_TEXTURE_FILE);
    m_textureFile = fileName.value_or("");
    if (!m_textureFile.empty()) {
      struct stat buffer;
      fileExists = (stat (m_textureFile.c_str(), &buffer) == 0);
      if (!fileExists)
        m_textureFile.clear();
    }
  }
  {
    auto x = panel_root->get_optional<bool>(NODE_SOLID_DRAW_LINES);
    m_drawLines = x.value_or(false);
  }
  {
    auto x = panel_root->get_optional<bool>(NODE_SOLID_SHOW_POINTS);
    m_showPoints = x.value_or(false);
  }
  {
    auto x = panel_root->get_optional<bool>(NODE_SOLID_USE_TEXTURE);
    m_useTexture = x.value_or(false);
  }
  {
    m_nextType = (isDefault ? PanelType::SOLID : PanelType::NONE);
    boost::optional<std::string> keyType = panel_root->get_optional<std::string>(NODE_PANEL_TYPE);
    for (const auto& typePair : panelTypes) {
      if (typePair.second == keyType) {
        m_nextType = typePair.first;
        if (isDefault && m_nextType == PanelType::NONE)
          m_nextType = PanelType::SOLID;
        break;
      }
    }
  }
  if (m_nextType != PanelType::NONE){
    if (m_nextType == PanelType::SOLID) {
      m_nextObject = ObjectType::TETRAHEDRON;
      boost::optional<std::string> keyObject = panel_root->get_optional<std::string>(NODE_SOLID_OBJECT_TYPE);
      for (const auto& objectPair : polyhedronTypes) {
        if (objectPair.second.key == keyObject) {
          m_nextObject = objectPair.first;
          break;
        }
      }
    } else
      m_nextObject = ObjectType::NONE;
  }
  // Solid - Dual compound transformation
  {
    auto x1 = panel_root->get_optional<bool>(NODE_SOLID_DUAL_COMPOUND);
    bool isDualCompound = x1.value_or(false);
    m_transform = isDualCompound ? PolyhedronTransformation::DUAL_COMPOUND : PolyhedronTransformation::NONE;
  }
  {
    boost::optional<std::string> keyMorph = panel_root->get_optional<std::string>(NODE_SOLID_MORPH_TYPE);
    m_morph = MorphType::NONE;
    for (const auto &morphPair : morphTypes) {
      if (morphPair.second == keyMorph) {
        m_morph = morphPair.first;
        break;
      }
    }
  }
  // Get the fractal settings
  m_fractalType = boost::optional<FractalType>();
  {
    boost::optional<std::string> keyType = panel_root->get_optional<std::string>(NODE_FRACTAL_TYPE);
    if (keyType.has_value()) {
      for (const auto &typePair : fractalTypes) {
        if (typePair.second == keyType) {
          m_fractalType = typePair.first;
          break;
        }
      }
    }
  }
  boost::optional<pt::ptree&> julia_root = panel_root->get_child_optional(NODE_JULIA);
  if (julia_root.has_value()) {
    getJuliaConfig(julia_root);
  }
  boost::optional<pt::ptree&> mandelbrot_root = panel_root->get_child_optional(NODE_MANDELBROT);
  if (mandelbrot_root.has_value()) {
    getMandelbrotConfig(mandelbrot_root);
  }
  return true;
}

void Panel::inwardEye() {
  m_eyeDistance *= 0.9;
  m_viewChanged = true;
  setEye();
}

void Panel::outwardEye() {
  m_eyeDistance *= 1.1;
  m_viewChanged = true;
  setEye();
}

auto Panel::prepareCompute() -> bool {
  ComputeSetup setup;
  if (m_fractalType == FractalType::JULIA)
    setup = m_juliaSetup.get();
  else
    setup = m_mandelbrotSetup.get();
  return prepareCompute(setup);
}

auto Panel::prepareCompute(const ComputeSetup& setup) -> bool {
  assert(m_nextType != PanelType::NONE);
  m_computeResource.reset(nullptr);
  if (m_nextType == PanelType::SOLID)
    m_computeResource = std::make_unique<ComputeResource>(m_graphics->getDevice(), 800, 600, m_fractalType.get(), setup);
  else if (m_nextType == PanelType::IMAGE_COMP || m_nextType == PanelType::IMAGE_FILE) {
    m_computeResource = std::make_unique<ComputeResource>(m_graphics->getDevice(), m_viewport.width, m_viewport.height, m_fractalType.get(), setup);
  }
  if (!m_computeResource->init()) {
    m_errorMessage = m_computeResource->getErrorMessage();
    m_computeResource.reset(nullptr);
    return false;
  }
  return true;
}

auto Panel::prepareCompute(FractalType newFractalType, const ComputeSetup& aSetup) -> bool {
  if (m_computeResource) {
    ComputeSetup oldSetup;
    auto oldFractalType = m_computeResource->getFractalType();
    oldSetup.colorA = m_computeResource->getColorA();
    oldSetup.colorB = m_computeResource->getColorB();
    oldSetup.juliaSeed1 = m_computeResource->getJuliaSeed1();
    oldSetup.xOffset = m_computeResource->getXoffset();
    oldSetup.yOffset = m_computeResource->getYoffset();
    oldSetup.zoom = m_computeResource->getZoom();
    if (oldFractalType == FractalType::JULIA)
      m_juliaSetup = oldSetup;
    else
      m_mandelbrotSetup = oldSetup;
  }
  m_fractalType = newFractalType;
  if (newFractalType == FractalType::JULIA && !m_juliaSetup.has_value())
    m_juliaSetup = aSetup;
  if (newFractalType == FractalType::MANDELBROT && !m_mandelbrotSetup.has_value())
    m_mandelbrotSetup = aSetup;
  return prepareCompute();
}

void Panel::prepareDrawingSolid() {
  assert(m_solid);
  // Apply a zoom if solid is larger than screen
  const auto transform = buildObjectMVP();
  glm::vec2 center(0.0f);
  double maxLength = 0.0;
  const auto nVertices = m_solid->getNumberOfVertices();
  for (const auto &vertex3D : m_solid->getVertices()) {
    auto p4 = transform * glm::vec4(vertex3D, 1.0f);
    auto p3 = glm::vec3(p4.x / p4.w, p4.y / p4.w, p4.z / p4.w);
    const glm::dvec2 p2(p3.x, p3.y);
    const auto len2 = glm::length(p2);
    center += p2;
    if (len2 > maxLength)
      maxLength = len2;
  }
  center /= nVertices;
  if (maxLength > M_SQRT2f64)
    setEyeDistance(2.0 * maxLength);
}

auto Panel::putFractalConfig(boost::optional<FractalType> optFractalType, FractalType setupType, const ComputeSetup& setup) const -> pt::ptree {
  pt::ptree root;
  pt::ptree node;
  // Use <m_m_computeResource> if the fractal setup <setupType>  is the same type as the current fractal
  bool useCompute = (optFractalType == setupType);
  {
    auto color = (useCompute ? m_computeResource->getColorA() : setup.colorA);
    Config::writeColor(root, NODE_FRACTAL_COLOR_A, color);
  }
  {
    auto color = (useCompute ? m_computeResource->getColorB() : setup.colorB);
    Config::writeColor(root, NODE_FRACTAL_COLOR_B, color);
  }
  {
    auto power = (useCompute ? m_computeResource->getFractalPower() : setup.fractalPower);
    Config::writeFractalPower(root, power);
  }
  if (setupType == FractalType::JULIA) {
    auto seed1 = (useCompute ? m_computeResource->getJuliaSeed1() : setup.juliaSeed1);
    auto seed2 = (useCompute ? m_computeResource->getJuliaSeed2() : setup.juliaSeed2);
    Config::writeJuliaSeed(root, seed1, seed2);
  }
  {
    auto x = (useCompute ? m_computeResource->getXoffset() : setup.xOffset);
    node.put_value(x);
    root.put_child(NODE_FRACTAL_XOFFSET, node);
  }
  {
    auto y = (useCompute ? m_computeResource->getYoffset() : setup.yOffset);
    node.put_value(y);
    root.put_child(NODE_FRACTAL_YOFFSET, node);
  }
  {
    auto zoom = (useCompute ? m_computeResource->getZoom() : setup.zoom);
    node.put_value(zoom);
    root.put_child(NODE_FRACTAL_ZOOM, node);
  }
  return root;
}

auto Panel::putConfig(pt::ptree &parent, const char *tag) -> void {
  pt::ptree node, panel_root, julia_root, mandelbrot_root;

  node.put_value(m_colorAlpha);
  panel_root.put_child(NODE_ALPHA, node);
  node.put_value(m_colorAmbient);
  panel_root.put_child(NODE_AMBIENT, node);

  node.put_value(m_eyeAzimuth);
  panel_root.put_child(NODE_EYE_AZIMUTH, node);
  node.put_value(m_eyeInclination);
  panel_root.put_child(NODE_EYE_INCLINATION, node);
  node.put_value(m_eyeDistance);
  panel_root.put_child(NODE_EYE_DISTANCE, node);

  node.put_value(m_lightAzimuth);
  panel_root.put_child(NODE_LIGHT_AZIMUTH, node);
  node.put_value(m_lightInclination);
  panel_root.put_child(NODE_LIGHT_INCLINATION, node);

  node.put_value(m_morphSpeed);
  panel_root.put_child(NODE_SPEED_MORPH, node);
  node.put_value(m_rotationSpeed);
  panel_root.put_child(NODE_SPEED_ROTATION, node);
  node.put_value(m_rotationAxisAzimuth);
  panel_root.put_child(NODE_ROTATION_AZIMUTH, node);
  node.put_value(m_rotationAxisInclination);
  panel_root.put_child(NODE_ROTATION_INCLINATION, node);
  // Mesh file
  if (!m_meshFile.empty()) {
    node.put_value(m_meshFile);
    panel_root.put_child(NODE_MESH_FILE, node);
  }
  // Texture file
  if (!m_textureFile.empty()) {
    node.put_value(m_textureFile);
    panel_root.put_child(NODE_TEXTURE_FILE, node);
  }
  // Draw lines or filled polygons
  node.put_value(m_drawLines);
  panel_root.put_child(NODE_SOLID_DRAW_LINES, node);
  // Show points at vertices
  node.put_value(m_showPoints);
  panel_root.put_child(NODE_SOLID_SHOW_POINTS, node);
  // Use texture
  node.put_value(m_useTexture);
  panel_root.put_child(NODE_SOLID_USE_TEXTURE, node);
  // Panel type
  const auto itPanelType = panelTypes.find(m_type);
  if (itPanelType != std::cend(panelTypes)) {
    node.put_value(itPanelType->second);
    panel_root.put_child(NODE_PANEL_TYPE, node);
  }
  // Enable rotation
  node.put_value(m_enableRotation);
  panel_root.put_child(NODE_PANEL_ENABLE_ROTATION, node);
  // Solid
  if (m_solid) {
    {
      const auto itObject = polyhedronTypes.find(m_solid->getType());
      if (itObject != std::cend(polyhedronTypes)) {
        node.put_value(itObject->second.key);
        panel_root.put_child(NODE_SOLID_OBJECT_TYPE, node);
      }
    }
    {
      node.put_value(m_solid->getIsDualCompound());
      panel_root.put_child(NODE_SOLID_DUAL_COMPOUND, node);
    }
    {
      const auto itMorph = morphTypes.find(m_morph);
      if (itMorph != std::cend(morphTypes)) {
        node.put_value(itMorph->second);
        panel_root.put_child(NODE_SOLID_MORPH_TYPE, node);
      }
    }
  }
  boost::optional<FractalType> optFractalType;
  if (m_computeResource) {
    // Fractal type
    optFractalType = m_computeResource->getFractalType();
    const auto itFractalType = fractalTypes.find(optFractalType.get());
    if (itFractalType != std::cend(fractalTypes)) {
      node.put_value(itFractalType->second);
      panel_root.put_child(NODE_FRACTAL_TYPE, node);
    }
  }
  if (optFractalType == FractalType::JULIA || m_juliaSetup.has_value()) {
    julia_root = putFractalConfig(optFractalType, FractalType::JULIA, m_juliaSetup.get());
    panel_root.put_child(NODE_JULIA, julia_root);
  }
  if (optFractalType == FractalType::MANDELBROT || m_mandelbrotSetup.has_value()) {
    mandelbrot_root = putFractalConfig(optFractalType, FractalType::MANDELBROT, m_mandelbrotSetup.get());
    panel_root.put_child(NODE_MANDELBROT, mandelbrot_root);
  }
  parent.put_child(tag, panel_root);
}

void Panel::releasePipelines() {
  if (m_solidLinePipeline)
    delete m_solidLinePipeline.release();
  if (m_solidFillTexturePipeline)
    delete m_solidFillTexturePipeline.release();
  if (m_solidFillPipeline)
    delete m_solidFillPipeline.release();
  if (m_solidPointPipeline)
    delete m_solidPointPipeline.release();
  if (m_axisPipeline)
    delete m_axisPipeline.release();
  if (m_morphPipeline)
    delete m_morphPipeline.release();
  if (m_imagePipeline)
    delete m_imagePipeline.release();
  if (m_meshMaterialPipeline)
    delete m_meshMaterialPipeline.release();
  if (m_meshPipeline)
    delete m_meshPipeline.release();
  if (m_meshTexSkinPipeline)
    delete m_meshTexSkinPipeline.release();
  if (m_meshTexturePipeline)
    delete m_meshTexturePipeline.release();
  if (m_textureResource)
    delete m_textureResource.release();
}
// Release the texture resource and compute a new image
auto Panel::resetTextureResource() -> bool {
  if (m_textureResource)
    m_textureResource.reset(nullptr);
  if (m_type == PanelType::IMAGE_COMP) {
    if (!m_computeResource) {
      ComputeSetup setup;
      if (m_fractalType == FractalType::JULIA)
        setup = m_juliaSetup.get();
      else
        setup = m_mandelbrotSetup.get();
      if (!prepareCompute(setup))
        return false;
      if (!computeImage())
        return false;
    } else if (!m_computeResource->changeSize(m_graphics->m_graphicsQueue2, m_viewport.width, m_viewport.height, m_duration)) {
      return false;
    }
  }
  return true;
}

void Panel::resetSolid() {
  auto pSolid = TransformSolid::transform(m_solid, m_transform, actionCount);
  if (pSolid) {
    resetSolid(pSolid);
  }
}

void Panel::resetTime() {
  m_angleOffset += m_duration * M_PI_2 * m_rotationSpeed;
  m_morphOffset += m_duration * M_PI_2 * m_morphSpeed;
  m_startTime = std::chrono::high_resolution_clock::now();
  m_duration = 0;
}

// Call this method after m_colorAlpha changes
void Panel::setColorAlpha() {
  // Solids
  if (m_solidFillPipeline)
    m_solidFillPipeline->setColorAlpha(m_colorAlpha);
  if (m_solidFillTexturePipeline)
    m_solidFillTexturePipeline->setColorAlpha(m_colorAlpha);
  if (m_morphPipeline)
    m_morphPipeline->setColorAlpha(m_colorAlpha);
  if (m_solidFillTexturePipeline)
    m_solidFillTexturePipeline->setColorAlpha(m_colorAlpha);
  // Mesh
  if (m_meshPipeline)
    m_meshPipeline->setColorAlpha(m_colorAlpha);
  if (m_meshTexturePipeline)
    m_meshTexturePipeline->setColorAlpha(m_colorAlpha);
  if (m_meshTexSkinPipeline)
    m_meshTexSkinPipeline->setColorAlpha(m_colorAlpha);
}

void Panel::setColorAmbient() {
  // Solids
  if (m_solidFillPipeline)
    m_solidFillPipeline->setColorAmbient(m_colorAmbient);
  if (m_solidFillTexturePipeline)
    m_solidFillTexturePipeline->setColorAmbient(m_colorAmbient);
  if (m_morphPipeline)
    m_morphPipeline->setColorAmbient(m_colorAmbient);
  // Mesh
  if (m_meshPipeline)
    m_meshPipeline->setColorAmbient(m_colorAmbient);
  if (m_meshTexturePipeline)
    m_meshTexturePipeline->setColorAmbient(m_colorAmbient);
  if (m_meshTexSkinPipeline)
    m_meshTexSkinPipeline->setColorAmbient(m_colorAmbient);
}

void Panel::setDuration() {
  auto currentTime = std::chrono::high_resolution_clock::now();
  m_duration = std::chrono::duration<double, std::chrono::seconds::period>(currentTime - m_startTime).count();
}

void Panel::setEye() {
  // Based on 'Y-axis is UP'
  m_eye[0] = sin(m_eyeInclination) * cos(m_eyeAzimuth) * m_eyeDistance;
  m_eye[1] = cos(m_eyeInclination) * m_eyeDistance;
  m_eye[2] = sin(m_eyeInclination) * sin(m_eyeAzimuth) * m_eyeDistance;
  m_viewChanged = true;
}

void Panel::setMetallic() {
  if (m_solidFillPipeline)
    m_solidFillPipeline->setMetallic(m_metallic);
  if (m_solidFillTexturePipeline)
    m_solidFillTexturePipeline->setMetallic(m_metallic);
}
void Panel::setMorphFraction(const glm::vec3& lightDirection, double cosine) {
  m_morphPipeline->setModel(getModel());
  m_morphPipeline->setProjectionView(buildObjectVP());
  m_morphPipeline->setLightDirection(lightDirection);
  if (m_morphPipeline->getMorphType() != MorphType::EXPAND) {
    // Oscillate between 0 and 1
    m_morphPipeline->setFraction(0.5 - 0.5 * cosine);
  } else {
    // Oscillate between 0 and 2
    if (m_morphPipeline->setFraction(1.0 - 1.0 * cosine)) {
      m_graphics->m_swapChainResource->redrawNeeded();
    }
  }
}

void Panel::setRotationAxis() {
  // Use Y-axis is UP
  m_rotationAxis[0] = sin(m_rotationAxisInclination) * cos(m_rotationAxisAzimuth);
  m_rotationAxis[1] = cos(m_rotationAxisInclination);
  m_rotationAxis[2] = sin(m_rotationAxisInclination) * sin(m_rotationAxisAzimuth);
}

void Panel::setRotationAxisAzimuth(double aValue, bool updateAxis) {
  m_rotationAxisAzimuth = aValue;
  if (updateAxis)
    setRotationAxis();
}
void Panel::setRotationAxisInclination(double aValue, bool updateAxis) {
  m_rotationAxisInclination = aValue;
  if (updateAxis)
    setRotationAxis();
}

auto Panel::setTypeImage() -> bool {
  if (m_type == PanelType::SOLID) {
    assert(!m_textureFile.empty());
    m_type = PanelType::IMAGE_FILE;
  } else if (m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE) {
    m_type = PanelType::SOLID;
    if (!m_solid)
      m_nextObject = ObjectType::TETRAHEDRON;
  }
  return createPipelines();
}

auto Panel::showComputeSetup() -> bool {
  static glm::vec3 colorA, colorB;
  static bool seedRed = false, enableSeedOscillation = false;
  static glm::vec2 seed1, seed2;

  assert(m_computeResource);
  if (!ImGui::CollapsingHeader("Compute setup"))
    return false;

  if (sliderFractalPower())
    return true;
  bool updated = false;
  if (ImGui::Button("color A")) {
    ImGui::OpenPopup("colorA");
    colorA = m_computeResource->getColorA();
  }
  if (ImGui::BeginPopup("colorA", ImGuiWindowFlags_AlwaysAutoResize)) {
    ImGui::ColorPicker3("##picker", reinterpret_cast<float *>(&colorA), ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
    if (ImGui::Button("Update", ImVec2(80, 0))) {
      m_computeResource->setColorA(colorA);
      updated = true;
    }
    ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
    if (ImGui::Button("Cancel", ImVec2(80, 0))) {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
  ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
  if (ImGui::Button("color B")) {
    ImGui::OpenPopup("colorB");
    colorB = m_computeResource->getColorB();
  }
  if (ImGui::BeginPopup("colorB", ImGuiWindowFlags_AlwaysAutoResize)) {
    ImGui::ColorPicker3("##picker", reinterpret_cast<float *>(&colorB), ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_NoSmallPreview);
    if (ImGui::Button("Update", ImVec2(80, 0))) {
      m_computeResource->setColorB(colorB);
      // Update the compute image
      updated = true;
    }
    ImGui::SameLine(0, ImGui::GetStyle().ItemInnerSpacing.x);
    if (ImGui::Button("Cancel", ImVec2(80, 0))) {
      ImGui::CloseCurrentPopup();
    }
    ImGui::EndPopup();
  }
  if (!seedRed) {
    seed1 = m_computeResource->getJuliaSeed1();
    seed2 = m_computeResource->getJuliaSeed2();
    enableSeedOscillation = m_computeResource->getSeedOscillation();
    seedRed = true;
  }
  if (!ImGui::DragFloat2("Seed 1", reinterpret_cast<float *>(&seed1), 0.01f, -5.0f, 5.0f)) {
    const auto seedOld = m_computeResource->getJuliaSeed1();
    if (seed1.x != seedOld.x || seed1.y != seedOld.y) {
      m_computeResource->setJuliaSeed1(seed1);
      updated = true;
    }
  }
  if (ImGui::Checkbox("Enable oscillation", &enableSeedOscillation)) {
    if (m_computeResource->getSeedOscillation() != enableSeedOscillation) {
      m_computeResource->setSeedOscillation(enableSeedOscillation);
      if (enableSeedOscillation)
        resetTime();
    }
  }
  if (enableSeedOscillation && !ImGui::DragFloat2("Seed 2", reinterpret_cast<float *>(&seed2), 0.01f, -5.0f, 5.0f)) {
    const auto seedOld = m_computeResource->getJuliaSeed2();
    if (seed2.x != seedOld.x || seed2.y != seedOld.y) {
      m_computeResource->setJuliaSeed2(seed2);
      updated = true;
    }
  }
  if (ImGui::Button("Julia seeds")) {
    ImGui::OpenPopup("Seeds");
  }
  if (ImGui::BeginPopup("Seeds", ImGuiWindowFlags_AlwaysAutoResize)) {
    if (ImGui::Button("Example 1", ImVec2(80, 0))) {
      seed1.x = -0.2f;
      seed1.y = 0.68f;
      m_computeResource->setJuliaSeed1(seed1);
      seed2 = seed1;
      m_computeResource->setJuliaSeed2(seed2);
      updated = true;
    }
    if (ImGui::Button("Example 2", ImVec2(80, 0))) {
      seed1.x = 0.255f;
      seed1.y = 0.0f;
      m_computeResource->setJuliaSeed1(seed1);
      seed2 = seed1;
      m_computeResource->setJuliaSeed2(seed2);
      updated = true;
    }
    ImGui::EndPopup();
  }
  return updated;
}

void Panel::showControls() {
  if (!ImGui::CollapsingHeader("Controls", ImGuiTreeNodeFlags_None))
    return;
  if (m_type == PanelType::SOLID || m_type == PanelType::MESH) {
    // Color alpha
    sliderColorAlpha();
    // Ambient light
    sliderColorAmbient();
    // Metallic
    sliderMetallic();
  }
  if (m_type == PanelType::SOLID) {
    // Morph speed
    sliderMorphSpeed();
  }
  sliderLightDirection();
  ImGui::Checkbox("Enable rotation", &m_enableRotation);
  if (m_enableRotation && ImGui::CollapsingHeader("Rotation", true)) {
    sliderRotation();
    auto rotate = m_rotate;
    if (ImGui::MenuItem("Rotate", "", rotate)) {
      toggleRotate();
    }
  }
}

void Panel::showError() {
  if (ImGui::IsPopupOpen(pErrorName) && ImGui::BeginPopup(pErrorName)) {
    ImGui::Text("ERROR: %s", m_errorMessage.c_str());
    ImGui::EndPopup();
  }
}
// Show info of the solid or fractal
void Panel::showInfo() {
  if (m_type == PanelType::SOLID) {
    assert(m_solid);
    if (ImGui::IsPopupOpen(pPanelInfoName) && ImGui::BeginPopup(pPanelInfoName)) {
      ImGui::Text("Number of vertices  : %5ld", m_solid->getNumberOfVertices());
      ImGui::Text("Number of faces     : %5ld", m_solid->getNumberOfFaces());
      ImGui::Text("Number of edges     : %5ld", m_solid->getNumberOfEdges());
      if (Polyhedron::getDualType(m_solid->getType()) != ObjectType::NONE && m_solid->m_isConvex)
        ImGui::Text("Dual                : %s", m_solid->getNameOfDual().c_str());
      const auto mapCorners = m_solid->getNumberOfFacesByCorners();
      for (const auto& pair : mapCorners) {
        ImGui::Text("Faces with %2ld edges : %5ld", pair.first, pair.second);
      }
      ImGui::EndPopup();
    }
  } else if (m_type == PanelType::IMAGE_COMP) {
    assert(m_computeResource);
    if (ImGui::IsPopupOpen(pPanelInfoName) && ImGui::BeginPopup(pPanelInfoName)) {
      ImGui::Text("X offset : %.5f", m_computeResource->getXoffset());
      ImGui::Text("Y offset : %.5f", m_computeResource->getYoffset());
      ImGui::Text("Zoom     : %.5f", m_computeResource->getZoom());
      auto colorA = m_computeResource->getColorA();
      ImGui::Text("Color A  : %.5f %.5f %.5f", colorA[0], colorA[1], colorA[2]);
      auto colorB = m_computeResource->getColorB();
      ImGui::Text("Color B  : %.5f %.5f %.5f", colorB[0], colorB[1], colorB[2]);
      auto fractalType = m_computeResource->getFractalType();
      if (fractalType == FractalType::JULIA) {
        auto seed = m_computeResource->getJuliaSeed1();
        ImGui::Text("Seed     : %.5f %.5f", seed[0], seed[1]);
      }
      ImGui::EndPopup();
    }
  } else if (m_type == PanelType::MESH) {
    if (ImGui::IsPopupOpen(pPanelInfoName) && ImGui::BeginPopup(pPanelInfoName)) {
      m_gltfModel->showInfo();
      ImGui::EndPopup();
    }
  }
}

void Panel::showSolidActions(PanelSettings& setting) {
  if (m_type == PanelType::SOLID && m_solid && ImGui::CollapsingHeader("Action", true)) {
    assert(m_solid);
    if (m_morph == MorphType::NONE && m_solid->m_isConvex) {
      const auto solidType = m_solid->getType();
      ImGui::Separator();
      if (ImGui::Button("Truncate")) {
        setting.transform = PolyhedronTransformation::TRUNCATE;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
      ImGui::Separator();
      if (ImGui::Button("Expand")) {
        setting.transform = PolyhedronTransformation::EXPAND;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
      ImGui::Separator();
      if (ImGui::Button("Dual")) {
        setting.transform = PolyhedronTransformation::DUAL;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
      ImGui::Separator();
      if (ImGui::Button("Regular")) {
        setting.transform = PolyhedronTransformation::REGULAR;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
      ImGui::Separator();
      if (ImGui::Button("Dual compound")) {
        setting.transform = PolyhedronTransformation::DUAL_COMPOUND;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
      if ((solidType == ObjectType::CUBOCTAHEDRON || solidType == ObjectType::DODECAHEDRON || solidType == ObjectType::ICOSAHEDRON || solidType == ObjectType::ICOSIDODECAHEDRON || solidType == ObjectType::OCTAHEDRON || solidType == ObjectType::RHOMBIC_DODECAHEDRON  || solidType == ObjectType::TETRAKIS_HEXAHEDRON || solidType == ObjectType::TRIAKIS_ICOSAHEDRON || solidType == ObjectType::TRIAKIS_OCTAHEDRON || solidType == ObjectType::TRIAKIS_TETRAHEDRON) && ImGui::Button("First stellation")) {
        setting.transform = PolyhedronTransformation::STELLATION;
        setting.panel = this;
        setting.actionCount = 1;
        ImGui::CloseCurrentPopup();
      }
      if ((solidType == ObjectType::CUBOCTAHEDRON || solidType == ObjectType::DODECAHEDRON || solidType == ObjectType::ICOSAHEDRON || solidType == ObjectType::ICOSIDODECAHEDRON || solidType == ObjectType::RHOMBIC_DODECAHEDRON  || solidType == ObjectType::TETRAKIS_HEXAHEDRON || solidType == ObjectType::TRIAKIS_OCTAHEDRON || solidType == ObjectType::TRIAKIS_TETRAHEDRON) && ImGui::Button("Second stellation")) {
        setting.transform = PolyhedronTransformation::STELLATION;
        setting.panel = this;
        setting.actionCount = 2;
        ImGui::CloseCurrentPopup();
      }
      // Third stellation of ObjectType::RHOMBIC_DODECAHEDRON is not OK
      if ((solidType == ObjectType::DODECAHEDRON  || solidType == ObjectType::ICOSAHEDRON  || solidType == ObjectType::RHOMBIC_DODECAHEDRON) && ImGui::Button("Third stellation")) {
        setting.transform = PolyhedronTransformation::STELLATION;
        setting.panel = this;
        setting.actionCount = 3;
        ImGui::CloseCurrentPopup();
      }
    }
    if (m_morph == MorphType::NONE) {
      ImGui::Separator();
      if (ImGui::Button("Zono")) {
        setting.transform = PolyhedronTransformation::ZONO;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
    }
    if (m_solid->m_isConvex) {
      ImGui::Separator();
      if (ImGui::Button("Morph dual")) {
        setting.morphType = (m_morph == MorphType::NONE ? MorphType::DUAL : MorphType::NONE);
        setting.panel = this;
        setting.changedMorph = true;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Morph edge")) {
        setting.morphType = (m_morph == MorphType::NONE ? MorphType::EDGE : MorphType::NONE);
        setting.panel = this;
        setting.changedMorph = true;
        ImGui::CloseCurrentPopup();
      }
      if (ImGui::Button("Morph expand dual")) {
        setting.morphType = (m_morph == MorphType::NONE ? MorphType::EXPAND : MorphType::NONE);
        setting.panel = this;
        setting.changedMorph = true;
        ImGui::CloseCurrentPopup();
      }
      ImGui::Separator();
      if (ImGui::Button("Morph regular")) {
        setting.morphType = (m_morph == MorphType::NONE ? MorphType::REGULAR : MorphType::NONE);
        setting.panel = this;
        setting.changedMorph = true;
        ImGui::CloseCurrentPopup();
      }
    }
    if (m_morph == MorphType::NONE && m_solid->m_isPlatonic) {
      ImGui::Separator();
      if (ImGui::Button("Platonic 2-compound")) {
        setting.transform = PolyhedronTransformation::PLATONIC_2_COMPOUND;
        setting.panel = this;
        ImGui::CloseCurrentPopup();
      }
    }
  }
}

auto Panel::sliderAngle(const char* label, double* angleRadian, double v_degrees_min, double v_degrees_max) -> bool {
  auto angleDegree = (*angleRadian) * 360.0 / (2 * M_PI);
  bool value_changed = ImGui::SliderScalar(label, ImGuiDataType_Double, &angleDegree, &v_degrees_min, &v_degrees_max, "%.0f deg", 1.0f);
  *angleRadian = angleDegree * (2 * M_PI) / 360.0;
  return value_changed;
}

void Panel::sliderColorAlpha() {
  static float alphaColorMin = 0.0f;
  static float alphaColorMax = 1.0f;
  if (ImGui::SliderScalar("Alpha", ImGuiDataType_Float, &m_colorAlpha, &alphaColorMin, &alphaColorMax, "%.3f")) {
    setColorAlpha();
  }
}

auto Panel::sliderColorAmbient() -> void {
  static float ambientLightMin = 0.0f;
  static float ambientLightMax = 1.0f;
  if (ImGui::SliderScalar("Ambient", ImGuiDataType_Float, &m_colorAmbient, &ambientLightMin, &ambientLightMax, "%.3f")) {
    setColorAmbient();
  }
}

auto Panel::sliderFractalPower() -> bool {
  static uint32_t powerMin = 2;
  static uint32_t powerMax = 5;
  auto power = m_computeResource->getFractalPower();
  if (ImGui::SliderScalar("Julia power", ImGuiDataType_U32, &power, &powerMin, &powerMax, "%d")) {
    m_computeResource->setJuliaPower(power);
    return true;
  } else
    return false;
}

void Panel::sliderLightDirection() {
  if (ImGui::CollapsingHeader("Light direction", true)) {
    // Inclination: Zero means light goes up
    if (sliderAngle("Inclination", &m_lightInclination, 0.0, 180.0)) {
      updateUniformBuffer(true);
    }
    if (sliderAngle("Azimuth", &m_lightAzimuth, -180.0, 180.0)) {
      updateUniformBuffer(true);
    }
  }
}

auto Panel::sliderMetallic() -> void {
  static float metallicMin = 0.0f;
  static float metallicMax = 1.0f;
  if (ImGui::SliderScalar("Metallic", ImGuiDataType_Float, &m_metallic, &metallicMin, &metallicMax, "%.3f")) {
    setMetallic();
  }
}

void Panel::sliderMorphSpeed() {
  static double morphSpeedMin = 0.0;
  static double morphSpeedMax = 1.0;
  auto morphSpeed = m_morphSpeed;
  if (ImGui::SliderScalar("Morph speed", ImGuiDataType_Double, &morphSpeed, &morphSpeedMin, &morphSpeedMax, "%.3f")) {
    resetTime();
    m_morphSpeed = morphSpeed;
  }
}

void Panel::sliderRotation() {
  if (Panel::sliderAngle("Axis inclination", &m_rotationAxisInclination, 0.0, 180.0)) {
    setRotationAxis();
  }
  if (Panel::sliderAngle("Axis azimuth", &m_rotationAxisAzimuth, -180.0, 180.0)) {
    setRotationAxis();
  }
  const double rotationSpeedMin = 0.0;
  const double rotationSpeedMax = 1.0;
  auto rotationSpeed = m_rotationSpeed;
  if (ImGui::SliderScalar("Speed", ImGuiDataType_Double, &rotationSpeed, &rotationSpeedMin, &rotationSpeedMax, "%.3f")) {
    resetTime();
    m_rotationSpeed = rotationSpeed;
  }
}

void Panel::toggleRotate() {
  m_rotate = !m_rotate;
  if (m_rotate) {
    m_startTime = std::chrono::high_resolution_clock::now();
  } else {
    resetTime();
  }
  updateMVP();
}

void Panel::updateColor(const std::vector<glm::vec3>& newColorList) {
  assert(m_solid);
  const auto oldColorList = m_solid->getColorList();
  m_solid->setColorList(newColorList);
  if (m_morphPipeline && m_morph != MorphType::NONE)
    m_morphPipeline->updateColor(oldColorList, newColorList);
  else if (m_solidFillPipeline)
    m_solidFillPipeline->updateColor(oldColorList, newColorList);
}

auto Panel::updateComputeImage() -> bool {
  if (!m_computeResource->changeSize(m_graphics->m_graphicsQueue2, m_viewport.width, m_viewport.height, m_duration)) {
    m_errorMessage = m_computeResource->getErrorMessage();
    m_computeResource.reset(nullptr);
    return false;
  }
  updateTextureResource(new TextureResource(m_graphics, m_computeResource));
  return true;
}

auto Panel::updateMorphPipeline(MorphType aType) -> bool {
  assert(m_morphPipeline);
  m_morphPipeline->setMorphType(aType);
  m_morph = aType;
  assert(m_solid);
  switch (aType) {
  case MorphType::DUAL: {
    std::unique_ptr<MorphDual> m = std::make_unique<MorphDual>(m_solid);
    m_morphPipeline->createIndexedVertices(m.get(), 0);
  }
    break;
  case MorphType::EDGE: {
    std::unique_ptr<MorphEdge> m = std::make_unique<MorphEdge>(m_solid);
    m_morphPipeline->createIndexedVertices(m.get(), 0);
  }
    break;
  case MorphType::EXPAND: {
    std::unique_ptr<MorphExpandDual> m = std::make_unique<MorphExpandDual>(m_solid);
    m_morphPipeline->createIndexedVertices(m.get(), 0);
    m_morphPipeline->createIndexedVertices(m.get(), 1);
  }
    break;
  case MorphType::REGULAR: {
    std::unique_ptr<MorphRegular> m = std::make_unique<MorphRegular>(m_solid);
    m_morphPipeline->createIndexedVertices(m.get(), 0);
  }
    break;
  default:
    m_errorMessage = "Illegal morph option!";
    return false;
  }
  return true;
}

auto Panel::updateMVP() -> void {
  const auto mvp = buildObjectMVP();
  if (m_type == PanelType::MESH) {
    if (m_meshUsesTexture) {
      assert(m_meshTexturePipeline);
      m_meshTexturePipeline->setModel(getModel());
      m_meshTexturePipeline->setProjectionView(buildObjectVP());
    } else if (m_meshUsesMaterial) {
      assert(m_meshMaterialPipeline);
      m_meshMaterialPipeline->setModel(getModel());
      m_meshMaterialPipeline->setProjectionView(buildObjectVP());
    } else {
      assert(m_meshPipeline);
      m_meshPipeline->setModel(getModel());
      m_meshPipeline->setProjectionView(buildObjectVP());
    }
  } else if (m_type == PanelType::SOLID) {
    if (m_drawLines) {
      if (m_solidLinePipeline)
        m_solidLinePipeline->setMVP(mvp);
    } else {
      if (m_solidFillPipeline) {
        m_solidFillPipeline->setModel(getModel());
        m_solidFillPipeline->setProjectionView(buildObjectVP());
      }
    }
    if (m_showPoints) {
      if (m_solidPointPipeline)
        m_solidPointPipeline->setMVP(mvp);
    }
    if (m_morphPipeline) {
      m_morphPipeline->setModel(getModel());
      m_morphPipeline->setProjectionView(buildObjectVP());
    }
  }
}

auto Panel::updateGltfModel() -> void {
  assert(!m_meshFile.empty());
  m_gltfModelNext = std::make_unique<GltfModel>(m_meshFile);
  m_enableRotation = false;
  bool usesMaterial = m_gltfModelNext->hasMaterials();
  bool usesSkin = m_gltfModelNext->hasSkins();
  bool usesTexture = m_gltfModelNext->hasTextures();
  if (usesSkin) {
    if (!m_meshTexSkinPipeline)
      m_meshTexSkinPipeline = std::make_unique<MeshTexSkinPipeline>(m_graphics);
    m_meshTexSkinPipeline->setColorAlpha(m_colorAlpha);
    m_meshTexSkinPipeline->setColorAmbient(m_colorAmbient);
    m_meshTexSkinPipeline->loadMeshes(m_gltfModelNext);
  } else if (usesTexture) {
    if (!m_meshTexturePipeline)
      m_meshTexturePipeline = std::make_unique<MeshTexturePipeline>(m_graphics);
    m_meshTexturePipeline->setColorAlpha(m_colorAlpha);
    m_meshTexturePipeline->setColorAmbient(m_colorAmbient);
    m_meshTexturePipeline->loadMeshes(m_gltfModelNext);
  } else if (usesMaterial) {
    if (!m_meshMaterialPipeline)
      m_meshMaterialPipeline = std::make_unique<MeshMaterialPipeline>(m_graphics);
    m_meshMaterialPipeline->setEye(m_eye);
    m_meshMaterialPipeline->loadMeshes(m_gltfModelNext);
  } else {
    if (!m_meshPipeline)
      m_meshPipeline = std::make_unique<MeshPipeline>(m_graphics);
    m_meshPipeline->setColorAlpha(m_colorAlpha);
    m_meshPipeline->setColorAmbient(m_colorAmbient);
    m_meshPipeline->loadMeshes(m_gltfModelNext);
  }
}

auto Panel::updateSolid(ObjectType aType, const std::vector<glm::vec3>& aColorList) -> void {
  std::unique_ptr<Polyhedron> solid = std::make_unique<Polyhedron>(aColorList, true);
  Polyhedron* pSolid = nullptr;
  switch (aType) {
  case ObjectType::CUBE:
    solid->createCube();
    pSolid = solid.release();
    break;
  case ObjectType::CUBE_2_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createCube2Compound();
  }
    break;
  case ObjectType::CUBE_3_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createCube3Compound();
  }
    break;
  case ObjectType::CUBE_4_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createCube4Compound();
  }
    break;
  case ObjectType::CUBE_5_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createCube5Compound();
  }
    break;
  case ObjectType::CUBOCTAHEDRON:
    solid->createCuboctahedron(0, 1);
    pSolid = solid.release();
    break;
  case ObjectType::CUBOCTAHEDRON_SECOND_STELLATION:
    solid->createCuboctahedron(0, 1);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::CUBOCTAHEDRON_2_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createCuboctahedron2Compound();
  }
    break;
  case ObjectType::DODECAHEDRON:
    solid->createDodecahedron(0);
    pSolid = solid.release();
    break;
  case ObjectType::DODECAHEDRON_2_COMPOUND: {
    solid->createTetrakisHexahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
  }
    break;
  case ObjectType::GREAT_DODECAHEDRON:
    solid->createDodecahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::GREAT_STELLATED_DODECAHEDRON:
    solid->createDodecahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 3);
    break;
  case ObjectType::HEXAGONAL_PRISM:
    solid->createTriangularBipyramid();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::ZONO);
    break;
  case ObjectType::ICOSAHEDRON:
    solid->createIcosahedron(0);
    pSolid = solid.release();
    break;
  case ObjectType::ICOSAHEDRON_2_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createIcosahedron2Compound();
  }
    break;
  case ObjectType::ICOSIDODECAHEDRON:
    solid->createIcosidodecahedron(0, 1);
    pSolid = solid.release();
    break;
  case ObjectType::ICOSIDODECAHEDRON_SECOND_STELLATION:
    solid->createIcosidodecahedron(0, 1);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::OCTAHEDRON:
    solid->createOctahedron(0);
    pSolid = solid.release();
    break;
  case ObjectType::OCTAHEDRON_2_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createOctahedron2Compound();
  }
    break;
  case ObjectType::OCTAHEDRON_3_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createOctahedron3Compound();
  }
    break;
  case ObjectType::OCTAHEDRON_4_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createOctahedron4Compound();
  }
    break;
  case ObjectType::OCTAHEDRON_5_COMPOUND:
    solid->createIcosahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::PENTAGONAL_BIPYRAMID:
    solid->createPentagonalBipyramid();
    pSolid = solid.release();
    break;
  case ObjectType::PENTAGONAL_ICOSITETRAHEDRON:
    solid->createSnubCube();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::PENTAGONAL_PRISM:
    solid->createPentagonalBipyramid();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::PENTAKIS_DODECAHEDRON:
    solid->createIcosahedron(0);
    solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE));
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::RHOMBI_CUBOCTAHEDRON:
    solid->createRhombiCuboctahedron(0, 1);
    pSolid = solid.release();
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON:
    solid->createRhombicDodecahedron();
    pSolid = solid.release();
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON_FIRST_STELLATION:
    solid->createRhombicDodecahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON_SECOND_STELLATION:
    solid->createRhombicDodecahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
    /*
  case ObjectType::RHOMBIC_DODECAHEDRON_THIRD_STELLATION:
    solid->createRhombicDodecahedron();
    pSolid = TransformSolid::transform(*solid.release(), PolyhedronTransformation::STELLATION, 3);
    break;
*/
  case ObjectType::RHOMBIC_DODECAHEDRON_2_COMPOUND: {
    std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(aColorList);
    pSolid = c->createRhombicDodecahedron2Compound();
  }
    break;
  case ObjectType::RHOMBIC_TRIACONTAHEDRON:
    solid->createIcosidodecahedron(0,1);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::RHOMBICOSIDODECAHEDRON:
    solid->createIcosidodecahedron(0,1);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::REGULAR);
    break;
  case ObjectType::SMALL_STELLATED_DODECAHEDRON:
    solid->createDodecahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::SMALL_TRIAMBIC_ICOSAHEDRON:
    solid->createIcosahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::SNUB_CUBE:
    solid->createSnubCube();
    pSolid = solid.release();
    break;
  case ObjectType::TETRAHEDRON:
    solid->createTetrahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TETRAHEDRON_4_COMPOUND: {
    CompoundPolyhedron c(aColorList);
    pSolid = c.createTetrahedron4Compound();
  }
    break;
  case ObjectType::TETRAKIS_HEXAHEDRON:
    solid->createTetrakisHexahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TETRAKIS_HEXAHEDRON_SECOND_STELLATION:
    solid->createTetrakisHexahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::TRIAKIS_ICOSAHEDRON:
    solid->createTruncatedDodecahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::TRIAKIS_ICOSAHEDRON_FIRST_STELLATION:
    solid->createTruncatedDodecahedron();
    solid.reset(TransformSolid::transform(solid, PolyhedronTransformation::DUAL));
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::TRIAKIS_OCTAHEDRON:
    solid->createTriakisOctahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TRIAKIS_OCTAHEDRON_FIRST_STELLATION:
    solid->createTriakisOctahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::TRIAKIS_OCTAHEDRON_SECOND_STELLATION:
    solid->createTriakisOctahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::TRIAKIS_TETRAHEDRON:
    solid->createTriakisTetrahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TRIAKIS_TETRAHEDRON_FIRST_STELLATION:
    solid->createTriakisTetrahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 1);
    break;
  case ObjectType::TRIAKIS_TETRAHEDRON_SECOND_STELLATION:
    solid->createTriakisTetrahedron();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::STELLATION, 2);
    break;
  case ObjectType::TRIANGULAR_BIPYRAMID:
    solid->createTriangularBipyramid();
    pSolid = solid.release();
    break;
  case ObjectType::TRIANGULAR_PRISM:
    solid->createTriangularBipyramid();
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::DUAL);
    break;
  case ObjectType::TRUNCATED_CUBE:
    solid->createTruncatedCube();
    pSolid = solid.release();
    break;
  case ObjectType::TRUNCATED_CUBOCTAHEDRON:
    solid->createCuboctahedron(0, 1);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE);
    break;
  case ObjectType::TRUNCATED_DODECAHEDRON:
    solid->createTruncatedDodecahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TRUNCATED_ICOSAHEDRON:
    solid->createIcosahedron(0);
    pSolid = TransformSolid::transform(solid, PolyhedronTransformation::TRUNCATE);
    break;
  case ObjectType::TRUNCATED_OCTAHEDRON:
    solid->createTruncatedOctahedron();
    pSolid = solid.release();
    break;
  case ObjectType::TRUNCATED_TETRAHEDRON:
    solid->createTruncatedTetrahedron();
    pSolid = solid.release();
    break;
  default:
    solid->createTetrahedron();
    pSolid = solid.release();
    break;
  }
  if (m_transform == PolyhedronTransformation::DUAL_COMPOUND && Polyhedron::getDualType(aType) != ObjectType::NONE) {
    std::unique_ptr<Polyhedron> solid2;
    solid2.reset(pSolid);
    pSolid = TransformSolid::transform(solid2, PolyhedronTransformation::DUAL_COMPOUND);
  }
  if (pSolid) {
    resetSolid(pSolid);
    // Calculate the normals for lighting
    m_solid->fillNormals();
  }
}

auto Panel::updateTextureResource(const char *filename) -> bool {
  std::unique_ptr<TextureResource> result;
  if (filename) {
    result.reset(new TextureResource(m_graphics, filename, true));
    if (!m_errorMessage.empty())
      return false;
    // Update m_textureResource with <result>
    if (result)
      updateTextureResource(result.release());
    if (!m_solidFillTexturePipeline)
      m_solidFillTexturePipeline = std::make_unique<SolidFillTexturePipeline>(m_graphics, m_colorAlpha, m_colorAmbient, m_textureResource);
    m_solidFillTexturePipeline->setTextureExtent(m_textureResource->getExtent());
    m_solidFillTexturePipeline->setModel(getModel());
    m_solidFillTexturePipeline->setProjectionView(buildObjectVP());
    // Clear any existing data
    m_solidFillTexturePipeline->clearAll();
    fillSolidFillTexturePipelineBuffer();
  } else {
    if (!m_solidFillPipeline)
      createSolidFillPipeline();
    m_solidFillPipeline->setModel(getModel());
    m_solidFillPipeline->setProjectionView(buildObjectVP());
    fillSolidFillPipelineBuffer();
  }
  return true;
}

auto Panel::updateTextureResource() -> bool {
  if (!m_solidFillTexturePipeline) {
    assert(m_textureResource);
    m_solidFillTexturePipeline = std::make_unique<SolidFillTexturePipeline>(m_graphics, m_colorAlpha, m_colorAmbient, m_textureResource);
    if (m_solidFillTexturePipeline->hasError()) {
      m_errorMessage = m_solidFillTexturePipeline->getErrorMessage();
      m_solidFillTexturePipeline.reset(nullptr);
      return false;
    }
  } else if (m_textureResource) {
    m_solidFillTexturePipeline->updateDescriptorSets(m_textureResource->m_imageView);
  }
  m_solidFillTexturePipeline->setTextureExtent(m_textureResource->getExtent());
  m_drawLines = false;
  m_useTexture = true;
  return true;
}

auto Panel::updateTextureResource(TextureResource* resource) -> void {
  assert(!m_textureResourceOld);
  // Save the original resource until buffers are rebuild
  if (m_textureResource)
    m_textureResourceOld.reset(m_textureResource.release());
  // Update the texture resource
  m_textureResource.reset(resource);
}

void Panel::updateTextureUniformBuffers() {
  assert(m_textureResource);
  assert(m_type != PanelType::NONE);
  if (m_type == PanelType::SOLID) {
    assert(m_solidFillTexturePipeline);
    m_solidFillTexturePipeline->updateDescriptorSets(m_textureResource->m_imageView);
  } else if (m_type == PanelType::IMAGE_COMP || m_type == PanelType::IMAGE_FILE) {
    assert(m_imagePipeline);
    m_imagePipeline->updateUniformBuffers(m_textureResource->m_imageView);
  }
}

void Panel::updateUniformBuffer(bool full) {
  assert(m_type == PanelType::MESH || m_type == PanelType::SOLID);
  glm::vec3 lightDirection(0.0f);
  if (full) {
    lightDirection = glm::vec3(sin(m_lightInclination) * cos(m_lightAzimuth),
                               sin(m_lightInclination) * sin(m_lightAzimuth),
                               cos(m_lightInclination));
  }
  if (m_type == PanelType::MESH) {
    if (m_meshUsesSkin) {
      assert(m_meshTexSkinPipeline);
      m_meshTexSkinPipeline->setModel(getModel());
      if (full) {
        m_meshTexSkinPipeline->setEye(m_eye);
        m_meshTexSkinPipeline->setProjectionView(buildObjectVP());
        m_meshTexSkinPipeline->setLightDirection(lightDirection);
      }
    } else if (m_meshUsesTexture) {
      assert(m_meshTexturePipeline);
      m_meshTexturePipeline->setModel(getModel());
      if (full) {
        m_meshTexturePipeline->setProjectionView(buildObjectVP());
        m_meshTexturePipeline->setLightDirection(lightDirection);
      }
    } else if (m_meshUsesMaterial) {
      assert(m_meshMaterialPipeline);
      m_meshMaterialPipeline->setModel(getModel());
      if (full) {
        m_meshMaterialPipeline->setEye(m_eye);
        m_meshMaterialPipeline->setProjectionView(buildObjectVP());
        m_meshMaterialPipeline->setLightDirection(lightDirection);
      }
    } else {
      assert(m_meshPipeline);
      m_meshPipeline->setModel(getModel());
      if (full) {
        m_meshPipeline->setProjectionView(buildObjectVP());
        m_meshPipeline->setLightDirection(lightDirection);
      }
    }
  } else if (m_type == PanelType::SOLID) {
    assert(m_solid);
    const auto mvp = buildObjectMVP();
    if (full)
      m_axisPipeline->setMVP(buildObjectVP());
    switch (m_shader) {
    case ShaderType::MORPH:
      assert(m_morphPipeline);
      setMorphFraction(lightDirection, glm::cos(getMorphTime()));
      break;
    case ShaderType::SOLID_FILL:
      assert(m_solidFillPipeline);
      m_solidFillPipeline->setModel(getModel());
      if (full) {
        m_solidFillPipeline->setProjectionView(buildObjectVP());
        m_solidFillPipeline->setEye(m_eye);
        m_solidFillPipeline->setLightDirection(lightDirection);
        m_solidFillPipeline->setMetallic(1.0f);
        m_solidFillPipeline->setRoughness(0.1f);
      }
      break;
    case ShaderType::SOLID_FILL_TEXTURE:
      assert(m_solidFillTexturePipeline);
      m_solidFillTexturePipeline->setModel(getModel());
      if (full) {
        m_solidFillTexturePipeline->setProjectionView(buildObjectVP());
        m_solidFillTexturePipeline->setEye(m_eye);
        m_solidFillTexturePipeline->setLightDirection(lightDirection);
        m_solidFillTexturePipeline->setMetallic(1.0f);
        m_solidFillTexturePipeline->setRoughness(0.1f);
      }
      break;
    case ShaderType::SOLID_LINE:
      assert(m_solidLinePipeline);
      m_solidLinePipeline->setMVP(mvp);
      break;
    default:
      break;
    }
    if (m_showPoints && (m_shader == ShaderType::SOLID_FILL || m_shader == ShaderType::SOLID_FILL_TEXTURE || m_shader == ShaderType::SOLID_LINE)) {
      assert(m_solidPointPipeline);
      m_solidPointPipeline->setMVP(mvp);
    }
  }
}

void Panel::updateViewPort(uint32_t x, uint32_t width, uint32_t height) {
  m_viewport.x = x;
  m_viewport.y = 0;
  m_viewport.width    = static_cast<float>(width);
  m_viewport.height   = static_cast<float>(height);
  m_viewport.minDepth = 0.0f;
  m_viewport.maxDepth = 1.0f;
}
