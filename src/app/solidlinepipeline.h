#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "pipelinebase.h"
#include "polyhedron.h"

class SolidLinePipeline : public PipelineBase {
public:
  explicit SolidLinePipeline(GraphicsResource* pGraphics);
  void clearAll();
  auto copyUBO(uint32_t iImage) -> bool;
  void createBuffers();
  void fillBuffers(const std::unique_ptr<Polyhedron>& solid);
  inline auto getNumberOfVertices() const -> std::size_t {
    return m_vertices.size();
  }
  inline void setMVP(const glm::mat4x4& aValue) {
    m_ubo.mvp = aValue;
  }
private:
  struct UniformBufferObject {
    glm::mat4 mvp;
    glm::vec3 color;
    float     alpha;
    float     pointSize;
  };
  UniformBufferObject    m_ubo;
  std::vector<glm::vec3> m_vertices;
  std::vector<uint16_t>  m_indices;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};
