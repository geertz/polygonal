#pragma once

#include "polyhedron.h"

class TruncatedPolyhedron
{
  struct TruncateEdge
  {
    uint16_t original1, original2;
    uint16_t iNewVertex1, iNewVertex2;
  };

public:
  std::unique_ptr<Polyhedron> m_target;
  TruncatedPolyhedron(const std::unique_ptr<Polyhedron>&);
private:
  std::vector<TruncateEdge> m_truncateEdges;
  std::vector<size_t>       m_excludeVertices;

  inline bool excludeContains(size_t element) {
    return (std::find(cbegin(m_excludeVertices), cend(m_excludeVertices), element) != cend(m_excludeVertices));
  }
  void addNewEdgeSimple(const std::vector<glm::dvec3>& originalVertices, uint16_t iOrigVertex1, uint16_t iOrigVertex2, Face& newFace);
  void addNewEdge(const std::vector<glm::dvec3>& originalVertices, const glm::dvec3& faceCenter, uint16_t iOrigVertex1, uint16_t iOrigVertex2, Face& newFace);
  void addNewEdge2(const std::vector<glm::dvec3>& originalVertices, const glm::dvec3& faceCenter, uint16_t iOrigVertex1, uint16_t iOrigVertex2, Face& newFace);
  double findLargestEntry(const glm::dmat3&);
  glm::dvec3 findEigenVectorAssociatedWithLargestEigenValue(const glm::dmat3&);
  bool findPlane(const Face& face, const std::vector<glm::dvec3>& vertices, glm::dvec3& center, glm::dvec3 &planeNormal);
  void addToTruncatedFace(const Edge& originalEdge, const glm::dvec3& faceCenter, Face& face);
  std::map<size_t, std::vector<size_t>, std::greater<double> > makeCornerMap(const std::vector<Face> &originalFaces);
};
