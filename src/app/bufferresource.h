#pragma once

#include <cstring>
#include <string>
#include <vector>
#include <vulkan/vulkan.hpp>

class BufferResource {
public:
  vk::UniqueBuffer m_buffer;
  size_t           nElements = 0;

  BufferResource(vk::Device aDevice);
  BufferResource(const BufferResource&) = delete; // No copy needed
  inline void createBuffer(const vk::BufferCreateInfo& info) {
    m_size = info.size;
    m_buffer = m_device.createBufferUnique(info);
  }
  inline auto getBufferMemoryRequirements() const -> vk::MemoryRequirements {
    return m_device.getBufferMemoryRequirements(m_buffer.get());
  }
  inline void allocateMemory(const vk::MemoryAllocateInfo& info) {
    m_deviceMemory = m_device.allocateMemoryUnique(info);
  }
  inline auto bindBufferMemory() {
    m_device.bindBufferMemory(m_buffer.get(), m_deviceMemory.get(), 0);
  }
  inline uint8_t *mapMemory(vk::DeviceSize size) {
    return static_cast<uint8_t *>(m_device.mapMemory(m_deviceMemory.get(), 0, size));
  }
  inline uint8_t * mapMemory() {
    return static_cast<uint8_t *>(m_device.mapMemory(m_deviceMemory.get(), 0, VK_WHOLE_SIZE));
  }
  inline void unmapMemory() {
    m_device.unmapMemory(m_deviceMemory.get());
  }
  static void cmdCopyBuffer(vk::CommandBuffer& commandBuffer, const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, vk::DeviceSize size);
  auto copyData(void *source, vk::DeviceSize size) -> std::string;
private:
  vk::Device             m_device;
  vk::UniqueDeviceMemory m_deviceMemory;
  vk::DeviceSize         m_size;

  static uint32_t findMemoryType( vk::PhysicalDeviceMemoryProperties const & memoryProperties,
                                  uint32_t                                   typeBits,
                                  vk::MemoryPropertyFlags                    requirementsMask );
};
