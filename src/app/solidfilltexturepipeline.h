#pragma once

#include <vulkan/vulkan.hpp>
#include "graphicspipelineresource.h"
#include "solidfillbasepipeline.h"
#include "textureresource.h"

class SolidFillTexturePipeline : public SolidFillBasePipeline {
public:
  explicit SolidFillTexturePipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient, const std::unique_ptr<TextureResource>& textureResource);
  void clearAll();
  void createBuffers();
  inline void draw(const SwapchainImageResource& imageResource, size_t iImage) {
    bindCommandBuffer(imageResource, iImage);
    drawIndexed(imageResource, m_meshes);
  }
  void fillBuffers(const std::unique_ptr<Polyhedron>& solid);
  inline void setTextureExtent(vk::Extent2D aValue) {
    m_texExtent = aValue;
  }
  inline void updateDescriptorSets(const vk::UniqueImageView& textureImageView) {
    m_pipelineResource->updateDescriptorSets(sizeof (UniformBufferObject), textureImageView, m_textureSampler);
  }
private:
  // To be used with shader <solid.vert>
  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 texCoord;
  };
  vk::Extent2D        m_texExtent{0,0};
  std::vector<Vertex> m_vertices;
  vk::UniqueSampler   m_textureSampler;
  std::vector<Mesh>   m_meshes;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
  void appendVertex(uint16_t iVertex, const Vertex& row);
};
