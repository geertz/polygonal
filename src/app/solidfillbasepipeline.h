#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "pipelinebase.h"
#include "polyhedron.h"

class SolidFillBasePipeline : public PipelineBase {
public:
  SolidFillBasePipeline(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel);
  auto copyUBO(uint32_t iImage) -> bool;
  inline size_t getNumberOfIndices() const {
    return m_indices.size();
  }
  inline void setColorAlpha(float aValue) {
    m_ubo.alpha = aValue;
  }
  inline void setColorAmbient(float aValue) {
    m_ubo.ambient = aValue;
  }
  inline void setEye(const glm::vec3& aValue) {
    m_ubo.eye = glm::vec4(aValue, 0.0f);
  }
  inline void setLightDirection(const glm::vec3& aValue) {
    m_ubo.lightDir = glm::vec4(aValue, 0.0f);
  }
  inline void setMetallic(float aValue) {
    m_ubo.metallic = aValue;
  }
  inline void setModel(const glm::mat4x4& aValue) {
    m_ubo.model = aValue;
  }
  inline void setProjectionView(const glm::mat4x4& aValue) {
    m_ubo.projView = aValue;
  }
  inline void setRoughness(float aValue) {
    m_ubo.roughness = aValue;
  }
protected:
  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 projView;
    glm::vec4 eye;
    glm::vec4 lightDir;
    float     ambient;
    float     alpha;
    float     metallic;
    float     roughness;
  };
  std::vector<uint16_t> m_indices;
  std::vector<uint16_t> m_listOfIndices;
  ObjectType            m_solidType = ObjectType::NONE;
  bool                  m_isDualCompound = false;
  std::vector<GraphicsPipelineResource::UniformBinding> m_ub_bindings;

  void appendIndex(uint16_t iVertex, uint16_t offset);
  void makeTriangles(const std::unique_ptr<Polyhedron>& , const Face& face, std::vector<uint16_t>& indices, uint16_t offset);
private:
  UniformBufferObject m_ubo;
};
