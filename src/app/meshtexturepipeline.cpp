#include "meshtexturepipeline.h"

MeshTexturePipeline::MeshTexturePipeline(GraphicsResource *graphics) : MeshPipelineBase(graphics, "shaders/vert_mesh.spv", "shaders/frag_mesh.spv", "Mesh texture pipeline") {
  if (!m_pipelineResource)
    return;
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, sizeof(UniformBufferObject)}
  };
  createDescriptorSetLayoutUB(bindings);
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  m_pipelineResource->createDescriptorSetLayoutCIS();

  vk::PushConstantRange pushRange;
  pushRange.offset = 0;
  pushRange.size = sizeof(GraphicsPipelineResource::PushPBR);
  pushRange.stageFlags = vk::ShaderStageFlagBits::eVertex;
  createPipeline(vertexInputInfo, &pushRange);
}

auto MeshTexturePipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  m_ubo.alpha    = m_alpha;
  m_ubo.ambient  = m_ambient;
  m_ubo.lightDir = m_lightDirection;
  m_ubo.model    = m_model;
  m_ubo.projView = m_projView;
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

void MeshTexturePipeline::fillBuffers(const std::unique_ptr<GltfModel>& model) {
  MeshPipelineBase::fillBuffers(model);
  m_vertices.clear();
  bool next = true;
  while (next) {
    Vertex v;
    v.color = glm::vec3(1.0f);
    if (m_vertices.empty())
      next = model->getFirstVertex(v.position, v.normal, v.uv);
    else
      next = model->getNextVertex(v.position, v.normal, v.uv);
    if (next)
      m_vertices.push_back(v);
  }
}

std::vector<vk::VertexInputAttributeDescription> MeshTexturePipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)},
    {2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, uv)},
    {3, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color)}
  };
  return attributeDescriptions;
}

void MeshTexturePipeline::updateVertexBuffer() {
  assert(m_pipelineResource);
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  if (!m_indices.empty())
    m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
}
