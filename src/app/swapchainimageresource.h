#pragma once

#include <memory>
#include "imageresource.h"
#include "logicaldevice.h"

class SwapchainImageResource {
public:
  std::unique_ptr<ImageResource> m_imageResource;
  std::string                    m_errorMessage;
  // Each image its own pool
  vk::UniqueCommandPool   m_imGuiCommandPool;
  vk::CommandBuffer       m_stdCommandBuffer;
  vk::CommandBuffer       m_imGuiCommandBuffer;
  vk::UniqueFramebuffer   m_stdFrameBuffer;
  vk::UniqueFramebuffer   m_imGuiFrameBuffer;
  bool                    m_redrawNeeded = false;

  SwapchainImageResource();
  inline auto bindGraphicsPipeline(const vk::UniquePipeline &pipeline) const -> void {
    m_stdCommandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline.get());
  }
  inline auto bindIndexBuffer(const vk::UniqueBuffer &buffer) const -> void {
    m_stdCommandBuffer.bindIndexBuffer(buffer.get(), 0, vk::IndexType::eUint16);
  }
  auto createImGuiCommandPool(LogicalDevice *device) -> bool;
  inline auto resetStdCommand() const -> void {
    m_stdCommandBuffer.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
  }
  inline auto cleanUp() -> void {
    m_imageResource->destroyImageView();
  }
  inline void draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const {
    m_stdCommandBuffer.draw(vertexCount, instanceCount, firstVertex, firstInstance);
  }
  inline auto resetCommandPool(const vk::Device &device) const -> void {
    if (m_imGuiCommandPool)
      device.resetCommandPool(m_imGuiCommandPool.get(), vk::CommandPoolResetFlags(0));
  }
  void setViewport(const vk::Viewport &view) const;
};
