#include "meshtexskinpipeline.h"

MeshTexSkinPipeline::MeshTexSkinPipeline(GraphicsResource* graphics) : MeshPipelineBase(graphics, "shaders/vert_mesh_skin.spv", "shaders/frag_mesh_skin.spv", "Mesh texture pipeline") {
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, sizeof(UniformBufferObject)}
  };
  createDescriptorSetLayoutUB(bindings);
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  m_pipelineResource->createDescriptorSetLayoutSB();
  m_pipelineResource->createDescriptorSetLayoutCIS();

  createPipeline(vertexInputInfo, nullptr);
}

auto MeshTexSkinPipeline::copyUBO(uint32_t iImage, const std::unique_ptr<GltfModel>& model) -> bool {
  assert(m_pipelineResource);
  updateAnimation(model);
  m_ubo.alpha    = m_alpha;
  m_ubo.ambient  = m_ambient;
  m_ubo.lightDir = m_lightDirection;
  m_ubo.model    = model->m_transform * m_model;
  m_ubo.projView = m_projView;
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

void MeshTexSkinPipeline::fillBuffers(const std::unique_ptr<GltfModel>& model) {
  MeshPipelineBase::fillBuffers(model);
  m_vertices.clear();
  bool next = true;
  while (next) {
    Vertex v;
    v.color = glm::vec3(1.0f);
    if (m_vertices.empty())
      next = model->getFirstVertex(v.position, v.normal, v.uv, v.jointIndices, v.jointWeights);
    else
      next = model->getNextVertex(v.position, v.normal, v.uv, v.jointIndices, v.jointWeights);
    if (next)
      m_vertices.push_back(v);
  }
}

std::vector<vk::VertexInputAttributeDescription> MeshTexSkinPipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)},
    {2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, uv)},
    {3, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color)},
    // POI: Per-Vertex Joint indices and weights are passed to the vertex shader
    {4, 0, vk::Format::eR32G32B32A32Sfloat, offsetof(Vertex, jointIndices)},
    {5, 0, vk::Format::eR32G32B32A32Sfloat, offsetof(Vertex, jointWeights)}
  };
  return attributeDescriptions;
}

void MeshTexSkinPipeline::updateVertexBuffer() {
  assert(m_pipelineResource);
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  if (!m_indices.empty())
    m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
}
