#include "solidfillbasepipeline.h"
#include <algorithm>

SolidFillBasePipeline::SolidFillBasePipeline(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel) : PipelineBase(pGraphics, vertexShaderFile, fragmentShaderFile, aLabel) {

}

auto SolidFillBasePipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}
/**
 * @brief SolidFillBasePipeline::appendIndex : Add vertex <iVertex> to the indices
 * @param iVertex
 * @param offset
 */
auto SolidFillBasePipeline::appendIndex(uint16_t iVertex, uint16_t offset) -> void {
  const auto it = std::find(cbegin(m_listOfIndices), cend(m_listOfIndices), iVertex);
  assert(it != m_listOfIndices.end());
  const auto index = std::distance(cbegin(m_listOfIndices), it);
  assert(index >= 0);
  m_indices.push_back(offset + static_cast<uint16_t>(index));
}

void SolidFillBasePipeline::makeTriangles(const std::unique_ptr<Polyhedron>& solid, const Face& face, std::vector<uint16_t>& indices, uint16_t offset) {
  std::vector<std::vector<uint16_t>> triangles;
  auto nCorners = face.getCorners();
  if (nCorners == 3)
    triangles.push_back(indices);
  else {
    // Simple ear clipping
    // Works only for convex polygons
    glm::dmat4 transform(1.0);
    const auto axis2D = solid->transform2D(face, transform);
    while (nCorners > 3) {
      const auto count = solid->createTriangle3(transform, axis2D, triangles, indices);
      assert(count > 0);
      nCorners = nCorners - count;
    }
    // Add the last triangle
    assert(indices.size() == 3);
    triangles.push_back(indices);
  }
  assert(!triangles.empty());
  for (const auto& triangle : triangles) {
    assert(triangle.size() == 3);
    for (const auto iVertex : triangle) {
      appendIndex(iVertex, offset);
    }
  }
}
