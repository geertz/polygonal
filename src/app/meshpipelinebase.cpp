#include <tiny_gltf.h>
#include <iostream>
#include "meshpipelinebase.h"

MeshPipelineBase::MeshPipelineBase(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel) : PipelineBase(pGraphics, vertexShaderFile, fragmentShaderFile, aLabel) {
  if (!m_pipelineResource)
    return;
  m_graphics = pGraphics;
}

auto MeshPipelineBase::createPipeline(const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, pVertexInputInfo, pPushConstantRange)) {
    setResourceError();
    return false;
  }
  m_usePushConstants = (pPushConstantRange != nullptr);
  return true;
}

void MeshPipelineBase::draw(const SwapchainImageResource& imageResource, size_t iImage) {
  assert(m_pipelineResource);
  m_pipelineResource->setCurrentBuffer(0);
  bindCommandBuffer(imageResource);
  m_pipelineResource->bindDescriptorSets(imageResource, iImage);
  if (m_usePushConstants && !m_materials.empty())
    m_pipelineResource->drawIndexed(imageResource, m_meshes, m_materials);
  else
    drawIndexed(imageResource, m_meshes, m_skins);
}

void MeshPipelineBase::fillBuffers(const std::unique_ptr<GltfModel>& model) {
  if (m_meshesNext.empty())
    return;
  assert(m_pipelineResource);
  m_textureResources.clear();
  m_meshes.clear();
  m_skins.clear();
  m_animations.clear();
  m_meshes = std::move(m_meshesNext);
  m_skins = std::move(m_skinsNext);
  m_animations = std::move(m_animationsNext);

  m_time = std::chrono::high_resolution_clock::now();
  // Calculate initial pose
  for (auto& node : model->m_nodes) {
    updateJoints(model, node);
  }
  m_pipelineResource->createUniformBuffers(m_ub_bindings, m_meshes, m_skins);
  if (!m_textures.empty()) {
    if (!m_textureSampler)
      m_textureSampler = m_graphics->getDevice()->createSampler();
    for (const auto& mesh : m_meshes) {
      if (!mesh.skins.empty()) {
        auto skinID = mesh.skins.front();
        auto& skin = m_skins.at(skinID);
        m_pipelineResource->updateDescriptorSetsSB(skin.descriptorSet, skin.ssbo->m_buffer.get(), skin.bufferSize);
      }
      for (const auto& p : mesh.primitives) {
        std::unique_ptr<TextureResource> resource;
        model->makeTextureResource(m_graphics, m_textures.at(m_materials.at(p.materialIndex).baseColorTextureIndex).imageIndex, resource);
        m_pipelineResource->updateDescriptorSetsCIS(p.descriptionSet, resource->m_imageView, m_textureSampler);
        m_textureResources.push_back(std::move(resource));
      }
    }
  }
}

void MeshPipelineBase::loadMeshes(const std::unique_ptr<GltfModel>& model) {
  m_indices.clear();
  m_materials.clear();
  m_textures.clear();
  assert(m_meshesNext.empty());
  assert(m_skinsNext.empty());
  assert(m_animationsNext.empty());
  model->loadMeshes(m_meshesNext, m_indices, m_materials, m_textures);
  model->loadNodes();
  model->loadSkins(m_graphics, m_meshesNext, m_skinsNext);
  model->loadAnimations(m_animationsNext);
}

// POI: Update the current animation
void MeshPipelineBase::updateAnimation(const std::unique_ptr<GltfModel>& model) {
  auto currentTime = std::chrono::high_resolution_clock::now();
  auto deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - m_time).count();
  m_time = currentTime;
  if (m_activeAnimation > static_cast<uint32_t>(m_animations.size()) - 1) {
    std::cout << "No animation with index " << m_activeAnimation << std::endl;
    return;
  }
  auto& animation = m_animations[m_activeAnimation];
  animation.currentTime += deltaTime;
  if (animation.currentTime > animation.end) {
    animation.currentTime -= animation.end;
  }

  for (auto &channel : animation.channels) {
    auto& sampler = animation.samplers[channel.samplerIndex];
    for (size_t i = 0; i < sampler.inputs.size() - 1; i++) {
      if (sampler.interpolation != "LINEAR") {
        std::cout << "This sample only supports linear interpolations\n";
        continue;
      }

      // Get the input keyframe values for the current time stamp
      if ((animation.currentTime >= sampler.inputs[i]) && (animation.currentTime <= sampler.inputs[i + 1])) {
        auto a = (animation.currentTime - sampler.inputs[i]) / (sampler.inputs[i + 1] - sampler.inputs[i]);
        if (channel.path == "translation") {
          channel.node->translation = glm::mix(sampler.outputsVec4[i], sampler.outputsVec4[i + 1], a);
        } else if (channel.path == "rotation") {
          glm::quat q1;
          q1.x = sampler.outputsVec4[i].x;
          q1.y = sampler.outputsVec4[i].y;
          q1.z = sampler.outputsVec4[i].z;
          q1.w = sampler.outputsVec4[i].w;

          glm::quat q2;
          q2.x = sampler.outputsVec4[i + 1].x;
          q2.y = sampler.outputsVec4[i + 1].y;
          q2.z = sampler.outputsVec4[i + 1].z;
          q2.w = sampler.outputsVec4[i + 1].w;

          channel.node->rotation = glm::normalize(glm::slerp(q1, q2, a));
        } else if (channel.path == "scale") {
          channel.node->scale = glm::mix(sampler.outputsVec4[i], sampler.outputsVec4[i + 1], a);
        }
      }
    }
  }
  for (auto& node : model->m_nodes) {
    updateJoints(model, node);
  }
}

// POI: Update the joint matrices from the current animation frame and pass them to the GPU
void MeshPipelineBase::updateJoints(const std::unique_ptr<GltfModel>& model, const std::unique_ptr<GltfNode>& node) {
  if (node->skin > -1) {
    assert(m_skins.size() > static_cast<size_t>(node->skin));
    // Update the joint matrices
    const auto inverseTransform = glm::inverse(GltfNode::getNodeMatrix(node.get()));
    auto& skin = m_skins[node->skin];
    auto numJoints = skin.joints.size();
    std::vector<glm::mat4> jointMatrices(numJoints);
    for (size_t i = 0; i < numJoints; i++) {
      jointMatrices[i] = GltfNode::getNodeMatrix(skin.joints[i]) * skin.inverseBindMatrices[i];
      jointMatrices[i] = inverseTransform * jointMatrices[i];
    }
    // Update ssbo
    skin.bufferSize = jointMatrices.size() * sizeof(glm::mat4);
    skin.ssbo->copyData(jointMatrices.data(), skin.bufferSize);
  }

  for (const auto& child : node->children){
    updateJoints(model, child);
  }
}
