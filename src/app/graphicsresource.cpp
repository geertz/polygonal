#include "graphicsresource.h"
#include "vulkanlib.h"
#include "shadermodule.h"
#include "singletimecommands.h"

#include <cassert>
#include <iostream>

FrameResource::FrameResource(const LogicalDevice *aDevice) {
  m_imageAvailableSemaphore = aDevice->createSemaphore();
  m_renderFinishedSemaphore = aDevice->createSemaphore();
  m_inFlightFence = aDevice->createFence(vk::FenceCreateFlagBits::eSignaled);
}

auto FrameResource::getInFlightFence() const -> vk::Fence {
  return m_inFlightFence.get();
}

auto FrameResource::getImageAvailableSemaphore() const -> vk::Semaphore {
  return m_imageAvailableSemaphore.get();
}

auto FrameResource::getRenderFinishedSemaphore() const -> vk::Semaphore {
  return m_renderFinishedSemaphore.get();
}

bool SwapchainResource::updateImages() {
  try {
    auto swapChainImages = m_device->getSwapchainImages(m_swapChain);

    for (uint32_t i = 0; i < m_imageCount; i++) {
      m_swapChainImageResources[i].m_imageResource = std::make_unique<ImageResource>(m_device, m_swapChainImageFormat, swapChainImages.at(i));
      m_swapChainImageResources[i].m_imageResource->createImageView(vk::ImageAspectFlagBits::eColor);
    }
    return true;
  } catch (...) {
    m_errorMessage = "Failed to create swapchain images!";
    return false;
  }
}

SwapchainResource::SwapchainResource(LogicalDevice *device, vk::SurfaceKHR surface, vk::SwapchainKHR oldOne, uint32_t width, uint32_t height) {
  m_device = device;
  auto swapChainSupport = m_device->m_physicalDevice->querySwapChainSupport();

  auto surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
#ifdef PRESENT_MODE_MAILBOX
  VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
#endif
  m_imageCount = swapChainSupport.capabilities.minImageCount + 1;
  if (swapChainSupport.capabilities.maxImageCount > 0 && m_imageCount > swapChainSupport.capabilities.maxImageCount) {
    m_imageCount = swapChainSupport.capabilities.maxImageCount;
  }
  if (swapChainSupport.capabilities.currentExtent.width == UINT_MAX) {
    m_swapChainExtent.width = width;
    m_swapChainExtent.height = height;
  } else {
    m_swapChainExtent.width = swapChainSupport.capabilities.currentExtent.width;
    m_swapChainExtent.height = swapChainSupport.capabilities.currentExtent.height;
  }
  if (swapChainSupport.capabilities.minImageExtent.width > m_swapChainExtent.width)
    m_swapChainExtent.width = swapChainSupport.capabilities.minImageExtent.width;
  if (swapChainSupport.capabilities.minImageExtent.height > m_swapChainExtent.height)
    m_swapChainExtent.height = swapChainSupport.capabilities.minImageExtent.height;

  vk::SwapchainCreateInfoKHR createInfo{};
  createInfo.surface = surface;
  createInfo.minImageCount = m_imageCount;
  createInfo.imageFormat = surfaceFormat.format;
  createInfo.imageColorSpace = surfaceFormat.colorSpace;
  createInfo.imageExtent = m_swapChainExtent;
  createInfo.imageArrayLayers = 1;
  createInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

  const auto graphicsFamily = m_device->getGraphicsFamily();
  const auto presentFamily = m_device->m_physicalDevice->getPresentFamily();
  if (graphicsFamily != presentFamily) {
    createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
    uint32_t queueFamilyIndices[] = {graphicsFamily, presentFamily};
    createInfo.queueFamilyIndexCount = 2;
    createInfo.pQueueFamilyIndices = queueFamilyIndices;
  } else {
    createInfo.imageSharingMode = vk::SharingMode::eExclusive;
    createInfo.queueFamilyIndexCount = 0; // Optional
    createInfo.pQueueFamilyIndices = nullptr; // Optional
  }
  createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
  createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
#ifdef PRESENT_MODE_MAILBOX
  createInfo.presentMode = presentMode;
#else
  createInfo.presentMode = vk::PresentModeKHR::eFifo;
#endif
  createInfo.clipped = VK_TRUE;
  createInfo.oldSwapchain = oldOne;
  m_swapChain = m_device->createSwapchain(createInfo);
  m_swapChainImageFormat = surfaceFormat.format;
  m_swapChainImageResources.resize(m_imageCount);
  updateImages();
}

SwapchainResource::~SwapchainResource() {
  for (auto& resource : m_swapChainImageResources) {
    resource.cleanUp();
  }
  for (auto &resource : m_swapChainImageResources) {
    // The image will be freed by destroying the swapchain.
    resource.m_imageResource->m_image = nullptr;
    resource.m_imageResource.reset(nullptr);
  }
  m_swapChainImageResources.clear();
  m_device->destroySwapchain(m_swapChain);
}

auto SwapchainResource::chooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats) -> vk::SurfaceFormatKHR {
  for (const auto& availableFormat : availableFormats) {
    if (availableFormat.format == vk::Format::eR8G8B8A8Unorm && availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
      return availableFormat;
    }
  }
  return availableFormats[0];
}

void SwapchainResource::redrawNeeded() {
  for (auto &imageResource : m_swapChainImageResources)
    imageResource.m_redrawNeeded = true;
}

GraphicsResource::GraphicsResource() {
}

GraphicsResource::~GraphicsResource() {
  if (m_swapChainResource)
    delete m_swapChainResource.release();
}
/**
 * @brief Acquire next image
 * @param pImageIndex : The index of the next image
 * @return vk::Result::eSuccess if OK
 */
auto GraphicsResource::acquireNextImage(uint32_t* pImageIndex) -> vk::Result {
  assert(m_currentFrame < m_frameResources.size());
  const auto &frameResource = m_frameResources.at(m_currentFrame);
  m_device->waitForFence(frameResource->getInFlightFence());
  return m_device->getDevice().acquireNextImageKHR(m_swapChainResource->m_swapChain, UINT64_MAX, frameResource->getImageAvailableSemaphore(), nullptr, pImageIndex);
}
/**
 * @brief Create the renderpass for ImGui
 */
auto GraphicsResource::createImGuiRenderPass() -> void {
  vk::AttachmentDescription attachment{};
  attachment.format = m_swapChainResource->m_swapChainImageFormat;
  attachment.samples = vk::SampleCountFlagBits::e1;
  attachment.loadOp = vk::AttachmentLoadOp::eLoad;
  attachment.storeOp = vk::AttachmentStoreOp::eStore;
  attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
  attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
  attachment.initialLayout = vk::ImageLayout::eColorAttachmentOptimal;
  attachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

  vk::AttachmentReference colorAttachmentRef(0, vk::ImageLayout::eColorAttachmentOptimal);

  vk::SubpassDescription subpass{};
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &colorAttachmentRef;

  vk::RenderPassCreateInfo info{};
  info.attachmentCount = 1;
  info.pAttachments = &attachment;
  info.subpassCount = 1;
  info.pSubpasses = &subpass;
  info.dependencyCount = 0;
  info.pDependencies = nullptr;
  m_imGuiRenderPass = m_device->createRenderPass(info);
}

auto GraphicsResource::createBuffer(vk::DeviceSize size, std::size_t nElements, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, BufferResource *resource) -> bool {
  assert(size > 0);
  assert(nElements > 0);
  vk::BufferCreateInfo bufferInfo{};
  bufferInfo.size = size * nElements;
  bufferInfo.usage = usage;
  bufferInfo.sharingMode = vk::SharingMode::eExclusive;

  try {
    resource->createBuffer(bufferInfo);
  } catch (...) {
    m_errorMessage = "Failed to create a buffer!";
    return false;
  }
  auto memRequirements = resource->getBufferMemoryRequirements();

  vk::MemoryAllocateInfo allocInfo{};
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = m_device->findMemoryType(memRequirements.memoryTypeBits, properties);

  try {
    resource->allocateMemory(allocInfo);
    resource->nElements = nElements;
    resource->bindBufferMemory();
    return true;
  } catch (...) {
    m_errorMessage = "Failed to allocate buffer memory!";
    return false;
  }
}

auto GraphicsResource::createSwapChain(uint32_t width, uint32_t height) -> bool {
  if (!m_swapChainResource) {
    m_swapChainResource = std::make_unique<SwapchainResource>(m_device, m_surface.get(), nullptr, width, height);
    if (!m_swapChainResource->m_errorMessage.empty()) {
      m_errorMessage = std::move(m_swapChainResource->m_errorMessage);
      m_swapChainResource.reset(nullptr);
      return false;
    }
  }
  return true;
}

auto GraphicsResource::findDepthFormat() -> vk::Format {
  return m_device->m_physicalDevice->findSupportedFormat(
  {vk::Format::eD32SfloatS8Uint, vk::Format::eD32Sfloat, vk::Format::eD24UnormS8Uint, vk::Format::eD16UnormS8Uint, vk::Format::eD16Unorm},
   vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

auto GraphicsResource::createStdRenderPass() -> void {
  vk::AttachmentDescription colorAttachment{};
  colorAttachment.flags = vk::AttachmentDescriptionFlags(0);
  colorAttachment.format = m_swapChainResource->m_swapChainImageFormat;
  colorAttachment.samples = m_device->getMsaaSamples();
  colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
  colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
  colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
  colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
  colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
  colorAttachment.finalLayout = vk::ImageLayout::eColorAttachmentOptimal;

  vk::AttachmentDescription depthAttachment{};
  depthAttachment.flags = vk::AttachmentDescriptionFlags(0);
  depthAttachment.format = findDepthFormat();
  depthAttachment.samples = m_device->getMsaaSamples();
  depthAttachment.loadOp = vk::AttachmentLoadOp::eClear;
  depthAttachment.storeOp = vk::AttachmentStoreOp::eDontCare;
  depthAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
  depthAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
  depthAttachment.initialLayout = vk::ImageLayout::eUndefined;
  depthAttachment.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

  vk::AttachmentDescription colorAttachmentResolve{};
  colorAttachmentResolve.format = m_swapChainResource->m_swapChainImageFormat;
  colorAttachmentResolve.samples = vk::SampleCountFlagBits::e1; // No msaaSamples here!
  colorAttachmentResolve.loadOp = vk::AttachmentLoadOp::eDontCare;
  colorAttachmentResolve.storeOp = vk::AttachmentStoreOp::eStore;
  colorAttachmentResolve.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
  colorAttachmentResolve.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
  colorAttachmentResolve.initialLayout = vk::ImageLayout::eUndefined;;
  colorAttachmentResolve.finalLayout = vk::ImageLayout::eColorAttachmentOptimal; // VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

  vk::AttachmentReference colorAttachmentSolidRef(0, vk::ImageLayout::eColorAttachmentOptimal);
  vk::AttachmentReference depthAttachmentRef(1, vk::ImageLayout::eDepthStencilAttachmentOptimal);

  vk::AttachmentReference colorAttachmentResolveRef{};
  colorAttachmentResolveRef.attachment = 2;
  colorAttachmentResolveRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

  std::vector<vk::AttachmentDescription> attachments = {colorAttachment, depthAttachment, colorAttachmentResolve};

  vk::SubpassDescription subpass{};
  subpass.colorAttachmentCount = 1;
  subpass.pColorAttachments = &colorAttachmentSolidRef;
  subpass.pDepthStencilAttachment = &depthAttachmentRef;
  subpass.pResolveAttachments = &colorAttachmentResolveRef;

  std::vector<vk::SubpassDescription> subpasses = {subpass};

  vk::RenderPassCreateInfo info{};
  info.dependencyCount = 0;
  info.pDependencies = nullptr;
  info.attachmentCount = static_cast<uint32_t>(attachments.size());
  info.pAttachments = attachments.data();
  info.subpassCount = static_cast<uint32_t>(subpasses.size());
  info.pSubpasses = subpasses.data();
  m_stdRenderPass = m_device->createRenderPass(info);
}

auto GraphicsResource::createColorResource() -> bool {
  assert(!m_colorImageResource);
  assert(m_device->m_physicalDevice);
  const auto colorFormat = m_swapChainResource->m_swapChainImageFormat;

  m_colorImageResource = std::make_unique<ImageResource>(m_device, colorFormat);
  if (!m_colorImageResource->createImage(m_swapChainResource->m_swapChainExtent, 1, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal)) {
    m_errorMessage = m_colorImageResource->getErrorMessage();
    return false;
  }
  m_colorImageResource->createImageView(vk::ImageAspectFlagBits::eColor);
  return true;
}

bool GraphicsResource::createDepthResource() {
  assert(!m_depthImageResource);
  assert(m_device->m_physicalDevice);
  const auto depthFormat = findDepthFormat();
  m_depthImageResource = std::make_unique<ImageResource>(m_device, depthFormat);
  if (!m_depthImageResource->createImage(m_swapChainResource->m_swapChainExtent, 1, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal)) {
    m_errorMessage = m_depthImageResource->getErrorMessage();
    return false;
  }
  m_depthImageResource->createImageView(vk::ImageAspectFlagBits::eDepth);
  return true;
}

auto GraphicsResource::createDescriptorPoolCIS() -> vk::UniqueDescriptorPool {
  const auto imageCount = m_swapChainResource->getImageCount();
  assert(imageCount > 0);
  std::vector<vk::DescriptorPoolSize> poolSizes = {
    {vk::DescriptorType::eCombinedImageSampler, imageCount}
  };
  return m_device->createDescriptorPool(poolSizes);
}

auto GraphicsResource::createDescriptorPool(uint32_t imageCount, uint32_t textureCount, uint32_t storageBufferCount) -> vk::UniqueDescriptorPool {
  assert(imageCount > 0);
  std::vector<vk::DescriptorPoolSize> poolSizes = {
    {vk::DescriptorType::eUniformBuffer, imageCount}
  };
  if (textureCount > 0) {
    poolSizes.push_back({vk::DescriptorType::eCombinedImageSampler, textureCount});
  }
  if (storageBufferCount > 0) {
    poolSizes.push_back({vk::DescriptorType::eStorageBuffer, storageBufferCount});
  }
  return m_device->createDescriptorPool(poolSizes);
}

auto GraphicsResource::createGraphicsDescriptorPool(uint32_t textureCount) -> vk::UniqueDescriptorPool {
  assert(textureCount > 0);
  const auto imageCount = m_swapChainResource->getImageCount();
  assert(imageCount > 0);
  std::vector<vk::DescriptorPoolSize> poolSizes = {
    {vk::DescriptorType::eUniformBuffer, imageCount},
    {vk::DescriptorType::eCombinedImageSampler, imageCount * textureCount}
  };
  return m_device->createDescriptorPool(poolSizes);
}

auto GraphicsResource::createStdCommandPool() -> bool {
  try {
    m_commandPool = m_device->createCommandPool(m_device->getGraphicsFamily());
    return true;
  } catch (...) {
    m_errorMessage = "Failed to create standard command pool";
    return false;
  }
}

auto GraphicsResource::createFramebuffers() -> bool {
  vk::FramebufferCreateInfo framebufferInfo{};
  framebufferInfo.width = m_swapChainResource->m_swapChainExtent.width;
  framebufferInfo.height = m_swapChainResource->m_swapChainExtent.height;
  framebufferInfo.layers = 1;
  for (auto &resource : m_swapChainResource->m_swapChainImageResources) {
    if (resource.m_stdFrameBuffer)
      resource.m_stdFrameBuffer.reset(nullptr);
    std::vector<vk::ImageView> stdAttachments = {
      m_colorImageResource->m_imageView.get(),
      m_depthImageResource->m_imageView.get(),
      resource.m_imageResource->m_imageView.get()
    };
    try {
      resource.m_stdFrameBuffer = m_device->createFrameBuffer(framebufferInfo, m_stdRenderPass.get(), stdAttachments.data(), static_cast<uint32_t>(stdAttachments.size()));
    } catch (...) {
      m_errorMessage = "failed to create framebuffer!";
      return false;
    }
    if (m_imGuiRenderPass) {
      if (resource.m_imGuiFrameBuffer)
        resource.m_imGuiFrameBuffer.reset(nullptr);
      vk::ImageView imGuiAttachments[] = {
        resource.m_imageResource->m_imageView.get()
      };
      try {
        resource.m_imGuiFrameBuffer = m_device->createFrameBuffer(framebufferInfo, m_imGuiRenderPass.get(), imGuiAttachments, 1);
      } catch (...) {
        m_errorMessage = "failed to create framebuffer!";
        return false;
      }
    }
  }
  return true;
}

auto GraphicsResource::copyBuffer(const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, vk::DeviceSize size) -> bool {
  assert(m_commandPool);
  SingleTimeCommands command(m_device->getDevice(), m_commandPool.get());
  BufferResource::cmdCopyBuffer(command.m_commandBuffer.get(), srcBuffer, dstBuffer, size);
  if (!command.submit(m_graphicsQueue2)) {
    m_errorMessage = std::move(command.m_errorMessage);
  }
  return true;
}

auto GraphicsResource::copyBufferToImage(const vk::UniqueBuffer& buffer, vk::Image& image, const vk::Extent2D extent) -> bool {
  assert(m_commandPool);
  SingleTimeCommands command(m_device->getDevice(), m_commandPool.get());
  vk::BufferImageCopy region{};
  region.bufferOffset = 0;
  region.bufferRowLength = 0;
  region.bufferImageHeight = 0;

  region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
  region.imageSubresource.mipLevel = 0;
  region.imageSubresource.baseArrayLayer = 0;
  region.imageSubresource.layerCount = 1;

  region.imageOffset = vk::Offset3D(0, 0, 0);
  region.imageExtent = vk::Extent3D(extent.width, extent.height, 1);
  command.m_commandBuffer->copyBufferToImage(buffer.get(),
                                             image,
                                             vk::ImageLayout::eTransferDstOptimal,
                                             1,
                                             &region);
  if (!command.submit(m_graphicsQueue2)) {
    m_errorMessage = std::move(command.m_errorMessage);
    return false;
  }
  return true;
}

bool GraphicsResource::createCommandBuffers() {
  const auto nImages = m_swapChainResource->getImageCount();

  vk::CommandBufferAllocateInfo allocInfo{};
  allocInfo.commandPool = m_commandPool.get();
  allocInfo.level = vk::CommandBufferLevel::ePrimary;
  allocInfo.commandBufferCount = static_cast<uint32_t>(nImages);
  const auto &device = m_device->getDevice();
  try {
    auto stdCommandBuffers = device.allocateCommandBuffers(allocInfo);
    for (size_t i = 0; i < nImages; i++) {
      m_swapChainResource->m_swapChainImageResources[i].m_stdCommandBuffer = std::move(stdCommandBuffers[i]);
    }
  } catch (...) {
    m_errorMessage = "Failed to allocate graphics command buffers!";
    return false;
  }
  // ImGui
  allocInfo.commandBufferCount = 1;
  for (auto &resource : m_swapChainResource->m_swapChainImageResources) {
    if (resource.m_imGuiCommandBuffer)
      continue;
    if (!resource.m_imGuiCommandPool) {
      if (!resource.createImGuiCommandPool(m_device)) {
        m_errorMessage = "Failed to create the ImGui command pool";
        return false;
      }
    }
    allocInfo.commandPool = resource.m_imGuiCommandPool.get();
    try {
      auto buffers = device.allocateCommandBuffers(allocInfo);
      resource.m_imGuiCommandBuffer = std::move(buffers[0]);
    } catch (...) {
      m_errorMessage = "Failed to allocate ImGui command buffer!";
      return false;
    }
  }
  return true;
}

auto GraphicsResource::resetStdCommandBuffers() -> bool {
  for (auto& imageResource : m_swapChainResource->m_swapChainImageResources) {
    try {
      imageResource.resetStdCommand();
    } catch (...) {
      m_errorMessage = "Failed to reset command buffer!";
      return false;
    }
  }
  return true;
}

bool GraphicsResource::createSyncObjects() {
  m_frameResources.clear();
  for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
    try {
      m_frameResources.emplace_back(std::make_unique<FrameResource>(m_device));
    } catch (...) {
      m_errorMessage = "Failed to create semaphores for a frame";
      return false;
    }
  }
  return true;
}

auto GraphicsResource::cleanupSwapChain(bool recreate, bool sameSize) -> void {
  if (!recreate || !sameSize) {
    m_colorImageResource.reset(nullptr);
    m_depthImageResource.reset(nullptr);
  }
  if (!sameSize && m_swapChainResource) {
    // Only delete the swapchain resource when a resize happens
    m_swapChainResource.reset(nullptr);
  }
}

auto GraphicsResource::drawFrame (uint32_t imageIndex) -> vk::Result {
  const auto& frameResource = m_frameResources.at(m_currentFrame);
  const auto& imageResource = m_swapChainResource->getImageResource(imageIndex);

  vk::Semaphore waitSemaphores[] = {frameResource->getImageAvailableSemaphore()};
  vk::PipelineStageFlags waitStages[] = {vk::PipelineStageFlagBits::eColorAttachmentOutput};
  vk::CommandBuffer buffers[] = {imageResource.m_stdCommandBuffer, imageResource.m_imGuiCommandBuffer};
  vk::Semaphore signalSemaphores[] = {frameResource->getRenderFinishedSemaphore()};

  vk::SubmitInfo submitInfo{};
  submitInfo.pWaitSemaphores = waitSemaphores;
  submitInfo.pWaitDstStageMask = waitStages;
  submitInfo.commandBufferCount = 2;
  submitInfo.pCommandBuffers = buffers;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = signalSemaphores;

  m_device->resetFence(frameResource->getInFlightFence());
  try {
    m_graphicsQueue2->submit(submitInfo, frameResource->getInFlightFence());
    vk::SwapchainKHR swapChains[] = {m_swapChainResource->m_swapChain};
    vk::PresentInfoKHR presentInfo{};
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;
    presentInfo.pImageIndices = &imageIndex;
    presentInfo.pResults = VK_NULL_HANDLE; // Optional
    return m_presentQueue.presentKHR(presentInfo);
  } catch ( vk::SystemError & err ) {
    if (err.code().value() == VK_ERROR_OUT_OF_DATE_KHR)
      return vk::Result::eErrorOutOfDateKHR;
    else {
      std::cerr << err.code().value() << std::endl;
      return vk::Result::eErrorUnknown;
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return vk::Result::eErrorUnknown;
  }
}

auto GraphicsResource::setBackgroundColor(const vk::ClearColorValue aColor) -> void {
  m_backgroundColor = aColor;
  if (m_swapChainResource)
    m_swapChainResource->redrawNeeded();
}

auto GraphicsResource::setQueues() -> void {
  m_graphicsQueue = m_device->getGraphicsQueue();
  m_graphicsQueue2.reset(new QueueThread(m_graphicsQueue));
  m_presentQueue = m_device->getPresentQueue();
}
