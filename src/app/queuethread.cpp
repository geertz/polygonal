#include "queuethread.h"

QueueThread::QueueThread(vk::Queue aQueue) {
  m_queue = aQueue;
}

void QueueThread::submit(const vk::SubmitInfo &info, vk::Fence fence) {
  std::lock_guard<std::mutex> guard(m_mutex);
  m_queue.submit(info, fence);
}

void QueueThread::waitIdle() {
  std::lock_guard<std::mutex> guard(m_mutex);
  m_queue.waitIdle();
}
