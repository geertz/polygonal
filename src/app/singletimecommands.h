#pragma once

#include <vulkan/vulkan.hpp>
#include "queuethread.h"

class SingleTimeCommands {
public:
  vk::UniqueCommandBuffer m_commandBuffer;
  std::string             m_errorMessage;

  SingleTimeCommands(vk::Device aDevice, const vk::CommandPool &aPool);
  ~SingleTimeCommands();
  auto submit(const std::unique_ptr<QueueThread> &queue) -> bool;
private:
  bool m_started = false;
};
