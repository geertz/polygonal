#pragma once

#include <vector>
#include <vulkan/vulkan.hpp>
#include <experimental/optional>

struct SwapChainSupportDetails {
  std::vector<vk::SurfaceFormatKHR> formats;
#ifdef PRESENT_MODE_MAILBOX
  std::vector<vk::PresentModeKHR> presentModes;
#endif
  vk::SurfaceCapabilitiesKHR capabilities;
};

class PhysicalDevice {
public:
  vk::PhysicalDevice      m_device = nullptr;
  vk::SampleCountFlagBits m_msaaSamples = vk::SampleCountFlagBits::e1;
  std::string             m_errorMessage;

  PhysicalDevice(vk::UniqueInstance &instance, vk::SurfaceKHR surface, bool computeNeeded);

  std::vector<vk::DeviceQueueCreateInfo> createInfos() const;
  auto findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) -> uint32_t;
  auto findSupportedFormat(const std::vector<vk::Format> &candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) -> vk::Format;
  inline auto getFormatProperties(vk::Format format) -> vk::FormatProperties {
    return m_device.getFormatProperties(format);
  }
  void pickPhysicalDevice();
  inline auto querySwapChainSupport() const -> SwapChainSupportDetails {
    return querySwapChainSupport(m_device);
  }

  inline auto canCompute() const -> bool {
    return bool(m_computeFamily);
  }
  inline auto getComputeFamily() const -> uint32_t {
    return m_computeFamily.value();
  }
  inline auto getGraphicsFamily() const -> uint32_t {
    return m_graphicsFamily.value();
  }
  inline auto getPresentFamily() const -> uint32_t {
    return m_presentFamily.value();
  }
  inline auto isComplete(bool computeNeeded) const -> bool {
    return m_graphicsFamily && m_presentFamily && (computeNeeded = false or m_computeFamily);
  }
private:
  vk::SurfaceKHR                        m_surface;
  std::experimental::optional<uint32_t> m_computeFamily;
  std::experimental::optional<uint32_t> m_graphicsFamily;
  std::experimental::optional<uint32_t> m_presentFamily;

  bool checkDeviceExtensionSupport(vk::PhysicalDevice aDevice);
  auto getMaxUsableSampleCount() -> vk::SampleCountFlagBits;
  bool isDeviceSuitable(vk::PhysicalDevice aDevice, bool computeNeeded);
  auto querySwapChainSupport(vk::PhysicalDevice aDevice) const -> SwapChainSupportDetails;
  void setFamilyIndices(vk::PhysicalDevice aDevice);
  inline void setFamilyIndices() {
    setFamilyIndices(m_device);
  }
};
