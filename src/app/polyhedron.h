#pragma once

#include <math.h>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include "face.h"

namespace bg = boost::geometry;
BOOST_GEOMETRY_REGISTER_POINT_2D(glm::dvec2, double, bg::cs::cartesian, x, y)
BOOST_GEOMETRY_REGISTER_POINT_3D(glm::dvec3, double, bg::cs::cartesian, x, y, z)

typedef bg::model::d2::point_xy<double> Point2D;
typedef bg::model::ring<glm::dvec2> Ring2D;

enum class ObjectType {NONE, CHAMFERED_CUBE, CUBE, CUBE_2_COMPOUND, CUBE_3_COMPOUND, CUBE_4_COMPOUND, CUBE_5_COMPOUND, CUBOCTAHEDRON, CUBOCTAHEDRON_SECOND_STELLATION, CUBOCTAHEDRON_2_COMPOUND,
                                  DELTOIDAL_ICOSITETRAHEDRON, DISDYAKIS_DODECAHEDRON, DODECAHEDRON, DODECAHEDRON_2_COMPOUND, GREAT_DODECAHEDRON, GREAT_STELLATED_DODECAHEDRON, HEXAGONAL_PRISM,
                                  ICOSAHEDRON, ICOSAHEDRON_2_COMPOUND, ICOSIDODECAHEDRON, ICOSIDODECAHEDRON_SECOND_STELLATION, OCTAHEDRON, OCTAHEDRON_2_COMPOUND, OCTAHEDRON_3_COMPOUND, OCTAHEDRON_4_COMPOUND, OCTAHEDRON_5_COMPOUND,
                                  PENTAGONAL_ICOSITETRAHEDRON, PENTAGONAL_BIPYRAMID, PENTAGONAL_PRISM, PENTAKIS_DODECAHEDRON,
                                  RHOMBI_CUBOCTAHEDRON, RHOMBIC_DODECAHEDRON, RHOMBIC_DODECAHEDRON_FIRST_STELLATION, RHOMBIC_DODECAHEDRON_SECOND_STELLATION, RHOMBIC_DODECAHEDRON_2_COMPOUND, RHOMBICOSIDODECAHEDRON,
                                  SMALL_STELLATED_DODECAHEDRON, SMALL_TRIAMBIC_ICOSAHEDRON, SNUB_CUBE, STELLATED_OCTAHEDRON, RHOMBIC_TRIACONTAHEDRON, TETRAHEDRON, TETRAHEDRON_4_COMPOUND,
                       TETRAKIS_HEXAHEDRON, TETRAKIS_HEXAHEDRON_SECOND_STELLATION, TRIAKIS_ICOSAHEDRON, TRIAKIS_ICOSAHEDRON_FIRST_STELLATION, TRIAKIS_OCTAHEDRON, TRIAKIS_OCTAHEDRON_FIRST_STELLATION, TRIAKIS_OCTAHEDRON_SECOND_STELLATION, TRIAKIS_TETRAHEDRON, TRIAKIS_TETRAHEDRON_FIRST_STELLATION, TRIAKIS_TETRAHEDRON_SECOND_STELLATION, TRIANGULAR_BIPYRAMID, TRIANGULAR_PRISM,
                       TRUNCATED_CUBE, TRUNCATED_CUBOCTAHEDRON, TRUNCATED_DODECAHEDRON, TRUNCATED_ICOSAHEDRON, TRUNCATED_OCTAHEDRON, TRUNCATED_TETRAHEDRON};

enum class IntersectFaces {NO_INTERSECT, INTERSECT, EQUIPLANAR};

enum class AxisType {X, Y, Z};

template<class C, class T>
auto contains(const C& v, const T& x)
-> decltype(cend(v), true)
{
    return cend(v) != std::find(cbegin(v), cend(v), x);
}

template<class C, class T>
auto findIter(const C& v, const T& x) {
  return std::find(cbegin(v), cend(v), x);
}

struct Line {
  glm::dvec3 point;
  glm::dvec3 direction;
};

class PolyhedronType {
public:
  ObjectType  dual;
  std::string key;
  std::string title;
  bool        canMorph = false;
};

static std::map<ObjectType, PolyhedronType> polyhedronTypes = {
  {ObjectType::NONE, {ObjectType::NONE, "NONE", "Unknown object", false}},
  {ObjectType::CUBE, {ObjectType::OCTAHEDRON , "CUBE", "Cube", true}},
  {ObjectType::CUBE_2_COMPOUND, {ObjectType::NONE , "CUBE_2_COMPOUND", "Cube 2-compound", false}},
  {ObjectType::CUBE_3_COMPOUND, {ObjectType::NONE , "CUBE_3_COMPOUND", "Cube 3-compound", false}},
  {ObjectType::CUBE_4_COMPOUND, {ObjectType::NONE , "CUBE_4_COMPOUND", "Cube 4-compound", false}},
  {ObjectType::CUBE_5_COMPOUND, {ObjectType::NONE , "CUBE_5_COMPOUND", "Cube 5-compound", false}},
  {ObjectType::CUBOCTAHEDRON, {ObjectType::RHOMBIC_DODECAHEDRON, "CUBOCTAHEDRON", "Cuboctahedron", true}},
  {ObjectType::CUBOCTAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "CUBOCTAHEDRON_SECOND_STELLATION", "Cuboctahedron second stellation", false}},
  {ObjectType::CUBOCTAHEDRON_2_COMPOUND, {ObjectType::NONE, "CUBOCTAHEDRON_2_COMPOUND", "Cuboctahedron 2-compound", false}},
  {ObjectType::DELTOIDAL_ICOSITETRAHEDRON,{ObjectType::RHOMBI_CUBOCTAHEDRON, "DELTOIDAL_ICOSITETRAHEDRON", "Deltoidal icositetrahedron", true}},
  {ObjectType::DISDYAKIS_DODECAHEDRON, {ObjectType::TRUNCATED_CUBOCTAHEDRON, "DISDYAKIS_DODECAHEDRON", "Disdyakis dodecahedron"}},
  {ObjectType::DODECAHEDRON, {ObjectType::ICOSAHEDRON, "DODECAHEDRON", "Dodecahedron", true}},
  {ObjectType::DODECAHEDRON_2_COMPOUND, {ObjectType::NONE, "DODECAHEDRON_2_COMPOUND", "Dodecahedron 2-compound", false}},
  {ObjectType::GREAT_DODECAHEDRON, {ObjectType::NONE, "GREAT_DODECAHEDRON", "Great dodecahedron", false}},
  {ObjectType::GREAT_STELLATED_DODECAHEDRON, {ObjectType::NONE, "GREAT_STELLATED_DODECAHEDRON", "Great stellated dodecahedron", false}},
  {ObjectType::HEXAGONAL_PRISM, {ObjectType::NONE, "HEXAGONAL_PRISM", "Hexagonal prism", true}},
  {ObjectType::ICOSAHEDRON, {ObjectType::DODECAHEDRON, "ICOSAHEDRON", "Icosahedron", true}},
  {ObjectType::ICOSAHEDRON_2_COMPOUND, {ObjectType::NONE, "ICOSAHEDRON_2_COMPOUND", "Icosahedron 2-compound", false}},
  {ObjectType::ICOSIDODECAHEDRON, {ObjectType::RHOMBIC_TRIACONTAHEDRON, "ICOSIDODECAHEDRON", "Icosidodecahedron", true}},
  {ObjectType::ICOSIDODECAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "ICOSIDODECAHEDRON_SECOND_STELLATION", "Icosidodecahedron second stellation", false}},
  {ObjectType::OCTAHEDRON, {ObjectType::CUBE, "OCTAHEDRON", "Octahedron", true}},
  {ObjectType::OCTAHEDRON_2_COMPOUND, {ObjectType::NONE, "OCTAHEDRON_2_COMPOUND", "Octahedron 2-compound", false}},
  {ObjectType::OCTAHEDRON_3_COMPOUND, {ObjectType::NONE, "OCTAHEDRON_3_COMPOUND", "Octahedron 3-compound", false}},
  {ObjectType::OCTAHEDRON_4_COMPOUND, {ObjectType::NONE, "OCTAHEDRON_4_COMPOUND", "Octahedron 4-compound", false}},
  {ObjectType::OCTAHEDRON_5_COMPOUND, {ObjectType::NONE, "OCTAHEDRON_5_COMPOUND", "Octahedron 5-compound", false}},
  {ObjectType::PENTAGONAL_ICOSITETRAHEDRON, {ObjectType::SNUB_CUBE, "PENTAGONAL_ICOSITETRAHEDRON", "Pentagonal icositetrahedron", true}},
  {ObjectType::PENTAGONAL_BIPYRAMID, {ObjectType::PENTAGONAL_PRISM, "PENTAGONAL_BIPYRAMID", "Pentagonal bipyramid", true}},
  {ObjectType::PENTAGONAL_PRISM, {ObjectType::PENTAGONAL_BIPYRAMID, "PENTAGONAL_PRISM", "Pentagonal prism", true}},
  {ObjectType::PENTAKIS_DODECAHEDRON, {ObjectType::TRUNCATED_ICOSAHEDRON, "PENTAKIS_DODECAHEDRON", "Pentakis dodecahedron", true}},
  {ObjectType::RHOMBI_CUBOCTAHEDRON, {ObjectType::DELTOIDAL_ICOSITETRAHEDRON, "RHOMBI_CUBOCTAHEDRON", "Rhombicuboctahedron", true}},
  {ObjectType::RHOMBIC_DODECAHEDRON, {ObjectType::CUBOCTAHEDRON, "RHOMBIC_DODECAHEDRON", "Rhombic dodecahedron", true}},
  {ObjectType::RHOMBIC_DODECAHEDRON_FIRST_STELLATION, {ObjectType::NONE, "RHOMBIC_DODECAHEDRON_FIRST_STELLATION", "Rhombic dodecahedron first stellation", false}},
  {ObjectType::RHOMBIC_DODECAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "RHOMBIC_DODECAHEDRON_SECOND_STELLATION", "Rhombic dodecahedron second stellation", false}},
  {ObjectType::RHOMBIC_DODECAHEDRON_2_COMPOUND, {ObjectType::NONE, "RHOMBIC_DODECAHEDRON_2_COMPOUND", "Rhombic dodecahedron 2-compound", false}},
  {ObjectType::RHOMBICOSIDODECAHEDRON, {ObjectType::NONE, "RHOMBICOSIDODECAHEDRON", "Rhombicosidodecahedron", true}},
  {ObjectType::RHOMBIC_TRIACONTAHEDRON, {ObjectType::ICOSIDODECAHEDRON, "RHOMBIC_TRIACONTAHEDRON", "Rhombic triacontahedron", true}},
  {ObjectType::SMALL_STELLATED_DODECAHEDRON, {ObjectType::NONE, "SMALL_STELLATED_DODECAHEDRON", "Small stellated dodecahedron", false}},
  {ObjectType::SMALL_TRIAMBIC_ICOSAHEDRON, {ObjectType::NONE, "SMALL_TRIAMBIC_ICOSAHEDRON", "Small triambic icosahedron", false}},
  {ObjectType::SNUB_CUBE, {ObjectType::PENTAGONAL_ICOSITETRAHEDRON, "SNUB_CUBE", "Snub cube", true}},
  {ObjectType::STELLATED_OCTAHEDRON, {ObjectType::NONE, "STELLATED_OCTAHEDRON", "Stellated octahedron", false}},
  {ObjectType::TETRAHEDRON, {ObjectType::TETRAHEDRON, "TETRAHEDRON", "Tetrahedron", true}},
  {ObjectType::TETRAHEDRON_4_COMPOUND, {ObjectType::NONE, "TETRAHEDRON_4_COMPOUND", "Tetrahedron 4-compound", false}},
  {ObjectType::TETRAKIS_HEXAHEDRON, {ObjectType::TRUNCATED_OCTAHEDRON, "TETRAKIS_HEXAHEDRON", "Tetrakis hexahedron", true}},
  {ObjectType::TETRAKIS_HEXAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "TETRAKIS_HEXAHEDRON_SECOND_STELLATION", "Tetrakis hexahedron second stellation", false}},
  {ObjectType::TRIAKIS_ICOSAHEDRON, {ObjectType::TRUNCATED_DODECAHEDRON, "TRIAKIS_ICOSAHEDRON", "Triakis icosahedron", true}},
  {ObjectType::TRIAKIS_ICOSAHEDRON_FIRST_STELLATION, {ObjectType::NONE, "TRIAKIS_ICOSAHEDRON_FIRST_STELLATION", "Triakis icosahedron first stellation", false}},
  {ObjectType::TRIAKIS_OCTAHEDRON, {ObjectType::TRUNCATED_CUBE, "TRIAKIS_OCTAHEDRON", "Triakis octahedron", true}},
  {ObjectType::TRIAKIS_OCTAHEDRON_FIRST_STELLATION, {ObjectType::NONE, "TRIAKIS_OCTAHEDRON_FIRST_STELLATION", "Triakis octahedron first stellation", false}},
  {ObjectType::TRIAKIS_OCTAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "TRIAKIS_OCTAHEDRON_SECOND_STELLATION", "Triakis octahedron second stellation", false}},
  {ObjectType::TRIAKIS_TETRAHEDRON, {ObjectType::TRUNCATED_TETRAHEDRON, "TRIAKIS_TETRAHEDRON", "Triakis tetrahedron", true}},
  {ObjectType::TRIAKIS_TETRAHEDRON_FIRST_STELLATION, {ObjectType::NONE, "TRIAKIS_TETRAHEDRON_FIRST_STELLATION", "Triakis tetrahedron first stellation", false}},
  {ObjectType::TRIAKIS_TETRAHEDRON_SECOND_STELLATION, {ObjectType::NONE, "TRIAKIS_TETRAHEDRON_SECOND_STELLATION", "Triakis tetrahedron second stellation", false}},
  {ObjectType::TRIANGULAR_BIPYRAMID, {ObjectType::TRIANGULAR_PRISM, "TRIANGULAR_BIPYRAMID", "Triangular bipyramid", true}},
  {ObjectType::TRIANGULAR_PRISM, {ObjectType::TRIANGULAR_BIPYRAMID, "TRIANGULAR_PRISM", "Triangular prism", true}},
  {ObjectType::TRUNCATED_CUBE, {ObjectType::TRIAKIS_OCTAHEDRON, "TRUNCATED_CUBE", "Truncated cube", true}},
  {ObjectType::TRUNCATED_CUBOCTAHEDRON, {ObjectType::DISDYAKIS_DODECAHEDRON, "TRUNCATED_CUBOCTAHEDRON", "Truncated cuboctahedron", true}},
  {ObjectType::TRUNCATED_DODECAHEDRON, {ObjectType::TRIAKIS_ICOSAHEDRON, "TRUNCATED_DODECAHEDRON", "Truncated dodecahedron", true}},
  {ObjectType::TRUNCATED_ICOSAHEDRON, {ObjectType::PENTAKIS_DODECAHEDRON, "TRUNCATED_ICOSAHEDRON", "Truncated icosahedron", true}},
  {ObjectType::TRUNCATED_OCTAHEDRON, {ObjectType::TETRAKIS_HEXAHEDRON, "TRUNCATED_OCTAHEDRON", "Truncated octahedron", true}},
  {ObjectType::TRUNCATED_TETRAHEDRON, {ObjectType::TRIAKIS_TETRAHEDRON, "TRUNCATED_TETRAHEDRON", "Truncated tetrahedron", true}}
};

class Edge {
public:
  size_t iVertexBegin, iVertexEnd;
  // Indices of the two bordering faces
  size_t iFace1, iFace2;

  Edge();
  Edge(size_t iVertex1, size_t iVertex2, size_t iFace);
  inline void setSecondFace(size_t iFace) {
    iFace2 = iFace;
  }
};

class Polyhedron {
  struct Edges {
    std::vector<Edge> list;
    double            minLength, maxLength;
  };

public:
  bool m_isPlatonic     = false;
  bool m_isConvex       = false;

  explicit Polyhedron(ObjectType aType, const std::vector<glm::vec3>& aColorList, bool createEdges);
  explicit Polyhedron(const std::vector<glm::vec3>& aColorList, bool createEdges);
  explicit Polyhedron(ObjectType aType, bool createEdges);
  explicit Polyhedron(const Polyhedron&) = default;

  auto appendCorrectedFace(const Face& face) -> void;
  auto appendFace(const Face& face) -> void;
  auto appendFace(const std::vector<glm::dvec3>& someVertices, ushort iColor) -> void;
  static int appendFaceWithVertex(Face& face, const glm::dvec3& vertex, std::vector<glm::dvec3>& someVertices);
  static int appendFaceWithUniqueVertex(Face& face, const glm::dvec3& vertex, std::vector<glm::dvec3>& someVertices);
  auto appendFaceWithUniqueVertex(Face& face, const glm::dvec3& vertex) -> uint16_t;
  auto appendFaceWithVertex0(Face& face, const glm::dvec3& vertex) -> uint16_t;
  uint32_t appendUnique(const glm::dvec3& vertex);
  static uint16_t appendUnique(const glm::dvec3& vertex, std::vector<glm::dvec3>& someVertices);
  inline void appendVertex(const glm::dvec3& vertex) {
    m_vertices.push_back(vertex);
  }
  inline auto at(const size_t i, const glm::dvec3& vertex) -> void {
    m_vertices.at(i) = vertex;
  }
  static auto canMorph(const ObjectType anObjectType) -> bool;
  auto cleanUp() -> void;
  static auto cleanRing2D(const Ring2D& ring) -> Ring2D;
  void compress();
  static void createCornerfaces(const std::unique_ptr<Polyhedron>& source, std::unique_ptr<Polyhedron>& target, const std::vector<std::vector<uint16_t>>& indexList, ushort iColor, std::map<size_t, size_t>& pairList);
  void createFace(const uint16_t aColorIndex, const double angle, const glm::dvec3& axis, const glm::dvec3& vertex);
  inline void createFace(double angle, const glm::dvec3& axis, const glm::dvec3& vertex) {
    createFace(0, angle, axis, vertex);
  }
  static auto createRing2D(const std::vector<glm::dvec3>& face, const glm::dmat4& xyMat, AxisType axis2D) -> Ring2D;
  static auto createRing2D(const std::vector<glm::dvec3>& face, const glm::dmat4& xyMat, AxisType axis2D, Ring2D &ring) -> bool;
  static void createTriangle(std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices);
  size_t createTriangle2(const glm::dmat4x4 xyMat, AxisType axis2D, std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices);
  size_t createTriangle3(const glm::dmat4x4 xyMat, AxisType axis2D, std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices) const;
  inline static double distanceLinePoint(const glm::dvec3 linePoint1, const glm::dvec3 linePoint2, const glm::dvec3 point3) {
    return glm::length(glm::cross((point3 - linePoint1), (point3 - linePoint2))) / glm::length(linePoint2 - linePoint1);
  }
  auto faceEdgesMaxMin(size_t iFace) const -> double;
  auto faceExists(const Face& face) const -> bool;
  void fillNormals();
  static auto getClosestPointOnLine (const glm::dvec3& point, const glm::dvec3& a, const glm::dvec3& b) -> glm::dvec3;
  static auto getClosestPointOnLineDir (const glm::dvec3& vertex, const glm::dvec3& linePoint, const glm::dvec3& lineDirection) -> glm::dvec3;
  inline auto getColorList() const -> std::vector<glm::vec3> {
    return m_colorList;
  }
  auto getCornerList() const -> std::vector<size_t>;
  inline static auto getDistanceToLine(const glm::dvec3& point, const glm::dvec3& a, const glm::dvec3& b) -> double {
    return glm::distance(point, getClosestPointOnLine(point, a, b));
  }
  inline static auto getDistanceToLineDir(const glm::dvec3& point, const glm::dvec3& a, const glm::dvec3 &direction) -> double {
    return glm::distance(point, getClosestPointOnLineDir(point, a, direction));
  }
  glm::dvec3 getEdgeNormal(size_t iEdge) const;
  inline auto getIsDualCompound() const {
    return m_isDualCompound;
  }
  inline auto getMaximumLengthEdge() const {
    return m_edges.maxLength;
  }
  static auto getLinesIntersect(const glm::dvec3& vertex1, const glm::dvec3& direction1, const glm::dvec3& vertex2, const glm::dvec3& direction2) -> glm::dvec3;
  inline static auto getLinesIntersect(const Line& line1, const Line& line2) {
    return Polyhedron::getLinesIntersect(line1.point, line1.direction, line2.point, line2.direction);
  }
  inline auto getMinimumLengthEdge() const {
    return m_edges.minLength;
  }
  static auto getPlaneLineIntersection(const Plane& plane, const Line& line) -> glm::dvec3;
  static int getPlanesIntersection (const glm::dvec3& n1, const glm::dvec3& C1,
                                    const glm::dvec3& n2, const glm::dvec3& C2,
                                    glm::dvec3& P0, glm::dvec3& u);
  inline static auto getPlanesIntersection(const Plane& plane1, const Plane& plane2, Line& line) {
    auto result = getPlanesIntersection(plane1.normal, plane1.point, plane2.normal, plane2.point, line.point, line.direction);
    glm::normalize(line.direction);
    return result;
  }
  void rotateFace(const Face& face1, const glm::dmat4x4& rotation1);
  void rotateFace(const Face& face1, const glm::dmat4x4& rotation1, size_t iFaceOffset);
  inline void setConvex(bool aValue) {
    m_isConvex = aValue;
  }
  void setDualCompound();
  inline auto getEdge(size_t iEdge) const & -> Edge {
    return m_edges.list.at(iEdge);
  }
  inline auto getEdges() const & -> std::vector<Edge> {
    return m_edges.list;
  }
  inline auto getFaceCenter(size_t iFace) const -> glm::dvec3 {
    return m_faces.at(iFace).getCenter(m_vertices);
  }
  inline auto getFace(size_t iFace) const & -> Face {
    assert(iFace < m_faces.size());
    return m_faces.at(iFace);
  }
  inline auto getFaces() const & -> std::vector<Face> {
    return m_faces;
  }
  static auto getDualType(const ObjectType anObjectType) -> ObjectType;
  auto getLastColor() const -> ushort;
  static auto getName(const ObjectType anObjectType) -> std::string;
  auto getName() const -> std::string;
  auto getNameOfDual() const -> std::string;
  auto getNormal(const Face& face, const ushort index) const -> glm::dvec3;
  inline auto getNumberOfEdges() const -> size_t {
    return m_edges.list.size();
  }
  inline auto getNumberOfFaces() const -> size_t {
    return m_faces.size();
  }
  inline auto getNumberOfVertices() const -> size_t {
    return m_vertices.size();
  }
  auto getNumberOfFacesByCorners() const -> std::map<size_t, size_t>;
  auto getMaxRadius() const -> double;
  static auto getRegularType(ObjectType anObjectType) -> ObjectType;
  inline auto getType() const -> ObjectType {
    return m_type;
  }
  inline auto getVertex(size_t iVertex) const & -> glm::dvec3 {
    assert(iVertex < m_vertices.size());
    return m_vertices.at(iVertex);
  }
  inline auto getVertices() const & -> std::vector<glm::dvec3> {
    return m_vertices;
  }
  auto indexOfVector(const glm::dvec3& vertex) const -> int32_t;
  static auto indexOfVector(const std::vector<glm::dvec3>& someVertices, const glm::dvec3& vertex) -> int32_t;
  inline void push_back(const glm::dvec3 &vertex) {
    m_vertices.push_back(vertex);
  }
  static auto rotation2D(glm::dmat4 &rotation, const std::vector<glm::dvec3> &vFace) -> AxisType;
  void scale(double scale);
  void setColorFaces();
  void setColorFaces2();
  inline void setColorList(const std::vector<glm::vec3> &aColorList) {
    m_colorList = aColorList;
  }
  static auto setFaceColorIndex(std::vector<Face> &someFaces) -> ushort;
  inline void setFaceColorIndex(size_t iFace, ushort iColor) {
    m_faces[iFace].setColorIndex(iColor);
  }
  void setPlatonic(bool aValue);
  inline void setVertex(size_t iVertex, const glm::dvec3 aValue) {
    m_vertices.at(iVertex) = aValue;
  }
  inline void setType(ObjectType aValue) {
    m_type = aValue;
  }
  auto setZonoColors() -> void;
  auto transform2D(const Face& face, glm::dmat4x4& transform) const -> AxisType;
  auto transform2D2(const Face& face, glm::dmat4x4& transform) const -> AxisType;

  void createCube();
  void createCuboctahedron(uint16_t iColor3, uint16_t iColor4);
  void createDodecahedron(uint16_t iColor);
  void createIcosahedron(uint16_t iColor);
  void createIcosidodecahedron(uint16_t iColor3, uint16_t iColor5);
  void createOctahedron(uint16_t iColor);
  void createPentagonalBipyramid();
  void createRhombicDodecahedron();
  void createRhombiCuboctahedron(uint16_t iColor3, uint16_t iColor4);
  void createSnubCube();
  void createTetrahedron();
  void createTetrahedron2();
  void createTetrakisHexahedron();
  void createTriakisOctahedron();
  void createTriakisTetrahedron();
  void createTriangularBipyramid();
  void createTruncatedCube();
  void createTruncatedDodecahedron();
  void createTruncatedOctahedron();
  void createTruncatedTetrahedron();
private:
  ObjectType              m_type = ObjectType::NONE;
  std::vector<glm::vec3>  m_colorList;
  // Create a list of edges?
  bool                    m_updateEdges = false;
  std::vector<glm::dvec3> m_vertices;
  Edges                   m_edges;
  std::vector<Face>       m_faces;
  bool                    m_isDualCompound = false;

  void addMirrorFaces();
  void addMirrorVertices();
  void appendEdges(const Face& face);
  inline void appendFace(const std::vector<uint16_t>& someIndices) {
    appendFace(Face(someIndices));
  }
  void createRhombicFace(const glm::dvec3& axis, const glm::dvec3& vertex1, const glm::dvec3& vertex2);
  Face makeClosestNeighbors(const Face& face);
  void rotateFaceOnce(const Face& face1, const glm::dmat4x4& rotation);
  inline void push_back(const Edge& edge) {
    m_edges.list.push_back(edge);
  }
};
