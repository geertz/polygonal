#pragma once

#include <vulkan/vulkan.hpp>
#include <thread>
#include <mutex>

class QueueThread {
private:
  vk::Queue  m_queue;
  std::mutex m_mutex;
public:
  QueueThread(vk::Queue aQueue);
  void submit(const vk::SubmitInfo &info, vk::Fence fence);
  void waitIdle();
};
