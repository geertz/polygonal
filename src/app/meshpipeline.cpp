#include "meshpipeline.h"

MeshPipeline::MeshPipeline(GraphicsResource* pGraphics) : MeshPipelineBase(pGraphics, "shaders/vert_mesh2.spv", "shaders/frag_mesh2.spv", "Mesh pipeline") {
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex, sizeof(UniformBufferObject)}
  };
  createDescriptorSetLayoutUB(bindings);
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

  createPipeline(vertexInputInfo, nullptr);
}

auto MeshPipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  m_ubo.alpha    = m_alpha;
  m_ubo.ambient  = m_ambient;
  m_ubo.lightDir = m_lightDirection;
  m_ubo.model    = m_model;
  m_ubo.projView = m_projView;
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

void MeshPipeline::fillBuffers(const std::unique_ptr<GltfModel>& model) {
  MeshPipelineBase::fillBuffers(model);
  bool next = true;
  m_vertices.clear();
  while (next) {
    Vertex v;
    v.color = glm::vec3(1.0f);
    if (m_vertices.empty())
      next = model->getFirstVertex(v.position, v.normal);
    else
      next = model->getNextVertex(v.position, v.normal);
    if (next)
      m_vertices.push_back(v);
  }
}

std::vector<vk::VertexInputAttributeDescription> MeshPipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)},
    {2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color)},
  };
  return attributeDescriptions;
}

void MeshPipeline::updateVertexBuffer() {
  assert(m_pipelineResource);
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  if (!m_indices.empty())
    m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
}
