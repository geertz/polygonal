#include "meshmaterialpipeline.h"

MeshMaterialPipeline::MeshMaterialPipeline(GraphicsResource* pGraphics) : MeshPipelineBase(pGraphics, "shaders/vert_mesh1.spv", "shaders/frag_mesh1.spv", "Mesh material pipeline") {
  if (!m_pipelineResource)
    return;
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, sizeof(UniformBufferObject)},
    {1, vk::ShaderStageFlagBits::eFragment, sizeof(glm::vec3)}
  };
  createDescriptorSetLayoutUB(bindings);
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

  vk::PushConstantRange pushRange;
  pushRange.offset = 0;
  pushRange.size = sizeof(GraphicsPipelineResource::PushPBR);
  pushRange.stageFlags = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
  createPipeline(vertexInputInfo, &pushRange);
}

auto MeshMaterialPipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  m_ubo.model = m_model;
  m_ubo.projView = m_projView;
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  auto bufferLights = m_pipelineResource->getUniformBufferLights(iImage);
  {
    const auto error = bufferLights->copyData(&m_lightDirection, sizeof(m_lightDirection));
    if (!error.empty()) {
      setError(std::move(error));
      return false;
    }
  }
  return true;
}

void MeshMaterialPipeline::fillBuffers(const std::unique_ptr<GltfModel>& model) {
  if (m_meshesNext.empty())
    return;
  assert(m_pipelineResource);
  MeshPipelineBase::fillBuffers(model);
  bool next = true;
  m_vertices.clear();
  while (next) {
    Vertex v;
    if (m_vertices.empty())
      next = model->getFirstVertex(v.position, v.normal);
    else
      next = model->getNextVertex(v.position, v.normal);
    if (next)
      m_vertices.push_back(v);
  }
}

std::vector<vk::VertexInputAttributeDescription> MeshMaterialPipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)}
  };
  return attributeDescriptions;
}

void MeshMaterialPipeline::updateVertexBuffer() {
  assert(m_pipelineResource);
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  if (!m_indices.empty())
    m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
}
