#include "transformsolid.h"
#include "dualcompound.h"
#include "dualpolyhedron.h"
#include "expandedpolyhedron.h"
#include "regularpolyhedron.h"
#include "truncatedpolyhedron.h"
#include "zonohedron.h"
#include "compoundpolyhedron.h"
#include "stellatedpolyhedron.h"

auto TransformSolid::transform(const std::unique_ptr<Polyhedron>& solid, PolyhedronTransformation action, size_t count) -> Polyhedron* {
  switch (action) {
  case PolyhedronTransformation::DUAL: {
    return TransformSolid::dual(solid);
  }
  case PolyhedronTransformation::DUAL_COMPOUND: {
    return TransformSolid::makeDualCompound(solid);
  }
  case PolyhedronTransformation::EXPAND: {
    return TransformSolid::expand(solid);
  }
  case PolyhedronTransformation::PLATONIC_2_COMPOUND: {
    return TransformSolid::makePlatonic2Compound(solid);
  }
  case PolyhedronTransformation::REGULAR: {
    return TransformSolid::regular(solid);
  }
  case PolyhedronTransformation::STELLATION: {
    return TransformSolid::stellation(solid, count);
  }
  case PolyhedronTransformation::TRUNCATE: {
    return TransformSolid::truncate(solid);
  }
  case PolyhedronTransformation::ZONO: {
    return TransformSolid::zono(solid);
  }
  default:
    return nullptr;
  }
}

auto TransformSolid::dual(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<DualPolyhedron> d = std::make_unique<DualPolyhedron>(solid);
  return d->m_target.release();
}

auto TransformSolid::expand(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<ExpandedPolyhedron> e = std::make_unique<ExpandedPolyhedron>(solid);
  e->m_target->m_isPlatonic = false;
  return e->m_target.release();
}

auto TransformSolid::makeDualCompound(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<DualCompound> d = std::make_unique<DualCompound>(solid);
  d->m_target->m_isConvex = false;
  d->m_target->m_isPlatonic = false;
  return d->m_target.release();
}

auto TransformSolid::makePlatonic2Compound(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  if (!solid->m_isPlatonic)
    return nullptr;
  if (solid->getNumberOfEdges() == 0)
    return nullptr;
  std::unique_ptr<CompoundPolyhedron> c = std::make_unique<CompoundPolyhedron>(solid->getColorList());
  return c->createPlatonic2Compound(solid);
}

auto TransformSolid::regular(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<RegularPolyhedron> r = std::make_unique<RegularPolyhedron>(solid);
  r->m_target->m_isPlatonic = false;
  return r->m_target.release();
}

auto TransformSolid::stellation(const std::unique_ptr<Polyhedron>& solid, int count) -> Polyhedron* {
  std::unique_ptr<StellatedPolyhedron> s = std::make_unique<StellatedPolyhedron>(solid);
  while (count > 0) {
    s->createStellation(solid);
    count--;
  }
  return s->m_target.release();
}

auto TransformSolid::truncate(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<TruncatedPolyhedron> t = std::make_unique<TruncatedPolyhedron>(solid);
  t->m_target->m_isPlatonic = false;
  return t->m_target.release();
}

auto TransformSolid::zono(const std::unique_ptr<Polyhedron>& solid) -> Polyhedron* {
  std::unique_ptr<Zonohedron> z = std::make_unique<Zonohedron>(solid);
  z->m_target->m_isPlatonic = false;
  return z->m_target.release();
}
