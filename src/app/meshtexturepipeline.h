#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "meshpipelinebase.h"
#include "gltfmodel.h"

class MeshTexturePipeline : public MeshPipelineBase {
public:
  explicit MeshTexturePipeline(GraphicsResource* graphics);
  auto copyUBO(uint32_t iImage) -> bool;
  void fillBuffers(const std::unique_ptr<GltfModel>& model);
  void updateVertexBuffer();
private:
  // To be used with shader <mesh.vert>
  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
    glm::vec3 color;
  };
  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 projView;
    glm::vec3 lightDir;
    float     ambient;
    float     alpha;
  };
  UniformBufferObject m_ubo;
  std::vector<Vertex> m_vertices;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};

