#include "morphedge.h"

glm::dvec3 MorphEdge::calculateFaceNormal(const std::unique_ptr<Polyhedron>& origin, const Edge& edge) {
  const auto& face1 = origin->getFace(edge.iFace1);
  const auto& face2 = origin->getFace(edge.iFace2);
  return glm::normalize(face1.normal + face2.normal);
}

MorphEdge::MorphEdge(const std::unique_ptr<Polyhedron>& origin) {
  m_colorList = origin->getColorList();
  m_originalVertices = origin->getVertices();
  m_originalFaces = origin->getFaces();
  m_originalEdges = origin->getEdges();
  m_faceVertexList.resize(origin->getNumberOfFaces());

  std::vector<size_t> faceVertices{};
  // Start-faces : Just copy the original faces
  m_vertices[0] = m_originalVertices;
  m_faces[0] = m_originalFaces;
  double minEdgeDistance, maxEdgeDistance;
  getDistanceCenterToEdge(minEdgeDistance, maxEdgeDistance);
  uint16_t lastColor = 0;
  // Create end-faces based on the original faces
  createEndFaces(origin, minEdgeDistance, faceVertices, lastColor);
  // Make a list of the edges for each original vertex
  const auto nOriginalEdges = m_originalEdges.size();
  std::vector<std::vector<size_t>> vertexEdges;
  for (size_t iVertex = 0; iVertex < m_originalVertices.size(); iVertex++) {
    std::vector<size_t> edgeList;
    for (size_t iEdge = 0; iEdge < nOriginalEdges; iEdge++) {
      const auto& originalEdge = m_originalEdges[iEdge];
      if (originalEdge.iVertexBegin == iVertex || originalEdge.iVertexEnd == iVertex) {
        edgeList.push_back(iEdge);
      }
    }
    vertexEdges.push_back(edgeList);
  }
  // Fill a list with the plane for each
  if (origin->m_isPlatonic) {
    fillEndPlanes(faceVertices);
    for (size_t iEdge = 0; iEdge < nOriginalEdges; iEdge++) {
      const auto& originalEdge = m_originalEdges.at(iEdge);
      Face startFace(lastColor + 1);
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
      // Build end face
      auto endFace = buildEndFace(iEdge, vertexEdges);
      assert(startFace.getCorners() == endFace.getCorners());
      const auto normal = calculateFaceNormal(origin, originalEdge);
      startFace.normal = normal;
      m_faces[0].push_back(startFace);
      endFace.normal = normal;
      m_faces[1].push_back(endFace);
    }
  } else {
    for (size_t iEdge = 0; iEdge < nOriginalEdges; iEdge++) {
      buildFaces(origin, iEdge, vertexEdges, lastColor + 1);
    }
  }
}
/**
 * @brief MorphEdge::buildFaces : create start and end face for each edge of the original solid
 * @param origin : The original solid
 * @param iEdge  : The current edge of the original solid
 * @param vertexEdges
 * @param iColor
 */
void MorphEdge::buildFaces(const std::unique_ptr<Polyhedron>& origin, size_t iEdge, const std::vector<std::vector<size_t>>& vertexEdges, uint16_t iColor) {
  Face endFace;
  glm::dvec3 vertex1, vertex2;

  assert(iEdge <= m_originalEdges.size());
  const auto& originalEdge = m_originalEdges.at(iEdge);
  Face startFace(iColor);
  // Get the edges linked the begin of the current edge <iEdge1>
  auto edgeListBegin = vertexEdges[originalEdge.iVertexBegin];
  // Remove the current edge
  removeFromVector(edgeListBegin, iEdge);
  // Get the intersecting vertices of the neighboring edges
  auto edgeBeginVertices = getEdgeVertices(origin, iEdge, edgeListBegin);
  // Part 1
  // start face
  startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
  // end face
  auto& vertexList1 = m_faceVertexList.at(originalEdge.iFace1);
  auto it1 = vertexList1.find(static_cast<uint16_t>(originalEdge.iVertexBegin));
  assert(it1 != vertexList1.end());
  vertex1 = m_vertices[1].at(it1->second);
  endFace.push_back(it1->second);
  // Part 2
  auto nVertices = edgeBeginVertices.size();
  if (nVertices == 1) {
    // start face
    startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
    // end face
    Polyhedron::appendFaceWithUniqueVertex(endFace, edgeBeginVertices.front(), m_vertices[1]);
  } else {
    for (size_t i = 0; i < nVertices; i++) {
      // Get the vertex nearest to <vertex1>
      double minDistance = -1;
      for (const auto& v : edgeBeginVertices) {
        const auto distance = glm::distance(v, vertex1);
        if (minDistance < 0 || distance < minDistance) {
          vertex2 = v;
          minDistance = distance;
        }
      }
      auto it = std::find(edgeBeginVertices.begin(), edgeBeginVertices.end(), vertex2);
      if (it != edgeBeginVertices.end())
        edgeBeginVertices.erase(it);
      // start face
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
      // end face
      Polyhedron::appendFaceWithVertex(endFace, vertex2, m_vertices[1]);
    }
  }
  // Part 3
  startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexBegin));
  auto& vertexList2 = m_faceVertexList.at(originalEdge.iFace2);
  auto it2begin = vertexList2.find(static_cast<uint16_t>(originalEdge.iVertexBegin));

  assert(it2begin != vertexList2.end());
  endFace.push_back(it2begin->second);

  // Get the edges linked the end of the current edge <iEdge1>
  auto edgeListEnd = vertexEdges[originalEdge.iVertexEnd];
  // Remove the current edge
  removeFromVector(edgeListEnd, iEdge);
  // Get the intersecting vertices of the neighboring edges
  auto edgeEndVertices = getEdgeVertices(origin, iEdge, edgeListEnd);
  // Part 1
  // start face
  startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
  // end face
  auto it2end = vertexList2.find(static_cast<uint16_t>(originalEdge.iVertexEnd));

  assert(it2end != vertexList2.end());
  vertex1 = m_vertices[1].at(it2end->second);
  endFace.push_back(it2end->second);
  // Part 2
  nVertices = edgeEndVertices.size();
  if (nVertices == 1) {
    // start face
    startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
    // end face
    Polyhedron::appendFaceWithUniqueVertex(endFace, edgeEndVertices.front(), m_vertices[1]);
  } else {
    for (size_t i = 0; i < nVertices; i++) {
      // Get the vertex nearest to <vertex1>
      double minDistance = -1;
      for (const auto &v : edgeEndVertices) {
        const auto distance = glm::distance(v, vertex1);
        if (minDistance < 0 || distance < minDistance) {
          vertex2 = v;
          minDistance = distance;
        }
      }
      auto it = std::find(edgeEndVertices.begin(), edgeEndVertices.end(), vertex2);
      if (it != edgeEndVertices.end())
        edgeEndVertices.erase(it);
      // start face
      startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
      // end face
      Polyhedron::appendFaceWithVertex(endFace, vertex2, m_vertices[1]);
    }
  }
  // Part 3
  startFace.push_back(static_cast<uint16_t>(originalEdge.iVertexEnd));
  auto it1End = vertexList1.find(static_cast<uint16_t>(originalEdge.iVertexEnd));

  const auto normal = calculateFaceNormal(origin, originalEdge);
  startFace.normal = normal;
  assert(it1End != vertexList1.end());
  endFace.push_back(it1End->second);
  assert(startFace.getCorners() == endFace.getCorners());
  appendFace(0, startFace);
  endFace.normal = normal;
  appendFace(1, endFace);
}

Face MorphEdge::buildEndFace(size_t iEdge1, const std::vector<std::vector<size_t>>& vertexEdges) {
  Face endFace;
  std::vector<size_t> endEdgesBegin, endEdgesEnd;
  std::unordered_map<size_t, Line> endLinesBegin, endLinesEnd;

  const auto& originalEdge1 = m_originalEdges.at(iEdge1);
  // Get the edges linked the begin-vertex of the current edge <iEdge1>
  auto edgeListBegin = vertexEdges.at(originalEdge1.iVertexBegin);
  // Remove the current edge
  removeFromVector(edgeListBegin, iEdge1);
  // Get the edges linked the end-vertex of the current edge <iEdge1>
  auto edgeListEnd = vertexEdges[originalEdge1.iVertexEnd];
  // Remove the current edge
  removeFromVector(edgeListEnd, iEdge1);
  //edgeListEnd.removeAll(iEdge1);
  getEdgesLines(edgeListBegin, iEdge1, endEdgesBegin, endLinesBegin);
  getEdgesLines(edgeListEnd, iEdge1, endEdgesEnd, endLinesEnd);
  // Determine the end vertex to start vertex S0
  // Line 1 from begin and line 2 from end
  auto iEdge2 = endEdgesBegin.front();
  auto originalEdge2 = m_originalEdges[iEdge2];
  auto line1 = endLinesBegin.at(iEdge2);
  for (const auto iEdge3 : endEdgesEnd) {
    const auto &originalEdge3 = m_originalEdges[iEdge3];
    // Check for common face
    if (originalEdge3.iFace1 == originalEdge2.iFace1 ||
        originalEdge3.iFace1 == originalEdge2.iFace2 ||
        originalEdge3.iFace2 == originalEdge2.iFace1 ||
        originalEdge3.iFace2 == originalEdge2.iFace2) {
      // Get intersecting point
      const auto line2 = endLinesEnd.at(iEdge3);
      const auto iEndVertex = getIntersectIndex(line1, line2, m_vertices[1]);
      endFace.push_back_unique(static_cast<uint16_t>(iEndVertex));
    }
  }
  // S1
  for (const auto iEdge3 : endEdgesBegin) {
    if (iEdge3 == iEdge2)
      continue;
    // Get intersecting point
    const auto line2 = endLinesBegin.at(iEdge3);
    const auto iEndVertex = getIntersectIndex(line1, line2, m_vertices[1]);
    endFace.push_back_unique(static_cast<uint16_t>(iEndVertex));
    iEdge2 = iEdge3;
    break;
  }
  // S3, S4
  originalEdge2 = m_originalEdges[iEdge2];
  line1 = endLinesBegin.at(iEdge2);
  for (const auto iEdge3 : endEdgesEnd) {
    const auto& originalEdge3 = m_originalEdges[iEdge3];
    // Check for common face
    if (originalEdge3.iFace1 == originalEdge2.iFace1 ||
        originalEdge3.iFace1 == originalEdge2.iFace2 ||
        originalEdge3.iFace2 == originalEdge2.iFace1 ||
        originalEdge3.iFace2 == originalEdge2.iFace2) {
      // Get intersecting point
      const auto line2 = endLinesEnd.at(iEdge3);
      const auto iEndVertex = static_cast<uint16_t>(getIntersectIndex(line1, line2, m_vertices[1]));
      endFace.push_back(iEndVertex);
      endFace.push_back(iEndVertex);
      iEdge2 = iEdge3;
      break;
    }
  }
  // S5
  originalEdge2 = m_originalEdges[iEdge2];
  line1 = endLinesEnd.at(iEdge2);
  for (const auto iEdge3 : endEdgesEnd) {
    if (iEdge3 == iEdge2)
      continue;
    // Get intersecting point
    const auto line2 = endLinesEnd.at(iEdge3);
    const auto iEndVertex = getIntersectIndex(line1, line2, m_vertices[1]);
    endFace.push_back_unique(static_cast<uint16_t>(iEndVertex));
  }
  endFace.push_back(endFace[0]);
  return endFace;
}

void MorphEdge::createEndFaces(const std::unique_ptr<Polyhedron>& origin, double minEdgeDistance, std::vector<size_t>& faceVertices, uint16_t &lastColor) {
  for (size_t iFace = 0; iFace < m_originalFaces.size(); iFace++) {
    const auto& originalFace = m_originalFaces.at(iFace);
    const auto iColor = originalFace.getColorIndex();
    if (iColor > lastColor)
      lastColor = iColor;
    // Get the center
    const auto center = originalFace.getCenter(m_originalVertices);
    const auto nCorners = originalFace.getCorners();
    Face endFace(lastColor);
    if (origin->m_isPlatonic) {
      // Add face center vertex to end vertices
      const auto iEndVertex = m_vertices[1].size();
      m_vertices[1].push_back(center);
      faceVertices.push_back(iEndVertex);
      endFace.assign(nCorners, static_cast<uint16_t>(iEndVertex));
    } else {
      // Substract <minEdgeDistance> from the original face to get the new end face
      for (size_t i = 0; i < nCorners; i++) {
        const auto iOriginalVertex1 = originalFace[i];
        const auto iOriginalVertex2 = originalFace[(i + 1) % nCorners];
        // Get a point and direction of the current edge
        const auto &point = m_originalVertices[iOriginalVertex1];
        const auto direction = glm::normalize(m_originalVertices[iOriginalVertex2] - point);
        // Get the distance from original face center
        const auto distance = glm::length(glm::cross(center - point, direction));
        assert(distance > minEdgeDistance * 0.9999);
        auto fraction = (distance - minEdgeDistance) / distance;
        if (fraction < 0)
          fraction = 0;
        const auto newVertex = center + fraction * (point - center);
        auto iNewVertex = Polyhedron::indexOfVector(m_vertices[1], newVertex);
        int iEndVertex;
        if (iNewVertex < 0) {
          iEndVertex = static_cast<int>(m_vertices[1].size());
          m_vertices[1].push_back(newVertex);
        } else
          iEndVertex = iNewVertex;
        endFace.push_back(static_cast<uint16_t>(iEndVertex));
        const auto pair = std::make_pair(iOriginalVertex1, iEndVertex);
        m_faceVertexList.at(iFace).insert(pair);
      }
    }
    m_faces[1].push_back(endFace);
  }
}

void MorphEdge::fillEndPlanes(const std::vector<size_t> &faceVertices) {
  m_endPlanes.clear();
  const auto nOriginalEdges = m_originalEdges.size();
  for (size_t iEdge = 0; iEdge < nOriginalEdges; iEdge++) {
    const auto& originalEdge = m_originalEdges[iEdge];
    Plane endPlane;
    const auto iEndvertex1 = faceVertices[originalEdge.iFace1];
    const auto iEndvertex2 = faceVertices[originalEdge.iFace2];
    endPlane.normal = glm::normalize(glm::cross(m_originalVertices[originalEdge.iVertexEnd] - m_originalVertices[originalEdge.iVertexBegin],
                                     m_vertices[1].at(iEndvertex2) - m_vertices[1].at(iEndvertex1)));
    endPlane.point = (m_vertices[1].at(iEndvertex1) + m_vertices[1].at(iEndvertex2)) / 2.0;
    m_endPlanes.push_back(endPlane);
  }
}

void MorphEdge::getDistanceCenterToEdge(double &minValue, double &maxValue) {
  minValue = SNAN; maxValue = SNAN;
  for (size_t iFace = 0; iFace < m_originalFaces.size(); iFace++) {
    const auto center = m_originalFaces.at(iFace).getCenter(m_originalVertices);
    for (const auto &edge : m_originalEdges) {
      if (edge.iFace1 == iFace || edge.iFace2 == iFace) {
        const auto& point = m_originalVertices[edge.iVertexBegin];
        const auto direction = glm::normalize(m_originalVertices[edge.iVertexEnd] - point);
        const auto distanceToEdge = glm::length(glm::cross(center - point, direction));
        if (isnan(minValue) || distanceToEdge < minValue)
          minValue = distanceToEdge;
        if (isnan(maxValue) || distanceToEdge > maxValue)
          maxValue = distanceToEdge;
      }
    }
  }
}

Plane MorphEdge::getEdgePlane(const std::unique_ptr<Polyhedron>& origin, size_t iEdge) const {
  const auto& edge = m_originalEdges.at(iEdge);
  const auto& vertexList = m_faceVertexList.at(edge.iFace1);
  const auto it1 = vertexList.find(static_cast<uint16_t>(edge.iVertexBegin));
  assert(it1 != vertexList.end());
  const auto iVertex = it1->second;
  Plane edgePlane;
  edgePlane.normal = origin->getEdgeNormal(iEdge);
  edgePlane.point  = m_vertices[1].at(iVertex);
  return edgePlane;
}

void MorphEdge::getEdgesLines(const std::vector<size_t>& edgeList, size_t iEdge1, std::vector<size_t>& edges, std::unordered_map<size_t, Line>& lines) {
  Line line1;

  const auto& plane1 = m_endPlanes[iEdge1];
  const auto& originalEdge1 = m_originalEdges.at(iEdge1);
  for (const auto iEdge2 : edgeList) {
    if (iEdge2 == iEdge1)
      continue;
    const auto& originalEdge2 = m_originalEdges.at(iEdge2);
    if (originalEdge2.iFace1 == originalEdge1.iFace1 ||
        originalEdge2.iFace1 == originalEdge1.iFace2 ||
        originalEdge2.iFace2 == originalEdge1.iFace1 ||
        originalEdge2.iFace2 == originalEdge1.iFace2) {
      // Two edges share a common face
      const auto& plane2 = m_endPlanes[iEdge2];
      Polyhedron::getPlanesIntersection(plane1, plane2, line1);
      edges.push_back(iEdge2);
      lines.insert(std::pair<size_t, Line>(iEdge2, line1));
    }
  }
  assert(edges.size() == 2);
}

std::vector<glm::dvec3> MorphEdge::getEdgeVertices(const std::unique_ptr<Polyhedron>& origin, size_t iEdge1, const std::vector<size_t>& edgeList) {
  std::vector<glm::dvec3> result;
  std::vector<Plane> sidePlanes, otherPlanes;

  const auto& originalEdge1 = m_originalEdges.at(iEdge1);
  // Get the truncation plan of edge <iEdge1>
  const auto edgePlane1 = getEdgePlane(origin, iEdge1);
  assert(!contains(edgeList, iEdge1));
  for (const auto iEdge2 : edgeList) {
    const auto& originalEdge2 = m_originalEdges[iEdge2];
    const auto edgePlane2 = getEdgePlane(origin, iEdge2);
    if (originalEdge2.iFace1 == originalEdge1.iFace1 ||
        originalEdge2.iFace1 == originalEdge1.iFace2 ||
        originalEdge2.iFace2 == originalEdge1.iFace1 ||
        originalEdge2.iFace2 == originalEdge1.iFace2) {
      // Two edges share a common face
      sidePlanes.push_back(edgePlane2);
    } else {
      otherPlanes.push_back(edgePlane2);
    }
  }
  assert(sidePlanes.size() == 2);
  // Determine the line of the intersection of the two 'side' planes
  Line line1;
  Polyhedron::getPlanesIntersection(sidePlanes.front(), sidePlanes.back(), line1);
  // Determine the point of the intersection of this line with the plane of the current edge
  const auto vertex1 = Polyhedron::getPlaneLineIntersection(edgePlane1, line1);
  if (!otherPlanes.empty()) {
    Polyhedron::getPlanesIntersection(edgePlane1, otherPlanes.front(), line1);
    const auto edgeCenter = (m_originalVertices[originalEdge1.iVertexBegin] + m_originalVertices[originalEdge1.iVertexEnd]) / 2.0;
    const auto distanceToPoint = glm::distance(edgeCenter, vertex1);
    const auto distanceToLine = glm::length(glm::cross(edgeCenter - line1.point, line1.direction));
    if (distanceToPoint < distanceToLine)
      result.push_back(vertex1);
    else {
      Line line2;
      Polyhedron::getPlanesIntersection(edgePlane1, sidePlanes.front(), line2);
      result.push_back(Polyhedron::getLinesIntersect(line1, line2));
      Polyhedron::getPlanesIntersection(edgePlane1, sidePlanes.back(), line2);
      result.push_back(Polyhedron::getLinesIntersect(line1, line2));
    }
  } else
    result.push_back(vertex1);
  return result;
}

int MorphEdge::getIntersectIndex(const Line& line1, const Line& line2, std::vector<glm::dvec3>& someVertices) {
  const auto vertex = Polyhedron::getLinesIntersect(line1, line2);
  auto iVertex = Polyhedron::indexOfVector(someVertices, vertex);
  if (iVertex < 0) {
    iVertex = static_cast<int>(someVertices.size());
    someVertices.push_back(vertex);
  }
  return iVertex;
}

void MorphEdge::removeFromVector(std::vector<size_t>& vector, size_t aValue) {
  for(auto it = vector.begin(); it != vector.end();) {
    if (*it == aValue)
      it = vector.erase(it);
    else
      it++;
  }
}
