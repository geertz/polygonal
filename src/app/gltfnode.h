#pragma once

#include <memory>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

class GltfNode {
public:
  GltfNode*                              parent;
  uint32_t                               index;
  std::vector<std::unique_ptr<GltfNode>> children{};
  glm::vec3                              translation{};
  glm::vec3                              scale{1.0f};
  glm::quat                              rotation{};
  int32_t                                skin = -1;
  int32_t                                mesh = -1;
  glm::mat4                              matrix;

  GltfNode(GltfNode *pParent, uint32_t nodeIndex);
  static GltfNode* findNode(const std::unique_ptr<GltfNode> &parent, uint32_t index);
  inline glm::mat4 getLocalMatrix() const {
    return glm::translate(glm::mat4(1.0f), translation) * glm::mat4(rotation) * glm::scale(glm::mat4(1.0f), scale) * matrix;
  }
  static glm::mat4 getNodeMatrix(const GltfNode *node);
};
