#include "vulkanwindow.h"
#include <stdlib.h>

VulkanWindow::VulkanWindow() {
  glfwSetErrorCallback(glfw_error);
  // Would not find the DISPLAY variable. Set set it now
  setenv("DISPLAY", ":0", 0);
  if (!glfwInit())
    return;

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
  m_active = true;
}

void VulkanWindow::glfw_error(int error_code, const char *description) {
  printf("Error %d: %s\n", error_code, description);
}

VulkanWindow::~VulkanWindow() {
  if (m_window)
    glfwDestroyWindow(m_window);
  glfwTerminate();
}

vk::UniqueSurfaceKHR VulkanWindow::createSurface(vk::Instance &instance) {
  VkSurfaceKHR _surface;
  if (glfwCreateWindowSurface( VkInstance( instance), m_window, nullptr, &_surface ) != VK_SUCCESS)
    throw std::runtime_error( "Failed to create window!" );
  vk::ObjectDestroy<vk::Instance, VULKAN_HPP_DEFAULT_DISPATCHER_TYPE> _deleter( instance );
  return vk::UniqueSurfaceKHR(vk::SurfaceKHR( _surface ), _deleter);
}

void VulkanWindow::init(int width, int height, const char *title) {
  m_window = glfwCreateWindow(width, height, title, nullptr, nullptr);
  glfwSetWindowUserPointer(m_window, this);
}

void VulkanWindow::initFullScreen(const char *title) {
  int count;
  const auto monitors = glfwGetMonitors(&count);
  const auto mode = glfwGetVideoMode(monitors[0]);

  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  m_window = glfwCreateWindow(mode->width, mode->height, title, monitors[0], nullptr);
  glfwSetWindowUserPointer(m_window, this);
}

std::vector<const char*> VulkanWindow::getRequiredExtensions() {
  uint32_t glfwExtensionCount = 0;
  const char** glfwExtensions;
  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

  std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);
#ifndef NDEBUG
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
#endif

  return extensions;
}

void VulkanWindow::getFramebufferSize(int *width, int *height) {
  glfwGetFramebufferSize(m_window, width, height);
  while (*width == 0 || *height == 0) {
    glfwGetFramebufferSize(m_window, width, height);
    glfwWaitEvents();
  }
}
