#include "morphregular.h"

MorphRegular::MorphRegular(const std::unique_ptr<Polyhedron>& origin) {
  Face startFace, endFace;
  std::vector<std::pair<uint16_t, uint16_t>> pairs;
  bool pairExists;

  m_colorList = origin->getColorList();
  const auto& originalVertices = origin->getVertices();
  const auto& originalFaces = origin->getFaces();
  auto iVertexList = std::vector<std::vector<ushort>>(originalVertices.size());
  ushort iEndVertex = 0;
  // Start with the original solid
  m_vertices[0] = originalVertices;
  ushort lastColor = 0;
  auto iFaceList = std::vector<std::vector<size_t>>(originalVertices.size());
  for (size_t iFace = 0; iFace < originalFaces.size(); iFace++) {
    const auto& originalFace = originalFaces.at(iFace);
    const auto nCorners = originalFace.getCorners();
    const auto iColor = originalFace.getColorIndex();
    if (iColor > lastColor)
      lastColor = iColor;
    startFace = Face(iColor);
    // Only need to set the color of the start faces
    endFace = Face();
    for (size_t i = 0; i < nCorners; i++) {
      // Get a point in between two original vertices
      // Pairs: 0,1 1,2 ..., corners - 1,0
      const auto iVertex1 = originalFace.at(i);
      const auto iVertex2 = originalFace[(i + 1) % nCorners];
      iFaceList[iVertex1].push_back(iFace);
      /*
       * Two faces have the same <iVertex1, iVertex2> pair.
       * Avoid adding duplicate to indexList.
       */
      if (iVertex2 > iVertex1) {
        std::pair<uint16_t, uint16_t> key{iVertex1, iVertex2};
        pairExists = contains(pairs, key);
        if (!pairExists)
          pairs.push_back(key);
      } else {
        std::pair<uint16_t, uint16_t> key = {iVertex2, iVertex1};
        pairExists = contains(pairs, key);
        if (!pairExists)
          pairs.push_back(key);
      }
      const auto vertex = (originalVertices.at(size_t(iVertex1)) + originalVertices.at(size_t(iVertex2))) / 2.0;
      // Contract: originalVertices[iVertex1] --> vertex <-- originalVertices[iVertex2]
      m_vertices[1].push_back(vertex);
      startFace.push_back(iVertex1);
      startFace.push_back(iVertex2);
      endFace.push_back(iEndVertex);
      endFace.push_back(iEndVertex);
      if (!pairExists) {
        iVertexList[iVertex1].push_back(iEndVertex);
        iVertexList[iVertex2].push_back(iEndVertex);
      }
      iEndVertex++;
    }
    assert(startFace.getCorners() == endFace.getCorners());
    // The points of the faces should be in the right order
    // No need to call 'appendFaceList'
    startFace.normal = originalFace.normal;
    appendFace(0, startFace);
    appendFace(1, endFace);
  }
  lastColor++;
  for (size_t iStartVertex = 0; iStartVertex < originalVertices.size(); iStartVertex++) {
    // Expand from originalVertices[i]
    startFace = Face(lastColor);
    endFace = Face();
    assert(iVertexList[iStartVertex].size() >= 3);
    for (auto iEndVertex : iVertexList[iStartVertex]) {
      startFace.push_back(static_cast<uint16_t>(iStartVertex));
      endFace.push_back(iEndVertex);
    }
    glm::dvec3 normal;
    for (const auto iFace : iFaceList[iStartVertex]) {
      const auto& face = origin->getFace(iFace);
      normal += face.normal;
    }
    startFace.normal = glm::normalize(normal);
    assert(startFace.getCorners() == endFace.getCorners());
    appendFaceList(m_faces[0], m_vertices[0], startFace);
    appendFaceList(m_faces[1], m_vertices[1], endFace);
  }
}
