#pragma once

#include "polyhedron.h"

class VertexFace {
public:
  ushort origin = 0;
  std::vector<glm::dvec3> vertices;
  inline void clear() {
    vertices.clear();
  }
  void push_back(const glm::dvec3 &v);
};

class CompoundPolyhedron {
  struct CompoundFace {
    const Face *oldFace;
    glm::dvec3  faceCenter;
    std::vector<VertexFace> vertexFaces;
  };
  struct IntersectData {
    ushort     firstIndex;
    glm::dvec3 vertex;
    double     factor;
  };
  struct PolyFace {
    VertexFace face = {};
    Ring2D     ring2D = {};
    double     area = 0;
    bool       skip = false;
  };
  struct MergeRing {
    Ring2D ring2D = {};
    bool   merged = false;
  };

public:
  CompoundPolyhedron(const std::vector<glm::vec3> &aColorList);
  ~CompoundPolyhedron();
  Polyhedron *createCube2Compound();
  Polyhedron *createCube3Compound();
  Polyhedron *createCube4Compound();
  Polyhedron *createCube5Compound();
  Polyhedron *createCuboctahedron2Compound();
  Polyhedron *createDodecahedron2Compound();
  Polyhedron *createIcosahedron2Compound();
  Polyhedron *createOctahedron2Compound();
  Polyhedron *createOctahedron3Compound();
  Polyhedron *createOctahedron4Compound();
  Polyhedron *createPlatonic2Compound(const std::unique_ptr<Polyhedron>& source);
  Polyhedron *createRhombicDodecahedron2Compound();
  Polyhedron *createTetrahedron4Compound();
  inline Polyhedron *getSolid() const {
    return m_target.get();
  }
private:
  std::vector<glm::vec3>      m_colorList;
  std::unique_ptr<Polyhedron> m_target;

  static std::vector<glm::dvec2> cleanClosedVec2Ring(Ring2D ring);
  static Ring2D cleanRing2D(const Ring2D& ring);
  Polyhedron *create3CompoundCubeOctahedron(const std::unique_ptr<Polyhedron>& source);
  Polyhedron *create4CompoundCubeOctahedron(const std::unique_ptr<Polyhedron>& source);
  static std::vector<Face> createCompoundFaces(const std::vector<glm::dvec3>& vertices, const std::vector<Face>& faces, const std::vector<uint32_t> rotatedIndices, ushort iColor);
  std::vector<Face> createCompoundFaces(const std::unique_ptr<Polyhedron>& source, const std::vector<uint32_t> rotatedIndices, ushort iColor);
  void createNewFace2(std::vector<glm::dvec3>& newFace, const std::vector<IntersectData> intersections, const Polygon& polygon, const Plane& secondPlane, const Plane &centerPlane) const;
  Polyhedron *createSecondCompound(const std::unique_ptr<Polyhedron>& source, const glm::dvec3 &vertex, double angle);
  void fillCompoundFaceList(const std::vector<glm::dvec3> &vertices, const std::vector<Face> &oldFaces1, const std::vector<Face> &oldFaces2, std::vector<CompoundFace> &cFaces);
  void fillMapMergedRing(const CompoundFace &cFace, const glm::dmat4 &xyMat, AxisType axis2D, std::multimap<double, MergeRing, std::greater_equal<double>> &mapRing2D);
  void getCubeVertices(const std::unique_ptr<Polyhedron> &source, const size_t iVertexBegin, const size_t iVertexMiddle, const size_t excludeFace, std::vector<uint16_t> &iVertices);
  void intersectCompounds(const std::vector<glm::dvec3>& vertices, const std::vector<std::vector<Face>>& oldFacesList);
  static void intersectPolygon(std::vector<IntersectData> &intersections, const Polygon& polygon, const Line& line1);
  void intersectSetOfFaces(const std::vector<glm::dvec3> &vertices, std::vector<std::vector<Face>>& oldFacesList);
  IntersectFaces intersectTwoFaces(const std::vector<glm::dvec3>& vertices, CompoundFace& cFace, const Face& oldFace2) const;
  static double lineDeltaT(const Line& line, const glm::dvec3& v);
  void mergeCompoundFaces(const std::vector<std::vector<CompoundFace>>& oldFaceList, std::vector<CompoundFace>& newFaces);
  void mergeFaces(const std::vector<glm::dvec3>& vertices, const Face& face1, const Face& face2, std::vector<glm::dvec3>& newFace);
  void mergePolygons(const CompoundFace& cFace);
  void mergeTwoCompounds(const CompoundFace& cFace1, const CompoundFace& cFace2, const glm::dmat4& xyMat, const glm::dmat4& xyMatInverted, const AxisType axis2D, CompoundFace& cFace12);
  Polyhedron *processRotatedVertices(const std::unique_ptr<Polyhedron>& source, const std::vector<glm::dvec3>& rotatedVertices);
  static std::vector<glm::dvec3> rotateVertices(const std::unique_ptr<Polyhedron>& source, double angle, const glm::dvec3& axis);
  static AxisType transform2D(const CompoundFace &cFace, glm::dmat4x4& transform);
  static AxisType transform2D(const std::vector<glm::dvec3>& someVertices, const glm::dvec3& faceCenter, glm::dmat4x4& transform);
  static AxisType transform2D(const Face& face, const std::vector<glm::dvec3>& vertices, glm::dmat4x4& transform);
  void updateCompoundFace(const Ring2D& ring, const glm::dmat4x4 xyMatInverted, AxisType axis2D, CompoundFace& cFace);
  int getNumberOfCommomVertices(const Ring2D& ring1, const Ring2D& ring2);
  void rebuildPolyFace(const glm::dmat4& transform3D, AxisType axis2D, PolyFace& pFace);
  void addPolyface(const PolyFace& pFace, const size_t nFaces, ushort iColor);

};

