#include "dualpolyhedron.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include <glm/gtx/normal.hpp>
#include <glm/gtx/intersect.hpp>
#include <algorithm>

DualPolyhedron::DualPolyhedron(const std::unique_ptr<Polyhedron>& source) {
  const auto& originalVertices = source->getVertices();
  // ox * x + oy * y + oz * z = r2
  // Determine the center of each original face
  m_duals = fillDuals(source);
  ObjectType dualType = Polyhedron::getDualType(source->getType());
  m_target = std::make_unique<Polyhedron>(dualType, true);
  m_target->m_isConvex = source->m_isConvex;
  m_target->setColorList(source->getColorList());
  const std::vector<Edge> &originalEdges = source->getEdges();
  for (uint16_t iVertex = 0; iVertex < originalVertices.size(); iVertex++) {
    // Determine the dual face for each original vertex
    createDualFace(iVertex, originalEdges);
  }
  m_target->setColorFaces2();
}

void DualPolyhedron::createDualFace(uint16_t iVertex, const std::vector<Edge>& originalEdges) {
  // First get the DualObjects for this new face
  // That are the DualObjects containing vertex <iVertex>
  std::deque<size_t> faceDuals{};
  const auto nDuals = m_duals.size();
  for (size_t iDual = 0; iDual < nDuals; ++iDual) {
    if (!contains(m_duals.at(iDual).iOriginalVertices, iVertex))
      continue;
    faceDuals.push_back(iDual);
  }
  assert(faceDuals.size() > 2);
  Face dualFace(0);
  // Get the vertices in the right order
  auto iDual1 = faceDuals.at(0);
  faceDuals.pop_front();
  auto& dualObj1 = m_duals.at(iDual1);
  dualObj1.iNewVertex = m_target->appendFaceWithUniqueVertex(dualFace, dualObj1.newVertex);
  // Find the next vertex
  while (!faceDuals.empty()) {
    size_t iDualFound = SIZE_MAX;
    for (size_t iDual = 0; iDual < faceDuals.size(); ++iDual) {
      const auto iDual2 = faceDuals.at(iDual);
      auto& dualObj2 = m_duals.at(iDual2);
      const auto iEdge = getCommonEdge(m_duals.at(iDual1), dualObj2, originalEdges);
      if (iEdge < SIZE_MAX) {
        iDualFound = iDual;
        dualObj2.iNewVertex = m_target->appendFaceWithUniqueVertex(dualFace, dualObj2.newVertex);
        break;
      }
    }
    assert(iDualFound  < SIZE_MAX);
    iDual1 = faceDuals.at(iDualFound);
    auto it = faceDuals.begin() + iDualFound;
    faceDuals.erase(it);
  }
  // No need to reorder the vertices of the new face
  // as is done by appendCorrectedFace(dualFace)
  m_pairVertexFaceList.insert(std::make_pair(iVertex, m_target->getNumberOfFaces()));
  m_target->appendFace(dualFace);
}

size_t DualPolyhedron::getTargetFaceFromSourceVertex(size_t iVertex) const {
  const auto it = m_pairVertexFaceList.find(iVertex);
  if (it == m_pairVertexFaceList.cend())
    return SIZE_MAX;
  return it->second;
}

// Do the original faces of <dual1> and <dual2> have a common original edge
size_t DualPolyhedron::getCommonEdge(const DualObject& dual1, const DualObject& dual2, const std::vector<Edge>& edges) {
  for (size_t iEdge = 0; iEdge < edges.size(); iEdge++) {
    const auto& edge = edges.at(iEdge);
    if (edge.iFace1 != dual1.iOriginalFace && edge.iFace2 != dual1.iOriginalFace)
      continue;
    if (edge.iFace1 == dual1.iOriginalFace) {
      if (edge.iFace2 != dual2.iOriginalFace)
        continue;
    } else {
      if (edge.iFace1 != dual2.iOriginalFace)
        continue;
    }
    return iEdge;
  }
  return SIZE_MAX;
}

std::vector<DualObject> DualPolyhedron::fillDuals(const std::unique_ptr<Polyhedron>& source) {
  std::vector<DualObject> result{};
  const auto& originalVertices = source->getVertices();
  // Assumed object is centered around (0,0,0)
  const glm::dvec3 solidCenter(0.0, 0.0, 0.0);
  // Polar reciprocation with sphere with radius <r>
  // Surface with distance <d> to center
  // is converted to a point at <r^2 / d>
  double r1_orig = 0;
  double r2_orig = 0;
  for (const auto& originalVertex : originalVertices) {
    if (glm::length2(originalVertex) > r2_orig)
      r2_orig = glm::length2(originalVertex);
    if (glm::length(originalVertex) > r1_orig)
      r1_orig = glm::length(originalVertex);
  }
  double maxRadius = 0;
  const auto nFaces = source->getNumberOfFaces();
  for (size_t iFace = 0; iFace < nFaces; iFace++) {
    const auto& face = source->getFace(iFace);
    DualObject dual;
    // Store the index of the original face
    dual.iOriginalFace = iFace;
    dual.iNewVertex = ULONG_MAX;
    dual.iOriginalVertices = {};
    // Three vertices to determine the normal of the original face (iFace)
    glm::dvec3 vertex1(0);
    glm::dvec3 vertex2(0);
    glm::dvec3 vertex3(0);
    glm::dvec3 faceVertex(0.0, 0.0, 0.0); // Direction  of the face relative to center
    int nVectors = 0;
    for (const auto iVertex : face) {
      const auto vertex = originalVertices.at(iVertex);
      faceVertex += vertex;
      // Store the vertex indices of the original face
      dual.iOriginalVertices.push_back(iVertex);
      // Get three vertices for current face to calculate the face normal
      if (nVectors < 3) {
        switch (nVectors) {
        case 0:
          vertex1 = vertex;
          break;
        case 1:
          vertex2 = vertex;
          break;
        case 2:
          vertex3 = vertex;
          break;
        default:
          break;
        }
        nVectors++;
      }
    }
    const auto faceCenter = face.getCenter(originalVertices);
    auto faceNormal = glm::triangleNormal(vertex1, vertex2, vertex3);
    double distance = 0.0;
    // Make sure the normal of the plane is towards the solid center
    if (glm::dot(solidCenter - faceCenter, faceNormal) < 0.0)
      faceNormal = -faceNormal;
    bool intersect = glm::intersectRayPlane(solidCenter, -faceNormal, faceCenter, faceNormal, distance);
    assert(intersect);
    const auto newVertex = faceNormal * r2_orig / distance;
    // Update the maximal distance from the center
    const auto radius = glm::length(newVertex);
    if (radius > maxRadius)
      maxRadius = radius;
    // n . (p1 - p2) = 0
    if (glm::dot(faceVertex, newVertex) < 0.0)
      dual.newVertex = -newVertex;
    else
      dual.newVertex = newVertex;
    assert(dual.iOriginalVertices.size() == face.getCorners());
    result.push_back(dual);
  }
  // Make the dual solid as large as the original solid
  for (auto& dual : result) {
    dual.newVertex = dual.newVertex * r1_orig / maxRadius;
  }
  return result;
}
