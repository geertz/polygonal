#include "imagepanel.h"
#include "singletimecommands.h"
#include <stb_image.h>
#include <iostream>
#include <tuple>

ImagePanel::ImagePanel(GraphicsResource *pGraphics) {
  m_graphics = pGraphics;
  m_pipelineResource = std::make_unique<GraphicsPipelineResource>(m_graphics);
  fillVertexBuffer();
}

void ImagePanel::bindCommandBuffer(const SwapchainImageResource &imageResource, size_t iImage) {
  m_pipelineResource->bindCommandBuffer(imageResource, iImage, m_viewport);
  imageResource.m_stdCommandBuffer.draw(6,1,0,0);
}

bool ImagePanel::createImage(const std::string &fileName) {
  m_textureResource.reset(new TextureResource(m_graphics, fileName.c_str(), false));
  m_textureSampler = m_graphics->getDevice()->createSampler();
  if (!m_pipelineResource->createVertexShader("shaders/vert_image.spv")) {
    m_errorMessage = std::move(m_pipelineResource->m_errorMessage);
    return false;
  }
  if (!m_pipelineResource->createFragmentShader("shaders/frag_image.spv")) {
    m_errorMessage = std::move(m_pipelineResource->m_errorMessage);
    return false;
  }
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = getBindingDescription();
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  m_pipelineResource->addSamplerDescriptorLayoutBinding(0);
  m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vertexInputInfo);
  m_pipelineResource->createUniformBuffers(0, m_textureResource->m_imageView, m_textureSampler.get());
  return true;
}

auto ImagePanel::fillVertexBuffer() -> void {
  // First triangle
  { // 0
    VertexInput vi{glm::vec2(-1.0, -1.0), glm::vec2(0.0, 0.0)};
    m_vertices.emplace_back(vi);
  }
  { // 1
    VertexInput vi{glm::vec2(-1.0, 1.0), glm::vec2(0.0, 1.0)};
    m_vertices.emplace_back(vi);
  }
  { // 2
    VertexInput vi{glm::vec2( 1.0, 1.0), glm::vec2(1.0, 1.0)};
    m_vertices.emplace_back(vi);
  }
  // Second triangle
  { // 2
    VertexInput vi{glm::vec2( 1.0, 1.0), glm::vec2(1.0, 1.0)};
    m_vertices.emplace_back(vi);
  }
  { // 3
    VertexInput vi{glm::vec2( 1.0, -1.0), glm::vec2(1.0, 0.0)};
    m_vertices.emplace_back(vi);
  }
  { // 0
    VertexInput vi{glm::vec2(-1.0, -1.0), glm::vec2(0.0, 0.0)};
    m_vertices.emplace_back(vi);
  }
}

vk::VertexInputBindingDescription ImagePanel::getBindingDescription() {
  vk::VertexInputBindingDescription bindingDescription{};
  bindingDescription.binding = 0;
  bindingDescription.stride = sizeof(VertexInput);
  bindingDescription.inputRate = vk::VertexInputRate::eVertex;
  return bindingDescription;
}

std::vector<vk::VertexInputAttributeDescription> ImagePanel::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(VertexInput, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(VertexInput, texCoord)}
  };
  return attributeDescriptions;
}

auto ImagePanel::createBuffers() -> void {
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
}

void ImagePanel::updateViewPort(uint32_t x, uint32_t width, uint32_t height) {
  m_viewport.x = x;
  m_viewport.y = 0;
  m_viewport.width = width;
  m_viewport.height = height;
  m_viewport.minDepth = 0.0f;
  m_viewport.maxDepth = 1.0f;
}
