#include "swapchainimageresource.h"

SwapchainImageResource::SwapchainImageResource() {
}

auto SwapchainImageResource::createImGuiCommandPool(LogicalDevice *device) -> bool {
  try {
    m_imGuiCommandPool = device->createCommandPool(device->getGraphicsFamily());
    return true;
  } catch (...) {
    m_errorMessage = "Failed to create ImGui command pool!";
    return false;
  }
}

void SwapchainImageResource::setViewport(const vk::Viewport &view) const {
  m_stdCommandBuffer.setViewport(0, 1, &view);

  vk::Rect2D scissor;
  scissor.offset.x = view.x;
  scissor.offset.y = view.y;
  scissor.extent.width = view.width;
  scissor.extent.height = view.height;
  m_stdCommandBuffer.setScissor(0, 1, &scissor);
}
