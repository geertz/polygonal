#include "logicaldevice.h"
#include "vulkanlib.h"
#include <numeric>

LogicalDevice::LogicalDevice(PhysicalDevice* aPhysicalDevice, vk::DeviceCreateInfo& info) {
  auto queueCreateInfos = aPhysicalDevice->createInfos();
  info.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
  info.pQueueCreateInfos = queueCreateInfos.data();
  try {
    m_device = aPhysicalDevice->m_device.createDeviceUnique(info);
    m_physicalDevice = aPhysicalDevice;
  } catch (...) {
    m_errorMessage = "Failed to create logical device!";
  }
}

auto LogicalDevice::createCommandPool(uint32_t queueFamilyIndex) const -> vk::UniqueCommandPool {
  vk::CommandPoolCreateInfo info{};
  info.queueFamilyIndex = queueFamilyIndex;
  info.flags = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
  return m_device->createCommandPoolUnique(info);
}

auto LogicalDevice::createDescriptorPool(const std::vector<vk::DescriptorPoolSize>& poolSizes) -> vk::UniqueDescriptorPool {
  assert(!poolSizes.empty() );
  uint32_t maxSets = std::accumulate( poolSizes.begin(), poolSizes.end(), 0, []( uint32_t sum, vk::DescriptorPoolSize const & dps ) {
      return sum + dps.descriptorCount;
} );
  assert( 0 < maxSets );

  vk::DescriptorPoolCreateInfo info(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet, maxSets, poolSizes.size(), poolSizes.data());
  return m_device->createDescriptorPoolUnique(info);
}

auto LogicalDevice::createFence(vk::FenceCreateFlagBits flags) const -> vk::UniqueFence {
  vk::FenceCreateInfo fenceCreateInfo{};
  fenceCreateInfo.flags = flags;
  return m_device->createFenceUnique(fenceCreateInfo);
}

auto LogicalDevice::createFrameBuffer(vk::FramebufferCreateInfo& info, const vk::RenderPass& renderPass, vk::ImageView* pAttachments, uint32_t count) const -> vk::UniqueFramebuffer {
  info.renderPass = renderPass;
  info.attachmentCount = count;
  info.pAttachments = pAttachments;
  return m_device->createFramebufferUnique(info);
}

auto LogicalDevice::createPipelineLayout(const std::vector<vk::DescriptorSetLayout>& layouts, vk::PushConstantRange* pPushConstantRange) const ->vk::UniquePipelineLayout {
  vk::PipelineLayoutCreateInfo info{};
  info.setLayoutCount = layouts.size();
  info.pSetLayouts = layouts.data();
  if (pPushConstantRange) {
    info.pushConstantRangeCount = 1;
    info.pPushConstantRanges = pPushConstantRange;
  } else {
    info.pushConstantRangeCount = 0;
    info.pPushConstantRanges = nullptr;
  }

  return m_device->createPipelineLayoutUnique(info);
}

auto LogicalDevice::createSampler(vk::SamplerAddressMode mode) const -> vk::UniqueSampler {
  vk::SamplerCreateInfo info{};
  info.pNext        = nullptr;
  info.flags        = vk::SamplerCreateFlags(0);
  info.magFilter    = vk::Filter::eLinear;
  info.minFilter    = vk::Filter::eLinear;
  info.mipmapMode   = vk::SamplerMipmapMode::eNearest;
  info.addressModeU     = mode;
  info.addressModeV     = mode;
  info.addressModeW     = mode;
  info.mipLodBias       = 0.0f;
  info.anisotropyEnable = false;
  info.maxAnisotropy    = 1.0f;
  info.compareEnable    = false;
  info.compareOp        = vk::CompareOp::eNever;
  info.minLod           = -1000.0f;
  info.maxLod           = 1000.0f;
  info.borderColor      = vk::BorderColor::eFloatTransparentBlack;
  info.unnormalizedCoordinates = false;
  return m_device->createSamplerUnique(info);
}

auto LogicalDevice::createSemaphore() const -> vk::UniqueSemaphore {
  vk::SemaphoreCreateInfo info{};
  return m_device->createSemaphoreUnique(info);
}

auto LogicalDevice::createShaderModule(const std::string& aFileName) -> ErrorValue<vk::UniqueShaderModule> {
  auto shaderCode = VulkanLib::readFile(aFileName);
  if (!shaderCode.error.empty()) {
    return ErrorValue(shaderCode.error, vk::UniqueShaderModule());
  }
  vk::ShaderModuleCreateInfo createInfo{};
  createInfo.codeSize = shaderCode.value.size();
  createInfo.pCode = reinterpret_cast<uint32_t*>(shaderCode.value.data());
  return ErrorValue(m_device->createShaderModuleUnique(createInfo));
}
