#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "meshpipelinebase.h"
#include "gltfmodel.h"

class MeshTexSkinPipeline : public MeshPipelineBase {
public:
  explicit MeshTexSkinPipeline(GraphicsResource* graphics);
  auto copyUBO(uint32_t iImage, const std::unique_ptr<GltfModel>& model) -> bool;
  void fillBuffers(const std::unique_ptr<GltfModel>& model);
  inline void setEye(const glm::vec3& aValue) {
    m_ubo.eye = aValue;
  }
  void updateVertexBuffer();
private:
  // To be used with shader <mesh.vert>
  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
    glm::vec3 color;
    glm::vec4 jointIndices;
    glm::vec4 jointWeights;
  };
  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 projView;
    glm::vec3 eye;
    float     ambient;
    glm::vec3 lightDir;
    float     alpha;
  };
  UniformBufferObject m_ubo;
  std::vector<Vertex> m_vertices;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};

