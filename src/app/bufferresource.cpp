#include "bufferresource.h"

BufferResource::BufferResource(vk::Device aDevice) {
  m_device = aDevice;
}

void BufferResource::cmdCopyBuffer(vk::CommandBuffer &commandBuffer, const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, vk::DeviceSize size) {
  vk::BufferCopy copyRegion{};
  copyRegion.srcOffset = 0; // Optional
  copyRegion.dstOffset = 0; // Optional
  copyRegion.size = size;
  commandBuffer.copyBuffer(srcBuffer, dstBuffer, 1, &copyRegion);
}

auto BufferResource::copyData(void *source, vk::DeviceSize size) -> std::string {
  try {
    auto dataObject = mapMemory(size);
    memcpy(dataObject, source, size);
    unmapMemory();

    return "";
  } catch (...) {
    return "Failed to map memory!";
  }
}
