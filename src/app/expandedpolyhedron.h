#pragma once

#include "polyhedron.h"

class ExpandedPolyhedron {
  struct ExpandObject {
    std::vector<size_t> newVertexIndices;
  };

public:
  std::unique_ptr<Polyhedron> m_target;
  ExpandedPolyhedron(const std::unique_ptr<Polyhedron>&);
  ExpandedPolyhedron(const ExpandedPolyhedron&) = delete; // No copy needed
  size_t getTargetFaceFromSourceEdge(size_t iEdgeSource) const;
  size_t getTargetFaceFromSourceFace(size_t iFaceSource) const;
  size_t getTargetFaceFromSourceVertex(size_t iVertexSource) const;
  inline const std::vector<uint16_t>getTargetVertices(size_t iVertexSource) const {
    return m_vertexSourceTarget.at(iVertexSource);
  }
private:
  // Link the original face with the expoanded face
  std::map<size_t,size_t> m_pairFaceFaceList;
  // Link the original vertex with the expanded face
  std::map<size_t,size_t> m_pairVertexFaceList;
  std::map<size_t,size_t> m_pairEdgeFaceList;
  // Store the indices of the new vertices related to the original one
  std::vector<std::vector<uint16_t>> m_vertexSourceTarget;

  double calculateMultiplier(const std::unique_ptr<Polyhedron>&);
  void addRectangles(const std::unique_ptr<Polyhedron>& source, size_t iFace, const std::vector<ExpandObject>&, uint16_t iColor);
};
