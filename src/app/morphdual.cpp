#include "morphdual.h"

// See https://en.wikipedia.org/wiki/Dual_polyhedron
MorphDual::MorphDual(const std::unique_ptr<Polyhedron>& origin) {
  m_colorList = origin->getColorList();
  m_solidType = origin->getType();
  const auto duals = DualPolyhedron::fillDuals(origin);
  // Start with the original solid
  m_vertices[0] = origin->getVertices();
  auto iLastColor = contractToDualPoint(duals, origin->getFaces(), 0);
  iLastColor++;
  // Faces starting at the <startVertices> and expanding
  const auto nVertices = m_vertices[0].size();
  const auto nDuals = duals.size();
  for (size_t iStartVertex = 0; iStartVertex < nVertices; iStartVertex++) {
    // Determine the dual face for each original vertex
    // Gather the duals which contain <iStartVertex>
    std::vector<size_t> vertexDuals;
    for (size_t iDual = 0; iDual < nDuals; iDual++) {
      if (contains(duals.at(iDual).iOriginalVertices, iStartVertex))
        vertexDuals.push_back(iDual);
    }
    Face startFace, endFace;
    startFace.setColorIndex(iLastColor);
    ushort nStart = 0;
    glm::dvec3 normal;
    std::vector<size_t> excludeDuals;
    auto iDual1 = vertexDuals.front();
    while (iDual1 != ULONG_MAX) {
      const auto& dual1 = duals.at(iDual1);
      nStart++;
      const auto& oldFace = origin->getFace(dual1.iOriginalFace);
      normal += oldFace.normal;
      const auto iEndVertex = Polyhedron::indexOfVector(m_vertices[1], dual1.newVertex);
      if (iEndVertex >= 0) {
        endFace.push_back_unique(static_cast<uint16_t>(iEndVertex));
      } else {
        endFace.push_back_unique(static_cast<uint16_t>(m_vertices[1].size()));
        m_vertices[1].push_back(dual1.newVertex);
      }
      excludeDuals.push_back(iDual1);
      size_t iDual2;
      findNextDual(origin, duals, vertexDuals, iDual1, iDual2, excludeDuals);
      iDual1 = iDual2;
    }
    startFace.assign(nStart, static_cast<uint16_t>(iStartVertex));
    assert(startFace.getCorners() == endFace.getCorners());
    startFace.normal = glm::normalize(normal);
    appendFace(0, startFace);
    appendFace(1, endFace);
  }
  iLastColor++;
  // Find common edges
  makeMorphEdges(origin, duals, iLastColor);
}

void MorphDual::findNextDual(const std::unique_ptr<Polyhedron>& origin, const std::vector<DualObject>& duals, const std::vector<size_t>& vertexDuals, size_t iPrevDual, size_t& iNextDual, const std::vector<size_t>& excludeDuals) {
  iNextDual = ULONG_MAX;
  const auto& edges = origin->getEdges();
  const auto& dualPrev = duals.at(iPrevDual);
  for (const auto iDual : vertexDuals) {
    if (contains(excludeDuals, iDual))
      continue;
    const auto& dual = duals.at(iDual);
    bool found = false;
    for (const auto& edge : edges) {
      if ((edge.iFace1 == dualPrev.iOriginalFace &&
           edge.iFace2 == dual.iOriginalFace) ||
          (edge.iFace2 == dualPrev.iOriginalFace &&
           edge.iFace1 == dual.iOriginalFace)){
        iNextDual = iDual;
        found = true;
        break;
      }
    }
    if (found)
      break;
  }
}

ushort MorphDual::contractToDualPoint(const std::vector<DualObject>& duals, const std::vector<Face>& originalFaces, ushort iColor) {
  auto iLastColor = iColor;

  uint16_t iEndVertex = m_vertices[1].size();
  for (const auto& dual : duals) {
    const auto& oldFace = originalFaces.at(dual.iOriginalFace);
    Face startFace(0), endFace(0);
    const auto startColorIndex = iColor + originalFaces.at(dual.iOriginalFace).getColorIndex();
    if (startColorIndex > iLastColor)
      iLastColor = static_cast<ushort>(startColorIndex);
    startFace.setColorIndex(static_cast<ushort>(startColorIndex));
    // Process the elements of the original face
    for (const auto iStartVertex : dual.iOriginalVertices) {
      startFace.push_back_unique(static_cast<uint16_t>(iStartVertex));
      m_vertices[1].push_back(dual.newVertex);
      endFace.push_back_unique(iEndVertex);
      iEndVertex++;
    }
    assert(startFace.getCorners() == endFace.getCorners());
    startFace.normal = oldFace.normal;
    appendFace(0, startFace);
    appendFace(1, endFace);
  }
  return iLastColor;
}

void MorphDual::makeMorphEdges(const std::unique_ptr<Polyhedron>& origin, const std::vector<DualObject>& duals, ushort iColor) {
  auto iEndVertex = m_vertices[1].size();
  const auto numberOfDuals = duals.size();
  for (size_t iDual1 = 0; iDual1 < numberOfDuals; iDual1++) {
    for (auto iDual2 = iDual1 + 1; iDual2 < numberOfDuals; iDual2++) {
      bool foundFirst = false;
      bool foundSecond = false;
      size_t iStartVertex1 = SIZE_MAX;
      size_t iStartVertex2 = SIZE_MAX;
      for (const auto iVertex1 : duals[iDual1].iOriginalVertices) {
        for (const auto iVertex2 : duals[iDual2].iOriginalVertices) {
          if (iVertex2 != iVertex1)
            continue;
          // Found a common original vertex
          if (!foundFirst) {
            foundFirst = true;
            // Save the first one
            iStartVertex1 = iVertex1;
          } else {
            foundSecond = true;
            // Save the second one
            iStartVertex2 = iVertex1;
            // We are done. Can create a new set of faces
            break;
          }
        }
        if (foundSecond)
          break;
      }
      if (!foundSecond)
        continue;
      // Add rectangle
      m_vertices[1].push_back(duals[iDual1].newVertex);
      m_vertices[1].push_back(duals[iDual2].newVertex);
      const auto& oldFace1 = origin->getFace(duals[iDual1].iOriginalFace);
      const auto& oldFace2 = origin->getFace(duals[iDual2].iOriginalFace);
      const auto normal = glm::normalize(oldFace1.normal + oldFace2.normal);
      Face startFace(iColor);
      startFace.push_back(static_cast<uint16_t>(iStartVertex1));
      startFace.push_back(static_cast<uint16_t>(iStartVertex2));
      startFace.push_back(static_cast<uint16_t>(iStartVertex2));
      startFace.push_back(static_cast<uint16_t>(iStartVertex1));
      startFace.normal = normal;
      appendFace(0, startFace);
      Face endFace(iColor);
      endFace.push_back(static_cast<uint16_t>(iEndVertex));
      endFace.push_back(static_cast<uint16_t>(iEndVertex));
      endFace.push_back(static_cast<uint16_t>(iEndVertex + 1));
      endFace.push_back(static_cast<uint16_t>(iEndVertex + 1));
      endFace.normal = normal;
      appendFace(1, endFace);
      iEndVertex += 2;
    }
  }
}
