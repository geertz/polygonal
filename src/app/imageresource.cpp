#include "imageresource.h"
#include "vulkanlib.h"
#include <iostream>

ImageResource::ImageResource(LogicalDevice* aDevice, vk::Format format) {
  m_device = aDevice;
  m_format = format;
}

ImageResource::ImageResource(LogicalDevice* aDevice, vk::Format format, const vk::Image& anImage) {
  m_device = aDevice;
  m_format = format;
  m_image = anImage;
}

ImageResource::~ImageResource() {
  if (m_device && m_image) {
    const auto &device = m_device->getDevice();
    device.destroyImage(m_image);
  }
}

auto ImageResource::createImage(const vk::Extent2D& extent, uint32_t mipLevels, vk::SampleCountFlagBits numSamples, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties) -> bool {
  // Create an image
  vk::ImageCreateInfo imageInfo{};
  imageInfo.imageType = vk::ImageType::e2D;
  imageInfo.extent.width = extent.width;
  imageInfo.extent.height = extent.height;
  imageInfo.extent.depth = 1;
  imageInfo.mipLevels = mipLevels;
  imageInfo.arrayLayers = 1;
  imageInfo.format = m_format;
  imageInfo.tiling = tiling;
  imageInfo.initialLayout = vk::ImageLayout::eUndefined;
  imageInfo.usage = usage;
  imageInfo.samples = numSamples;
  imageInfo.sharingMode = vk::SharingMode::eExclusive;
  const auto &device = m_device->getDevice();
  try {
    m_image = device.createImage(imageInfo);
  } catch (...) {
    m_errorMessage = "Failed to create image!";
    return false;
  }
  // Allocate memory
  assert(m_image);
  auto memRequirements = device.getImageMemoryRequirements(m_image);

  vk::MemoryAllocateInfo allocInfo{};
  allocInfo.allocationSize = memRequirements.size;
  allocInfo.memoryTypeIndex = m_device->findMemoryType(memRequirements.memoryTypeBits, properties);
  try {
    m_memory = device.allocateMemoryUnique(allocInfo);
  } catch (...) {
    m_errorMessage = "Failed to allocate image memory";
    device.destroyImage(m_image);
    return false;
  }
  // Bind memory to image
  try {
    device.bindImageMemory(m_image, m_memory.get(), 0);
  } catch (...) {
    m_errorMessage = "Failed to bind image memory!";
    device.destroyImage(m_image);
    return false;
  }
  return true;
}

void ImageResource::createImageView(vk::ImageAspectFlags aspectFlags, uint32_t mipLevels) {
  assert(m_image);
  vk::ImageViewCreateInfo createInfo{};
  createInfo.image = m_image;
  createInfo.viewType = vk::ImageViewType::e2D;
  createInfo.format = m_format;
  createInfo.components.r = vk::ComponentSwizzle::eIdentity;
  createInfo.components.g = vk::ComponentSwizzle::eIdentity;
  createInfo.components.b = vk::ComponentSwizzle::eIdentity;
  createInfo.components.a = vk::ComponentSwizzle::eIdentity;
  createInfo.subresourceRange.aspectMask = aspectFlags;
  createInfo.subresourceRange.baseMipLevel = 0;
  createInfo.subresourceRange.levelCount = mipLevels;
  createInfo.subresourceRange.baseArrayLayer = 0;
  createInfo.subresourceRange.layerCount = 1;
  m_imageView = m_device->getDevice().createImageViewUnique(createInfo);
}

void ImageResource::destroyImageView() {
  if (m_imageView) {
    m_imageView.reset(nullptr);
  }
}
