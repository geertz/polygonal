#pragma once

#include <vulkan/vulkan.hpp>
#include <string>

class RenderPassCommands {
public:
  std::string m_errorMessage;

  RenderPassCommands(const vk::CommandBuffer& aBuffer, vk::CommandBufferUsageFlags usage);
  ~RenderPassCommands();
  void beginRenderPass(const vk::RenderPassBeginInfo& info);
  void nextSubpass();
private:
  bool started = false;
  vk::CommandBuffer m_buffer;
};
