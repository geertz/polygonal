#include "gltfmodel.h"
#include <glm/gtc/type_ptr.hpp>
#include "imgui/imgui.h"

GltfModel::GltfModel(const std::string& fileName) {
  tinygltf::TinyGLTF gltf_ctx;
  std::string err;
  std::string warn;

  const auto result = gltf_ctx.LoadASCIIFromFile(&m_model, &err, &warn, fileName);
  if (!warn.empty()) {
    printf("Warn: %s\n", warn.c_str());
  }

  if (!err.empty()) {
    printf("Err: %s\n", err.c_str());
  }
  if (!result)
    return;
}

auto GltfModel::loadMeshes(std::vector<Mesh>& meshes, std::vector<uint16_t>& indices, std::vector<Material>& materials, std::vector<Texture>& textures) -> void {
  assert(!m_model.meshes.empty());
  const auto& mesh = m_model.meshes.front();
  m_name = mesh.name;
  readMaterials(materials);
  readTextures(textures);
  const auto& scene = m_model.scenes[0];
  for (const auto& nodeID : scene.nodes) {
    const auto& node = m_model.nodes[nodeID];
    loadNode(node, nullptr, nodeID);
    if (node.mesh == -1 && node.skin == -1)
      m_transform = m_nodes.back()->getLocalMatrix();
  }
  // Iterate through all primitives of this node's mesh
  int iMesh = 0;
  for (const auto& glTFMesh : m_model.meshes) {
    Mesh mesh(glTFMesh.name);
    // Determine the mesh transformation
    for (const auto& node : m_nodes) {
      if (node->mesh == iMesh) {
        mesh.transformMatrices.push_back(node->getLocalMatrix());
      }
    }
    for (const auto& glTFPrimitive : glTFMesh.primitives) {
      assert(glTFPrimitive.mode == 4);
      uint32_t vertexStart = static_cast<uint32_t>(m_vertexPositions.size());
      DrawPrimitive primitive(0);
      primitive.firstIndex = static_cast<uint32_t>(indices.size());
      // Vertices
      readVertices(glTFPrimitive);
      // Indices
      readIndices(glTFPrimitive, vertexStart, primitive.indexCount, indices);
      // Material index
      primitive.materialIndex = glTFPrimitive.material;
      mesh.primitives.push_back(primitive);
    }
    meshes.push_back(mesh);
    iMesh++;
  }
}

auto GltfModel::getFirstVertex(glm::vec3& position, glm::vec3& normal) -> bool {
  m_currentPos = 0;
  if (m_vertexPositions.empty())
    return false;
  position = m_vertexPositions.at(m_currentPos);
  normal = m_vertexNormals.at(m_currentPos);
  return true;
}

auto GltfModel::getFirstVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv) -> bool {
  if (!getFirstVertex(position, normal))
    return false;
  uv = m_vertexUVs.at(m_currentPos);
  return true;
}

auto GltfModel::getFirstVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv, glm::vec4& jointIndices, glm::vec4& jointWeights) -> bool {
  if (!getFirstVertex(position, normal, uv))
    return false;
  jointIndices = m_vertexJointIndices.at(m_currentPos);
  jointWeights = m_vertexJointWeights.at(m_currentPos);
  return true;
}

auto GltfModel::getNextVertex(glm::vec3& position, glm::vec3& normal) -> bool {
  m_currentPos++;
  if (m_currentPos >= m_vertexPositions.size())
    return false;
  position = m_vertexPositions.at(m_currentPos);
  normal = m_vertexNormals.at(m_currentPos);
  return true;
}

auto GltfModel::getNextVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv) -> bool {
  if (!getNextVertex(position, normal))
    return false;
  uv = m_vertexUVs.at(m_currentPos);
  return true;
}

auto GltfModel::getNextVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv, glm::vec4& jointIndices, glm::vec4& jointWeights) -> bool {
  if (!getNextVertex(position, normal, uv))
    return false;
  jointIndices = m_vertexJointIndices.at(m_currentPos);
  jointWeights = m_vertexJointWeights.at(m_currentPos);
  return true;
}

void GltfModel::readIndices(const tinygltf::Primitive& glTFPrimitive, uint32_t vertexStart, uint32_t &indexCount, std::vector<uint16_t>& indices) {
  const auto& accessor = m_model.accessors[glTFPrimitive.indices];
  const auto& bufferView = m_model.bufferViews[accessor.bufferView];
  const auto& buffer = m_model.buffers[bufferView.buffer];

  indexCount += static_cast<uint32_t>(accessor.count);
  // glTF supports different component types of indices
  switch (accessor.componentType) {
  case TINYGLTF_PARAMETER_TYPE_UNSIGNED_INT: {
    uint32_t* buf = new uint32_t[accessor.count];
    memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint32_t));
    for (size_t index = 0; index < accessor.count; index++) {
      indices.push_back(buf[index] + vertexStart);
    }
    break;
  }
  case TINYGLTF_PARAMETER_TYPE_UNSIGNED_SHORT: {
    uint16_t* buf = new uint16_t[accessor.count];
    memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint16_t));
    for (size_t index = 0; index < accessor.count; index++) {
      indices.push_back(buf[index] + vertexStart);
    }
    break;
  }
  case TINYGLTF_PARAMETER_TYPE_UNSIGNED_BYTE: {
    uint8_t* buf = new uint8_t[accessor.count];
    memcpy(buf, &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(uint8_t));
    for (size_t index = 0; index < accessor.count; index++) {
      indices.push_back(buf[index] + vertexStart);
    }
    break;
  }
    /*
default:
  std::cerr << "Index component type " << accessor.componentType << " not supported!" << std::endl;
  return;
  */
  }
}

void GltfModel::readMaterials(std::vector<Material>& materials) {
  materials.clear();
  for (const auto& glTFMaterial : m_model.materials) {
    Material m{};
    // Get the base color factor
    if (glTFMaterial.values.find("baseColorFactor") != glTFMaterial.values.end()) {
      m.baseColorFactor = glm::make_vec4(glTFMaterial.values.at("baseColorFactor").ColorFactor().data());
    }
    // Get base color texture index
    if (glTFMaterial.values.find("baseColorTexture") != glTFMaterial.values.end()) {
      m.baseColorTextureIndex = glTFMaterial.values.at("baseColorTexture").TextureIndex();
    }
    // Get the metallic factor
    if (glTFMaterial.values.find("metallicFactor") != glTFMaterial.values.end()) {
      m.metallicFactor = glTFMaterial.values.at("metallicFactor").Factor();
    }
    // Get the roughness factor
    if (glTFMaterial.values.find("roughnessFactor") != glTFMaterial.values.end()) {
      m.roughnessFactor = glTFMaterial.values.at("roughnessFactor").Factor();
    }
    materials.push_back(m);
  }
}

void GltfModel::loadNode(const tinygltf::Node& inputNode, GltfNode* parent, uint32_t nodeIndex) {
  std::unique_ptr<GltfNode> node  = std::make_unique<GltfNode>(parent, nodeIndex);
  node->mesh = inputNode.mesh;
  node->skin = inputNode.skin;

  // Get the local node matrix
  // It's either made up from translation, rotation, scale or a 4x4 matrix
  if (inputNode.translation.size() == 3) {
    node->translation = glm::make_vec3(inputNode.translation.data());
  }
  if (inputNode.rotation.size() == 4) {
    glm::quat q    = glm::make_quat(inputNode.rotation.data());
    node->rotation = glm::mat4(q);
  }
  if (inputNode.scale.size() == 3) {
    node->scale = glm::make_vec3(inputNode.scale.data());
  }
  if (inputNode.matrix.size() == 16) {
    node->matrix = glm::make_mat4x4(inputNode.matrix.data());
  };

  // Load node's children
  if (inputNode.children.size() > 0) {
    for (size_t i = 0; i < inputNode.children.size(); i++) {
      loadNode(m_model.nodes[inputNode.children[i]], node.get(), inputNode.children[i]);
    }
  }
  if (parent)
    parent->children.push_back(std::move(node));
  else
    m_nodes.push_back(std::move(node));
}

GltfNode *GltfModel::nodeFromIndex(uint32_t index) {
  GltfNode* nodeFound = nullptr;
  for (const auto& node : m_nodes) {
    nodeFound = GltfNode::findNode(node, index);
    if (nodeFound)
      return nodeFound;
  }
  return nullptr;
}

// POI: Load the animations from the glTF model
void GltfModel::loadAnimations(std::vector<Animation>& animations) {
  animations.resize(m_model.animations.size());

  for (size_t i = 0; i < m_model.animations.size(); i++) {
    tinygltf::Animation glTFAnimation = m_model.animations[i];
    animations[i].name                = glTFAnimation.name;

    // Samplers
    animations[i].samplers.resize(glTFAnimation.samplers.size());
    for (size_t j = 0; j < glTFAnimation.samplers.size(); j++)
    {
      const auto& glTFSampler = glTFAnimation.samplers[j];
      auto& dstSampler = animations[i].samplers[j];
      dstSampler.interpolation = glTFSampler.interpolation;

      // Read sampler keyframe input time values
      {
        const auto& accessor   = m_model.accessors[glTFSampler.input];
        const auto& bufferView = m_model.bufferViews[accessor.bufferView];
        const auto& buffer     = m_model.buffers[bufferView.buffer];
        const void *                dataPtr    = &buffer.data[accessor.byteOffset + bufferView.byteOffset];
        const float *               buf        = static_cast<const float *>(dataPtr);
        for (size_t index = 0; index < accessor.count; index++) {
          dstSampler.inputs.push_back(buf[index]);
        }
        // Adjust animation's start and end times
        for (auto input : animations[i].samplers[j].inputs) {
          if (input < animations[i].start) {
            animations[i].start = input;
          };
          if (input > animations[i].end) {
            animations[i].end = input;
          }
        }
      }

      // Read sampler keyframe output translate/rotate/scale values
      {
        const auto& accessor   = m_model.accessors[glTFSampler.output];
        const auto& bufferView = m_model.bufferViews[accessor.bufferView];
        const auto& buffer     = m_model.buffers[bufferView.buffer];
        const void* dataPtr    = &buffer.data[accessor.byteOffset + bufferView.byteOffset];
        switch (accessor.type) {
          case TINYGLTF_TYPE_VEC3: {
            const glm::vec3 *buf = static_cast<const glm::vec3 *>(dataPtr);
            for (size_t index = 0; index < accessor.count; index++) {
              dstSampler.outputsVec4.push_back(glm::vec4(buf[index], 0.0f));
            }
            break;
          }
          case TINYGLTF_TYPE_VEC4: {
            const glm::vec4 *buf = static_cast<const glm::vec4 *>(dataPtr);
            for (size_t index = 0; index < accessor.count; index++) {
              dstSampler.outputsVec4.push_back(buf[index]);
            }
            break;
          }
          default: {
       //     std::cout << "unknown type" << std::endl;
            break;
          }
        }
      }
    }

    // Channels
    animations[i].channels.resize(glTFAnimation.channels.size());
    for (size_t j = 0; j < glTFAnimation.channels.size(); j++) {
      const auto& glTFChannel = glTFAnimation.channels[j];
      auto& dstChannel  = animations[i].channels[j];
      dstChannel.path                        = glTFChannel.target_path;
      dstChannel.samplerIndex                = glTFChannel.sampler;
      dstChannel.node                        = nodeFromIndex(glTFChannel.target_node);
    }
  }
}

void GltfModel::loadNodes() {
  m_nodes.clear();
  const auto& scene = m_model.scenes[0];
  for (size_t i = 0; i < scene.nodes.size(); i++) {
    const auto node = m_model.nodes[scene.nodes[i]];
    loadNode(node, nullptr, scene.nodes[i]);
  }
}

void GltfModel::loadSkins(GraphicsResource* pGraphics, std::vector<Mesh>& meshes, std::vector<Skin>& skins) {
  for (const auto& glTFSkin : m_model.skins) {
    std::vector<glm::mat4>          inverseBindMatrices;
    std::unique_ptr<BufferResource> ssbo = nullptr;
    if (glTFSkin.inverseBindMatrices > -1) {
      const auto& accessor   = m_model.accessors[glTFSkin.inverseBindMatrices];
      const auto& bufferView = m_model.bufferViews[accessor.bufferView];
      const auto& buffer     = m_model.buffers[bufferView.buffer];
      inverseBindMatrices.resize(accessor.count);
      memcpy(inverseBindMatrices.data(), &buffer.data[accessor.byteOffset + bufferView.byteOffset], accessor.count * sizeof(glm::mat4));
      ssbo = std::make_unique<BufferResource>(pGraphics->getDevice()->getDevice());
      auto bufferSize = static_cast<VkDeviceSize>(sizeof(glm::mat4) * inverseBindMatrices.size());
      pGraphics->createBuffer(bufferSize, 1, vk::BufferUsageFlagBits::eStorageBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, ssbo.get());
    }
    std::vector<GltfNode *> joints;
    // Find joint nodes
    for (auto jointIndex : glTFSkin.joints) {
      auto* node = nodeFromIndex(jointIndex);
      if (node) {
        joints.push_back(node);
      }
    }
    const auto& node = m_model.nodes.at(glTFSkin.skeleton - 1);
    assert(node.mesh >= 0);
    auto& mesh = meshes.at(node.mesh);
    mesh.skins.push_back(skins.size());
    skins.push_back({inverseBindMatrices, ssbo, joints});
  }
}

void GltfModel::readTextures(std::vector<Texture>& textures) {
  for (const auto& gltfTexture : m_model.textures) {
    Texture texture{};
    texture.imageIndex = gltfTexture.source;
    textures.push_back(texture);
  }
}

void GltfModel::readVertices(const tinygltf::Primitive& glTFPrimitive) {
  const float*    positionBuffer = nullptr;
  const float*    normalsBuffer = nullptr;
  const float*    texCoordsBuffer = nullptr;
  const uint16_t* jointIndicesBuffer = nullptr;
  const float *   jointWeightsBuffer = nullptr;
  size_t vertexCount = 0;
  // Get buffer data for vertex positions
  if (glTFPrimitive.attributes.find("POSITION") != glTFPrimitive.attributes.end()) {
    const auto& accessor = m_model.accessors[glTFPrimitive.attributes.find("POSITION")->second];
    const auto& view = m_model.bufferViews[accessor.bufferView];
    positionBuffer = reinterpret_cast<const float*>(&(m_model.buffers[view.buffer].data[accessor.byteOffset + view.byteOffset]));
    vertexCount = accessor.count;
  }
  size_t normalCount = 0;
  // Get buffer data for vertex normals
  if (glTFPrimitive.attributes.find("NORMAL") != glTFPrimitive.attributes.end()) {
    const auto& accessor = m_model.accessors[glTFPrimitive.attributes.find("NORMAL")->second];
    const auto& view = m_model.bufferViews[accessor.bufferView];
    normalsBuffer = reinterpret_cast<const float*>(&(m_model.buffers[view.buffer].data[accessor.byteOffset + view.byteOffset]));
    normalCount = accessor.count;
  }
  size_t uvCount = 0;
  // Get buffer data for vertex texture coordinates
  // glTF supports multiple sets, we only load the first one
  if (glTFPrimitive.attributes.find("TEXCOORD_0") != glTFPrimitive.attributes.end()) {
    const auto& accessor = m_model.accessors[glTFPrimitive.attributes.find("TEXCOORD_0")->second];
    const auto& view = m_model.bufferViews[accessor.bufferView];
    texCoordsBuffer = reinterpret_cast<const float*>(&(m_model.buffers[view.buffer].data[accessor.byteOffset + view.byteOffset]));
    uvCount = accessor.count;
  }
  // POI: Get buffer data required for vertex skinning
  // Get vertex joint indices
  if (glTFPrimitive.attributes.find("JOINTS_0") != glTFPrimitive.attributes.end()) {
    const auto& accessor = m_model.accessors[glTFPrimitive.attributes.find("JOINTS_0")->second];
    const auto& view = m_model.bufferViews[accessor.bufferView];
    jointIndicesBuffer = reinterpret_cast<const uint16_t *>(&(m_model.buffers[view.buffer].data[accessor.byteOffset + view.byteOffset]));
  }
  // Get vertex joint weights
  if (glTFPrimitive.attributes.find("WEIGHTS_0") != glTFPrimitive.attributes.end()) {
    const auto& accessor = m_model.accessors[glTFPrimitive.attributes.find("WEIGHTS_0")->second];
    const auto& view = m_model.bufferViews[accessor.bufferView];
    jointWeightsBuffer = reinterpret_cast<const float *>(&(m_model.buffers[view.buffer].data[accessor.byteOffset + view.byteOffset]));
  }
  bool hasSkin = (jointIndicesBuffer && jointWeightsBuffer);
  // Append data to model's vertex buffer
  for (size_t v = 0; v < vertexCount; v++) {
    m_vertexPositions.push_back(glm::make_vec3(&positionBuffer[v * 3]));
    m_vertexNormals.push_back(glm::normalize(glm::vec3(normalsBuffer ? glm::make_vec3(&normalsBuffer[v * 3]) : glm::vec3(0.0f))));
    m_vertexUVs.push_back(texCoordsBuffer ? glm::make_vec2(&texCoordsBuffer[v * 2]) : glm::vec2(0.0f));
    m_vertexJointIndices.push_back(hasSkin ? glm::vec4(glm::make_vec4(&jointIndicesBuffer[v * 4])) : glm::vec4(0.0f));
    m_vertexJointWeights.push_back(hasSkin ? glm::make_vec4(&jointWeightsBuffer[v * 4]) : glm::vec4(0.0f));
  }
}

void GltfModel::showInfo() {
  for (const auto& mesh : m_model.meshes) {
    ImGui::Text("Name mesh : %s", mesh.name.c_str());
    ImGui::Text("  Number of primitives : %ld", mesh.primitives.size());
  }
  ImGui::Text("Number of materials : %ld", m_model.materials.size());
  ImGui::Text("Number of textures : %ld", m_model.textures.size());
  ImGui::Text("Number of skins : %ld", m_model.skins.size());
}
