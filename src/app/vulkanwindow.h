#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <string>
#include <vector>
#include <vulkan/vulkan.hpp>

class VulkanWindow {
public:
  bool        m_active = false;
  GLFWwindow* m_window = nullptr;

  VulkanWindow();
  ~VulkanWindow();
  static void glfw_error(int error_code, const char* description);
  void init(int width, int height, const char *title);
  void initFullScreen(const char *title);
  std::vector<const char*> getRequiredExtensions();
  vk::UniqueSurfaceKHR createSurface(vk::Instance& instance);
  inline VkResult createWindowSurface(vk::Instance instance, VkSurfaceKHR& surface) {
    return glfwCreateWindowSurface(instance, m_window, nullptr, &surface);
  }
  void getFramebufferSize(int *width, int *height);
  inline void getWindowSize(int *width, int *height) {
    glfwGetWindowSize(m_window, width, height);
  }
  inline void pollEvents() {
    glfwPollEvents();
  }
  inline void setTitle(const std::string aTitle) {
    glfwSetWindowTitle(m_window, aTitle.data());
  }
  inline bool windowShouldClose() {
    return glfwWindowShouldClose(m_window);
  }
};
