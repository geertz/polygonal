#pragma once

#include "polyhedron.h"

class RegularPolyhedron {
private:
  // Link the original vertex with the expanded face
  std::map<size_t,size_t> pairVertexFaceList;

public:
  std::unique_ptr<Polyhedron> m_target;
  RegularPolyhedron(const std::unique_ptr<Polyhedron>& source);
};
