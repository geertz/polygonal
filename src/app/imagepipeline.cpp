#include "imagepipeline.h"

ImagePipeline::ImagePipeline(GraphicsResource* pGraphics) : PipelineBase(pGraphics, "shaders/vert_image.spv", "shaders/frag_image.spv", "Image pipeline") {
  if (!m_pipelineResource)
    return;
  // Create the descriptor layout
  m_pipelineResource->createDescriptorSetLayoutCIS();
  // Create the pipeline
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vertexInputInfo);
  m_textureSampler = pGraphics->getDevice()->createSampler(vk::SamplerAddressMode::eClampToBorder);
  if (!m_pipelineResource->addDescriptorSetsCIS()) {
    setResourceError();
    return;
  }
}

auto ImagePipeline::updateVertexBuffer() -> void {
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
}

auto ImagePipeline::fillVertices(const vk::Viewport& viewport, const vk::Extent2D& texExtent) -> void {
  const auto viewRatio = static_cast<double>(viewport.height) / viewport.width;
  const auto texRatio = static_cast<double>(texExtent.height) / texExtent.width;
  const auto xFactor = static_cast<float>(viewRatio > texRatio ? 1.0 : texRatio / viewRatio);
  const auto yFactor = static_cast<float>(viewRatio > texRatio ? viewRatio / texRatio : 1.0);
  const auto x0 = (1.0f - xFactor) / 2.0;
  const auto x1 = x0 + xFactor;
  const auto y0 = (1.0f - yFactor) / 2.0f;
  const auto y1 = y0 + yFactor;

  m_vertices.clear();
  // First triangle
  { // 0
    Vertex vi{glm::vec2(-1.0, -1.0), glm::vec2(x0, y0)};
    m_vertices.emplace_back(vi);
  }
  { // 1
    Vertex vi{glm::vec2(-1.0, 1.0), glm::vec2(x0, y1)};
    m_vertices.emplace_back(vi);
  }
  { // 2
    Vertex vi{glm::vec2( 1.0, 1.0), glm::vec2(x1, y1)};
    m_vertices.emplace_back(vi);
  }
  // Second triangle
  { // 2
    Vertex vi{glm::vec2( 1.0, 1.0), glm::vec2(x1, y1)};
    m_vertices.emplace_back(vi);
  }
  { // 3
    Vertex vi{glm::vec2( 1.0, -1.0), glm::vec2(x1, y0)};
    m_vertices.emplace_back(vi);
  }
  { // 0
    Vertex vi{glm::vec2(-1.0, -1.0), glm::vec2(x0, y0)};
    m_vertices.emplace_back(vi);
  }
}

std::vector<vk::VertexInputAttributeDescription> ImagePipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, texCoord)}
  };
  return attributeDescriptions;
}
