#pragma once

#include <glm/glm.hpp>
#include "pipelinebase.h"

class ImagePipeline : public PipelineBase {
public:
  explicit ImagePipeline(GraphicsResource* pGraphics);
  auto updateVertexBuffer() -> void;
  auto fillVertices(const vk::Viewport& viewport, const vk::Extent2D& texExtent) -> void;
  inline void updateUniformBuffers(const vk::UniqueImageView& textureImageView) {
    assert(m_pipelineResource);
    m_pipelineResource->updateDescriptorSets(0, textureImageView, m_textureSampler);
  }
private:
  struct Vertex {
    glm::vec2 position;
    glm::vec2 texCoord;
  };

  std::vector<Vertex> m_vertices;
  vk::UniqueSampler   m_textureSampler;

  std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};

