#pragma once

#include "polyhedron.h"
//#include "panel.h"

enum class PolyhedronTransformation {NONE, EXPAND, DUAL, DUAL_COMPOUND, PLATONIC_2_COMPOUND, REGULAR, STELLATION, TRUNCATE, ZONO};

class TransformSolid {
private:
  static Polyhedron *dual(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *expand(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *makeDualCompound(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *makePlatonic2Compound(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *regular(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *stellation(const std::unique_ptr<Polyhedron>& solid, int count);
  static Polyhedron *truncate(const std::unique_ptr<Polyhedron>& solid);
  static Polyhedron *zono(const std::unique_ptr<Polyhedron>& solid);
public:
  static Polyhedron *transform(const std::unique_ptr<Polyhedron>& solid, PolyhedronTransformation action, size_t count);
  inline static Polyhedron *transform(const std::unique_ptr<Polyhedron>& solid, PolyhedronTransformation action) {
    return TransformSolid::transform(solid, action, 1);
  }
};
