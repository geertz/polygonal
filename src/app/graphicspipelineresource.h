#pragma once

#include <cstddef>
#include "bufferresource.h"
#include "graphicsresource.h"
#include "textureresource.h"
#include "gltfnode.h"

// A primitive contains the data for a single draw call
struct DrawPrimitive {
  DrawPrimitive(uint32_t indices) {
    indexCount = indices;
  }
  uint32_t          firstIndex = 0;
  uint32_t          indexCount = 0;
  int32_t           materialIndex = -1;
  vk::DescriptorSet descriptionSet;
};
struct Material {
  glm::vec4 baseColorFactor = glm::vec4(1.0f);
  uint32_t  baseColorTextureIndex;
  float     metallicFactor;
  float     roughnessFactor;
};
struct Texture {
  int32_t imageIndex;
};
struct Skin {
  Skin(std::vector<glm::mat4> &m, std::unique_ptr<BufferResource>& bo, std::vector<GltfNode *>& j) {
    inverseBindMatrices = std::move(m);
    ssbo = std::move(bo);
    joints = std::move(j);
  }
  std::vector<glm::mat4>          inverseBindMatrices;
  // Node pointers related to m_nodes of GltfModel
  std::vector<GltfNode *>         joints;
  std::unique_ptr<BufferResource> ssbo = nullptr;
  // No vk::UniqueDescriptorSet
  // Would not destroy nicely
  vk::DescriptorSet               descriptorSet;
  vk::DeviceSize                  bufferSize;
};
struct Mesh {
  Mesh(const std::string& aName) {
    name = aName;
  }
  std::string                name;
  std::vector<glm::mat4x4>   transformMatrices;
  std::vector<DrawPrimitive> primitives;
  std::vector<int>           skins;
};

class GraphicsPipelineResource {
  struct UniformDescriptor {
    std::unique_ptr<BufferResource> bufferMatrices;
    std::unique_ptr<BufferResource> bufferLights;
    // Uniform buffer
    vk::DescriptorSet               descriptorSetUB = nullptr;
    // Combined image sampler
    vk::DescriptorSet               descriptorSetCIS = nullptr;
  };
  struct BufferPair {
    std::unique_ptr<BufferResource> vertexBufferResource;
    std::unique_ptr<BufferResource> indexBufferResource;
  };

public:
  struct UniformBinding {
    UniformBinding(uint32_t b, vk::ShaderStageFlags someFlags, vk::DeviceSize aSize) {
      binding    = b;
      stageFlags = someFlags;
      bufferSize = aSize;
    }
    uint32_t             binding;
    vk::ShaderStageFlags stageFlags;
    vk::DeviceSize       bufferSize;
  };
  struct PushPBR {
    PushPBR() {
      transform = glm::mat4x4(1.0f);
      roughness = 0.0f;
      metallic = 0.0f;
      r = 1.0f;
      b = 1.0f;
      g = 1.0f;
    }
    glm::mat4x4 transform;
    float roughness;
    float metallic;
    float r;
    float g;
    float b;
  };
  explicit GraphicsPipelineResource(GraphicsResource* pGraphics);
  GraphicsPipelineResource(const GraphicsPipelineResource&) = delete; // No copy needed

  auto addDescriptorSetsCIS() -> bool;
  void bindCommandBuffer(const SwapchainImageResource& imageResource, size_t iImage);
  void bindCommandBuffer(const SwapchainImageResource& imageResource);
  inline void bindDescriptorSets(const SwapchainImageResource& imageResource, size_t iImage) {
    assert(!m_imageUniformResources.empty());
    imageResource.m_stdCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout.get(), 0, 1, &m_imageUniformResources.at(iImage).descriptorSetUB, 0, nullptr);
  }
  inline void bindDescriptorSets(const SwapchainImageResource& imageResource, const DrawPrimitive& primitive, uint32_t firstSet) const {
    assert(primitive.descriptionSet);
    imageResource.m_stdCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout.get(), firstSet, 1, &primitive.descriptionSet, 0, nullptr);
  }
  auto copyUBLights(uint32_t iImage, void* source, vk::DeviceSize size) -> bool;
  auto copyUBMatrices(uint32_t iImage, void* source, vk::DeviceSize size) -> bool;
  auto createDefaultDescriptorPool() -> void {
    m_descriptorPool = m_graphics->createGraphicsDescriptorPool(1);
  }
  auto createDescriptorSetLayoutCIS() -> void;
  // Create the layout for the storage buffer
  auto createDescriptorSetLayoutSB() -> void;
  void createDescriptorSetLayoutUB(const std::vector<UniformBinding>& setup);
  auto createFragmentShader(const std::string& aFileName) -> bool;
  bool createIndexBuffer(size_t iBuffer, vk::DeviceSize indexSize, size_t nIndices, const void *indexData);
  inline void createIndexBuffer(vk::DeviceSize indexSize, size_t nIndices, const void *indexData) {
    createIndexBuffer(0, indexSize, nIndices, indexData);
  }
  auto createPipeline(vk::PrimitiveTopology aTopology, vk::PolygonMode aPolygonMode, const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange = nullptr) -> bool;
  auto createUniformBuffers(vk::DeviceSize bufferSize, const vk::UniqueImageView& textureImageView, const vk::UniqueSampler& textureSampler) -> bool;
  auto createUniformBuffers(vk::DeviceSize bufferSize) -> bool;
  auto createUniformBuffers(const std::vector<UniformBinding>& bindings, std::vector<Mesh>& meshes, std::vector<Skin>& skins) -> bool;
  inline auto createUniformBuffers(const std::vector<UniformBinding>& bindings, std::vector<Mesh>& meshes) -> bool {
    assert(!m_descriptorSetLayoutSB);
    std::vector<Skin> skins{};
    return createUniformBuffers(bindings, meshes, skins);
  }
  bool createVertexBuffer(vk::DeviceSize vertexSize, size_t nVertices, const void* vertexData);
  auto createVertexShader(const std::string& aFileName) -> bool;
  inline void drawIndexed(const vk::CommandBuffer& buffer) {
    buffer.drawIndexed(static_cast<uint32_t>(m_bufferPairs[m_currentBuffer].indexBufferResource->nElements), 1, 0, 0, 0);
  }
  auto drawIndexed(const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes, const std::vector<Skin>& skins) const -> void;
  inline auto drawIndexed(const SwapchainImageResource& imageResource, const std::vector<Mesh> &meshes) const -> void {
    const std::vector<Skin> skins;
    drawIndexed(imageResource, meshes, skins);
  }
  auto drawIndexed(const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes, const std::vector<Material>& materials) const -> void;
  inline auto getCurrentBuffer() const -> size_t {
    return m_currentBuffer;
  }
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  // Used by meshmaterialpipeline class
  inline BufferResource *getUniformBufferLights(size_t iImage) const {
    return m_imageUniformResources.at(iImage).bufferLights.get();
  }
  inline void pushUBO(const SwapchainImageResource& imageResource, size_t uboSize, const void *ubo) {
    imageResource.m_stdCommandBuffer.pushConstants(m_pipelineLayout.get(), vk::ShaderStageFlagBits::eVertex, 0, uboSize, ubo);
  }
  // Currenty max 2 sets of vertex/index buffers
  inline void setCurrentBuffer(size_t aValue) {
    assert(aValue <= 1);
    m_currentBuffer = aValue;
  }
  void updateDescriptorSetsCIS(const vk::DescriptorSet& descriptorSet, const vk::UniqueImageView& imageView, const vk::UniqueSampler& textureSampler) const;
  void updateDescriptorSetsSB(const vk::DescriptorSet& descriptorSet, vk::Buffer buffer, vk::DeviceSize bufferSize) const;
  void updateDescriptorSets(vk::DeviceSize bufferSize, const vk::UniqueImageView& imageView, const vk::UniqueSampler& textureSampler) const;
  void updateVertexBuffer(size_t iMorph, vk::DeviceSize vertexSize, size_t nVertices, const void *vertexData);
private:
  GraphicsResource               *m_graphics;
  vk::Device                      m_device = nullptr;
  std::string                     m_errorMessage;
  vk::UniqueDescriptorPool        m_descriptorPool;
  vk::UniquePipelineLayout        m_pipelineLayout;
  vk::UniquePipeline              m_pipeline;
  // Layout for Uniform buffers
  vk::UniqueDescriptorSetLayout   m_descriptorSetLayoutUB;
  // Layout for Combined Image Samplers
  vk::UniqueDescriptorSetLayout   m_descriptorSetLayoutCIS;
  // Layout for Storage buffer
  vk::UniqueDescriptorSetLayout   m_descriptorSetLayoutSB;
  vk::UniqueShaderModule          m_vertexShaderModule;
  vk::UniqueShaderModule          m_fragmentShaderModule;
  vk::PrimitiveTopology           m_topology = vk::PrimitiveTopology::ePointList;
  vk::PolygonMode                 m_polygonMode = vk::PolygonMode::eFill;
  std::vector<UniformDescriptor>  m_imageUniformResources{};
  std::array<BufferPair, 2>       m_bufferPairs;
  size_t                          m_currentBuffer = 0;
  vk::ShaderStageFlags            m_pushStageFlags;

  std::vector<vk::DescriptorSet> allocatedDescriptorSets(uint32_t descriptorSetCount, const vk::UniqueDescriptorSetLayout& layout);
  inline void clearUniformResources() {
    m_imageUniformResources.clear();
  }
  vk::Result createPipelineBase(vk::PrimitiveTopology aTopology, vk::PolygonMode aPolygonMode, const vk::PipelineVertexInputStateCreateInfo* pVertexInputInfo);
  auto createUniformBuffers2(vk::DeviceSize bufferSize) -> bool;
  void updateDescriptorSetsUB(vk::DeviceSize bufferSize, uint32_t dstBinding) const;
  void WDS_ImageSampler(std::vector<vk::WriteDescriptorSet>& set, const vk::DescriptorSet& descriptorSet, const vk::DescriptorImageInfo& imageInfo) const;
  void WDS_StorageBuffer(std::vector<vk::WriteDescriptorSet>& set, const vk::DescriptorSet& descriptorSet, const vk::DescriptorBufferInfo& bufferInfo, uint32_t dstBinding) const;
  void WDS_UniformBuffer(std::vector<vk::WriteDescriptorSet>& set, const UniformDescriptor& resource, const vk::DescriptorBufferInfo& bufferInfo, uint32_t dstBinding) const;
};
