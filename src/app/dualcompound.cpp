#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>
#include "dualcompound.h"

DualCompound::DualCompound(const std::unique_ptr<Polyhedron>& source) : DualPolyhedron(source) {
  const auto& originalVertices = source->getVertices();
  // Copy the vertices of the dual polyhedron
  const auto dualVertices = m_target->getVertices();
  const auto& originalEdges = source->getEdges();
  // Copy the edges of the dual polyhedron
  const auto dualEdges = m_target->getEdges();
  const auto nFaces = source->getNumberOfFaces();
  m_target->cleanUp();
  for (const auto& vertex : originalVertices) {
    m_target->appendVertex(vertex);
  }
  m_target->setType(source->getType());
  m_target->setDualCompound();
  for (size_t iFace = 0; iFace < nFaces; iFace++) {
    // Find the DualObject associated with this face.
    // Get the center vertex <centerVertex> and its index <iDualVertex>
    glm::dvec3 centerVertex = {0,0,0};
    size_t iDualVertex;
    bool foundFace = false;
    for (const auto &dual : m_duals) {
      if (dual.iOriginalFace == iFace) {
        foundFace = true;
        centerVertex = dual.newVertex;
        iDualVertex = dual.iNewVertex;
        break;
      }
    }
    assert(foundFace);
    // Find the dual edges with <iDualVertex>
    // Add the other end of the edge to <vertexEdges>
    std::vector<size_t> vertexEdges;
    for (const auto& edge : dualEdges) {
      if (edge.iVertexBegin == size_t(iDualVertex))
        vertexEdges.push_back(edge.iVertexEnd);
      else if (edge.iVertexEnd == size_t(iDualVertex))
        vertexEdges.push_back(edge.iVertexBegin);
    }
    std::vector<EdgeInfo> originalFaceEdges;
    for (size_t iEdge = 0; iEdge < originalEdges.size(); iEdge++) {
      const auto& originalEdge = originalEdges[iEdge];
      if (originalEdge.iFace1 == iFace || originalEdge.iFace2 == iFace) {
        EdgeInfo info;
        info.iEdge = iEdge;
        originalFaceEdges.push_back(info);
      }
    }
    assert(originalFaceEdges.size() == source->getFace(iFace).getCorners());
    const auto faceCenter = source->getFaceCenter(iFace);
    glm::dvec3 p1{};
    glm::dvec3 p2{};
    for (const auto iEdgeEnd : vertexEdges) {
      const auto directionDual = dualVertices[iEdgeEnd] - dualVertices.at(size_t(iDualVertex));
      size_t minOrigEdge;
      glm::dvec3 dualVertex;
      bool foundEdge = false;
      for (size_t iEdgeInfo = 0; iEdgeInfo < originalFaceEdges.size(); iEdgeInfo++) {
        const auto& info = originalFaceEdges[iEdgeInfo];
        const auto& originalEdge = originalEdges[info.iEdge];
        const auto directionOriginal = originalVertices[originalEdge.iVertexEnd] - originalVertices[originalEdge.iVertexBegin];
        // Both edges should be perpendicular
        if (abs(glm::dot(directionDual, directionOriginal) / (glm::length(directionDual) * glm::length(directionOriginal))) > 0.001)
          continue;
        // Determine the distance between both edges
        glm::dvec3 pointOriginal;
        glm::dvec3 pointDual;
        double dualMultiplier1;
        crossingLines(originalVertices[originalEdge.iVertexBegin], directionOriginal,
                      dualVertices[iDualVertex], directionDual,
                      pointOriginal, pointDual, dualMultiplier1);
        /* Closest point not on the opposite site of dualVertices[iDualVertex] */
        if (dualMultiplier1 < 0)
          continue;
        minOrigEdge = iEdgeInfo;
        dualVertex = pointOriginal;
        foundEdge = true;
        break;
      }
      double dualMultiplier2;
      crossingLines(faceCenter, centerVertex - faceCenter,
                    dualVertex, directionDual,
                    p1, p2, dualMultiplier2);
      assert(foundEdge);
      originalFaceEdges[minOrigEdge].dualVertex = dualVertex;
    }
    createFaces(p1, originalFaceEdges, originalEdges);
  }
}

void DualCompound::createFaces(const glm::dvec3& p1, std::vector<EdgeInfo>& originalFaceEdges, const std::vector<Edge>& originalEdges) {
  // The index of the new vertex (p1)
  const auto iCenterVertex = m_target->getNumberOfVertices();
  // Add the new vertex (p1) to the polyhedron (vertices)
  m_target->appendVertex(p1);
  // Save the first edge
  const auto info0 = originalFaceEdges.front();
  // EdgeInfo info1;
  long iEdgeInfo1 = 0;
  while (!originalFaceEdges.empty()) {
    auto it = originalFaceEdges.begin() + iEdgeInfo1;
    const auto info1 = *it;
    originalFaceEdges.erase(it);
    //const auto info1 = originalFaceEdges.takeAt(iEdgeInfo1);
    const auto& edge1 = originalEdges[info1.iEdge];
    const auto nFaceEdges = originalFaceEdges.size();
    const auto nFaces = m_target->getNumberOfFaces();
    // Get second edge linked to first one
    if (nFaceEdges > 0) {
      // Find an edge <edge2> linked to the current one <edge1>
      for (size_t iEdgeInfo2 = 0; iEdgeInfo2 < nFaceEdges; iEdgeInfo2++) {
        const auto info2 = originalFaceEdges[iEdgeInfo2];
        const auto& edge2 = originalEdges[info2.iEdge];
        if (edge2.iVertexBegin == edge1.iVertexBegin || edge2.iVertexEnd == edge1.iVertexBegin) {
          addFaces(uint16_t(edge1.iVertexBegin), uint16_t(iCenterVertex), info1.dualVertex, info2.dualVertex);
          iEdgeInfo1 = static_cast<long>(iEdgeInfo2);
          break;
        } else if (edge2.iVertexBegin == edge1.iVertexEnd || edge2.iVertexEnd == edge1.iVertexEnd) {
          addFaces(uint16_t(edge1.iVertexEnd), uint16_t(iCenterVertex), info1.dualVertex, info2.dualVertex);
          iEdgeInfo1 = static_cast<long>(iEdgeInfo2);
          break;
        }
      }
    } else {
      // Link the last and the first edge and create the faces
      const auto& edge0 = originalEdges[info0.iEdge];
      if (edge0.iVertexBegin == edge1.iVertexBegin || edge0.iVertexEnd == edge1.iVertexBegin)
        addFaces(uint16_t(edge1.iVertexBegin), uint16_t(iCenterVertex), info1.dualVertex, info0.dualVertex);
      else if (edge0.iVertexBegin == edge1.iVertexEnd || edge0.iVertexEnd == edge1.iVertexEnd)
        addFaces(uint16_t(edge1.iVertexEnd), uint16_t(iCenterVertex), info1.dualVertex, info0.dualVertex);
    }
    // Two faces should have been created
    assert(m_target->getNumberOfFaces() == nFaces + 2);
  }
}

// Find the points on two line at the closest distance
void DualCompound::crossingLines(const glm::dvec3& point1, const glm::dvec3 direction1,
                                 const glm::dvec3& point2, const glm::dvec3 direction2,
                                 glm::dvec3& pointCross1, glm::dvec3& pointCross2, double& t) {
  const auto a = glm::length2(direction1);
  const auto b = glm::dot(direction1, direction2);
  const auto c = glm::length2(direction2);
  const auto deltaP = point1 - point2;
  const auto d = glm::dot(direction1, deltaP);
  const auto e = glm::dot(direction2, deltaP);
  const auto divider = a * c - b * b;
  const auto s = (b * e - c * d) / divider;
  t = (a * e - b * d) / divider;
  pointCross1 = point1 + s * direction1;
  pointCross2 = point2 + t * direction2;
}

// Add two faces to the compound
// iCornerVertex : Index of a vertex of the original solid
// iCenterVertex : Index of the dual vertex associated with a face of the original solid
// dualVertex1   : Vertex halfway between the current corner and the next
// dualVertex2   : Vertex halfway between the current corner and the previous
void DualCompound::addFaces(uint16_t iCornerVertex, uint16_t iCenterVertex, const glm::dvec3& dualVertex1,  const glm::dvec3& dualVertex2) {
  // Triangle from original solid
  Face face1(0);
  const auto iVertex1 = m_target->appendFaceWithUniqueVertex(face1, dualVertex1);
  face1.push_back_unique(iCornerVertex);
  const auto iVertex2 = m_target->appendFaceWithUniqueVertex(face1, dualVertex2);
  m_target->appendFace(face1);
  // Triangle from the dual
  Face face2(1);
  face2.push_back_unique(iVertex1);
  face2.push_back_unique(iVertex2);
  face2.push_back_unique(iCenterVertex);
  m_target->appendFace(face2);
}
