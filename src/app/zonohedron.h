#pragma once

#include "polyhedron.h"

class Zonohedron
{
  struct ZonoPlane {
    glm::dvec3          offset;
    std::vector<size_t> vectors;
  };

public:
  std::unique_ptr<Polyhedron> m_target;

  Zonohedron(const std::unique_ptr<Polyhedron>&);
private:
  std::vector<glm::dvec3> getZonoGenerators(const std::unique_ptr<Polyhedron>&) const;
  static glm::dvec3 findBase(const std::vector<glm::dvec3>& star, const ZonoPlane& plane);
  void makePlanes(const std::vector<glm::dvec3>& originalPoints, std::vector<ZonoPlane>& planes);
  void appendZonoFace(const std::vector<glm::dvec3>&, const ZonoPlane&);
  void makeFaces(const std::vector<glm::dvec3>&, const std::vector<ZonoPlane>&);
};
