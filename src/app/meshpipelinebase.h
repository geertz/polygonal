#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <sys/stat.h>
#include "pipelinebase.h"
#include "textureresource.h"
#include "gltfmodel.h"

class MeshPipelineBase : public PipelineBase {
public:
  explicit MeshPipelineBase(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel);
  void draw(const SwapchainImageResource& imageResource, size_t iImage);
  void fillBuffers(const std::unique_ptr<GltfModel>& model);
  void loadMeshes(const std::unique_ptr<GltfModel>& model);
  inline void setColorAlpha(float aValue) {
    m_alpha = aValue;
  }
  inline void setColorAmbient(float aValue) {
    m_ambient = aValue;
  }
  inline void setLightDirection(const glm::vec3& aValue) {
    m_lightDirection = aValue;
  }
  inline void setModel(const glm::mat4x4& aValue) {
    m_model = aValue;
  }
  inline void setProjectionView(const glm::mat4x4& aValue) {
    m_projView = aValue;
  }
protected:
  GraphicsResource                             *m_graphics = nullptr;
  float                                         m_ambient;
  float                                         m_alpha;
  glm::vec3                                     m_lightDirection;
  glm::mat4x4                                   m_model = glm::mat4x4(1.0f);
  glm::mat4x4                                   m_projView;
  std::vector<uint16_t>                         m_indices;
  std::vector<Animation>                        m_animations, m_animationsNext;
  std::vector<Mesh>                             m_meshes, m_meshesNext;
  std::vector<Texture>                          m_textures;
  std::vector<Skin>                             m_skins, m_skinsNext;
  std::vector<std::unique_ptr<TextureResource>> m_textureResources;

  inline void createDescriptorSetLayoutUB(std::vector<GraphicsPipelineResource::UniformBinding>& bindings) {
    m_ub_bindings = bindings;
    m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  }
  auto createPipeline(const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange) -> bool;
  void updateAnimation(const std::unique_ptr<GltfModel>& model);
private:
  bool                                                  m_usePushConstants = false;
  std::vector<GraphicsPipelineResource::UniformBinding> m_ub_bindings;
  std::vector<Material>                                 m_materials;
  vk::UniqueSampler                                     m_textureSampler;
  uint32_t                                              m_activeAnimation = 0;
  std::chrono::time_point<std::chrono::system_clock>    m_time;

  void updateJoints(const std::unique_ptr<GltfModel>& model, const std::unique_ptr<GltfNode> &node);
};

