#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/normal.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/intersect.hpp>

#include "compoundpolyhedron.h"
#include "transformsolid.h"

void VertexFace::push_back(const glm::dvec3& v) {
  if (!vertices.empty()) {
    const auto length = glm::length(v) + glm::length(vertices.back());
    if (glm::distance(v, vertices.back()) > 0.001 * length)
      vertices.emplace_back(v);
  } else
    vertices.emplace_back(v);
}

CompoundPolyhedron::CompoundPolyhedron(const std::vector<glm::vec3>& aColorList) {
  m_colorList = aColorList;
}

CompoundPolyhedron::~CompoundPolyhedron() {
  m_colorList.clear();
}

std::vector<glm::dvec2> CompoundPolyhedron::cleanClosedVec2Ring(Ring2D ring) {
  assert(bg::is_valid(ring));
  std::vector<glm::dvec2> result{};
  auto it = ring.begin();
  glm::dvec2 v0 = *it; // pointToVertex(*it);
  it++;
  do {
    const auto v1 = *it; // pointToVertex(*it);
    const auto d = glm::distance2(v1, v0);
    const auto l = glm::length2(v0 + v1);
    if (d > 0.0001 * l) {
      if (!result.empty()) {
        const auto dir0 = glm::normalize(v0 - result.back());
        const auto dir1 = glm::normalize(v1 - v0);
        if (glm::distance2(dir1, dir0) > 0.0001)
          result.emplace_back(v0);
      } else
        result.emplace_back(v0);
      v0 = v1;
    }
    it++;
  } while (it != ring.end());
  const auto ringSize = result.size();
  if (ringSize > 3) {
    const auto dirFirst = glm::normalize(result[1] - result[0]);
    const auto dirLast = glm::normalize(result[0] - result[ringSize - 1]);
    if (glm::distance2(dirFirst, dirLast) < 0.0001)
      result.erase(result.begin());
  }
  return result;
}

Ring2D CompoundPolyhedron::cleanRing2D(const Ring2D& ring) {
  assert(bg::is_valid(ring));
  Ring2D result{};
  auto it = ring.begin();
  auto v0 = *it;
  it++;
  do {
    const auto v1 = *it;
    // Two vertices v0 and v1
    // Put v0 in <result> when it differs from v1
    const auto d = glm::distance2(v1, v0);
    const auto l = glm::length2(v0 + v1);
    if (d > 0.0001 * l) {
      if (!result.empty()) {
        const auto dir0 = glm::normalize(v0 - result.back());
        const auto dir1 = glm::normalize(v1 - v0);
        if (glm::distance2(dir1, dir0) > 0.0001)
          result.push_back(v0);
      } else
        result.push_back(v0);
      v0 = v1;
    }
    it++;
  } while (it != ring.end());
  const auto ringSize = result.size();
  if (ringSize > 3) {
    // Test first and last
    auto vFirst = result.at(1) - result.at(0);
    const auto dirFirst = glm::normalize(vFirst);
    auto vLast = result[0] - result[ringSize - 1];
    const auto dirLast = glm::normalize(vLast);
    if (glm::distance2(dirFirst, dirLast) < 0.0001)
      result.erase(result.begin());
  }
  // Close the ring
  bg::correct(result);
  assert(bg::is_valid(result));
  return result;
}

Polyhedron *CompoundPolyhedron::create3CompoundCubeOctahedron(const std::unique_ptr<Polyhedron>& source) {
  // Get the vertices of the second cube rotated by 90 degree around axis <axis1>
  const glm::dvec3 axis1(1.0, 1.0, 0.0);
  const auto rotatedVertices1 = rotateVertices(source, M_PI_2f64, axis1);
  // Get the vertices of the third cube rotated by 90 degree around axis <axis2>
  const glm::dvec3 axis2(1.0, -1.0, 0.0);
  const auto rotatedVertices2 = rotateVertices(source, M_PI_2f64, axis2);
  // Add all vertices
  const auto nVertices = source->getNumberOfVertices();
  // Copy the vertices from <source>
  auto vertices = source->getVertices();
  // Prevent duplicate vertices
  // Can not do: vertices += rotatedVertices;
  std::vector<uint32_t> rotatedIndices1;
  std::vector<uint32_t> rotatedIndices2;
  for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
    rotatedIndices1.push_back(Polyhedron::appendUnique(rotatedVertices1[iVertex], vertices));
    rotatedIndices2.push_back(Polyhedron::appendUnique(rotatedVertices2[iVertex], vertices));
  }
  std::vector<std::vector<Face>> compoundFacesList;
  const auto& oldFaces = source->getFaces();
  // Get faces of second compound
  const auto compoundFaces1 = createCompoundFaces(vertices, source->getFaces(), rotatedIndices1, 1);
  // Get faces of third compound
  const auto compoundFaces2 = createCompoundFaces(vertices, source->getFaces(), rotatedIndices2, 2);
  // There are three sets of faces: <faces>, compoundFaces1 and compoundFaces2
  compoundFacesList.push_back(oldFaces);
  compoundFacesList.push_back(compoundFaces1);
  compoundFacesList.push_back(compoundFaces2);
  intersectSetOfFaces(vertices, compoundFacesList);
  compoundFacesList.clear();
  return m_target.release();
}

Polyhedron *CompoundPolyhedron::create4CompoundCubeOctahedron(const std::unique_ptr<Polyhedron>& source) {
  // Get the vertices of the first cube rotated by 60 degree around a 3-fold axis <axis1>
  constexpr auto angle_60 = M_PIf64 / 3.0;
  const glm::dvec3 axis1(1.0, 1.0, 1.0);
  const auto rotatedVertices1 = rotateVertices(source, angle_60, axis1);
  // Get the vertices of the second cube rotated by 60 degree around another 3-fold axis <axis2>
  const glm::dvec3 axis2(1.0, -1.0, 1.0);
  const auto rotatedVertices2 = rotateVertices(source, angle_60, axis2);
  // Get the vertices of the third cube by rotating 60 degree around another 3-fold axis <axis3>
  const glm::dvec3 axis3(-1.0, -1.0, 1.0);
  const auto rotatedVertices3 = rotateVertices(source, angle_60, axis3);
  // Get the vertices of the fourth cube by rotating 60 degree around another 3-fold axis <axis4>
  const glm::dvec3 axis4(-1.0, 1.0, 1.0);
  const auto rotatedVertices4 = rotateVertices(source, angle_60, axis4);
  const auto nVertices = source->getNumberOfVertices();
  // Copy the vertices from <source>
  auto vertices = source->getVertices();
  // Prevent duplicate vertices
  // Can not do: vertices += rotatedVertices;
  std::vector<uint32_t> rotatedIndices[4];
  for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
    rotatedIndices[0].push_back(Polyhedron::appendUnique(rotatedVertices1[iVertex], vertices));
    rotatedIndices[1].push_back(Polyhedron::appendUnique(rotatedVertices2[iVertex], vertices));
    rotatedIndices[2].push_back(Polyhedron::appendUnique(rotatedVertices3[iVertex], vertices));
    rotatedIndices[3].push_back(Polyhedron::appendUnique(rotatedVertices4[iVertex], vertices));
  }
  std::vector<std::vector<Face>> compoundFacesList;
  for (ushort i = 0; i < 4; i++) {
    compoundFacesList.push_back(createCompoundFaces(vertices, source->getFaces(), rotatedIndices[i], i % m_colorList.size()));
    rotatedIndices[i].clear();
  }
  intersectSetOfFaces(vertices, compoundFacesList);
  compoundFacesList.clear();
  return m_target.release();
}

std::vector<Face> CompoundPolyhedron::createCompoundFaces(const std::vector<glm::dvec3>& vertices, const std::vector<Face>& faces, const std::vector<uint32_t> rotatedIndices, ushort iColor) {
  std::vector<Face> result;
  for (const auto& face : faces) {
    Face newFace(iColor);
    for (const auto iVertex : face)
      newFace.push_back_unique(static_cast<uint16_t>(rotatedIndices[iVertex]));
    newFace.setNormal(vertices);
    result.push_back(newFace);
  }
  return result;
}

std::vector<Face> CompoundPolyhedron::createCompoundFaces(const std::unique_ptr<Polyhedron>& source, const std::vector<uint32_t> rotatedIndices, ushort iColor) {
  std::vector<Face> result;
  const auto& oldVertices = source->getVertices();
  for (const auto& face1 : source->getFaces()) {
    Face newFace(iColor);
    for (const auto iVertex : face1)
      newFace.push_back_unique(static_cast<uint16_t>(rotatedIndices[iVertex]));
    newFace.setNormal(oldVertices);
    result.push_back(newFace);
  }
  return result;
}
/*
 * newFace : new face to be created
 * intersections : Two points intersecting <polygon>
 */
void CompoundPolyhedron::createNewFace2(std::vector<glm::dvec3>& newFace, const std::vector<IntersectData> intersections, const Polygon& polygon, const Plane& secondPlane, const Plane& centerPlane) const {
  Line centerLine;
  centerLine.direction = centerPlane.normal;
  assert(intersections.size() == 2);
  const auto& dataFirst = intersections.front();
  const auto& dataLast = intersections.back();
  const auto nCorners = polygon.size();
  for (size_t i = 0; i < nCorners; i++) {
    auto j = (i + nCorners - 1) % nCorners;
    if (((dataFirst.firstIndex == j && dataFirst.factor > 0.999) || (dataLast.firstIndex == j && dataLast.factor > 0.999))) {
      continue;
    }
    const auto& oldVertex1 = polygon[static_cast<ushort>(i)];
    // get distance to center plane
    centerLine.point = oldVertex1;
    double distance1 = 0.0;
    // Make sure the normal of the plane is towards the solid center
    const auto normal = (glm::dot(centerLine.point - centerPlane.point, centerPlane.normal) < 0.0) ? -centerPlane.normal : centerPlane.normal;

    const auto intersect = glm::intersectRayPlane(centerLine.point, -normal, centerPlane.point, normal, distance1);
    if (!intersect) {
      glm::intersectRayPlane(centerLine.point, normal, centerPlane.point, normal, distance1);
    }
    const auto v4 = Polyhedron::getPlaneLineIntersection(centerPlane, centerLine);
    const auto v5 = Polyhedron::getPlaneLineIntersection(secondPlane, centerLine);
    const auto distance2 = glm::length(v5 - v4);
    if (distance1 > distance2 * 0.999) {
      auto iNewVertex = m_target->indexOfVector(newFace, oldVertex1);
      assert(iNewVertex < 0);
      newFace.push_back(oldVertex1);
    }
    if (i == dataFirst.firstIndex && abs(dataFirst.factor) > 0.001) {
      if (m_target->indexOfVector(newFace, dataFirst.vertex) < 0) {
        newFace.push_back(dataFirst.vertex);
      }
    } else if (i == dataLast.firstIndex && abs(dataLast.factor) > 0.001) {
      if (m_target->indexOfVector(newFace, dataLast.vertex) < 0)
        newFace.push_back(dataLast.vertex);
    }
  }
  assert(newFace.size() >= 3);
}

Polyhedron *CompoundPolyhedron::createPlatonic2Compound(const std::unique_ptr<Polyhedron>& source) {
  assert(!source->getEdges().empty());
  const auto& edge = source->getEdge(0);
  const auto& vertex1 = source->getVertex(edge.iVertexBegin);
  const auto& vertex2 = source->getVertex(edge.iVertexEnd);
  // Get the middle of an edge
  const auto vertex = (vertex1 + vertex2) / 2.0;
  ObjectType newType = ObjectType::NONE;
  switch (source->getType()) {
  case ObjectType::CUBE:
    newType = ObjectType::CUBE_2_COMPOUND;
    break;
  case ObjectType::DODECAHEDRON:
    newType = ObjectType::DODECAHEDRON_2_COMPOUND;
    break;
  case ObjectType::ICOSAHEDRON:
    newType = ObjectType::ICOSAHEDRON_2_COMPOUND;
    break;
  case ObjectType::OCTAHEDRON:
    newType = ObjectType::OCTAHEDRON_2_COMPOUND;
    break;
  case ObjectType::TETRAHEDRON:
    newType = ObjectType::STELLATED_OCTAHEDRON;
    break;
  default:
    newType = ObjectType::NONE;
  }
  m_target = std::make_unique<Polyhedron>(newType, m_colorList, true);
  m_target->m_isConvex = false;
  // Rotate by 90 degrees
  return createSecondCompound(source, vertex, M_PI_2f64);
}

Polyhedron *CompoundPolyhedron::createSecondCompound(const std::unique_ptr<Polyhedron>& source, const glm::dvec3& vertex, double angle) {
  const auto rotatedVertices = rotateVertices(source, angle, vertex);
  return processRotatedVertices(source, rotatedVertices);
}

void CompoundPolyhedron::fillCompoundFaceList(const std::vector<glm::dvec3>& vertices, const std::vector<Face>& oldFaces1, const std::vector<Face>& oldFaces2, std::vector<CompoundFace>& cFaces) {
  for (const auto& oldFace1 : oldFaces1) {
    CompoundFace cFace{};
    cFace.oldFace     = &oldFace1;
    cFace.faceCenter  = oldFace1.getCenter(vertices);
    cFace.vertexFaces = {};
    // Intersect with oldFaces2
    bool madeNewFaces = false;
    bool equiPlanar = false;
    for (const auto& oldFace2 : oldFaces2) {
      const auto intersect = intersectTwoFaces(vertices, cFace, oldFace2);
      if (intersect == IntersectFaces::INTERSECT)
        madeNewFaces = true;
      if (intersect == IntersectFaces::EQUIPLANAR) {
        equiPlanar = true;
        VertexFace newFace;
        newFace.origin = 1;
        mergeFaces(vertices, *cFace.oldFace, oldFace2, newFace.vertices);
        cFace.vertexFaces.push_back(newFace);
      }
    }
    if (!madeNewFaces && !equiPlanar) {
      VertexFace newFace;
      newFace.origin = 2;
      const auto& oldFaceVertices = cFace.oldFace->getVertices(vertices);
      newFace.vertices.insert(newFace.vertices.begin(), oldFaceVertices.begin(), oldFaceVertices.end());
      cFace.vertexFaces.push_back(newFace);
    }
    cFaces.push_back(cFace);
  }
}

void CompoundPolyhedron::fillMapMergedRing(const CompoundFace& cFace, const glm::dmat4& xyMat, AxisType axis2D, std::multimap<double, MergeRing, std::greater_equal<double>>& mapRing2D) {
  for (const auto& vertexFace : cFace.vertexFaces) {
    MergeRing mRing{};
    mRing.ring2D = Polyhedron::createRing2D(vertexFace.vertices, xyMat, axis2D);
    assert(bg::is_valid(mRing.ring2D));
    mapRing2D.insert(std::make_pair(bg::area(mRing.ring2D), mRing));
  }
}

// Intersect one compound (oldFacesList.begin()) with the other ones
void CompoundPolyhedron::intersectCompounds(const std::vector<glm::dvec3>& vertices, const std::vector<std::vector<Face>>& oldFacesList) {
  std::vector<std::vector<CompoundFace>> cFaceList;
  const auto it1 = oldFacesList.begin();
  auto it2 = std::next(it1, 1);
  while (it2 != oldFacesList.end()) {
    std::vector<CompoundFace> cFaces{};
    fillCompoundFaceList(vertices, *it1.base(), *it2.base(), cFaces);
    cFaceList.push_back(cFaces);
    it2++;
  }
  std::vector<CompoundFace> cMergedFaces;
  mergeCompoundFaces(cFaceList, cMergedFaces);
  for (auto& cFace : cMergedFaces) {
    mergePolygons(cFace);
  }
}

void CompoundPolyhedron::intersectPolygon(std::vector<IntersectData>& intersections, const Polygon& polygon, const Line& line1) {
  const auto nCorners = polygon.size();
  for (uint16_t i = 0; i < nCorners; i++) {
    const auto& currentVertex = polygon[i];
    const auto& nextVertex = polygon[(i + 1) % nCorners];
    if (glm::epsilonEqual(currentVertex.x, nextVertex.x, 1.0e-6) &&
        glm::epsilonEqual(currentVertex.y, nextVertex.y, 1.0e-6) &&
        glm::epsilonEqual(currentVertex.z, nextVertex.z, 1.0e-6))
      continue;
    Line line2;
    line2.direction = glm::normalize(nextVertex - currentVertex);
    line2.point = currentVertex;
    const auto intersectVertex = Polyhedron::getLinesIntersect(line1, line2);
    assert(!isnan(intersectVertex.x));
    if (glm::length2(intersectVertex) < 1.0e-8)
      // Parallel lines
      continue;
    if (!intersections.empty()) {
      const auto prevVertex = intersections.back().vertex;
      if (glm::epsilonEqual(currentVertex.x, prevVertex.x, 1.0e-4) &&
          glm::epsilonEqual(currentVertex.y, prevVertex.y, 1.0e-4) &&
          glm::epsilonEqual(currentVertex.z, prevVertex.z, 1.0e-4))
        continue;
    }
    const auto edgeLength = glm::distance(currentVertex, nextVertex);

    const auto distance13 = glm::distance(intersectVertex, currentVertex);
    if (distance13 < edgeLength * 0.0001)
      continue;

    const auto t = lineDeltaT(line2, intersectVertex);
    if (t < -0.01 * edgeLength || t > 1.01 * edgeLength)
      continue;

    IntersectData data;
    data.firstIndex = i;
    data.factor = t / edgeLength;
    data.vertex = intersectVertex;
    intersections.push_back(data);
  }
}

void CompoundPolyhedron::intersectSetOfFaces(const std::vector<glm::dvec3>& vertices, std::vector<std::vector<Face>>& oldFacesList) {
  const auto nCompounds = oldFacesList.size();
  for (size_t i = 0; i < nCompounds; i++) {
    intersectCompounds(vertices, oldFacesList);
    // Move the first to the end
    // 1-2 -> 2-1
    // 1-2-3-4 -> 2-3-4-1 -> ...
    oldFacesList.push_back(oldFacesList.front());
    oldFacesList.erase(oldFacesList.begin());
  }
}

IntersectFaces CompoundPolyhedron::intersectTwoFaces(const std::vector<glm::dvec3>& vertices, CompoundFace& cFace, const Face& oldFace2) const {
  // Determine the planes of the two supplied faces
  const auto plane1 = cFace.oldFace->getPlane(vertices);
  const auto plane2 = oldFace2.getPlane(vertices);
  if (glm::length2(plane1.normal - plane2.normal) < 0.00001) {
    return IntersectFaces::EQUIPLANAR;
  }
  // Get the line intersection plane 1 and 2
  Line line1;
  Polyhedron::getPlanesIntersection(plane1, plane2, line1);

  const auto polygon1 = cFace.oldFace->getPolygon(vertices);
  std::vector<IntersectData> intersections1 = {};
  intersectPolygon(intersections1, polygon1, line1);
  // One intersection means intersecting at a corner
  if (intersections1.size() < 2)
    return IntersectFaces::NO_INTERSECT;
  assert(intersections1.size() == 2);
  if (glm::length2(intersections1.front().vertex - intersections1.back().vertex) < 0.0001)
    return IntersectFaces::NO_INTERSECT;

  const auto polygon2 = oldFace2.getPolygon(vertices);
  std::vector<IntersectData> intersections2;
  intersectPolygon(intersections2, polygon2, line1);
  if (intersections2.size() < 2)
    return IntersectFaces::NO_INTERSECT;
  assert(intersections2.size() == 2);
  // The segments of <intersections1> and <intersections2> may not overlap
  const auto P0 = intersections1.at(0).vertex;
  const auto dir1 = intersections1.at(1).vertex - P0;
  auto intersections = intersections1;
  if (glm::length2(intersections.at(1).vertex - intersections.at(0).vertex) < 0.0001 * glm::length2(dir1))
    return IntersectFaces::NO_INTERSECT;
  // Create a new face
  glm::dvec3 center(0.0, 0.0, 0.0);
  // Create plane through <center> parallel to <line1>
  Plane centerPlane;
  centerPlane.normal = line1.direction;
  centerPlane.point = center;
  const auto v3 = Polyhedron::getPlaneLineIntersection(centerPlane, line1);
  centerPlane.normal = glm::normalize(v3 - center);

  VertexFace newFace{};
  newFace.origin = 4;
  createNewFace2(newFace.vertices, intersections, polygon1, plane2, centerPlane);

  glm::dmat4 xyMat(1.0);
  const auto axis2D = transform2D(newFace.vertices, cFace.faceCenter, xyMat);
  Ring2D ring;
  if (Polyhedron::createRing2D(newFace.vertices, xyMat, axis2D, ring))
    cFace.vertexFaces.push_back(newFace);
  else
    printf("Invalid ring\n");
  return IntersectFaces::INTERSECT;
}

double CompoundPolyhedron::lineDeltaT(const Line& line, const glm::dvec3& v) {
  // Check whether v is between v1 and v2
  // v3 = v1 + t * dir
  const auto dx = (v.x - line.point.x);
  const auto dy = (v.y - line.point.y);
  const auto dz = (v.z - line.point.z);
  if (abs(dx) > abs(dy) && abs(dx) > abs(dz))
    return dx / line.direction.x;
  else if (abs(dy) > abs(dz))
    return dy / line.direction.y;
  else
    return dz / line.direction.z;
}

void CompoundPolyhedron::mergeCompoundFaces(const std::vector<std::vector<CompoundFace>>& oldFaceList, std::vector<CompoundFace>& newFaces) {
  assert(!oldFaceList.empty());
  const auto nFaces = oldFaceList.front().size();
  for (size_t i = 0; i < nFaces; i++) {
    auto it1 = oldFaceList.begin();
    auto cFace1 = it1.base()->at(i);
    assert(!cFace1.vertexFaces.empty());
    glm::dmat4 xyMat(1.0); // Create an identity matrix
    const auto axis2D = transform2D(cFace1, xyMat);
    const auto xyMatInverted = glm::inverse(xyMat);
    auto it2 = std::next(it1, 1);
    while (it2 != oldFaceList.end()) {
      assert(it2.base()->size() == nFaces);
      CompoundFace cFaceMerge{};
      const auto& cFace2 = it2.base()->at(i);
      mergeTwoCompounds(cFace1, cFace2, xyMat, xyMatInverted, axis2D, cFaceMerge);
      cFace1 = cFaceMerge;
      it2 = std::next(it2, 1);
    }
    newFaces.push_back(cFace1);
  }
}

AxisType CompoundPolyhedron::transform2D(const Face& face, const std::vector<glm::dvec3>& vertices, glm::dmat4x4& transform) {
  auto rotation = glm::dmat4(1.0);
  const auto result = Polyhedron::rotation2D(rotation, face.getVertices(vertices));
  const auto translation = glm::translate(glm::dmat4(1.0), -face.getCenter(vertices));
  transform = rotation * translation;
  return result;
}

void CompoundPolyhedron::mergeFaces(const std::vector<glm::dvec3>& vertices, const Face& face1, const Face& face2, std::vector<glm::dvec3>& newFace) {
  glm::dmat4 transform;
  const auto axis2D = transform2D(face1, vertices, transform);
  const auto ring1 = Polyhedron::createRing2D(face1.getVertices(vertices), transform, axis2D);
  const auto ring2 = Polyhedron::createRing2D(face2.getVertices(vertices), transform, axis2D);
  // Unite the polygons
  std::vector<Ring2D> united;
  bg::union_(ring1, ring2, united);
  auto pUnited = united.front();
  const auto rotateInverted = glm::inverse(transform);
  // Build <newFace>
  const auto ring3 = cleanClosedVec2Ring(pUnited);
  for (const auto& p : ring3) {
    glm::dvec3 v1;
    switch (axis2D) {
    case AxisType::X:
      v1 = {0.0, p.x, p.y};
      break;
    case AxisType::Y:
      v1 = {p.y, 0.0, p.x};
      break;
    case AxisType::Z:
      v1 = {p.x, p.y, 0.0};
      break;
    }
    // Rotate and translate to position in the polyhedron
    newFace.push_back(glm::dvec3(rotateInverted * glm::dvec4(v1, 1.0)));
  }
}

int CompoundPolyhedron::getNumberOfCommomVertices(const Ring2D& ring1, const Ring2D& ring2) {
  int result = 0;
  for (size_t i = 0; i < ring1.size() - 1; i++) {
    for (size_t j = 0; j < ring2.size() - 1; j++) {
      if (glm::length(ring1[i] - ring2[j]) < 0.0001 * glm::length(ring1[i] + ring2[j])) {
        result++;
      }
    }
  }
  return result;
}

void CompoundPolyhedron::rebuildPolyFace(const glm::dmat4& transform3D, AxisType axis2D, PolyFace& pFace) {
  pFace.face.clear();
  const auto& ring = pFace.ring2D;
  for (auto it = std::begin(ring); it != std::end(ring) - 1; ++it) {
    // Make 3D vector in XY plane (z=0)
    glm::dvec3 v1(0);
    const auto x = bg::get<0>(*it);
    const auto y = bg::get<1>(*it);
    switch (axis2D) {
    case AxisType::X:
      // Y -> Z -> X
      v1 = {0.0, x, y};
      break;
    case AxisType::Y:
      // Z -> X -> Y
      v1 = {y, 0.0, x};
      break;
    case AxisType::Z:
      v1 = {x, y, 0.0};
      break;
    }
    // Rotate and translate to position in the polyhedron
    const glm::dvec3 v2(transform3D * glm::dvec4(v1, 1.0));
    pFace.face.push_back(v2);
  }
}

void CompoundPolyhedron::addPolyface(const PolyFace& pFace, const size_t nFaces, ushort iColor) {
  auto normal1 = glm::triangleNormal(pFace.face.vertices.at(0), pFace.face.vertices.at(1), pFace.face.vertices.at(2));
  if (glm::dot(normal1, pFace.face.vertices.at(0)) < 0)
    normal1 = -normal1;
  bool equiplanar = false;
  for(size_t iFace = 0; iFace < nFaces; iFace++) {
    const auto& face = m_target->getFace(iFace);
    const auto normal2 = face.calculateNormal(m_target->getVertices());
    if (glm::dot(normal2, normal1) > 0.9999) {
      equiplanar = true;
      m_target->setFaceColorIndex(iFace, 2);
      break;
    }
  }
  if (!equiplanar) {
    assert(bg::is_valid(pFace.ring2D));
    m_target->appendFace(pFace.face.vertices, iColor);
  }
}

void CompoundPolyhedron::mergePolygons (const CompoundFace& cFace) {
  // Assume all faces are in one plane
  // So have all the same normal
  assert(!cFace.vertexFaces.empty());
  std::multimap<double, PolyFace, std::greater_equal<double>> polyFaces;
  glm::dmat4 xyMat(1.0); // Create an identity matrix
  const auto axis2D = transform2D(cFace, xyMat);
  const auto xyMatInverted = glm::inverse(xyMat);
  for (const auto& vFace : cFace.vertexFaces) {
    PolyFace pFace = {};
    pFace.face = vFace;
    pFace.ring2D = Polyhedron::createRing2D(vFace.vertices, xyMat, axis2D);
    pFace.area = bg::area(pFace.ring2D);
    pFace.skip = false;
    polyFaces.insert(std::make_pair(pFace.area, pFace));
  }
  // Get the number of faces before new faces are added
  const auto nFaces = m_target->getNumberOfFaces();
  for (auto it1 = polyFaces.begin(); it1 != polyFaces.end(); ++it1) {
    if (it1->second.skip)
      continue;

    const auto iColor = cFace.oldFace->getColorIndex();
    const auto& ring1 = it1->second.ring2D;
    for (auto it2 = std::next(it1, 1); it2 != polyFaces.end(); ++it2) {
      // Do the rings intersect?
      const auto& ring2 = it2->second.ring2D;
      if (bg::disjoint(ring1, ring2))
        continue;
      const auto sizeRing2 = ring2.size();
      if (bg::covered_by(ring2, ring1)) {
        glm::dvec2 center(0.0,0.0);
        for (size_t j = 0; j < sizeRing2 - 1; j++) {
          center = center + ring2[j];
        }
        center /= (sizeRing2 - 1);
        if (bg::covered_by(center, ring1))
          it2->second.skip = true;
        continue;
      }
      std::vector<Ring2D> united;
      bg::union_(ring1, ring2, united);
      assert(!united.empty());
      if (united.size() > 1)
        continue;
      const auto commonPoints = getNumberOfCommomVertices(ring1, ring2);
      std::vector<Ring2D> intersections{};
      bg::intersection(ring1, ring2, intersections);
      const auto unitedRing = united.front();
      if (commonPoints < 2) {
        if (intersections.empty())
          continue;
        const auto area_united = bg::area(unitedRing);
        double area_max_overlap = 0;
        for (const auto& intersection : intersections) {
          const auto area_overlap = bg::area(intersection);
          if (area_overlap > area_max_overlap)
            area_max_overlap = area_overlap;
        }
        if (area_max_overlap < 0.0001 * area_united)
          continue;
      }
      if (!bg::is_valid(unitedRing)) {
        assert(bg::overlaps(ring1, ring2));
        continue;
      }
      it2->second.skip = true;
      it1->second.ring2D = cleanRing2D(unitedRing);
      assert(bg::is_valid(it1->second.ring2D));
      // Rebuild the face <face1>
      rebuildPolyFace(xyMatInverted, axis2D, it1->second);
    }
    // Check for equiplanar faces
    addPolyface(it1->second, nFaces, iColor);
  }
}

void CompoundPolyhedron::mergeTwoCompounds(const CompoundFace& cFace1, const CompoundFace& cFace2, const glm::dmat4& xyMat, const glm::dmat4& xyMatInverted, const AxisType axis2D, CompoundFace& cFace12) {
  std::multimap<double, MergeRing, std::greater_equal<double>> mapRing2D1;
  fillMapMergedRing(cFace1, xyMat, axis2D, mapRing2D1);
  std::multimap<double, MergeRing, std::greater_equal<double>> mapRing2D2;
  fillMapMergedRing(cFace2, xyMat, axis2D, mapRing2D2);

  cFace12.oldFace = cFace1.oldFace;
  cFace12.faceCenter = cFace1.faceCenter;
  for (const auto& pRing1 : mapRing2D1) {
    const auto& ring1 = pRing1.second.ring2D;
    assert(bg::is_valid(ring1));
    int countIntersects = 0;
    for (auto& pRing2 : mapRing2D2) {
      const auto& ring2 = pRing2.second.ring2D;
      assert(bg::is_valid(ring2));
      if (bg::disjoint(ring1, ring2))
        continue;
      std::vector<Ring2D> intersections = {};
      bg::intersection(ring1, ring2, intersections);
      if (intersections.empty())
        continue;
      countIntersects++;
      for (auto& intersection : intersections) {
        const auto area_overlap = bg::area(intersection);
        const auto min_area = (pRing1.first > pRing2.first ? pRing2.first : pRing1.first);
        if (area_overlap / min_area < 0.001)
          continue;
        bg::correct(intersection);
        updateCompoundFace(intersection, xyMatInverted, axis2D, cFace12);
      }
      // Signal this one can be skipped in the second loop
      pRing2.second.merged = true;
    }
    if (countIntersects == 0)
      updateCompoundFace(ring1, xyMatInverted, axis2D, cFace12);
  }
  // Process the faces from the second set that had no overlap with the first set
  for (const auto& pRing2 : mapRing2D2) {
    if (pRing2.second.merged)
      continue;
    updateCompoundFace(pRing2.second.ring2D, xyMatInverted, axis2D, cFace12);
  }
}

Polyhedron *CompoundPolyhedron::processRotatedVertices(const std::unique_ptr<Polyhedron>& source, const std::vector<glm::dvec3>& rotatedVertices) {
  std::vector<uint32_t> rotatedIndices{};
  auto vertices = source->getVertices();
  for (const auto& vertex : rotatedVertices) {
    rotatedIndices.push_back(Polyhedron::appendUnique(vertex, vertices));
  }
  std::vector<std::vector<Face>> compoundFacesList;
  // Get faces of first compound
  compoundFacesList.push_back(source->getFaces());
  compoundFacesList.push_back(createCompoundFaces(vertices, source->getFaces(), rotatedIndices, 1));
  rotatedIndices.clear();
  intersectSetOfFaces(vertices, compoundFacesList);
  compoundFacesList.clear();
  return m_target.release();
}

// angle : Rotation angle in radians
std::vector<glm::dvec3> CompoundPolyhedron::rotateVertices(const std::unique_ptr<Polyhedron>& source, double angle, const glm::dvec3& axis) {
  auto rotation = glm::rotate(glm::dmat4(1.0), angle, axis);
  std::vector<glm::dvec3> result{};
  for (const auto& vertex : source->getVertices()) {
    result.push_back(glm::dvec3(rotation * glm::dvec4(vertex, 1.0)));
  }
  return result;
}

AxisType CompoundPolyhedron::transform2D(const CompoundFace& cFace, glm::dmat4x4& transform) {
  glm::dmat4 rotation(1.0); // Create an identity matrix
  const auto result = Polyhedron::rotation2D(rotation, cFace.vertexFaces.front().vertices);
  transform = rotation * glm::translate(glm::dmat4(1.0), -cFace.faceCenter);
  return result;
}
AxisType CompoundPolyhedron::transform2D(const std::vector<glm::dvec3>& someVertices, const glm::dvec3& faceCenter, glm::dmat4x4& transform) {
  glm::dmat4 rotation(1.0); // Create an identity matrix
  const auto result = Polyhedron::rotation2D(rotation, someVertices);
  transform = rotation * glm::translate(glm::dmat4(1.0), -faceCenter);
  return result;
}

void CompoundPolyhedron::updateCompoundFace(const Ring2D& ring, const glm::dmat4x4 xyMatInverted, AxisType axis2D, CompoundFace& cFace) {
  const auto ring2 = cleanRing2D(ring);
  assert(bg::is_valid(ring2));
  VertexFace vFace;

  vFace.origin = 3;
  for (auto it = std::begin(ring2); it != std::end(ring2) - 1; ++it) {
    // Make 3D vector in XY plane (z=0)
    // Rotate and translate to position in the polyhedron
    switch (axis2D) {
    case AxisType::X:
      vFace.push_back(glm::dvec3(xyMatInverted * glm::dvec4(0.0, bg::get<0>(*it), bg::get<1>(*it), 1.0)));
      break;
    case AxisType::Y:
      // Z -> X -> Y
      vFace.push_back(glm::dvec3(xyMatInverted * glm::dvec4(bg::get<1>(*it), 0.0, bg::get<0>(*it), 1.0)));
      break;
    case AxisType::Z:
      vFace.push_back(glm::dvec3(xyMatInverted * glm::dvec4(bg::get<0>(*it), bg::get<1>(*it), 0.0, 1.0)));
      break;
    }
  }
  cFace.vertexFaces.push_back(vFace);
}

Polyhedron *CompoundPolyhedron::createCube2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createCube();
  m_target = std::make_unique<Polyhedron>(ObjectType::CUBE_2_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  // Rotational axis through one of the corners
  const glm::dvec3 axis(1.0, 1.0, 1.0);
  // Create second compound by rotating over 60 degrees
  return createSecondCompound(source, axis, M_PIf64 / 3.0);
}

Polyhedron *CompoundPolyhedron::createCube3Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createCube();
  m_target = std::make_unique<Polyhedron>(ObjectType::CUBE_3_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  return create3CompoundCubeOctahedron(source);
}

Polyhedron *CompoundPolyhedron::createCube4Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createCube();
  m_target = std::make_unique<Polyhedron>(ObjectType::CUBE_4_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  return create4CompoundCubeOctahedron(source);
}

void CompoundPolyhedron::getCubeVertices(const std::unique_ptr<Polyhedron>& source, const size_t iVertexBegin, const size_t iVertexMiddle, const size_t excludeFace, std::vector<uint16_t>& iVertices) {
  for (size_t iFace = 0; iFace < source->getNumberOfFaces(); iFace++) {
    // Skip the original face <iVertexBegin> is in
    if (iFace == excludeFace)
      continue;
    const auto face = source->getFace(iFace);
    const auto nCorners = face.getCorners();
    for (size_t i = 0; i < nCorners; i++) {
      if (face[i] != iVertexBegin)
        continue;
      // Found vertex <iVertexBegin> in another face
      if (face[(i + 1) % nCorners] == iVertexMiddle)
        iVertices.push_back(face[(i - 2) % nCorners]);
      else
        iVertices.push_back(face[(i + 2) % nCorners]);
      break;
    }
  }
  assert(iVertices.size() == 2);
}

Polyhedron *CompoundPolyhedron::createCube5Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createDodecahedron(0);

  std::unique_ptr<Polyhedron> compound = std::make_unique<Polyhedron>(m_colorList, false);
  const auto oldFace1 = source->getFace(0);
  auto iOldVertex1 = oldFace1[0];
  auto iOldVertex2 = oldFace1[2];
  std::vector<uint16_t> iOldVertices;
  getCubeVertices(source, iOldVertex2, oldFace1[1], 0, iOldVertices);
  const auto vertex1 = source->getVertex(iOldVertex1);
  const auto axis1 = vertex1 + source->getVertex(iOldVertices.front());
  compound->createFace(0, M_PI_2f64, axis1, vertex1);
  // Make a copy; No reference. So no '  &face1 ' here
  const auto face1 = compound->getFace(0);
  // Build other faces
  const auto vertex2 = source->getVertex(iOldVertex2);
  const auto axis2 = vertex1 - vertex2;
  glm::dmat4 unit_m4(1.0); // Create an identity matrix
  const auto rotation2 = glm::rotate(unit_m4, M_PI_2f64, axis2);
  compound->rotateFace(face1, rotation2);
  const auto axis3 = vertex2 - source->getVertex(iOldVertices.front());
  const auto rotation3 = glm::rotate(unit_m4, M_PI_2f64, axis3);
  compound->rotateFace(face1, rotation3);
  const auto axis4 = oldFace1.getCenter(source->getVertices());
  const auto rotatedVertices1 = rotateVertices(compound, 2 * M_PIf64 / 5, axis4);
  const auto rotatedVertices2 = rotateVertices(compound, 4 * M_PIf64 / 5, axis4);
  const auto rotatedVertices3 = rotateVertices(compound, -2 * M_PIf64 / 5, axis4);
  const auto rotatedVertices4 = rotateVertices(compound, -4 * M_PIf64 / 5, axis4);
  const auto nVertices = compound->getNumberOfVertices();
  // Prevent duplicate vertices
  // Can not do: vertices += rotatedVertices;
  std::vector<uint32_t> rotatedIndices1;
  std::vector<uint32_t> rotatedIndices2;
  std::vector<uint32_t> rotatedIndices3;
  std::vector<uint32_t> rotatedIndices4;
  for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
    rotatedIndices1.push_back(compound->appendUnique(rotatedVertices1[iVertex]));
    rotatedIndices2.push_back(compound->appendUnique(rotatedVertices2[iVertex]));
    rotatedIndices3.push_back(compound->appendUnique(rotatedVertices3[iVertex]));
    rotatedIndices4.push_back(compound->appendUnique(rotatedVertices4[iVertex]));
  }
  std::vector<std::vector<Face>> compoundFacesList;
  const auto& oldFaces = compound->getFaces();
  // Get faces of second compound
  const auto compoundFaces1 = createCompoundFaces(compound, rotatedIndices1, 1);
  // Get faces of third compound
  const auto compoundFaces2 = createCompoundFaces(compound, rotatedIndices2, 2);
  // Get faces of fourth compound
  const auto compoundFaces3 = createCompoundFaces(compound, rotatedIndices3, 3);
  // Get faces of fifth compound
  const auto compoundFaces4 = createCompoundFaces(compound, rotatedIndices4, 4);
  // There are three sets of faces: <faces>, compoundFaces1 and compoundFaces2
  compoundFacesList.push_back(oldFaces);
  compoundFacesList.push_back(compoundFaces1);
  compoundFacesList.push_back(compoundFaces2);
  compoundFacesList.push_back(compoundFaces3);
  compoundFacesList.push_back(compoundFaces4);
  m_target = std::make_unique<Polyhedron>(ObjectType::CUBE_5_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  intersectSetOfFaces(compound->getVertices(), compoundFacesList);
  compoundFacesList.clear();
  return m_target.release();
}

Polyhedron *CompoundPolyhedron::createCuboctahedron2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createCuboctahedron(0, 1);
  m_target = std::make_unique<Polyhedron>(ObjectType::CUBOCTAHEDRON_2_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  const glm::dvec3 axis(1.0, 1.0, 1.0);
  const auto rotatedVertices = rotateVertices(source, M_PIf64 / 3.0, axis);
  return processRotatedVertices(source, rotatedVertices);
}

Polyhedron *CompoundPolyhedron::createDodecahedron2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createTetrakisHexahedron();
  return TransformSolid::transform(source, PolyhedronTransformation::STELLATION, 1);
}

Polyhedron *CompoundPolyhedron::createIcosahedron2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createIcosahedron(0);
  m_target = std::make_unique<Polyhedron>(ObjectType::ICOSAHEDRON_2_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  // Rotational axis right between two neighbor vertices
  const auto vertex = (source->getVertex(0) + source->getVertex(1)) / 2.0;
  // Rotate 90 degrees
  return createSecondCompound(source, vertex, M_PI_2f64);
}

Polyhedron *CompoundPolyhedron::createOctahedron2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createOctahedron(0);
  m_target = std::make_unique<Polyhedron>(ObjectType::OCTAHEDRON_2_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  // Pick one of its three-fold axis
  const glm::dvec3 axis(1.0, 1.0, 1.0);
  // Rotate all vertices 60 degrees to get the second compound
  const auto rotatedVertices = rotateVertices(source, M_PIf64 / 3.0, axis);
  return processRotatedVertices(source, rotatedVertices);
}

Polyhedron *CompoundPolyhedron::createOctahedron3Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createOctahedron(0);
  m_target = std::make_unique<Polyhedron>(ObjectType::OCTAHEDRON_3_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  return create3CompoundCubeOctahedron(source);
}

Polyhedron *CompoundPolyhedron::createOctahedron4Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createOctahedron(0);
  m_target = std::make_unique<Polyhedron>(ObjectType::OCTAHEDRON_4_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  return create4CompoundCubeOctahedron(source);
}

Polyhedron *CompoundPolyhedron::createRhombicDodecahedron2Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createRhombicDodecahedron();
  m_target = std::make_unique<Polyhedron>(ObjectType::RHOMBIC_DODECAHEDRON_2_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  const glm::dvec3 axis1(1.0, 1.0, 1.0);
  const auto rotatedVertices = rotateVertices(source, M_PIf64 / 3.0, axis1);
  return processRotatedVertices(source, rotatedVertices);
}

Polyhedron *CompoundPolyhedron::createTetrahedron4Compound() {
  std::unique_ptr<Polyhedron> source = std::make_unique<Polyhedron>(m_colorList, false);
  source->createTetrahedron();
  m_target = std::make_unique<Polyhedron>(ObjectType::TETRAHEDRON_4_COMPOUND, m_colorList, true);
  m_target->m_isConvex = false;
  std::vector<std::vector<glm::dvec3>> rotatedVerticesList;
  for (const auto& vertex : source->getVertices()) {
    const auto rotatedVertices = rotateVertices(source, M_PIf64 / 3.0, vertex);
    rotatedVerticesList.push_back(rotatedVertices);
  }
  const auto nVertices = source->getNumberOfVertices();
  // Prevent duplicate vertices
  // Can not do: vertices += rotatedVertices;
  std::vector<uint32_t> rotatedIndices[4];
  for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
    rotatedIndices[0].push_back(source->appendUnique(rotatedVerticesList[0][iVertex]));
    rotatedIndices[1].push_back(source->appendUnique(rotatedVerticesList[1][iVertex]));
    rotatedIndices[2].push_back(source->appendUnique(rotatedVerticesList[2][iVertex]));
    rotatedIndices[3].push_back(source->appendUnique(rotatedVerticesList[3][iVertex]));
  }
  std::vector<std::vector<Face>> compoundFacesList;
  for (ushort i = 0; i < nVertices; i++) {
    compoundFacesList.push_back(createCompoundFaces(source, rotatedIndices[i], i % m_colorList.size()));
    rotatedIndices[i].clear();
  }
  intersectSetOfFaces(source->getVertices(), compoundFacesList);
  compoundFacesList.clear();
  return m_target.release();
}
