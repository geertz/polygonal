#include "expandedpolyhedron.h"

ExpandedPolyhedron::ExpandedPolyhedron(const std::unique_ptr<Polyhedron>& source) {
  const auto& originalVertices = source->getVertices();
  const auto multiplier = calculateMultiplier(source);
  std::vector<ExpandObject> objList;
  uint16_t iNewVertex = 0;
  ObjectType newType = ObjectType::NONE;
  switch (source->getType()) {
  case ObjectType::CUBE:
    newType = ObjectType::RHOMBI_CUBOCTAHEDRON;
    break;
  case ObjectType::DODECAHEDRON:
    newType = ObjectType::RHOMBICOSIDODECAHEDRON;
    break;
  case ObjectType::ICOSAHEDRON:
    newType = ObjectType::RHOMBICOSIDODECAHEDRON;
    break;
  case ObjectType::OCTAHEDRON:
    newType = ObjectType::RHOMBI_CUBOCTAHEDRON;
    break;
  case ObjectType::TETRAHEDRON:
    newType = ObjectType::CUBOCTAHEDRON;
    break;
  default:
    newType = ObjectType::NONE;
    break;
  }
  m_target = std::make_unique<Polyhedron>(newType, source->getColorList(), true);
  m_target->m_isConvex = source->m_isConvex;
  const auto nColors = source->getColorList().size();
  auto iNewColor = (source->getLastColor() + 1) % nColors;
  const auto nFaces = source->getNumberOfFaces();
  // Store the indices of the new vertices related to the original one
  m_vertexSourceTarget.resize(originalVertices.size());
  //auto indexList = std::vector<std::vector<uint16_t>>(originalVertices.size());
  for (size_t iFace = 0; iFace < nFaces; iFace++) {
    const auto& originalFace1 = source->getFace(iFace);
    const auto addOn = originalFace1.calculateNormal(originalVertices) * multiplier;
    Face newFace(originalFace1.getColorIndex());
    ExpandObject obj;
    for (const auto iOriginalVertex : originalFace1) {
      newFace.push_back_unique(iNewVertex);
      auto& list1 = m_vertexSourceTarget[iOriginalVertex];
      list1.push_back(iNewVertex);
      obj.newVertexIndices.push_back(iNewVertex);
      m_target->appendVertex(originalVertices[iOriginalVertex] + addOn);
      iNewVertex++;
    }
    objList.push_back(obj);
    // Link the original face with the new Face
    m_pairFaceFaceList.insert(std::make_pair(iFace, m_target->getNumberOfFaces()));
    m_target->appendFace(newFace);
    // Add rectangles between two faces
    if (iFace > 0)
      addRectangles(source, iFace, objList, static_cast<uint16_t>(iNewColor));
  }
  // Create new faces at the original vertices
  iNewColor = (iNewColor + 1) % nColors;
  Polyhedron::createCornerfaces(source, m_target, m_vertexSourceTarget, static_cast<ushort>(iNewColor), m_pairVertexFaceList);
  m_target->scale(source->getMaxRadius() / m_target->getMaxRadius());
}

// Calculate the multiplier of the face normal
double ExpandedPolyhedron::calculateMultiplier(const std::unique_ptr<Polyhedron>& source) {
  const auto& originalVertices = source->getVertices();
  const auto lengthEdge = glm::distance(originalVertices.at(0), originalVertices.at(1));
  const auto& face0 = source->getFace(0);
  const auto normal0 = face0.calculateNormal(originalVertices);
  // Find the first face neighboring the first one and
  // return the multiplier for which the edge length of the new faces will be similar the original faces
  const auto nFaces = source->getNumberOfFaces();
  for (size_t i = 1; i < nFaces; i++) {
    const auto& face1 = source->getFace(i);
    if (face0.areNeighbors(face1)) {
      const auto normal1 = face1.calculateNormal(originalVertices);
      const auto dotp = glm::dot(normal0, normal1);
      const auto angle = acos(dotp);
      return lengthEdge / angle;
    }
  }
  return 0;
}
// Create the rectangles between face <iFace> and previous faces
void ExpandedPolyhedron::addRectangles(const std::unique_ptr<Polyhedron>& source, size_t iFace, const std::vector<ExpandObject>& objList, uint16_t iColor) {
  const auto& originalFace1 = source->getFace(iFace);
  const auto nCorners1 = originalFace1.getCorners();
  const auto& obj1 = objList.at(iFace);
  for (size_t jFace = 0; jFace < iFace; jFace++) {
    bool foundFirstEdge = false;
    bool foundSecondEdge = false;
    size_t edgeI1;
    size_t edgeI2;
    size_t edgeJ1;
    size_t edgeJ2;
    const auto& originalFace2 = source->getFace(jFace);
    const auto nCorners2 = originalFace2.getCorners();
    for (size_t corner1 = 0; corner1 < nCorners1; corner1++) {
      const auto iVertex1 = originalFace1.at(corner1);
      for (size_t corner2 = 0; corner2 < nCorners2; corner2++) {
        if (originalFace2.at(corner2) != iVertex1)
          continue;
        if (!foundFirstEdge) {
          // First two corners
          foundFirstEdge = true;
          edgeI1 = corner1;
          edgeJ1 = corner2;
        } else {
          // Second set of two corners
          foundSecondEdge = true;
          edgeI2 = corner1;
          edgeJ2 = corner2;
        }
        break;
      }
      if (foundSecondEdge)
        // Found all four
        break;
    }
    if (!foundSecondEdge)
      continue;
    Face newFace(iColor);
    const auto& obj2 = objList[jFace];
    newFace.push_back_unique(static_cast<uint16_t>(obj1.newVertexIndices[edgeI1]));
    newFace.push_back_unique(static_cast<uint16_t>(obj2.newVertexIndices[edgeJ1]));
    newFace.push_back_unique(static_cast<uint16_t>(obj2.newVertexIndices[edgeJ2]));
    newFace.push_back_unique(static_cast<uint16_t>(obj1.newVertexIndices[edgeI2]));
    // Find the original edge
    bool foundEdge = false;
    for (size_t iEdge = 0; iEdge < source->getNumberOfEdges(); iEdge++) {
      const auto& edge = source->getEdge(iEdge);
      if (edge.iFace1 != iFace && edge.iFace2 != iFace)
        continue;
      if (edge.iFace1 != jFace && edge.iFace2 != jFace)
        continue;
      foundEdge = true;
      m_pairEdgeFaceList.insert(std::make_pair(iEdge, m_target->getNumberOfFaces()));
      break;
    }
    assert(foundEdge);
    m_target->appendFace(newFace);
  }
}

size_t ExpandedPolyhedron::getTargetFaceFromSourceEdge(size_t iFaceSource) const {
  const auto it = m_pairEdgeFaceList.find(iFaceSource);
  if (it == m_pairEdgeFaceList.cend())
    return size_t(SIZE_MAX);
  return it->second;
}

size_t ExpandedPolyhedron::getTargetFaceFromSourceFace(size_t iFaceSource) const {
  const auto it = m_pairFaceFaceList.find(iFaceSource);
  if (it == m_pairFaceFaceList.cend())
    return size_t(SIZE_MAX);
  return it->second;
}

size_t ExpandedPolyhedron::getTargetFaceFromSourceVertex(size_t iVertexSource) const {
  const auto it = m_pairVertexFaceList.find(iVertexSource);
  if (it == m_pairVertexFaceList.cend())
    return size_t(SIZE_MAX);
  return it->second;
}
