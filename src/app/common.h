#pragma once

#include <string>

template <typename T>
struct ErrorValue {
  ErrorValue( std::string e, T & v )
    : error( e )
    , value( v )
  {}
  ErrorValue(T & v )
    : error( "" )
    , value( v )
  {}
  ErrorValue( std::string e, T && v )
    : error( e )
    , value( std::move( v ) )
  {}
  ErrorValue(T && v )
    : error( "" )
    , value( std::move( v ) )
  {}

  std::string error;
  T           value;
};
