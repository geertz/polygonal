#include "polyhedron.h"
#include <algorithm>
#include <utility>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/normal.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtc/epsilon.hpp>

#define golden_ratio double((1.0 + sqrt(5)) / 2.0)
#define reverse_golden_ratio double(1.0 / golden_ratio)

typedef bg::model::segment<Point2D> Segment2D;

struct ClipEar {
  Ring2D triangle;
  ulong start;
};

Edge::Edge() {}

Edge::Edge(size_t iVertex1, size_t iVertex2, size_t iFace) {
  if (iVertex2 > iVertex1) {
    iVertexBegin = iVertex1;
    iVertexEnd = iVertex2;
  } else {
    iVertexBegin = iVertex2;
    iVertexEnd = iVertex1;
  }
  iFace1 = iFace;
  iFace2 = SIZE_MAX;
}

Polyhedron::Polyhedron(ObjectType aType, const std::vector<glm::vec3>& aColorList, bool createEdges) {
  m_type        = aType;
  m_colorList   = aColorList;
  m_updateEdges = createEdges;
}

Polyhedron::Polyhedron(const std::vector<glm::vec3>& aColorList, bool createEdges) {
  m_type        = ObjectType::NONE;
  m_colorList   = aColorList;
  m_updateEdges = createEdges;
}

Polyhedron::Polyhedron(ObjectType aType, bool createEdges) {
  m_type        = aType;
  m_updateEdges = createEdges;
}
// Add the edges of face <face>. <iFace> is the index of face <face>
void Polyhedron::appendEdges(const Face &face) {
  const auto iFace = m_faces.size() - 1;
  const auto nCorners = face.getCorners();
  for (size_t i = 0; i < nCorners; i++) {
    const auto iVertex1 = face.at(i);
    const auto iVertex2 = face.at((i + 1) % nCorners);
    Edge newEdge(iVertex1, iVertex2, iFace);
    bool edgeFound = false;
    for (Edge &edge : m_edges.list) {
      if (edge.iVertexBegin == newEdge.iVertexBegin &&
          edge.iVertexEnd == newEdge.iVertexEnd) {
        edgeFound = true;
        edge.setSecondFace(iFace);
        break;
      }
    }
    if (!edgeFound) {
      const auto edgeLength = glm::length(m_vertices.at(newEdge.iVertexEnd) - m_vertices.at(newEdge.iVertexBegin));
      if (m_edges.list.empty()) {
        m_edges.maxLength = edgeLength;
        m_edges.minLength = edgeLength;
      } else if (m_edges.maxLength < edgeLength)
        m_edges.maxLength = edgeLength;
      else if (m_edges.minLength > edgeLength)
        m_edges.minLength = edgeLength;
      m_edges.list.push_back(newEdge);
    }
  }
}
// Add face <face>
void Polyhedron::appendFace(const Face& face) {
  m_faces.push_back(face);
  if (m_updateEdges) {
    appendEdges(face);
  }
}
// Create a face using the vertices <someVertices> with colot <iColor>
// Used to create compounds
void Polyhedron::appendFace(const std::vector<glm::dvec3>& someVertices, ushort iColor) {
  Face newFace(iColor);
  for (const auto &vertex : someVertices) {
    appendFaceWithUniqueVertex(newFace, vertex);
  }
  assert(!newFace.invalid);
  m_faces.push_back(newFace);
  if (m_updateEdges) {
    appendEdges(newFace);
  }
}

int Polyhedron::appendFaceWithVertex(Face& face, const glm::dvec3& vertex, std::vector<glm::dvec3>& someVertices) {
  auto iVertex = Polyhedron::indexOfVector(someVertices, vertex);
  if (iVertex < 0) {
    iVertex = static_cast<int>(someVertices.size());
    someVertices.push_back(vertex);
  }
  face.push_back(static_cast<uint16_t>(iVertex));
  return iVertex;
}
// Add vertex <vertex> to face <face> and collection <someVertices>
int Polyhedron::appendFaceWithUniqueVertex(Face& face, const glm::dvec3& vertex, std::vector<glm::dvec3>& someVertices) {
  auto iVertex = Polyhedron::indexOfVector(someVertices, vertex);
  if (iVertex < 0) {
    iVertex = static_cast<int>(someVertices.size());
    someVertices.push_back(vertex);
  }
  face.push_back_unique(static_cast<uint16_t>(iVertex));
  return iVertex;
}

void Polyhedron::cleanUp() {
  m_edges.list.clear();
  m_faces.clear();
  m_vertices.clear();
}
// Check whether already exists
// Used in an assert to create stellations
auto Polyhedron::faceExists(const Face& face) const -> bool {
  for (const auto& f : m_faces) {
    size_t count = 0;
    for (const auto iVertex : f) {
      if (!face.contains(iVertex))
        break;
      count++;
    }
    if (count == face.getCorners())
      return true;
  }
  return false;
}

void Polyhedron::fillNormals() {
  for (auto& face : m_faces) {
    face.setNormal(m_vertices);
  }
}

/*! A method to calculate intersection line between two planes.
* \param n1 is normal of the first plane,
* \param C1 is arbitrary 3D point on the first plane,
* \param n2 is normal of the second plane,
* \param C2 is arbitrary point on the second plane,
* \param P0 is result point on the intersection line,
* \param u is result directional vector of the intersection line.
* \return 2 if intersection exists and was found.
*/
int Polyhedron::getPlanesIntersection(const glm::dvec3& n1, const glm::dvec3& C1,
                                      const glm::dvec3& n2, const glm::dvec3& C2,
                                      glm::dvec3& P0, glm::dvec3& u) {
  /* cross product of normals */
  u = glm::cross(n1, n2);

  /* absolute values of u */
  auto ax = (u.x >= 0 ? u.x : -u.x);
  auto ay = (u.y >= 0 ? u.y : -u.y);
  auto az = (u.z >= 0 ? u.z : -u.z);

  /* are two planes parallel? */
  if (glm::length2(ax + ay + az) < 1.0e-6) {
    //if (fabs(ax+ay+az) < 0.001f) {
    /* normals are near parallel */
    /* are they disjoint or coincide? */
    auto v = glm::normalize(C2 - C1);
    //v.normalize();
    /* coincide */
    if (abs(glm::dot(n1, v)) < 0.00001) return 1;
    /* disjoint */
    else return 0;
  }

  /* canvases intersect in a line */
  int maxc; // coordinate dimension index
  if (ax > ay) {
    if (ax > az) maxc =  1;
    else maxc = 3;
  }
  else {
    if (ay > az) maxc =  2;
    else maxc = 3;
  }

  /* obtain a point on the intersection line:
     * zero the max coord, and solve for the other two */
  const auto d1 = -1 * glm::dot(n1, C1);
  const auto d2 = -1 * glm::dot(n2, C2); // the constants in the 2 plane equations

  /* result coordinates */
  double xi = 0, yi = 0, zi = 0;
  switch (maxc) {
  case 1:
    xi = 0;
    yi = (d2*n1.z - d1*n2.z) /  u.x;
    zi = (d1*n2.y - d2*n1.y) /  u.x;
    break;
  case 2:                     // intersect with y=0
    xi = (d1*n2.z - d2*n1.z) /  u.y;
    yi = 0;
    zi = (d2*n1.x - d1*n2.x) /  u.y;
    break;
  case 3:                     // intersect with z=0
    xi = (d2*n1.y - d1*n2.y) /  u.z;
    yi = (d1*n2.x - d2*n1.x) /  u.z;
    zi = 0;
  }
  P0 = glm::dvec3(xi, yi, zi);
  return 2;
}

glm::dvec3 Polyhedron::getPlaneLineIntersection(const Plane& plane, const Line& line) {
  const auto m = glm::dot((plane.point - line.point), plane.normal) / glm::dot(line.direction, plane.normal);
  return line.point + m * line.direction;
}

AxisType Polyhedron::rotation2D(glm::dmat4& rotation, const std::vector<glm::dvec3>& vFace) {
  AxisType result;
  auto faceNormal = glm::triangleNormal(vFace[0], vFace[1], vFace[2]);
/*  if (glm::dot(glm::normalize(vFace[0]), faceNormal) < 0)
    faceNormal = -faceNormal;*/
  glm::dvec3 xNormal(1.0, 0.0, 0.0);
  glm::dvec3 yNormal(0.0, 1.0, 0.0);
  glm::dvec3 zNormal(0.0, 0.0, 1.0);
  if (faceNormal[0] < 0.0)
    xNormal[0] = -1.0;
  if (faceNormal[1] < 0.0)
    yNormal[1] = -1.0;
  if (faceNormal[2] < 0.0)
    zNormal[2] = -1.0;
  const auto dotpX = glm::dot(xNormal, faceNormal);
  const auto dotpY = glm::dot(yNormal, faceNormal);
  const auto dotpZ = glm::dot(zNormal, faceNormal);
  double dotp;
  glm::dvec3 normal;
  if (abs(dotpX) > abs(dotpY) && abs(dotpX) > abs(dotpZ)) {
    result = AxisType::X;
    dotp = dotpX;
    normal = xNormal;
  } else if (abs(dotpY) > abs(dotpZ)) {
    result = AxisType::Y;
    dotp = dotpY;
    normal = yNormal;
  } else {
    result = AxisType::Z;
    dotp = dotpZ;
    normal = zNormal;
  }
  const auto angle = acos(dotp); // Angle in radians
  assert(angle >= 0);
  if (abs(angle) > 0.0001 && abs(angle) < 0.9999 * M_PIf64)
    rotation = glm::rotate(rotation, angle, glm::cross(faceNormal, normal));
  else
    rotation = glm::rotate(rotation, 0.0, xNormal);
  return result;
}

AxisType Polyhedron::transform2D(const Face& face, glm::dmat4x4& transform) const {
  auto rotation = glm::dmat4(1.0);
  const auto result = rotation2D(rotation, face.getVertices(m_vertices));
  const auto translation = glm::translate(glm::dmat4(1.0), -face.getCenter(m_vertices));
  transform = rotation * translation;
  return result;
}

AxisType Polyhedron::transform2D2(const Face& face, glm::dmat4x4& transform) const {
  auto rotation = glm::dmat4(1.0);
  const auto faceCenter = face.getCenter(m_vertices);
  auto faceVertices = face.getVertices(m_vertices);
  for (auto &v : faceVertices) {
    v -= faceCenter;
  }
  const auto result = rotation2D(rotation, faceVertices);
  const auto translation = glm::translate(glm::dmat4(1.0), -faceCenter);
  transform = rotation * translation;
  return result;
}

void Polyhedron::createCube() {
  m_type = ObjectType::CUBE;
  setPlatonic(true);
#ifdef USE_VERTICES
  vertices = {
    {-1.0, -1.0, -1.0},
    { 1.0, -1.0, -1.0},
    {-1.0,  1.0, -1.0},
    { 1.0,  1.0, -1.0}
  };
  addMirrorVertices();
  // X
  appendFace({0, 2, 6, 4});
  // Y
  appendFace({0, 1, 5, 4});
  // Z
  appendFace({0, 1, 3, 2});
  // Add the mirror faces
  addMirrorFaces();
#else
  glm::dmat4x4 axis[] = {glm::dmat4x4(1.0), glm::dmat4x4(1.0)}; // Create identity matrices
  // Y-axis
  axis[0][0][0] =  0.0;
  axis[0][0][2] = -1.0;
  axis[0][2][0] =  1.0;
  axis[0][2][2] =  0.0;
  // Z-axis
  axis[1][0][0] =  0.0;
  axis[1][0][1] = -1.0;
  axis[1][1][0] =  1.0;
  axis[1][1][1] =  0.0;
  const glm::dvec3 axis1{1.0, 0.0, 0.0};
  const glm::dvec3 vertex1{1.0, 1.0, 1.0};
  // Use first axis to build a face
  createFace(M_PI_2f64, axis1, vertex1);
  // Make a copy; No reference. So no '  &face1 ' here
  const auto face1 = m_faces[0];
  // Build other faces
  for (auto iAxis = 0; iAxis < 2; ++iAxis) {
    rotateFace(face1, axis[iAxis]);
  }
#endif
}

void Polyhedron::createIcosahedron(uint16_t iColor) {
  m_type = ObjectType::ICOSAHEDRON;
  setPlatonic(true);
  m_vertices = {
    {-golden_ratio,  0.0, -1.0},
    {-golden_ratio,  0.0,  1.0},
    {-1.0, -golden_ratio, 0.0},
    {-1.0,  golden_ratio, 0.0},
    { 0.0, -1.0, -golden_ratio},
    { 0.0, -1.0,  golden_ratio}
  };
  addMirrorVertices();

  appendFace(Face({0, 1, 2}, iColor));
  appendFace(Face({0, 1, 3}, iColor));
  appendFace(Face({0, 2, 4}, iColor));
  appendFace(Face({0, 3, 6}, iColor));
  appendFace(Face({0, 4, 6}, iColor));
  appendFace(Face({1, 2, 5}, iColor));
  appendFace(Face({1, 3, 7}, iColor));
  appendFace(Face({1, 7, 5}, iColor));
  appendFace(Face({2, 4, 8}, iColor));
  appendFace(Face({2, 8, 5}, iColor));

  addMirrorFaces();
}

void Polyhedron::createIcosidodecahedron(uint16_t iColor3, uint16_t iColor5) {
  static auto golden_ratioplus1 = golden_ratio + 1.0;
  static auto golden_ratiox2 = golden_ratio * 2.0;

  m_type = ObjectType::ICOSIDODECAHEDRON;
  m_isConvex = true;
  m_vertices = {
    // (0, 0, ±2φ)
    // (±1, ±φ, ±(1 + φ)
    {-golden_ratiox2, 0.0, 0.0},
    {-golden_ratioplus1, -1.0, -golden_ratio},
    {-golden_ratioplus1, -1.0,  golden_ratio},
    {-golden_ratioplus1,  1.0, -golden_ratio},
    {-golden_ratioplus1,  1.0,  golden_ratio},
    {-golden_ratio, -golden_ratioplus1, -1.0},
    {-golden_ratio, -golden_ratioplus1,  1.0},
    {-golden_ratio,  golden_ratioplus1, -1.0},
    {-golden_ratio,  golden_ratioplus1,  1.0},
    {-1.0, -golden_ratio, -golden_ratioplus1},
    {-1.0, -golden_ratio,  golden_ratioplus1},
    {-1.0,  golden_ratio, -golden_ratioplus1},
    {-1.0,  golden_ratio,  golden_ratioplus1},
    { 0.0, -golden_ratiox2, 0.0},
    { 0.0,  0.0, -golden_ratiox2}
  };
  addMirrorVertices();

  appendFace(Face({0, 1, 3}, iColor3));
  appendFace(Face({0, 2, 4}, iColor3));
  appendFace(Face({1, 5, 9}, iColor3));
  appendFace(Face({2, 6, 10}, iColor3));
  appendFace(Face({3, 7, 11}, iColor3));
  appendFace(Face({4, 8, 12}, iColor3));
  appendFace(Face({5, 6, 13}, iColor3));
  appendFace(Face({7, 8, 16}, iColor3));
  appendFace(Face({9, 14, 17}, iColor3));
  appendFace(Face({10, 15, 18}, iColor3));

  appendFace(Face({0, 1, 5, 6, 2}, iColor5));
  appendFace(Face({0, 3, 7, 8, 4}, iColor5));
  appendFace(Face({1, 3, 11, 14, 9}, iColor5));
  appendFace(Face({2, 4, 12, 15, 10}, iColor5));
  appendFace(Face({5, 9, 17, 21, 13}, iColor5));
  appendFace(Face({6, 10, 18, 22, 13}, iColor5));

  addMirrorFaces();
}

void Polyhedron::createOctahedron(uint16_t iColor) {
  m_type = ObjectType::OCTAHEDRON;
  setPlatonic(true);
  m_vertices = {
    {-1.0,  0.0,  0.0},
    { 0.0, -1.0,  0.0},
    { 0.0,  0.0, -1.0},
    { 0.0,  0.0,  1.0},
    { 0.0,  1.0,  0.0},
    { 1.0,  0.0,  0.0}
  };
  appendFace(Face({0, 1, 2}, iColor));
  appendFace(Face({0, 1, 3}, iColor));
  appendFace(Face({0, 2, 4}, iColor));
  appendFace(Face({0, 3, 4}, iColor));
  addMirrorFaces();
}

void Polyhedron::createPentagonalBipyramid() {
  static auto a = sqrt((10.0 - 2 * sqrt(5)) / 5.0);
  static auto b = sqrt((10.0 + 2 * sqrt(5)) / 5.0);
  static auto c = sqrt((5.0 - sqrt(5)) / 10.0);
  static auto d = -1 * sqrt((5.0 + 2 * sqrt(5)) / 5.0);

  m_type = ObjectType::PENTAGONAL_BIPYRAMID;
  m_isConvex = true;
  m_vertices = {
    {0.0, 0.0, -a},
    {0.0, 0.0,  a},
    {b  , 0.0, 0.0},
    {c  , -1 * golden_ratio, 0.0},
    {c  ,      golden_ratio, 0.0},
    {d  , -1.0             , 0.0},
    {d  ,  1.0             , 0.0}
  };

  appendFace(Face({0, 2, 3}, 0));
  appendFace(Face({0, 2, 4}, 0));
  appendFace(Face({0, 3, 5}, 0));
  appendFace(Face({0, 4, 6}, 0));
  appendFace(Face({0, 5, 6}, 0));

  appendFace(Face({1, 2, 3}, 0));
  appendFace(Face({1, 2, 4}, 0));
  appendFace(Face({1, 3, 5}, 0));
  appendFace(Face({1, 4, 6}, 0));
  appendFace(Face({1, 5, 6}, 0));
}

void Polyhedron::createRhombicDodecahedron() {
  m_type = ObjectType::RHOMBIC_DODECAHEDRON;
  m_isConvex = true;
  const glm::dvec3 vertex1{0.0, 0.0, 2.0};
  const glm::dvec3 vertex2{1.0, 1.0, 1.0};
  const glm::dvec3 axis{0.0, 1.0, 1.0};
  createRhombicFace(axis, vertex1, vertex2);
  glm::dmat4 unit_m4(1.0); // Create an identity matrix
  auto rotation1 = glm::rotate(unit_m4, M_PI_2f64, m_vertices[0]);
  // Make a copy; No reference. So no '  &face1 ' here
  const auto face1 = m_faces[0];
  rotateFace(face1, rotation1);
  for (size_t iFace = 0; iFace < 4; ++iFace) {
    const auto axis2 = m_faces.at(iFace).getCenter(m_vertices);
    const auto vertex3 = 2.0 * axis2 - vertex1;
    auto rotation2 = glm::rotate(unit_m4, M_PI_2f64, vertex3);
    rotateFaceOnce(m_faces.at(iFace), rotation2);
    rotation2 = glm::rotate(unit_m4, M_PIf64, vertex3);
    rotateFaceOnce(m_faces.at(iFace), rotation2);
  }
}
// Even mutations of (±1, ±1, ±(1 + √2)).
void Polyhedron::createRhombiCuboctahedron(uint16_t iColor3, uint16_t iColor4) {
  static auto a = 1.0 + sqrt(2.0);

  m_type = ObjectType::RHOMBI_CUBOCTAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-a, -1.0, -1.0},
    {-a, -1.0,  1.0},
    {-a,  1.0, -1.0},
    {-a,  1.0,  1.0},
    {-1.0, -a, -1.0},
    {-1.0, -a,  1.0},
    {-1.0,  a, -1.0},
    {-1.0,  a,  1.0},
    {-1.0, -1.0, -a},
    {-1.0, -1.0,  a},
    {-1.0,  1.0, -a},
    {-1.0,  1.0,  a}
  };
  addMirrorVertices();

  appendFace(Face({0, 4, 8}, iColor3));
  appendFace(Face({1, 5, 9}, iColor3));
  appendFace(Face({2, 6, 10}, iColor3));
  appendFace(Face({3, 7, 11}, iColor3));

  appendFace(Face({0, 1, 3, 2}, iColor4));
  appendFace(Face({0, 4, 5, 1}, iColor4));
  appendFace(Face({0, 8, 10, 2}, iColor4));
  appendFace(Face({1, 9, 11, 3}, iColor4));
  appendFace(Face({2, 6, 7, 3}, iColor4));
  appendFace(Face({4, 5, 17, 16}, iColor4));
  appendFace(Face({4, 8, 12, 16}, iColor4));
  appendFace(Face({5, 9, 13, 17}, iColor4));
  appendFace(Face({8, 10, 14, 12}, iColor4));
  addMirrorFaces();
}

void Polyhedron::createDodecahedron(uint16_t iColor) {
  m_type = ObjectType::DODECAHEDRON;
  setPlatonic(true);
  m_vertices = {
    {-golden_ratio, -reverse_golden_ratio, 0.0},
    {-golden_ratio,  reverse_golden_ratio, 0.0},
    {-1.0, -1.0, -1.0},
    {-1.0, -1.0,  1.0},
    {-1.0,  1.0, -1.0},
    {-1.0,  1.0,  1.0},
    {-reverse_golden_ratio, 0.0, -golden_ratio},
    {-reverse_golden_ratio, 0.0,  golden_ratio},
    { 0.0, -golden_ratio, -reverse_golden_ratio},
    { 0.0, -golden_ratio,  reverse_golden_ratio}
  };
  addMirrorVertices();

  appendFace(Face({0, 1, 4, 6, 2}, iColor));
  appendFace(Face({0, 1, 5, 7, 3}, iColor));
  appendFace(Face({0, 2, 8, 9, 3}, iColor));
  appendFace(Face({1, 4, 10, 11, 5}, iColor));
  appendFace(Face({2, 6, 12, 14, 8}, iColor));
  appendFace(Face({3, 7, 13, 15, 9}, iColor));

  addMirrorFaces();
}
// iColor3 : Color index of the triangles
// iColor4 : Color index of the squares
void Polyhedron::createCuboctahedron(uint16_t iColor3, uint16_t iColor4) {
  m_type = ObjectType::CUBOCTAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-1.0, -1.0,  0.0},
    {-1.0,  0.0, -1.0},
    {-1.0,  0.0,  1.0},
    {-1.0,  1.0,  0.0},
    { 0.0, -1.0, -1.0},
    { 0.0, -1.0,  1.0}
  };
  addMirrorVertices();
  appendFace(Face({0, 1, 4}, iColor3));
  appendFace(Face({0, 2, 5}, iColor3));
  appendFace(Face({1, 3, 6}, iColor3));
  appendFace(Face({2, 3, 7}, iColor3));

  appendFace(Face({0, 1, 3, 2}, iColor4));
  appendFace(Face({0, 4, 8, 5}, iColor4));
  appendFace(Face({1, 4, 9, 6}, iColor4));

  addMirrorFaces();
}

// Snub cube
// (∛(17+3√33) − ∛(−17+3√33) − 1)/3
// (±1, ±1/t, ±t)
void Polyhedron::createSnubCube() {
  static const auto t1 = 3 * sqrt(33);
  static const auto C0 = sqrt(3 * (4 - cbrt(17 + t1) - cbrt(17 - t1))) / 6;
  static const auto C1 = sqrt(3 * (2 + cbrt(17 + t1) + cbrt(17 - t1))) / 6;
  static const auto C2 = sqrt(3 * (4 + cbrt(199 + t1) + cbrt(199 - t1))) / 6;

  m_type = ObjectType::SNUB_CUBE;
  m_isConvex = true;
  m_vertices = {
    { C1,  C0,  C2},
    { C1, -C0, -C2},
    {-C1, -C0,  C2},
    {-C1,  C0, -C2},
    { C2,  C1,  C0},
    { C2, -C1, -C0},
    {-C2, -C1,  C0},
    {-C2,  C1, -C0},
    { C0,  C2,  C1},
    { C0, -C2, -C1},
    {-C0, -C2,  C1},
    {-C0,  C2, -C1},
    { C0, -C1,  C2},
    { C0,  C1, -C2},
    {-C0,  C1,  C2},
    {-C0, -C1, -C2},
    { C2, -C0,  C1},
    { C2,  C0, -C1},
    {-C2,  C0,  C1},
    {-C2, -C0, -C1},
    { C1, -C2,  C0},
    { C1,  C2, -C0},
    {-C1,  C2,  C0},
    {-C1, -C2, -C0}
  };
  appendFace(Face({  2, 12,  0, 14 }, 1));
  appendFace(Face({  3, 13,  1, 15 }, 1));
  appendFace(Face({  4, 16,  5, 17 }, 1));
  appendFace(Face({  7, 19,  6, 18 }, 1));
  appendFace(Face({  8, 21, 11, 22 }, 1));
  appendFace(Face({  9, 20, 10, 23 }, 1));
  appendFace({  0,  8, 14 });
  appendFace({  1,  9, 15 });
  appendFace({  2, 10, 12 });
  appendFace({  3, 11, 13 });
  appendFace({  4,  0, 16 });
  appendFace({  5,  1, 17 });
  appendFace({  6,  2, 18 });
  appendFace({  7,  3, 19 });
  appendFace({  8,  4, 21 });
  appendFace({  9,  5, 20 });
  appendFace({ 10,  6, 23 });
  appendFace({ 11,  7, 22 });
  appendFace({ 12, 16,  0 });
  appendFace({ 13, 17,  1 });
  appendFace({ 14, 18,  2 });
  appendFace({ 15, 19,  3 });
  appendFace({ 16, 20,  5 });
  appendFace({ 17, 21,  4 });
  appendFace({ 18, 22,  7 });
  appendFace({ 19, 23,  6 });
  appendFace({ 20, 12, 10 });
  appendFace({ 21, 13, 11 });
  appendFace({ 22, 14,  8 });
  appendFace({ 23, 15,  9 });
  appendFace({  8,  0,  4 });
  appendFace({  9,  1,  5 });
  appendFace({ 10,  2,  6 });
  appendFace({ 11,  3,  7 });
  appendFace({ 12, 20, 16 });
  appendFace({ 13, 21, 17 });
  appendFace({ 14, 22, 18 });
  appendFace({ 15, 23, 19 });

}

void Polyhedron::createTetrahedron() {
  m_type = ObjectType::TETRAHEDRON;
  setPlatonic(true);
#ifdef USE_VERTICES
  vertices = {
    {-1.0f, -1.0f, -1.0f},
    {-1.0f,  1.0f,  1.0f},
    { 1.0f,  1.0f, -1.0f},
    { 1.0f, -1.0f,  1.0f}
  };
  appendFace({0, 1, 2});
  appendFace({0, 1, 3});
  addMirrorFaces();
#else
  const glm::dvec3 axis(1.0, 1.0, 1.0);
  const glm::dvec3 vertex(-1.0, 1.0, 1.0);
  const auto angle = 2 * M_PIf64 / 3; // 120 degrees
  createFace(angle, axis, vertex);
  createFace(angle, m_vertices[0], -axis);
  createFace(angle, m_vertices[1], -axis);
  createFace(angle, m_vertices[2], -axis);
#endif
}

void Polyhedron::createTetrakisHexahedron() {
  m_type = ObjectType::TETRAKIS_HEXAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-1.5, 0, 0},
    {-1.0, -1.0, -1.0},
    {-1.0, -1.0,  1.0},
    {-1.0,  1.0, -1.0},
    {-1.0,  1.0,  1.0},
    {0, -1.5, 0},
    {0, 0, -1.5}
  };
  addMirrorVertices();
  appendFace(Face({0, 1, 2}, 0));
  appendFace(Face({0, 1, 3}, 0));
  appendFace(Face({0, 2, 4}, 0));
  appendFace(Face({0, 3, 4}, 0));
  appendFace(Face({1, 2, 5}, 0));
  appendFace(Face({1, 3, 6}, 0));
  appendFace(Face({1, 5, 9}, 0));
  appendFace(Face({1, 6, 9}, 0));
  appendFace(Face({2, 4, 7}, 0));
  appendFace(Face({2, 5, 10}, 0));
  appendFace(Face({2, 7, 10}, 0));
  appendFace(Face({3, 4, 8}, 0));
  addMirrorFaces();
}

void Polyhedron::createTriakisOctahedron() {
  static auto a = sqrt(2.0) - 1.0;

  m_type = ObjectType::TRIAKIS_OCTAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-1.0,  0.0,  0.0},
    {-a, -a,  -a},
    {-a, -a,   a},
    {-a,  a,  -a},
    {-a,  a,   a},
    {0.0,  -1.0,  0.0},
    {0.0,  0.0,  -1.0}
  };
  addMirrorVertices();
  appendFace(Face({0, 1, 5}, 0));
  appendFace(Face({0, 1, 6}, 0));
  appendFace(Face({0, 2, 5}, 0));
  appendFace(Face({0, 2, 7}, 0));
  appendFace(Face({0, 3, 6}, 0));
  appendFace(Face({0, 3, 8}, 0));
  appendFace(Face({0, 4, 7}, 0));
  appendFace(Face({0, 4, 8}, 0));
  appendFace(Face({1, 5, 6}, 0));
  appendFace(Face({2, 5, 7}, 0));
  appendFace(Face({3, 6, 8}, 0));
  appendFace(Face({4, 7, 8}, 0));
  addMirrorFaces();
}

void Polyhedron::createTriakisTetrahedron() {
  static auto a = 5.0 / 3.0;

  m_type = ObjectType::TRIAKIS_TETRAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-a, -a,  a},
    {-a,  a, -a},
    {-1.0, -1.0, -1.0},
    {-1.0,  1.0,  1.0},
    { 1.0, -1.0,  1.0},
    { 1.0,  1.0, -1.0},
    { a, -a, -a},
    { a,  a,  a}
  };
  appendFace(Face({0, 1, 2}, 0));
  appendFace(Face({0, 1, 3}, 0));
  appendFace(Face({0, 2, 6}, 0));
  appendFace(Face({0, 4, 6}, 0));
  appendFace(Face({0, 3, 7}, 0));
  appendFace(Face({0, 4, 7}, 0));
  appendFace(Face({1, 2, 6}, 0));
  appendFace(Face({1, 5, 6}, 0));
  appendFace(Face({1, 3, 7}, 0));
  appendFace(Face({1, 5, 7}, 0));
  appendFace(Face({4, 6, 7}, 0));
  appendFace(Face({5, 6, 7}, 0));
}

void Polyhedron::createTriangularBipyramid() {
  static const auto a = 1.0 / sqrt(3);
  static const auto b = a - sqrt(3) / 2;
  static const auto c = M_SQRT2 / sqrt(3);

  m_type = ObjectType::TRIANGULAR_BIPYRAMID;
  m_isConvex = true;
  m_vertices = {
    { 0.0, a, 0.0},
    {-0.5, b, 0.0},
    { 0.5, b, 0.0},
    { 0.0, 0.0, -c},
    { 0.0, 0.0,  c}
  };

  appendFace(Face({0, 1, 3}, 0));
  appendFace(Face({0, 1, 4}, 0));
  appendFace(Face({0, 2, 3}, 0));
  appendFace(Face({0, 2, 4}, 0));
  appendFace(Face({1, 2, 3}, 0));
  appendFace(Face({1, 2, 4}, 0));
}

void Polyhedron::createTruncatedCube() {
  static auto halfEdge = M_SQRT2 - 1.0;

  m_type = ObjectType::TRUNCATED_CUBE;
  m_isConvex = true;
  m_vertices = {
      {-1.0, -1.0, -halfEdge},
      {-1.0, -1.0,  halfEdge},
      {-1.0, -halfEdge, -1.0},
      {-1.0, -halfEdge,  1.0},
      {-1.0,  halfEdge, -1.0},
      {-1.0,  halfEdge,  1.0},
      {-1.0,  1.0, -halfEdge},
      {-1.0,  1.0,  halfEdge},
      {-halfEdge, -1.0, -1.0},
      {-halfEdge, -1.0,  1.0},
      {-halfEdge,  1.0, -1.0},
      {-halfEdge,  1.0,  1.0}
  };
  addMirrorVertices();

  appendFace(Face({0, 2, 8}, 0));
  appendFace(Face({1, 3, 9}, 0));
  appendFace(Face({4, 6, 10}, 0));
  appendFace(Face({5, 7, 11}, 0));

  appendFace(Face({0, 1, 3, 5, 7, 6, 4, 2}, 1));
  appendFace(Face({0, 1, 9, 13, 17, 16, 12, 8}, 1));
  appendFace(Face({2, 4, 10, 14, 20, 18, 12, 8}, 1));

  addMirrorFaces();
}

// (0, ±1/φ, ±(2 + φ))
// (±1/φ, ±φ, ±2*φ)
// (±φ, ±2, ±(φ + 1))
// Edge length : 2*φ - 2 = sqrt(5) - 1
void Polyhedron::createTruncatedDodecahedron() {
  static auto golden_ratioplus1 = golden_ratio + 1.0;
  static auto golden_ratioplus2 = golden_ratio + 2.0;
  static auto golden_ratiox2 = 1.0 + sqrt(5.0); // golden_ratio * 2.0;

  m_type = ObjectType::TRUNCATED_DODECAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-golden_ratioplus2, 0.0, -reverse_golden_ratio},
    {-golden_ratioplus2, 0.0,  reverse_golden_ratio},
    {-golden_ratiox2, -reverse_golden_ratio, -golden_ratio}, // 2
    {-golden_ratiox2, -reverse_golden_ratio,  golden_ratio},
    {-golden_ratiox2,  reverse_golden_ratio, -golden_ratio},
    {-golden_ratiox2,  reverse_golden_ratio,  golden_ratio},
    {-golden_ratioplus1, -golden_ratio, -2.0}, // 6
    {-golden_ratioplus1, -golden_ratio,  2.0},
    {-golden_ratioplus1,  golden_ratio, -2.0},
    {-golden_ratioplus1,  golden_ratio,  2.0},
    {-2.0, -golden_ratioplus1, -golden_ratio}, // 10
    {-2.0, -golden_ratioplus1,  golden_ratio},
    {-2.0,  golden_ratioplus1, -golden_ratio},
    {-2.0,  golden_ratioplus1,  golden_ratio},
    {-golden_ratio, -golden_ratiox2, -reverse_golden_ratio}, //14
    {-golden_ratio, -golden_ratiox2,  reverse_golden_ratio},
    {-golden_ratio,  golden_ratiox2, -reverse_golden_ratio},
    {-golden_ratio,  golden_ratiox2,  reverse_golden_ratio},
    {-golden_ratio, -2.0, -golden_ratioplus1}, // 18
    {-golden_ratio, -2.0,  golden_ratioplus1},
    {-golden_ratio,  2.0, -golden_ratioplus1},
    {-golden_ratio,  2.0,  golden_ratioplus1},
    {-reverse_golden_ratio, -golden_ratioplus2, 0.0}, // 22
    {-reverse_golden_ratio, -golden_ratio, -golden_ratiox2},
    {-reverse_golden_ratio, -golden_ratio,  golden_ratiox2},
    {-reverse_golden_ratio,  golden_ratio, -golden_ratiox2},
    {-reverse_golden_ratio,  golden_ratio,  golden_ratiox2},
    {-reverse_golden_ratio,  golden_ratioplus2, 0.0},
    {0.0, -reverse_golden_ratio, -golden_ratioplus2}, // 28
    {0.0, -reverse_golden_ratio,  golden_ratioplus2}
  };
  addMirrorVertices();
  // Triangles
  appendFace(Face({0, 2, 4}, 0));
  appendFace(Face({1, 3, 5}, 0));
  appendFace(Face({6, 10, 18}, 0));
  appendFace(Face({7, 11, 19}, 0));
  appendFace(Face({8, 12, 20}, 0));
  appendFace(Face({9, 13, 21}, 0));
  appendFace(Face({14, 15, 22}, 0));
  appendFace(Face({16, 17, 27}, 0));
  appendFace(Face({23, 28, 33}, 0));
  appendFace(Face({24, 29, 34}, 0));
  // Decagons
  appendFace(Face({0, 1, 3, 7, 11, 15, 14, 10, 6, 2}, 1));
  appendFace(Face({0, 1, 5, 9, 13, 17, 16, 12, 8, 4}, 1));
  appendFace(Face({22, 14, 10, 18, 23, 33, 38, 46, 42, 32}, 1));
  appendFace(Face({27, 16, 12, 20, 25, 35, 40, 48, 44, 37}, 1));
  appendFace(Face({28, 23, 18, 6, 2, 4, 8, 20, 25, 30}, 1));
  appendFace(Face({29, 24, 19, 7, 3, 5, 9, 21, 26, 31}, 1));

  addMirrorFaces();
}

void Polyhedron::createTruncatedOctahedron() {
  m_type = ObjectType::TRUNCATED_OCTAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-2.0, -1.0,  0.0},
    {-2.0,  0.0, -1.0},
    {-2.0,  0.0,  1.0},
    {-2.0,  1.0,  0.0},
    {-1.0, -2.0,  0.0},
    {-1.0,  0.0, -2.0},
    {-1.0,  0.0,  2.0},
    {-1.0,  2.0,  0.0},
    { 0.0, -2.0, -1.0},
    { 0.0, -2.0,  1.0},
    { 0.0, -1.0, -2.0},
    { 0.0, -1.0,  2.0}
  };
  addMirrorVertices();
  appendFace(Face({0, 1, 3, 2}, 0));
  appendFace(Face({4, 8, 16, 9}, 0));
  appendFace(Face({5, 10, 17, 12}, 0));

  appendFace(Face({0, 1, 5, 10, 8, 4}, 1));
  appendFace(Face({0, 2, 6, 11, 9, 4}, 1));
  appendFace(Face({1, 3, 7, 14, 12, 5}, 1));
  appendFace(Face({2, 3, 7, 15, 13, 6}, 1));

  addMirrorFaces();
}

void Polyhedron::createTruncatedTetrahedron() {
  m_type = ObjectType::TRUNCATED_TETRAHEDRON;
  m_isConvex = true;
  m_vertices = {
    {-3.0, -1.0,  1.0},
    {-3.0,  1.0, -1.0},
    {-1.0, -3.0,  1.0},
    {-1.0, -1.0,  3.0},
    {-1.0,  1.0, -3.0},
    {-1.0,  3.0, -1.0},
    { 1.0, -3.0, -1.0},
    { 1.0, -1.0, -3.0},
    { 1.0,  1.0,  3.0},
    { 1.0,  3.0,  1.0},
    { 3.0, -1.0, -1.0},
    { 3.0,  1.0,  1.0}
  };
  appendFace(Face({0, 2, 3}, 0));
  appendFace(Face({1, 5, 4}, 0));

  appendFace(Face({0, 1, 4, 7, 6, 2}, 1));
  appendFace(Face({0, 1, 5, 9, 8, 3}, 1));

  addMirrorFaces();
}

void Polyhedron::addMirrorFaces() {
  const auto pivotElement = static_cast<uint16_t>(m_vertices.size() - 1);
  const auto originalFaces = m_faces;
  for (const Face &face1 : originalFaces) {
    Face face2 = face1;
    for (size_t i = 0; i < face1.getCorners(); i++) {
      face2.at(i) = pivotElement - face1.at(i);
    }
    appendFace(face2);
  }
}

// Mirror the supplied points and add them to the vector
// Assume center is (0,0,0)
void Polyhedron::addMirrorVertices() {
  std::vector<glm::dvec3> mirror = {};
  // Process the vertices reversed
  for (auto it = m_vertices.rbegin(); it != m_vertices.rend(); ++it) {
     mirror.push_back(*it * -1.0);
  }
  assert(mirror.size() == m_vertices.size());
  m_vertices.insert(m_vertices.end(), mirror.begin(), mirror.end());
}

void Polyhedron::appendCorrectedFace(const Face &face) {
  const auto nCorners = face.getCorners();
  assert(nCorners >= 3);
  if (nCorners > 3)
    // Align vertices as closest neighbors
    appendFace(makeClosestNeighbors(face));
  else
    appendFace(face);
}

uint32_t Polyhedron::appendUnique(const glm::dvec3 &vertex) {
  auto iVertex = indexOfVector(vertex);
  if (iVertex >= 0)
    return static_cast<uint32_t>(iVertex);
  iVertex = static_cast<int32_t>(m_vertices.size());
  push_back(vertex);
  return static_cast<uint32_t>(iVertex);
}

uint16_t Polyhedron::appendUnique(const glm::dvec3 &vertex, std::vector<glm::dvec3> &someVertices) {
  auto iVertex = indexOfVector(someVertices, vertex);
  if (iVertex >= 0)
    return static_cast<uint16_t>(iVertex);
  iVertex = static_cast<int32_t>(someVertices.size());
  someVertices.push_back(vertex);
  return static_cast<uint16_t>(iVertex);
}

uint16_t Polyhedron::appendFaceWithUniqueVertex(Face &face, const glm::dvec3 &vertex) {
  auto iVertex = indexOfVector(vertex);
  if (iVertex < 0) {
    // Add new vertex
    iVertex = static_cast<uint16_t>(m_vertices.size());
    m_vertices.push_back(vertex);
  } else {
    assert(!face.contains(static_cast<uint16_t>(iVertex)));
  }
  face.push_back_unique(static_cast<uint16_t>(iVertex));
  return static_cast<uint16_t>(iVertex);
}

uint16_t Polyhedron::appendFaceWithVertex0(Face &face, const glm::dvec3 &vertex) {
  auto iVertex = indexOfVector(vertex);
  if (iVertex < 0) {
    // Add new vertex
    iVertex = static_cast<uint16_t>(m_vertices.size());
    m_vertices.push_back(vertex);
    face.push_back_unique(static_cast<uint16_t>(iVertex));
  } else if (!face.contains(static_cast<uint16_t>(iVertex)))
    face.push_back_unique(static_cast<uint16_t>(iVertex));
  return static_cast<uint16_t>(iVertex);
}

bool Polyhedron::canMorph(const ObjectType anObjectType) {
  const auto it = polyhedronTypes.find(anObjectType);
  if (it != polyhedronTypes.end())
    return it->second.canMorph;
  else
    return false;
}

// Remove the vertices not used in a face
void Polyhedron::compress() {
  std::vector<size_t> indices;
  for (const auto &face : m_faces) {
    for (const auto iVertex : face) {
      if (!contains(indices, iVertex))
        indices.push_back(iVertex);
    }
  }
  const auto nVertices =  m_vertices.size();
  if (indices.size() == nVertices)
    return;
  // Remove all vertices not in use by a face
  std::vector<size_t> decrList;
  size_t iMinus = 0;
  for (size_t i = 0; i < nVertices; i++) {
    if (!contains(indices, i)) {
      m_vertices.erase(m_vertices.begin() + i - iMinus);
      iMinus++;
    }
    decrList.push_back(iMinus);
  }
  for (auto &face : m_faces) {
    for (size_t i = 0; i < face.getCorners(); i++) {
      face.at(i) = face[i] - decrList.at(face[i]);
    }
  }
}

void Polyhedron::createFace(const uint16_t aColorIndex, const double angle, const glm::dvec3 &axis, const glm::dvec3 &vertex) {
  glm::dvec4 v1(vertex, 0.0);
  glm::dmat4 unit_m4(1.0); // Create an identity matrix
  const auto rotation = glm::rotate(unit_m4, angle, axis);
  Face face(aColorIndex);
  appendFaceWithUniqueVertex(face, vertex);
  const auto length = glm::length(vertex);
  auto v2 = rotation * v1;
  while (glm::distance(glm::dvec3(v2), vertex) > 0.001 * length) {
    const auto iVertex = appendUnique(glm::dvec3(v2));
    face.push_back_unique(static_cast<uint16_t>(iVertex));
    v2 = rotation * v2;
  }
  face.setNormal(axis);
  appendFace(face);
}

Ring2D Polyhedron::cleanRing2D(const Ring2D &ring) {
  assert(bg::is_valid(ring));
  Ring2D result = {};
  auto it = ring.begin();
  auto v0 = *it;
  it++;
  do {
    const auto v1 = *it;
    // Two vertices v0 and v1
    // Put v0 in <result> when it differs from v1
    const auto d = glm::distance2(v1, v0);
    const auto l = glm::length2(v0 + v1);
    if (d > 0.0001 * l) {
      if (!result.empty()) {
        const auto dir0 = glm::normalize(v0 - result.back());
        const auto dir1 = glm::normalize(v1 - v0);
        if (glm::distance2(dir1, dir0) > 0.0001)
          result.push_back(v0);
      } else
        result.push_back(v0);
      v0 = v1;
    }
    it++;
  } while (it != ring.end());
  const auto ringSize = result.size();
  if (ringSize > 3) {
    // Test first and last
    auto vFirst = result.at(1) - result.at(0);
    const auto dirFirst = glm::normalize(vFirst);
    auto vLast = result[0] - result[ringSize - 1];
    const auto dirLast = glm::normalize(vLast);
    if (glm::distance2(dirFirst, dirLast) < 0.0001)
      result.erase(result.begin());
  }
  // Close the ring
  bg::correct(result);
  assert(bg::is_valid(result));
  return result;
}

bool Polyhedron::createRing2D(const std::vector<glm::dvec3> &face, const glm::dmat4 &xyMat, AxisType axis2D, Ring2D &ring) {
  assert(ring.empty());
  for (const auto &vertex : face) {
    const auto v1 = xyMat * glm::dvec4(vertex, 1.0);
    Point2D point(0,0);
    switch (axis2D) {
    case AxisType::X:
      // Y -> Z -> X
      assert(abs(v1.x) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.y);
      bg::set<1>(point, v1.z);
      break;
    case AxisType::Y:
      // Z -> X -> Y
      assert(abs(v1.y) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.z);
      bg::set<1>(point, v1.x);
      break;
    case AxisType::Z:
      // X -> Y -> Z
      assert(abs(v1.z) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.x);
      bg::set<1>(point, v1.y);
      break;
    }
    bg::append(ring, point);
  }
  // Close it
  bg::correct(ring);
  return bg::is_valid(ring);
}

Ring2D Polyhedron::createRing2D(const std::vector<glm::dvec3> &face, const glm::dmat4 &xyMat, AxisType axis2D) {
  Ring2D result{};

  for (const auto &vertex : face) {
    const auto v1 = xyMat * glm::dvec4(vertex, 1.0);
    Point2D point(0,0);
    switch (axis2D) {
    case AxisType::X:
      // Y -> Z -> X
      assert(abs(v1.x) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.y);
      bg::set<1>(point, v1.z);
      break;
    case AxisType::Y:
      // Z -> X -> Y
      assert(abs(v1.y) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.z);
      bg::set<1>(point, v1.x);
      break;
    case AxisType::Z:
      // X -> Y -> Z
      assert(abs(v1.z) < 0.0001 * glm::length(v1));
      bg::set<0>(point, v1.x);
      bg::set<1>(point, v1.y);
      break;
    }
    bg::append(result, point);
  }
  // Close it
  bg::correct(result);
  // Close it; Make it clockwise
  if (bg::intersects(result)) {
    const auto nCorners = result.size() - 1;
    const auto resultCopy = result;
    bg::clear(result);
    for (size_t i = 0; i < nCorners; i++) {
      // Skip the points which have a 180 degree angle
      const auto dir1 = glm::normalize(resultCopy[i] - resultCopy[(i + nCorners - 1) % nCorners]);
      const auto dir2 = glm::normalize(resultCopy[(i + 1) % nCorners] - resultCopy[i]);
      const auto angle = acos(glm::dot(dir1, dir2));
      if (angle < 0.999 * M_PIf64)
        bg::append(result, result[i]);
    }
    // Close it
    bg::correct(result);
  }
  assert(!bg::intersects(result));
  return result;
}

// Create Faces for expand or regular target. Related to original vertex
void Polyhedron::createCornerfaces(const std::unique_ptr<Polyhedron>& source, std::unique_ptr<Polyhedron>& target, const std::vector<std::vector<uint16_t>>& indexList, ushort iColor, std::map<size_t,size_t>& pairList) {
  const auto& originalVertices = source->getVertices();
  // Create faces at the position of the original vertices using <indexList>
  for (uint16_t iVertex = 0; iVertex < originalVertices.size(); iVertex++) {
    auto vertexIndices = indexList[iVertex];
    assert(vertexIndices.size() >= 3);
    if (vertexIndices.size() == 3) {
      // Add a simple triangle
      Face newFace(indexList[iVertex], iColor);
      pairList.insert(std::make_pair(iVertex, target->getNumberOfFaces()));
      target->appendFace(newFace);
    } else {
      Face newFace(iColor);
      // Start with the last index
      auto iVertex1 = vertexIndices.back();
      vertexIndices.pop_back();
      newFace.push_back_unique(iVertex1);
      while (!vertexIndices.empty()) {
        // Scan the remaining indices for an index which forms an edge
        bool found = false;
        for (auto it = vertexIndices.begin(); it != vertexIndices.end(); ++it) {
          const auto iVertex2 = *it;
          // Find an edge assiociated with <iVertex1> and <iVertex2>
          for (const auto& edge : target->getEdges()) {
            if ((edge.iVertexBegin == iVertex1 && edge.iVertexEnd == iVertex2) ||
                (edge.iVertexBegin == iVertex2 && edge.iVertexEnd == iVertex1)) {
              found = true;
              iVertex1 = iVertex2;
              newFace.push_back_unique(iVertex1);
              break;
            }
          }
          if (found) {
            vertexIndices.erase(it);
            break;
          }
        }
        assert(found);
      }
      pairList.insert(std::make_pair(iVertex, target->getNumberOfFaces()));
      target->appendFace(newFace);
    }
  }
}

void Polyhedron::createRhombicFace(const glm::dvec3& axis, const glm::dvec3& vertex1, const glm::dvec3& vertex2) {
  Face face(0);
  appendFaceWithUniqueVertex(face, vertex1);
  appendFaceWithUniqueVertex(face, vertex2);
  appendFaceWithUniqueVertex(face, 2.0 * axis - vertex1);
  appendFaceWithUniqueVertex(face, 2.0 * axis - vertex2);
  face.setNormal(m_vertices);
  appendFace(face);
}

// Simple ear-clipping
// Add a triangle to <triangles>
// Remove an index from <indices>
void Polyhedron::createTriangle(std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices) {
  std::vector<uint16_t> triangle{};
  triangle.push_back(indices.at(0));
  triangle.push_back(indices.at(1));
  triangle.push_back(indices.at(2));
  triangles.push_back(triangle);
  // Erase the second index
  indices.erase(indices.begin() + 1);
}

size_t Polyhedron::createTriangle2(const glm::dmat4x4 xyMat, AxisType axis2D, std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices) {
  const auto nCorners = indices.size();
  assert(nCorners > 3);
  std::vector<glm::dvec3> faceVertices;
  for (size_t i = 0; i < nCorners; i++) {
    faceVertices.push_back(m_vertices.at(indices.at(i)));
  }
  auto ring1 = createRing2D(faceVertices, xyMat, axis2D);
  std::multimap<double, ClipEar, std::greater<double>> ears = {};
  for (ushort iStart = 0; iStart < nCorners - 1; iStart++) {
    Ring2D ring2{};
    ring2.push_back(ring1.at(iStart));
    ring2.push_back(ring1.at((iStart + 1) % nCorners));
    ring2.push_back(ring1.at((iStart + 2) % nCorners));

    bg::correct(ring2);
    if (bg::within(ring2, ring1)) {
      const auto dir1 = glm::normalize(ring2.at(0) - ring2.at(1));
      const auto dir2 = glm::normalize(ring2.at(2) - ring2.at(1));
      const auto dotp = glm::dot(dir1, dir2);
      ClipEar ear;
      //ear.triangle = ring2;
      ear.start = iStart;
      ears.insert(std::make_pair(acos(dotp), ear));
    }
  }
  // Use the ear with the greatest angle
  assert(!ears.empty());
  const auto iStart = ears.begin()->second.start;
  std::vector<uint16_t> triangle{};
  triangle.push_back(indices.at(iStart));
  triangle.push_back(indices.at((iStart + 1) % nCorners));
  triangle.push_back(indices.at((iStart + 2) % nCorners));
  triangles.push_back(triangle);
  indices.erase(indices.begin() + static_cast<long>((iStart + 1) % nCorners));
  return 1;
}

size_t Polyhedron::createTriangle3(const glm::dmat4x4 xyMat, AxisType axis2D, std::vector<std::vector<uint16_t>>& triangles, std::vector<ushort>& indices) const {
  size_t result = 0;
  const auto nCorners = indices.size();
  assert(nCorners > 3);
  std::vector<glm::dvec3> faceVertices;
  for (size_t i = 0; i < nCorners; i++) {
    faceVertices.push_back(m_vertices.at(indices.at(i)));
  }
  auto ring1 = createRing2D(faceVertices, xyMat, axis2D);
  std::vector<long> ears;
  for (ushort iStart = 0; iStart < nCorners - 1; iStart++) {
    Ring2D ring2{};
    ring2.push_back(ring1.at(iStart));
    ring2.push_back(ring1.at((iStart + 1) % nCorners));
    ring2.push_back(ring1.at((iStart + 2) % nCorners));

    bg::correct(ring2);
    if (bg::within(ring2, ring1)) {
      std::vector<uint16_t> triangle{};
      triangle.push_back(indices.at(iStart));
      triangle.push_back(indices.at((iStart + 1) % nCorners));
      triangle.push_back(indices.at((iStart + 2) % nCorners));
      triangles.push_back(triangle);
      ears.push_back(iStart + 1);
      result++;
      if (nCorners == 4)
        break;
      iStart++;
    }
  }
  std::vector<ushort> indicesOld = indices;
  indices.clear();
  for (size_t i = 0; i < nCorners; i++) {
    if (contains(ears, i))
      continue;
    indices.push_back(indicesOld.at(i));
  }
  assert(indices.size() == nCorners - result);
  return result;
}

double Polyhedron::faceEdgesMaxMin(size_t iFace) const {
  const auto& face = m_faces[iFace];
  const auto nCorners = face.getCorners();
  auto maxLength = glm::length(m_vertices[face[1]] - m_vertices[face[0]]);
  auto minLength = maxLength;
  for (size_t iFirst = 1; iFirst < nCorners; ++iFirst) {
    const auto iSecond = (iFirst + 1) % nCorners;
    const auto length = glm::length(m_vertices[face[iSecond]] - m_vertices[face[iFirst]]);
    if (length > maxLength)
      maxLength = length;
    if (length < minLength)
      minLength = length;
  }
  return maxLength / minLength;
}

glm::dvec3 Polyhedron::getClosestPointOnLine (const glm::dvec3& point, const glm::dvec3& a, const glm::dvec3& b) {
  auto LineLength = distance(a, b);
  const auto LineDirection = (b - a) / LineLength;
  return getClosestPointOnLineDir(point, a, LineDirection);
}

glm::dvec3 Polyhedron::getClosestPointOnLineDir(const glm::dvec3& vertex, const glm::dvec3& linePoint, const glm::dvec3& lineDirection) {
  const auto vector = vertex - linePoint;
  // Project Vector to LineDirection to get the distance of point from a
  const auto distance = glm::dot(vector, lineDirection);

  return linePoint + lineDirection * distance;
}

std::vector<size_t> Polyhedron::getCornerList() const {
  std::vector<size_t> result;
  for (const auto& face : m_faces) {
    const auto nCorners = face.getCorners();
    if (!contains(result, nCorners))
      result.push_back(nCorners);
  }
  return result;
}

ObjectType Polyhedron::getDualType(ObjectType anObjectType) {
  const auto it = polyhedronTypes.find(anObjectType);
  if (it != polyhedronTypes.end())
    return it->second.dual;
  else
    return ObjectType::NONE;
}

ObjectType Polyhedron::getRegularType(ObjectType anObjectType) {
 switch (anObjectType) {
  case ObjectType::CUBE:
   return ObjectType::CUBOCTAHEDRON;
 case ObjectType::CUBOCTAHEDRON:
   return ObjectType::RHOMBI_CUBOCTAHEDRON;
  case ObjectType::DODECAHEDRON:
    return ObjectType::ICOSIDODECAHEDRON;
  case ObjectType::ICOSAHEDRON:
    return ObjectType::ICOSIDODECAHEDRON;
  case ObjectType::ICOSIDODECAHEDRON:
    return ObjectType::RHOMBICOSIDODECAHEDRON;
  case ObjectType::OCTAHEDRON:
    return ObjectType::CUBOCTAHEDRON;
  case ObjectType::RHOMBIC_DODECAHEDRON:
    return ObjectType::RHOMBI_CUBOCTAHEDRON;
  case ObjectType::TETRAHEDRON:
    return ObjectType::OCTAHEDRON;
  default:
    return ObjectType::NONE;
  }
}

glm::dvec3 Polyhedron::getEdgeNormal(const size_t iEdge) const {
  const auto& edge = m_edges.list[iEdge];
  m_faces[edge.iFace1].calculateNormal(m_vertices);
  return glm::normalize(m_faces[edge.iFace1].calculateNormal(m_vertices) + m_faces[edge.iFace2].calculateNormal(m_vertices));
}

ushort Polyhedron::getLastColor() const {
  ushort result = 0;
  for (const auto& face : m_faces) {
    const auto iColor = face.getColorIndex();
    if (iColor > result)
      result = iColor;
  }
  return  result;
}
// Create a map with the number of faces with <x> corners
std::map<size_t, size_t> Polyhedron::getNumberOfFacesByCorners() const {
  std::map<size_t, size_t> result{};
  for (const auto& face : m_faces) {
    // Get the number of corners of each face
    const auto nCorners = face.getCorners();
    // Is there an entry with <nCorners> in <mapCorners>
    auto it = result.find(nCorners);
    if (it == result.end()) {
      // No entry found; Create a new entry
      result.insert(std::make_pair(nCorners, 0));
      it = result.find(nCorners);
    }
    it->second++;
  }
  return result;
}

double Polyhedron::getMaxRadius() const {
  auto result = 0.0;
  if (m_vertices.empty())
    return result;
  for (const auto& vertex : m_vertices) {
    result += glm::length(vertex);
  }
  return result / m_vertices.size();
}

std::string Polyhedron::getName(const ObjectType anObjectType) {
  const auto it = polyhedronTypes.find(anObjectType);
  if (it != polyhedronTypes.end())
    return it->second.title;
  else
    return "Unknown object";
}

std::string Polyhedron::getName() const {
  if (m_isDualCompound) {
    if (m_type == ObjectType::TETRAHEDRON)
      return "Stellated octahedron";
    else {
      const auto it1 = polyhedronTypes.find(m_type);
      if (it1 != polyhedronTypes.end()) {
        const auto dualName = Polyhedron::getName(it1->second.dual);
        return "Compound of " + it1->second.title + " and " + dualName;
      } else
        return "Unknown compound";
    }
  } else
    return getName(m_type);
}

std::string Polyhedron::getNameOfDual() const {
  if (m_isDualCompound)
    return "Dual unknown";
  else
    return Polyhedron::getName(Polyhedron::getDualType(m_type));
}

glm::dvec3 Polyhedron::getNormal(const Face& face, const uint16_t index) const {
  const auto nCorners = face.getCorners();
  assert(index < nCorners);
  const auto& p1 = m_vertices[face[index]];
  // Get the preceding vertex
  const auto& p2 = m_vertices[face[index > 0 ? index - 1 : static_cast<uint16_t>(nCorners - 1)]];
  // Get the next vertex
  const auto& p3 = m_vertices[face[(index + 1) % nCorners]];
  return glm::triangleNormal(p1, p2, p3);
}

glm::dvec3 Polyhedron::getLinesIntersect(const glm::dvec3& vertex1, const glm::dvec3& direction1,
                                         const glm::dvec3& vertex2, const glm::dvec3& direction2) {
  glm::vec3 result{};
  // Calculate the intersection of two lines:
  // p = vertex1 + t1 * direction1
  // p = vertex2 + t2 * direction2
  const auto cross = glm::cross(direction1, direction2);
  // Check for parallel lines
  if (glm::length(cross) < 0.001)
    return result;
  glm::dmat4x4 matrix4(1.0); // Create an identity matrix
  matrix4[0] = glm::dvec4(vertex2 - vertex1, 0.0);
  matrix4[1] = glm::dvec4(direction2, 0.0);
  matrix4[2] = glm::dvec4(cross, 0.0);
  const auto t1 = glm::determinant(matrix4) / glm::length2(cross);
  return vertex1 + t1 * direction1;
}

int32_t Polyhedron::indexOfVector(const glm::dvec3& vertex) const {
  auto result = -1;
  const auto nVertices = m_vertices.size();
  const auto length1 = glm::length(vertex);
  for (size_t iVertex = 0; iVertex < nVertices; ++iVertex) {
    const auto vertex2 = m_vertices[iVertex];
    const auto length = glm::length(vertex2) + length1;
    if (glm::distance(vertex2, vertex) < 0.001 * length) {
      // vertex2 and vertex are close or equal
      result =  static_cast<int32_t>(iVertex);
      break;
    }
  }
  return result;
}

int32_t Polyhedron::indexOfVector(const std::vector<glm::dvec3>& someVertices, const glm::dvec3& vertex) {
  auto result = -1;
  const auto nVertices = someVertices.size();
  const auto length1 = glm::length(vertex);
  for (size_t iVertex = 0; iVertex < nVertices; ++iVertex) {
    const auto vertex2 = someVertices.at(iVertex);
    const auto length = glm::length(vertex2) + length1;
    if (glm::distance(vertex2, vertex) < 0.001 * length) {
      // vertex2 and vertex are close or equal
      result = static_cast<int32_t>(iVertex);
      break;
    }
  }
  return result;
}

// Find neighbors using the cross products
Face Polyhedron::makeClosestNeighbors(const Face& face) {
  Face result = Face(face.getColorIndex());
  size_t iVertex2;

  // Determine the center of this face
  const auto center = face.getCenter(m_vertices);
  auto faceElements0 = face.copyIndices();
  std::deque<uint16_t> faceElements = {};
  std::copy(faceElements0.begin(), faceElements0.begin() + long(faceElements0.size()), std::inserter(faceElements, faceElements.begin()));
  auto iVertex1 = faceElements.at(0);
  faceElements.pop_front();
  result.push_back_unique(iVertex1);
  // Use cross products to make sure vertices are (anti) clockwise
  glm::dvec3 cp0(0.0);
  while (!faceElements.empty()) {
    const auto dir0 = glm::normalize(m_vertices[iVertex1] - center);
    auto dMax = SNAN;
    size_t jMax = 0;
    for (iVertex2 = 0; iVertex2 < faceElements.size(); iVertex2++) {
      const auto dir1 = glm::normalize(m_vertices[faceElements[iVertex2]] - center);
      if (glm::length(cp0) > 0.0001) {
        auto cp1 = glm::cross(dir1, dir0);
        if (glm::dot(cp1, cp0) < 0)
          continue;
      }
      const auto d = glm::dot(dir0, dir1);
      if (isnan(dMax) || d > dMax) {
        dMax = d * 1.001;
        jMax = iVertex2;
      }
    }
    iVertex1 = faceElements.at(jMax);
    faceElements.erase(faceElements.begin() + long(jMax));
    result.push_back_unique(iVertex1);
    if (glm::length(cp0) == 0.0)
      cp0 = glm::cross(glm::normalize(m_vertices[iVertex1] - center), dir0);
  }
  return result;
}

void Polyhedron::setColorFaces() {
  struct CornerFaces {
    size_t nCorners;
    std::vector<size_t> iFaces;
  };
  std::multimap<size_t, size_t> list = {};
  for (size_t iFace = 0; iFace < m_faces.size(); iFace++) {
    const auto nCorners = m_faces.at(iFace).getCorners();
    list.insert(std::pair<size_t, size_t>(nCorners, iFace));
  }
  short iColor = -1;
  size_t lastCorner = 0;
  for (auto item : list) {
    if (item.first != lastCorner)
      iColor++;
    m_faces.at(item.second).setColorIndex(static_cast<uint16_t>(iColor));
  }
}

void Polyhedron::setColorFaces2() {
  auto cornerList = getCornerList();
  std::sort(cornerList.begin(), cornerList.end(), std::greater<size_t>());
  for (auto& face : m_faces) {
    const auto it = std::find(cornerList.begin(), cornerList.end(), face.getCorners());
    face.setColorIndex(std::distance(cornerList.begin(), it));
  }
}

void Polyhedron::rotateFace(const Face& face1, const glm::dmat4x4& rotation1) {
  assert(face1.getCorners() >= 3);
  auto rotation2 = rotation1;
  const auto normal1 = face1.normal;
  auto normal2 = glm::dvec3(rotation2 * glm::dvec4(normal1, 1.0));
  do {
    // Avoid duplicate
    bool isDuplicate = false;
    for (const auto& face : m_faces) {
      if (glm::dot(face.normal, normal2) > 0.999) {
        isDuplicate = true;
        break;
      }
    }
    if (!isDuplicate) {
      // Create a new face
      rotateFaceOnce(face1, rotation2);
    }
    rotation2 *= rotation1;
    normal2 = rotation2 * glm::dvec4(normal1, 1.0);
  } while (glm::dot(normal2, normal1) < 0.99);
}

void Polyhedron::rotateFace(const Face& face1, const glm::dmat4x4& rotation1, size_t iFaceOffset) {
  assert(face1.getCorners() >= 3);
  auto rotation2 = rotation1;
  const auto normal1 = face1.normal;
  auto normal2 = glm::dvec3(rotation2 * glm::dvec4(normal1, 1.0));
  do {
    // Avoid duplicate
    bool isDuplicate = false;
    for (size_t iFace = iFaceOffset; iFace < m_faces.size(); iFace++) {
      const auto face = m_faces.at(iFace);
      if (glm::dot(face.normal, normal2) > 0.999) {
        isDuplicate = true;
        break;
      }
    }
    if (!isDuplicate) {
      // Create a new face
      rotateFaceOnce(face1, rotation2);
    }
    rotation2 *= rotation1;
    normal2 = rotation2 * glm::dvec4(normal1, 1.0);
  } while (glm::dot(normal2, normal1) < 0.99);
}

void Polyhedron::rotateFaceOnce(const Face& face1, const glm::dmat4x4& rotation) {
  const auto normal = glm::dvec3(rotation * glm::vec4(face1.normal, 1.0));
  // Create a new face
  Face face2(face1.getColorIndex());
  face2.setNormal(normal);
  assert(face1.getCorners() > 2);
  for (const auto iVertex1 : face1) {
    const auto vertex2 = glm::vec3(rotation * glm::vec4(m_vertices[iVertex1], 1.0));
    const auto iVertex2 = appendUnique(vertex2);
    face2.push_back_unique(static_cast<uint16_t>(iVertex2));
  }
  if (m_updateEdges)
    appendFace(face2);
  else
    m_faces.push_back(face2);
}

void Polyhedron::scale(double scale) {
  for (auto& vertex : m_vertices) {
    vertex *= scale;
  }
}

void Polyhedron::setDualCompound() {
  m_isDualCompound = true;
  m_isConvex = false;
}
ushort Polyhedron::setFaceColorIndex(std::vector<Face>& someFaces) {
  ushort facesDone = 0;
  ushort iColor = 0;
  ushort corners = 3;
  while (facesDone < someFaces.size()) {
    ushort count = 0;
    for (auto& face : someFaces) {
      if (face.getCorners() == corners) {
        face.setColorIndex(iColor);
        count++;
      }
    }
    corners++;
    if (count > 0) {
      facesDone += count;
      iColor++;
    }
  }
  return iColor;
}

void Polyhedron::setPlatonic(bool aValue) {
  m_isPlatonic = aValue;
  if (m_isPlatonic)
    m_isConvex = true;
}
void Polyhedron::setZonoColors() {
  std::vector<ushort> dList;
  ushort d;
  setColorFaces();
  for (Face& face : m_faces) {
    if (face.getCorners() != 4)
      continue;
    // Give different 4-cornered faces different colors
    // depending on the 0 - 2 verses 1 - 3 distance
    const auto l1 = glm::distance(getVertex(face[0]), getVertex(face[2]));
    const auto l2 = glm::distance(getVertex(face[1]), getVertex(face[3]));
    if (l1 > l2)
      d = ushort(round(1000 * l1 / l2));
    else
      d = ushort(round(1000 * l2 / l1));
    std::vector<ushort>::iterator it = std::find(dList.begin(), dList.end(), d);
    ushort iColor;
    if (it == dList.end()) {
      iColor = ushort(dList.size());
      dList.push_back(d);
    } else {
      iColor = ushort(std::distance(dList.begin(), it));
    }
    face.setColorIndex(iColor);
  }
}

