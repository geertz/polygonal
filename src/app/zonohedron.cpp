#include "zonohedron.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/norm.hpp>

Zonohedron::Zonohedron(const std::unique_ptr<Polyhedron>& source) {
  std::vector<ZonoPlane> planes;
  const auto originalPoints = getZonoGenerators(source);
  makePlanes(originalPoints, planes);
  ObjectType newType = ObjectType::NONE;
  switch (source->getType()) {
  case ObjectType::CUBE:
    newType = ObjectType::RHOMBIC_DODECAHEDRON;
    break;
  case ObjectType::CUBOCTAHEDRON:
    newType = ObjectType::TRUNCATED_OCTAHEDRON;
    break;
  case ObjectType::RHOMBIC_DODECAHEDRON:
    newType = ObjectType::CHAMFERED_CUBE;
    break;
  case ObjectType::OCTAHEDRON:
    newType = ObjectType::CUBE;
    break;
  case ObjectType::TETRAHEDRON:
    newType = ObjectType::RHOMBIC_DODECAHEDRON;
    break;
  case ObjectType::TRIANGULAR_BIPYRAMID:
    newType = ObjectType::HEXAGONAL_PRISM;
    break;
  default:
    newType = ObjectType::NONE;
    break;
  }
  m_target = std::make_unique<Polyhedron>(newType, true);
  m_target->setColorList(source->getColorList());
  makeFaces(originalPoints, planes);
  // Make the new object fit within the current view
  m_target->scale(1.0 / (originalPoints.size() - 2));
  m_target->setZonoColors();
}

//returns -1, 0, 1 depending on sign of val
template <typename T> double sign(T val) {
  return (T(0) < val) - (val < T(0));
}

// Get the vectors needed to create a zonohedron
std::vector<glm::dvec3> Zonohedron::getZonoGenerators(const std::unique_ptr<Polyhedron>& source) const {
  std::vector<glm::dvec3> result;
  for (const auto& vertex : source->getVertices()) {
    bool isMirror = false;
    const auto normal1 = glm::normalize(vertex);
    for (size_t i = 0; i < result.size(); i++) {
      if (glm::length2(glm::normalize(result[i]) + normal1) < 0.00001) {
        // Here vertices with a positive z-coordinate are used. Could have used x- or y-coordinate too.
        if (vertex.z > 0)
          result.at(i) = vertex;
        isMirror = true;
        break;
      }
    }
    if (isMirror)
      continue;
    result.push_back(vertex);
  }
  return result;
}

glm::dvec3 Zonohedron::findBase(const std::vector<glm::dvec3> &star, const ZonoPlane &plane) {
  glm::dvec3 result = {};
  const auto n = star.size();
  const glm::dvec3 normal = plane.offset;
  for(size_t i = 0; i < n; i++) {
    if (contains(plane.vectors, i))
      continue;
    result += star[i] * sign(glm::dot(normal, star[i]));
  }
  return result;
}

void Zonohedron::makePlanes(const std::vector<glm::dvec3> &originalPoints, std::vector<ZonoPlane> &planes) {
  ZonoPlane plane;

  const auto oSize = originalPoints.size();
  for (size_t iVertex1 = 0; iVertex1 < oSize; iVertex1++) {
    // Get the first vector
    const auto &vector1 = originalPoints[iVertex1];
    // Get the second vector
    for (auto iVertex2 = iVertex1 + 1; iVertex2 < oSize; iVertex2++) {
      const auto &vector2 = originalPoints[iVertex2];
      // Add the two vectors to an existing plane or create a new one
      // First get the normal of the two vectors
      const auto normal = glm::normalize(glm::cross(vector1, vector2));
      bool planeFound = false;
      for (ZonoPlane &plane2 : planes) {
        const auto planeNormal = glm::normalize(plane2.offset);
        if (glm::length2(planeNormal - normal) < 0.001 ||
            glm::length2(planeNormal + normal) < 0.001) {
          // Plane2 has either the same normal or the opposite one
          planeFound = true;
          if (!contains(plane2.vectors, iVertex1))
            plane2.vectors.push_back(iVertex1);
          if (!contains(plane2.vectors, iVertex2))
            plane2.vectors.push_back(iVertex2);
        }
      }
      if (!planeFound) {
        // Add new plane
        plane.vectors.clear();
        plane.vectors.push_back(iVertex1);
        plane.vectors.push_back(iVertex2);
        plane.offset = normal;
        planes.push_back(plane);
      }
    }
  }
  // Calculate the offset of the planes
  const auto pSize = planes.size();
  for (size_t iPlane = 0; iPlane < pSize; iPlane++) {
    ZonoPlane plane = planes[iPlane];
    planes[iPlane].offset = findBase(originalPoints, plane);
    // Add opposite plane with same plane vectors
    plane.offset = -1.0 * planes[iPlane].offset;
    planes.push_back(plane);
  }
}

void Zonohedron::appendZonoFace(const std::vector<glm::dvec3> &originalPoints, const ZonoPlane &plane) {
  glm::dvec3 point = {};
  double pLength2, pLengthMax = 0;
  size_t jCurrent = 0;
  std::vector<short> flip = std::vector<short>();

  Face face(0);
  const auto vSize = plane.vectors.size();
  assert(vSize >= 2);
  flip.assign(vSize, 1);
  glm::dvec3 previousVertex = {};
  std::vector<glm::dvec3> vertices = {};
  std::vector<size_t> listDone;
  for (size_t i = 0; i < vSize; i++) {
    glm::dvec3 currentVertex = {0,0,0};
    bool foundFirst = false;
    if (i > 0) {
      // Flip a sign
      for (size_t j = 0; j < vSize; j++) {
        if (contains(listDone, j))
          continue;
        point = previousVertex - 2.0 * flip[j] * originalPoints[plane.vectors[j]];
        // Use length2 instead of length for performance reasons only
        pLength2 = glm::length2(point);
        if (!foundFirst) {
          // First point
          currentVertex = point;
          pLengthMax = pLength2;
          jCurrent = j;
          foundFirst = true;
        }
        else if (pLength2 > pLengthMax) {
          currentVertex = point;
          pLengthMax = pLength2;
          jCurrent = j;
        }
      }
      // Prevent having the same <currentVertex> twice
      if (foundFirst)
        listDone.push_back(jCurrent);
    } else {
      // Find first vertex. Maximise length
      for (size_t j = 0; j < vSize; ++j) {
        const auto &point = originalPoints[plane.vectors[j]];
        if (glm::length2(currentVertex) < glm::length2(currentVertex + point)) {
          // Vector <point> is pointing in the same direction. So add it
          currentVertex += point;
          flip[j] = 1;
        } else {
          // Vector <point> is pointing in the opposite direction. So flip it around.
          currentVertex -= point;
          flip[j] = -1;
        }
      }
    }
    assert(glm::length(currentVertex) > 0.01);
    vertices.push_back(currentVertex);
    assert(glm::distance(plane.offset, currentVertex) > 0.01);
    m_target->appendFaceWithVertex0(face, plane.offset + currentVertex);
    // Keep last point
    previousVertex = currentVertex;
  }
  assert(vertices.size() >= 2);
  assert(face.getCorners() >= 2);
  // Add the opposite vertices of the face
  for (const auto &vertex : vertices) {
    m_target->appendFaceWithVertex0(face, plane.offset - vertex);
  }
  m_target->appendCorrectedFace(face);
}

void Zonohedron::makeFaces(const std::vector<glm::dvec3> &originalPoints, const std::vector<ZonoPlane> &planes) {
  std::vector<ushort> dList = std::vector<ushort>();
  for (const auto &plane : planes) {
    appendZonoFace(originalPoints, plane);
  }
}
