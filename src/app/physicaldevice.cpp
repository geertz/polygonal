#include "physicaldevice.h"
#include <set>
#include <iostream>

const std::vector<const char*> deviceExtensions = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

PhysicalDevice::PhysicalDevice(vk::UniqueInstance &instance, vk::SurfaceKHR surface, bool computeNeeded) {
  m_surface = surface;
  auto devices = instance->enumeratePhysicalDevices();
  for (const auto& device : devices) {
    if (isDeviceSuitable(device, computeNeeded)) {
      m_device = device;
      vk::PhysicalDeviceSubgroupProperties subProps;
      vk::PhysicalDeviceProperties2 props2;
      props2.pNext = &subProps;
      m_device.getProperties2(&props2);
      m_msaaSamples = getMaxUsableSampleCount();
      break;
    }
  }
  if (m_device)
    setFamilyIndices();
  else
    m_errorMessage = "Failed to find a suitable GPU!";
}

auto PhysicalDevice::checkDeviceExtensionSupport(vk::PhysicalDevice aDevice) -> bool {
  uint32_t extensionCount;
  vkEnumerateDeviceExtensionProperties(aDevice, VK_NULL_HANDLE, &extensionCount, VK_NULL_HANDLE);

  std::vector<VkExtensionProperties> availableExtensions(extensionCount);
  vkEnumerateDeviceExtensionProperties(aDevice, VK_NULL_HANDLE, &extensionCount, availableExtensions.data());

  std::set<std::string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

  for (const auto& extension : availableExtensions) {
    requiredExtensions.erase(extension.extensionName);
  }

  return requiredExtensions.empty();
}

auto PhysicalDevice::createInfos() const -> std::vector<vk::DeviceQueueCreateInfo> {
  float queuePriority = 1.0f;
  std::vector<vk::DeviceQueueCreateInfo> result;
  std::set<uint32_t> uniqueQueueFamilies = {m_graphicsFamily.value(), m_presentFamily.value()};
  if (m_computeFamily)
    uniqueQueueFamilies.insert(m_computeFamily.value());
  for (auto queueFamily : uniqueQueueFamilies) {
    vk::DeviceQueueCreateInfo queueCreateInfo{};
    queueCreateInfo.queueFamilyIndex = queueFamily;
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;
    result.push_back(queueCreateInfo);
  }
  return result;
}

auto PhysicalDevice::findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) -> uint32_t {
  const auto memProperties = m_device.getMemoryProperties();
  for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
    if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
      return i;
    }
  }
  m_errorMessage = "Failed to find suitable memory type!";
  return UINT32_MAX;
}

auto PhysicalDevice::findSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features) -> vk::Format {
  for (const auto format : candidates) {
    auto props = m_device.getFormatProperties(format);
    if (tiling == vk::ImageTiling::eLinear && (props.linearTilingFeatures & features) == features) {
      return format;
    } else if (tiling == vk::ImageTiling::eOptimal && (props.optimalTilingFeatures & features) == features) {
      return format;
    }
  }
  m_errorMessage = "failed to find supported format!";
  return vk::Format::eUndefined;
}

auto PhysicalDevice::getMaxUsableSampleCount() -> vk::SampleCountFlagBits {
  const auto physicalDeviceProperties = m_device.getProperties();

  const auto counts = physicalDeviceProperties.limits.framebufferColorSampleCounts & physicalDeviceProperties.limits.framebufferDepthSampleCounts;
  if (counts & vk::SampleCountFlagBits::e64) { return vk::SampleCountFlagBits::e64; }
  if (counts & vk::SampleCountFlagBits::e32) { return vk::SampleCountFlagBits::e32; }
  if (counts & vk::SampleCountFlagBits::e16) { return vk::SampleCountFlagBits::e16; }
  if (counts & vk::SampleCountFlagBits::e8) { return vk::SampleCountFlagBits::e8; }
  if (counts & vk::SampleCountFlagBits::e4) { return vk::SampleCountFlagBits::e4; }
  if (counts & vk::SampleCountFlagBits::e2) { return vk::SampleCountFlagBits::e2; }

  return vk::SampleCountFlagBits::e1;
}

bool PhysicalDevice::isDeviceSuitable(vk::PhysicalDevice aDevice, bool computeNeeded) {
  setFamilyIndices(aDevice);
  bool extensionsSupported = checkDeviceExtensionSupport(aDevice);
  bool swapChainAdequate = false;
  if (extensionsSupported) {
    auto swapChainSupport = querySwapChainSupport(aDevice);
#ifdef PRESENT_MODE_MAILBOX
    swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
#else
    swapChainAdequate = !swapChainSupport.formats.empty();
#endif
  }
  auto supportedFeatures = aDevice.getFeatures();
  return isComplete(computeNeeded) && extensionsSupported && swapChainAdequate && supportedFeatures.multiDrawIndirect && supportedFeatures.samplerAnisotropy;
}

auto PhysicalDevice::querySwapChainSupport(vk::PhysicalDevice aDevice) const -> SwapChainSupportDetails {
  SwapChainSupportDetails details;
  details.capabilities = aDevice.getSurfaceCapabilitiesKHR(m_surface);
  details.formats = aDevice.getSurfaceFormatsKHR(m_surface);
#ifdef PRESENT_MODE_MAILBOX
  details.presentModes = aDevice.getSurfacePresentModesKHR(m_surface);
#endif
  return details;
}

void PhysicalDevice::setFamilyIndices(vk::PhysicalDevice aDevice) {
  auto queueFamilies = aDevice.getQueueFamilyProperties();
  uint32_t i = 0;
  for (const auto &queueFamily : queueFamilies) {
    if (queueFamily.queueCount > 0 && queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
      m_graphicsFamily = i;
    }
    VkBool32 presentSupport = false;
    vkGetPhysicalDeviceSurfaceSupportKHR(aDevice, i, m_surface, &presentSupport);
    if (queueFamily.queueCount > 0 && presentSupport) {
      m_presentFamily = i;
    }
    if (queueFamily.queueCount > 0 && queueFamily.queueFlags & vk::QueueFlagBits::eCompute) {
      m_computeFamily = i;
    }
    i++;
  }
}
