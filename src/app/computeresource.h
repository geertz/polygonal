#pragma once

#include <vulkan/vulkan.hpp>
#include "logicaldevice.h"
#include "bufferresource.h"
#include "queuethread.h"
#include <memory>
#include <string>
#include <glm/glm.hpp>
#include <boost/optional.hpp>

// Compute constants
constexpr int WORKGROUP_SIZE = 32; // Workgroup size in compute shader.

enum class ComputeState {INITIAL, BUSY, READY_FOR_SUBMIT, FINISHED, ERROR};

enum class FractalType {JULIA, MANDELBROT};

struct Pixel {
  float r, g, b, a;
};

struct ComputeSetup {
  float xOffset, yOffset, zoom;
  glm::vec3 colorA, colorB;
  uint32_t fractalPower;
  bool seedOscillate = false;
  glm::vec2 juliaSeed1, juliaSeed2;
  ComputeSetup() {
    xOffset = 0.0f;
    yOffset = 0.0f;
    zoom = 2.34f;
    colorA = glm::vec3(0.0f, 0.0f, 0.0f);
    colorB = glm::vec3(0.0f, 1.0f, 0.0f);
    fractalPower = 2;
    juliaSeed1 = glm::vec2(0.0f, 0.66f);
    juliaSeed2 = glm::vec2(0.0f, 0.66f);
  }
  /*
  ComputeSetup(float _x) {
    xOffset = _x;
  }
  */
};

class ComputeResource {
public:
  std::unique_ptr<BufferResource> m_bufferResource;
  ComputeState                    m_status;

  ComputeResource(LogicalDevice *aDevice, uint32_t width, uint32_t height, FractalType type, const ComputeSetup& setup);
  ComputeResource(const ComputeResource&) = delete; // No copy needed
  auto changeSize(const std::unique_ptr<QueueThread>& queue, uint32_t width, uint32_t height, double duration) -> bool;
  inline auto getColorA() const -> glm::vec3 {
    return m_push.colorA;
  }
  inline auto getColorB() const -> glm::vec3 {
    return m_push.colorB;
  }
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  inline auto getFractalType() const -> FractalType {
    if (m_push.fractal_type == 1)
      return FractalType::JULIA;
    else
      return FractalType::MANDELBROT;
  }
  inline auto getFractalPower() const -> uint32_t {
    return m_push.juliaPower;
  }
  inline auto getHeight() const -> uint32_t {
    return m_push.height;
  }
  inline auto getJuliaSeed1() const -> glm::vec2 {
    return m_juliaSeed1;
  }
  inline auto getJuliaSeed2() const -> glm::vec2 {
    return m_juliaSeed2;
  }
  inline auto getSeedOscillation() const -> bool {
    return m_seedOscillate;
  }
  inline auto getWidth() const -> uint32_t {
    return m_push.width;
  }
  inline auto getXoffset() const -> float {
    return m_push.xOffset;
  }
  auto getXoffetResized(uint32_t width, uint32_t height) const -> float;
  inline auto getYoffset() const -> float {
    return m_push.yOffset;
  }
  inline auto getZoom() const -> float {
    return m_push.zoom;
  }
  auto init() -> bool;
  inline void reset() {
    m_status = ComputeState::INITIAL;
  }
  void saveRenderedImage(const std::string& fileName);
  inline void setColorA(const glm::vec3& aValue) {
    m_push.colorA = aValue;
  }
  inline void setColorB(const glm::vec3& aValue) {
    m_push.colorB = aValue;
  }
  inline void setJuliaPower(uint32_t aValue) {
    m_push.juliaPower = aValue;
  }
  inline void setJuliaSeed(double aValue) {
    float speed = 0.2f;
    m_push.juliaSeed = m_juliaSeed1 + speed * (m_juliaSeed2 - m_juliaSeed1) * static_cast<float>(1.0 + glm::cos(aValue)) / 2.0f;
  }
  inline void setJuliaSeed1(const glm::vec2& aValue) {
    m_juliaSeed1 = aValue;
  }
  inline void setJuliaSeed2(const glm::vec2& aValue) {
    m_juliaSeed2 = aValue;
  }
  inline void setSeedOscillation(bool aValue) {
    m_seedOscillate = aValue;
  }
  auto setXYoffsets(double deltaX, double deltaY) -> ComputeState;
  auto submit(const std::unique_ptr<QueueThread>& queue, double duration) -> bool;
  inline auto zoomIn() -> ComputeState {
    m_push.zoom *= 0.95f;
    return m_status;
  }
  inline auto zoomOut() -> ComputeState {
    m_push.zoom *= 1.05f;
    return m_status;
  }
private:
  struct PushConstants {
    uint32_t fractal_type, juliaPower, width, height;
    float xOffset, yOffset, zoom;
    glm::vec3 colorA, colorB;
    glm::vec2 juliaSeed;
  };
  LogicalDevice                  *m_device = nullptr;
  std::string                     m_errorMessage;
  vk::UniqueDescriptorSetLayout   m_descriptorSetLayout;
  vk::UniqueDescriptorPool        m_descriptorPool;
  vk::UniqueDescriptorSet         m_descriptorSet;
  vk::UniquePipeline              m_pipeline;
  vk::UniquePipelineLayout        m_pipelineLayout;
  vk::UniqueCommandPool           m_commandPool;
  vk::CommandBuffer               m_commandBuffer;
  PushConstants                   m_push;
  vk::DeviceSize                  m_pixelSize;
  bool                            m_seedOscillate = false;
  glm::vec2                       m_juliaSeed1, m_juliaSeed2;

  auto createBuffer(vk::DeviceSize bufferSize) -> bool;
  auto createCommandBuffer() -> bool;
  auto createCommandPool() -> bool;
  auto createDescriptorPool() -> bool;
  auto createDescriptorSet(vk::DeviceSize bufferSize) -> bool;
  auto createDescriptorSetLayout() -> bool;
  auto createPipeline(const std::string& aShaderFileName) -> bool;
  auto recordCommandBuffer(double duration) -> bool;
};
