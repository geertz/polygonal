#pragma once

#include "logicaldevice.h"

class ImageResource {
public:
  vk::Image           m_image;
  vk::UniqueImageView m_imageView;
  vk::Format          m_format;

  ImageResource(LogicalDevice* aDevice, vk::Format format);
  ImageResource(LogicalDevice* aDevice, vk::Format format, const vk::Image& anImage);
  ImageResource(const ImageResource&) = delete; // No copy needed
  ~ImageResource();
  auto createImage(const vk::Extent2D& extent, uint32_t mipLevels, vk::SampleCountFlagBits numSamples, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties) -> bool;
  inline auto createImage(const vk::Extent2D& extent, uint32_t mipLevels, vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties) -> bool {
    return createImage(extent, mipLevels, m_device->m_physicalDevice->m_msaaSamples, tiling, usage, properties);
  }
  inline auto createImageView(vk::ImageAspectFlags aspectFlags) -> void {
    createImageView(aspectFlags, 1);
  }
  auto destroyImageView() -> void;
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
protected:
  LogicalDevice* m_device = nullptr;
  auto createImageView(vk::ImageAspectFlags aspectFlags, uint32_t mipLevels) -> void;
private:
  std::string            m_errorMessage;
  vk::UniqueDeviceMemory m_memory;
};
