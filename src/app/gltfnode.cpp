#include "gltfnode.h"

GltfNode::GltfNode(GltfNode *pParent, uint32_t nodeIndex) {
  parent = pParent;
  matrix = glm::mat4(1.0f);
  index  = nodeIndex;
}

GltfNode *GltfNode::findNode(const std::unique_ptr<GltfNode> &parent, uint32_t index) {
  GltfNode *nodeFound = nullptr;
  if (parent->index == index)	{
    return parent.get();
  }
  for (const auto &child : parent->children) {
    nodeFound = findNode(child, index);
    if (nodeFound)
      return nodeFound;
  }
  return nullptr;
}

// POI: Traverse the node hierarchy to the top-most parent to get the local matrix of the given node
glm::mat4 GltfNode::getNodeMatrix(const GltfNode *node) {
  glm::mat4 nodeMatrix    = node->getLocalMatrix();
  GltfNode *currentParent = node->parent;
  while (currentParent) {
    nodeMatrix    = currentParent->getLocalMatrix() * nodeMatrix;
    currentParent = currentParent->parent;
  }
  return nodeMatrix;
}
