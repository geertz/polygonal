#include "morphbase.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/closest_point.hpp>

MorphBase::MorphBase()
{

}

MorphBase::~MorphBase() {
}

void MorphBase::appendFace(size_t i, const Face& face) {
  assert(i < 4);
  assert(face.getCorners() >= 3);
  m_faces[i].push_back(face);
}

void MorphBase::appendFaceList(std::vector<Face>& faces, const std::vector<glm::dvec3>& vertices, const Face& face) {
  assert(face.getCorners() >= 3);
  if (face.getCorners() > 3) {
    // Align points as closest neighbors
    Face newFace(face.getColorIndex());
    makeClosestNeighbors(newFace, vertices, face);
    faces.push_back(newFace);
  } else {
    faces.push_back(face);
  }
}

void MorphBase::makeClosestNeighbors(Face &newFace, const std::vector<glm::dvec3> &vertices, const Face &oldFace) {
  assert(oldFace.getCorners() > 3);
  // Determine the center of this face
  const auto center = oldFace.getCenter(vertices);
  std::deque<uint16_t> faceVertexIndices = {};
  const auto vec = oldFace.copyIndices();
  for (const auto iVertex : vec) {
    faceVertexIndices.push_back(iVertex);
  }
  auto iVertex = faceVertexIndices.at(0);
  faceVertexIndices.pop_front();
  newFace.push_back_unique(iVertex);
  // Use cross products to make sure vertices are (anti) clockwise
  bool hasCP = false;
  while (!faceVertexIndices.empty()) {
    const auto vertex0 = vertices[iVertex];
    double dMax = 0;
    size_t jMax = 0;
    for (size_t j = 0; j < faceVertexIndices.size(); j++) {
      const auto vertex1 = vertices[faceVertexIndices[j]];
      if (hasCP) {
        auto cp1 = glm::cross(vertex1 - center, vertex0 - center);
        if (glm::dot(cp1, crossProduct0) < 0.0)
          continue;
      }
      const auto pointOnLine = glm::closestPointOnLine(center, vertex0, vertex1);
      const auto distance = glm::distance(center, pointOnLine);
      if (distance > dMax) {
        dMax = distance * 1.001;
        jMax = j;
      }
    }
    iVertex = faceVertexIndices.at(jMax);
    faceVertexIndices.erase(faceVertexIndices.begin() + static_cast<long>(jMax));
    newFace.push_back(iVertex);
    if (!hasCP) {
      // Set crossProduct0 to make each start and end face pair
      // either clockwise or anti-clockwise
      crossProduct0 = glm::cross(vertices[iVertex] - center, vertex0 - center);
      hasCP = true;
    }
  }
}
