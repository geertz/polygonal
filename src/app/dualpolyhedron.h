#pragma once

#include "polyhedron.h"

struct DualObject {
  glm::dvec3 newVertex;
  size_t iNewVertex;
  std::vector<size_t> iOriginalVertices;
  size_t iOriginalFace;
};

class DualPolyhedron {
public:
  std::unique_ptr<Polyhedron> m_target;

  DualPolyhedron(const std::unique_ptr<Polyhedron>&);
  DualPolyhedron(const DualPolyhedron&) = delete; // No copy needed
  static std::vector<DualObject> fillDuals(const std::unique_ptr<Polyhedron>&);
  size_t getTargetFaceFromSourceVertex(size_t iVertex) const;
protected:
  std::vector<DualObject> m_duals;
private:
  // Link the original vertex with the dual face
  std::map<size_t,size_t> m_pairVertexFaceList;

  static size_t getCommonEdge(const DualObject& dual1, const DualObject& dual2, const std::vector<Edge>& edges);
  void createDualFace(uint16_t iVertex, const std::vector<Edge>& originalEdges);
};
