#include "morphpipeline.h"

MorphPipeline::MorphPipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient) : PipelineBase(pGraphics, "shaders/vert_morph.spv", "shaders/frag_object.spv", "Morph pipeline") {
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  if (!m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vertexInputInfo)) {
    setResourceError();
    return;
  }
  m_ubo.alpha = static_cast<float>(anAlpha);
  m_ubo.ambient = static_cast<float>(anAmbient);
  if (!m_pipelineResource->createUniformBuffers(sizeof(UniformBufferObject))) {
    setResourceError();
  }
}

auto MorphPipeline::appendIndex(size_t iMorph, uint16_t iStart, uint16_t iEnd, uint16_t offset) -> void {
  std::pair<uint16_t,uint16_t> key = {iStart, iEnd};
  const auto it = std::find(m_listOfIndexPairs[iMorph].begin(), m_listOfIndexPairs[iMorph].end(), key);
  const auto index = std::distance(m_listOfIndexPairs[iMorph].begin(), it);
  assert(index >=0);
  m_indices[iMorph].push_back(offset + static_cast<uint16_t>(index));
}

auto MorphPipeline::appendVertex(size_t iMorph, uint16_t iStart, uint16_t iEnd, const Vertex& row) -> void {
  m_vertices[iMorph].push_back(row);
  m_listOfIndexPairs[iMorph].push_back({iStart, iEnd});
}

auto MorphPipeline::clearAll(size_t iMorph) -> void {
  m_indices[iMorph].clear();
  m_vertices[iMorph].clear();
  m_listOfIndexPairs[iMorph].clear();
}
// Copy Uniform buffer object to image <currentImage>
auto MorphPipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

auto MorphPipeline::createBuffers() -> void {
  assert(m_pipelineResource);
  const size_t nMorphs = (m_type == MorphType::EXPAND ? 2 : 1);
  for (size_t iMorph = 0; iMorph < nMorphs; ++iMorph) {
    m_pipelineResource->updateVertexBuffer(iMorph, sizeof(m_vertices[iMorph][0]), m_vertices[iMorph].size(), reinterpret_cast<const void *>(m_vertices[iMorph].data()));
    m_pipelineResource->createIndexBuffer(iMorph, sizeof(m_indices[iMorph][0]), m_indices[iMorph].size(), reinterpret_cast<const void *>(m_indices[iMorph].data()));
  }
}
// Fill m_vertices and m_indices
auto MorphPipeline::createIndexedVertices(const MorphBase* morphObject, size_t iMorph) -> void {
  clearAll(iMorph);
  uint8_t faceCounter = 0;
  uint16_t pos = 0;
  const auto nColors = morphObject->m_colorList.size();
  const auto nFaces = morphObject->getNumberOfFaces();
  for (size_t iFace = 0; iFace < nFaces; iFace++) {
    const auto& face1 = morphObject->getFace(iMorph * 2, iFace);
    const auto& face2 = morphObject->getFace(iMorph * 2 + 1, iFace);
    auto nCorners = face1.getCorners();
    assert(face2.getCorners() == nCorners);
    auto indices1 = face1.getIndices();
    auto indices2 = face2.getIndices();
    assert(indices1.size() == indices2.size());
    const auto faceColor1 = morphObject->m_colorList[face1.getColorIndex() % nColors];
    auto offset = pos;
    // Store the {iStartVertex, iEndVertex} combo to create the vkIndices later
    m_listOfIndexPairs[iMorph].clear();
    for (size_t i = 0; i < indices1.size(); i++) {
      const auto iStartVertex = indices1.at(i);
      const auto iEndVertex = indices2.at(i);
      Vertex vi{};
      vi.pos1 = morphObject->getVertex(iMorph * 2, iStartVertex);
      if (nColors > 1)
        vi.color1 = faceColor1;
      else {
        if ((faceCounter % 2) == 0)
          vi.color1 = {1.0f, 0.0f, 0.0f};
        else
          vi.color1 = {0.0f, 0.0f, 1.0f};
      }
      vi.normal1 = face1.normal;
      vi.pos2 = morphObject->getVertex(iMorph * 2 + 1, iEndVertex);
      appendVertex(iMorph, iStartVertex, iEndVertex, vi);
      pos++;
    }
    std::vector<std::vector<uint16_t>> startTriangles{};
    std::vector<std::vector<uint16_t>> endTriangles{};
    if (nCorners == 3) {
      startTriangles.push_back(indices1);
      endTriangles.push_back(indices2);
    } else {
      // Simple ear clipping
      // Works only for convex polygons
      while (nCorners > 3) {
        Polyhedron::createTriangle(startTriangles, indices1);
        Polyhedron::createTriangle(endTriangles, indices2);
        nCorners--;
      }
      // Add the last triangle
      startTriangles.push_back(indices1);
      endTriangles.push_back(indices2);
    }
    for (size_t iTriangle = 0; iTriangle < startTriangles.size(); iTriangle++) {
      const auto& startTriangle = startTriangles.at(iTriangle);
      const auto& endTriangle = endTriangles.at(iTriangle);
      for (size_t i = 0; i < 3; i++) {
        const auto iStartVertex = startTriangle.at(i);
        const auto iEndVertex = endTriangle.at(i);
        appendIndex(iMorph, iStartVertex, iEndVertex, offset);
      }
    }
    faceCounter++;
  }
}

std::array<vk::VertexInputAttributeDescription, 4> MorphPipeline::getAttributeDescriptions() {
  std::array<vk::VertexInputAttributeDescription, 4> attributeDescriptions{};
  // inPosition1
  attributeDescriptions[0].binding = 0;
  attributeDescriptions[0].location = 0;
  attributeDescriptions[0].format = vk::Format::eR32G32B32Sfloat;
  attributeDescriptions[0].offset = offsetof(Vertex, pos1);
  // inNormal1
  attributeDescriptions[1].binding = 0;
  attributeDescriptions[1].location = 1;
  attributeDescriptions[1].format = vk::Format::eR32G32B32Sfloat;
  attributeDescriptions[1].offset = offsetof(Vertex, normal1);
  // inColor1
  attributeDescriptions[2].binding = 0;
  attributeDescriptions[2].location = 2;
  attributeDescriptions[2].format = vk::Format::eR32G32B32Sfloat;
  attributeDescriptions[2].offset = offsetof(Vertex, color1);
  // inPosition2
  attributeDescriptions[3].binding = 0;
  attributeDescriptions[3].location = 3;
  attributeDescriptions[3].format = vk::Format::eR32G32B32Sfloat;
  attributeDescriptions[3].offset = offsetof(Vertex, pos2);

  return attributeDescriptions;
}

bool MorphPipeline::setFraction(double aValue) {
  bool result = false;
  if (m_type != MorphType::EXPAND) {
    m_ubo.fraction = static_cast<float>(aValue);
  } else {
    const auto currentBuffer = m_pipelineResource->getCurrentBuffer();
    if (aValue > 1.0) {
      m_ubo.fraction = static_cast<float>(aValue - 1.0);
      result = (currentBuffer == 0);
      if (result)
        m_pipelineResource->setCurrentBuffer(1);
    } else {
      m_ubo.fraction = static_cast<float>(aValue);
      result = (currentBuffer == 1);
      if (result)
        m_pipelineResource->setCurrentBuffer(0);
    }
  }
  m_fraction = aValue;
  return result;
}

void MorphPipeline::updateColor(const std::vector<glm::vec3> &oldColorList, const std::vector<glm::vec3> &newColorList) {
  for (auto &vertices : m_vertices) {
    for (auto &vertex : vertices) {
      for (size_t i = 0; i < oldColorList.size(); i++) {
        if (vertex.color1 == oldColorList.at(i))
          vertex.color1 = newColorList.at(i);
      }
    }
  }
}
