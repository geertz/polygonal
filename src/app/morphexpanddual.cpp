#include "morphexpanddual.h"
#include "dualpolyhedron.h"
#include "transformsolid.h"

void MorphExpandDual::expandFaces(size_t iRow, Face& face, const ExpandedPolyhedron& expand, size_t iSourceFace, const std::vector<uint16_t> &someVertices) {
  const auto iTargetFace = expand.getTargetFaceFromSourceFace(iSourceFace);
  const auto expandFace = expand.m_target->getFace(iTargetFace);
  for (const auto iVertex : expandFace) {
    if (contains(someVertices, iVertex)) {
      const auto vertex = expand.m_target->getVertex(iVertex);
      appendFaceWithUniqueVertex(iRow, face, vertex);
      break;
    }
  }
}

MorphExpandDual::MorphExpandDual(const std::unique_ptr<Polyhedron>& origin) {
  m_colorList = origin->getColorList();
  // origin --> expand --> dual
  DualPolyhedron dual(origin);
  ExpandedPolyhedron expandOrigin(origin);
  ExpandedPolyhedron expandDual(dual.m_target);
  // Start with the original solid
  m_vertices[0] = origin->getVertices();
  m_vertices[3] = dual.m_target->getVertices();
  // Part 1: Origin face -> expand face -> dual vertex
  for (size_t iFace = 0; iFace < origin->getNumberOfFaces(); iFace++) {
    const auto& sourceFace = origin->getFace(iFace);
    const auto nCorners = sourceFace.getCorners();
    // Create four new faces. All with the same number of indices (corners)
    Face face1(sourceFace.getIndices());
    const auto &expandFace = expandOrigin.m_target->getFace(expandOrigin.getTargetFaceFromSourceFace(iFace));
    assert(expandFace.getCorners() == nCorners);
    Face face2(0);
    Face face3(0);
    for (const auto iVertex : expandFace) {
      const auto vertex = expandOrigin.m_target->getVertex(iVertex);
      appendFaceWithUniqueVertex(1, face2, vertex);
      appendFaceWithUniqueVertex(2, face3, vertex);
    }
    appendFace(0, face1);
    appendFace(1, face2);
    appendFace(2, face3);
    // Create the fourth face from the dual of the origin
    const auto faceCenterNormal = glm::normalize(expandFace.getCenter(expandOrigin.m_target->getVertices()));
    size_t iDualVertex = SIZE_MAX;
    double maxDotP = -2;
    for (size_t iVertex = 0; iVertex < m_vertices[3].size(); iVertex++) {
      const auto vertex = m_vertices[3].at(iVertex);
      const auto dotp = glm::dot(glm::normalize(vertex), faceCenterNormal);
      if (dotp >= maxDotP) {
        //dualVertex = vertex;
        iDualVertex = iVertex;
        maxDotP = dotp;
      }
    }
    Face face4(0);
    face4.assign(nCorners, iDualVertex);
    appendFace(3, face4);
  }
  // Part 2: Original vertex -> expand face -> dual face
  for (size_t iOriginalVertex = 0; iOriginalVertex < origin->getNumberOfVertices(); iOriginalVertex++) {
    const auto iExpandFace = expandOrigin.getTargetFaceFromSourceVertex(iOriginalVertex);
    assert(iExpandFace < expandOrigin.m_target->getNumberOfFaces());
    const auto& expandFace = expandOrigin.m_target->getFace(iExpandFace);
    Face face1(0);
    face1.assign(expandFace.getCorners(), iOriginalVertex);
    appendFace(0, face1);
    Face face2(0);
    Face face3(0);
    for (const auto iExpandVertex : expandFace) {
      const auto vertex = expandOrigin.m_target->getVertex(iExpandVertex);
      appendFaceWithUniqueVertex(1, face2, vertex);
      appendFaceWithUniqueVertex(2, face3, vertex);
    }
    appendFace(1, face2);
    appendFace(2, face3);
    // Get the dual face
    Face face4(0);
    const auto iDualFace = dual.getTargetFaceFromSourceVertex(iOriginalVertex);
    assert(iDualFace < dual.m_target->getNumberOfFaces());
    const auto& dualFace = dual.m_target->getFace(iDualFace);
    for (const auto iDualVertex : dualFace) {
      face4.push_back(iDualVertex);
    }
    appendFace(3, face4);
  }
  // Part 3: Original edge -> expand face -> dual edge
  for (size_t iOriginalEdge = 0; iOriginalEdge < origin->getNumberOfEdges(); iOriginalEdge++) {
    const auto& originEdge = origin->getEdge(iOriginalEdge);
    Face face1(1);
    face1.push_back(originEdge.iVertexBegin);
    face1.push_back(originEdge.iVertexBegin);
    face1.push_back(originEdge.iVertexEnd);
    face1.push_back(originEdge.iVertexEnd);
    appendFace(0, face1);
    const auto& beginVertices = expandOrigin.getTargetVertices(originEdge.iVertexBegin);
    const auto& endVertices = expandOrigin.getTargetVertices(originEdge.iVertexEnd);
    Face face2(1);
    expandFaces(1, face2, expandOrigin, originEdge.iFace1, beginVertices);
    expandFaces(1, face2, expandOrigin, originEdge.iFace2, beginVertices);
    expandFaces(1, face2, expandOrigin, originEdge.iFace2, endVertices);
    expandFaces(1, face2, expandOrigin, originEdge.iFace1, endVertices);
    appendFace(1, face2);
  }
  for (size_t iDualEdge = 0; iDualEdge < dual.m_target->getNumberOfEdges(); iDualEdge++) {
    const auto& dualEdge = dual.m_target->getEdge(iDualEdge);
    Face face4(1);
    face4.push_back(dualEdge.iVertexBegin);
    face4.push_back(dualEdge.iVertexBegin);
    face4.push_back(dualEdge.iVertexEnd);
    face4.push_back(dualEdge.iVertexEnd);
    appendFace(3, face4);
    const auto& beginVertices = expandDual.getTargetVertices(dualEdge.iVertexBegin);
    const auto& endVertices = expandDual.getTargetVertices(dualEdge.iVertexEnd);
    Face face3(1);
    expandFaces(2, face3, expandDual, dualEdge.iFace1, beginVertices);
    expandFaces(2, face3, expandDual, dualEdge.iFace2, beginVertices);
    expandFaces(2, face3, expandDual, dualEdge.iFace2, endVertices);
    expandFaces(2, face3, expandDual, dualEdge.iFace1, endVertices);
    appendFace(2, face3);
  }
}
