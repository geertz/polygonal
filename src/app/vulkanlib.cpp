#include "vulkanlib.h"
#include "shadermodule.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_vulkan.h"
#include "config.h"
#include "renderpasscommands.h"
#include "singletimecommands.h"
#include <filesystem>
#include <iostream>
#include <set>
#include <fstream>
#include <cstring>
#include <utility>
#include <vulkan/vulkan.hpp>

#define VL_ARRAYSIZE(_ARR) (sizeof(_ARR)/sizeof(*_ARR))         // Size of a static C-style array. Don't use on pointers!

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger) {
  auto func = PFN_vkCreateDebugUtilsMessengerEXT(vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"));
  if (func != nullptr) {
    return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
  } else {
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
}
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator) {
  auto func = PFN_vkDestroyDebugUtilsMessengerEXT(vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT"));
  if (func != nullptr) {
    func(instance, debugMessenger, pAllocator);
  }
}

bool checkValidationLayerSupport() {
  uint32_t layerCount;
  vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

  std::vector<VkLayerProperties> availableLayers(layerCount);
  vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

  for (const char* layerName : validationLayers) {
    bool layerFound = false;

    for (const auto& layerProperties : availableLayers) {
      if (strcmp(layerName, layerProperties.layerName) == 0) {
        layerFound = true;
        break;
      }
    }

    if (!layerFound) {
      return false;
    }
  }

  return true;
}

const std::vector<const char*> deviceExtensions = {
  VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

VulkanLib::VulkanLib() {
  if (enableValidationLayers && !checkValidationLayerSupport()) {
    m_errorMessage = "Validation layers requested, but not available!";
    return;
  }
  m_graphics.reset(new GraphicsResource());
}

VulkanLib::~VulkanLib() {
  m_leftPanel.reset(nullptr);
  m_rightPanel.reset(nullptr);
  delete m_graphics.release();
  if (m_logicalDevice)
    delete m_logicalDevice.release();
  if (enableValidationLayers) {
    DestroyDebugUtilsMessengerEXT(m_instance.get(), debugMessenger, nullptr);
  }
}

auto VulkanLib::acquireNextImage(uint32_t *pImageIndex) -> vk::Result {
  const auto result = m_graphics->acquireNextImage(pImageIndex);
  if (result == vk::Result::eSuccess) {
    auto &imageResource = m_graphics->m_swapChainResource->m_swapChainImageResources.at(*pImageIndex);
    if (imageResource.m_redrawNeeded) {
      printf("Redraw needed %d\n", *pImageIndex);
      imageResource.m_redrawNeeded = false;
      if (!recordStdCommandBuffers(*pImageIndex))
        return vk::Result::eErrorUnknown;
    }
  }
  return result;
}

void VulkanLib::check_vk_result(VkResult err) {
  if (err == 0) return;
  printf("VkResult %d\n", err);
  if (err < 0)
    abort();
}

void VulkanLib::getConfig(pt::ptree& root) {
  vk::ClearColorValue aColor;
  Config::readBackgroundColor(root, aColor);
  setBackgroundColor(aColor);

  m_rightPanel->getConfig(root, ptree_rightPanel, false);
  m_leftPanel->getConfig(root, ptree_leftPanel, m_rightPanel->m_nextType == PanelType::NONE);
}

auto VulkanLib::getPanel() const -> Panel* {
  if (m_leftPanel->m_type != PanelType::NONE && m_mousePosX < m_leftPanel->m_viewport.width)
    return m_leftPanel.get();
  else
    return m_rightPanel.get();
}

auto VulkanLib::initImGui() -> bool {
  ImGui_ImplVulkan_InitInfo init_info{};
  init_info.Instance = m_instance.get();
  init_info.PhysicalDevice = m_physicalDevice->m_device;
  init_info.Device = m_logicalDevice->getDevice();
  init_info.QueueFamily = m_physicalDevice->getGraphicsFamily();
  init_info.Queue = m_graphics->m_graphicsQueue;
  //init_info.PipelineCache = VK_NULL_HANDLE;
  init_info.DescriptorPool = m_imGuiDescriptorPool;
  //init_info.Allocator = VK_NULL_HANDLE;
  init_info.MinImageCount = 2;
  init_info.ImageCount = m_graphics->m_swapChainResource->getImageCount();
  init_info.CheckVkResultFn = check_vk_result;
  ImGui_ImplVulkan_Init(&init_info, m_graphics->m_imGuiRenderPass.get());
  SingleTimeCommands command(m_logicalDevice->getDevice(), m_graphics->m_commandPool.get());
  ImGui_ImplVulkan_CreateFontsTexture(command.m_commandBuffer.get());
  if (!command.submit(m_graphics->m_graphicsQueue2)) {
    m_errorMessage = command.m_errorMessage;
    return false;
  }
  return true;
}

void VulkanLib::cleanup() {
  m_graphics->cleanupSwapChain(false, false);
  if (m_leftPanel)
    m_leftPanel->releasePipelines();
  if (m_rightPanel)
    m_rightPanel->releasePipelines();
  if (m_imGuiDescriptorPool) {
    m_logicalDevice->getDevice().destroyDescriptorPool(m_imGuiDescriptorPool);
    m_imGuiDescriptorPool = nullptr;
  }
}

void VulkanLib::logExtensions() {
  uint32_t extensionCount = 0;
  vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
  std::vector<VkExtensionProperties> extensions(extensionCount);
  vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
  std::cout << "available extensions:" << std::endl;

  for (const auto& extension : extensions) {
    std::cout << "\t" << extension.extensionName << std::endl;
  }
}

bool VulkanLib::createInstance(const std::vector<const char *>& extensions) {
  vk::ApplicationInfo appInfo("Polygonal", 1, "No Engine", 1, VK_API_VERSION_1_1);
  vk::InstanceCreateInfo info{ vk::InstanceCreateFlags(), &appInfo, 0, nullptr, static_cast<uint32_t>(extensions.size()), extensions.data() };

  vk::DebugUtilsMessengerCreateInfoEXT debugCreateInfo;
  if (enableValidationLayers) {
    info.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    info.ppEnabledLayerNames = validationLayers.data();
    populateDebugMessengerCreateInfo(debugCreateInfo);
    info.pNext = static_cast<vk::DebugUtilsMessengerCreateInfoEXT*>(&debugCreateInfo);
  } else {
    info.enabledLayerCount = 0;
    info.pNext = nullptr;
  }

  m_instance = vk::createInstanceUnique(info);
  // logExtensions();
  return setupDebugMessenger();
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData) {
  (void)pUserData;
  if (messageTypes != VkDebugUtilsMessageTypeFlagsEXT(vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral)) {
  std::cerr << vk::to_string( static_cast<vk::DebugUtilsMessageSeverityFlagBitsEXT>( messageSeverity ) ) << ": "
                  << vk::to_string( static_cast<vk::DebugUtilsMessageTypeFlagsEXT>( messageTypes ) ) << ":\n";
        std::cerr << "\t"
                  << "messageIDName   = <" << pCallbackData->pMessageIdName << ">\n";
        std::cerr << "\t"
                  << "messageIdNumber = " << pCallbackData->messageIdNumber << "\n";
        std::cerr << "\t"
                  << "message         = <" << pCallbackData->pMessage << ">\n";
  }
  return VK_FALSE;
}

void VulkanLib::populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo) {
    createInfo = {};
    createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo.pfnUserCallback = debugCallback;
}

bool VulkanLib::setupDebugMessenger() {
  if (!enableValidationLayers)
    return true;
  VkDebugUtilsMessengerCreateInfoEXT createInfo{};
  populateDebugMessengerCreateInfo(createInfo);
  if (CreateDebugUtilsMessengerEXT(m_instance.get(), &createInfo, nullptr, &debugMessenger) != VK_SUCCESS) {
    m_errorMessage = "Failed to set up debug messenger!";
    return false;
  }
  return true;
}

auto VulkanLib::createLogicalDevice() -> bool {
  vk::PhysicalDeviceFeatures deviceFeatures{};
  deviceFeatures.samplerAnisotropy = VK_TRUE;
  // Want to be able to draw the lines
  deviceFeatures.fillModeNonSolid = true;
  // Multiple draw commands
  deviceFeatures.multiDrawIndirect = true;
  vk::PhysicalDeviceBufferAddressFeaturesEXT extra{};
  extra.bufferDeviceAddress = true;
  extra.bufferDeviceAddressCaptureReplay = true;

  vk::DeviceCreateInfo createInfo{};
  createInfo.pEnabledFeatures = &deviceFeatures;
  createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
  createInfo.ppEnabledExtensionNames = deviceExtensions.data();
  createInfo.pNext = &extra;

  if (enableValidationLayers) {
    createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
    createInfo.ppEnabledLayerNames = validationLayers.data();
  } else {
    createInfo.enabledLayerCount = 0;
  }
  m_logicalDevice = std::make_unique<LogicalDevice>(m_physicalDevice.get(), createInfo);
  if (!m_logicalDevice->m_errorMessage.empty()) {
    m_errorMessage = m_logicalDevice->m_errorMessage;
    return false;
  }
  m_graphics->setDevice(m_logicalDevice.get());
  m_graphics->setQueues();
  m_leftPanel = std::make_unique<Panel>(m_graphics.get());
  m_rightPanel = std::make_unique<Panel>(m_graphics.get());
  return true;
}

#ifdef PRESENT_MODE_MAILBOX
VkPresentModeKHR VulkanLib::chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
  for (const auto& availablePresentMode : availablePresentModes) {
    if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
      return availablePresentMode;
    }
  }
  return VK_PRESENT_MODE_FIFO_KHR;
}
#endif

auto VulkanLib::buildSwapChain(bool recreate, uint32_t width, uint32_t height) -> bool {
  // Updates graphics->swapChainImageFormat
  m_graphics->createSwapChain(width, height);
  if (m_leftPanel->m_type != PanelType::NONE || m_leftPanel->m_nextType != PanelType::NONE) {
    if (m_rightPanel->m_type == PanelType::NONE && m_rightPanel->m_nextType == PanelType::NONE)
      m_leftPanel->updateViewPort (0, width, height);
    else
      m_leftPanel->updateViewPort (0, width / 2, height);
  }
  if (m_rightPanel->m_type != PanelType::NONE  || m_rightPanel->m_nextType != PanelType::NONE) {
    if (m_leftPanel->m_type == PanelType::NONE && m_leftPanel->m_nextType == PanelType::NONE)
      m_rightPanel->updateViewPort (0, width, height);
    else
      m_rightPanel->updateViewPort (width / 2, width / 2, height);
  }
  // The viewport is updated
  // Can create the texture / image
  if (!m_leftPanel->resetTextureResource()) {
    m_errorMessage = m_leftPanel->getErrorMessage();
    return false;
  }
  if (!m_rightPanel->resetTextureResource()) {
    m_errorMessage = m_rightPanel->getErrorMessage();
    return false;
  }
  if (m_graphics->m_colorImageResource)
    delete m_graphics->m_colorImageResource.release();
  if (!m_graphics->m_colorImageResource) {
    if (!m_graphics->createColorResource()) {
      m_errorMessage = m_graphics->getErrorMessage();
      return false;
    }
  }
  if (!m_graphics->m_depthImageResource && !m_graphics->createDepthResource()) {
    m_errorMessage = m_graphics->getErrorMessage();
    return false;
  }
  if (!m_graphics->m_stdRenderPass) {
    try {
      m_graphics->createStdRenderPass();
    } catch (...) {
      m_errorMessage = "Failed to create render pass!";
      return false;
    }
  }
  if (!recreate) {
    try {
      m_graphics->createImGuiRenderPass();
    } catch (...) {
      m_errorMessage = "Could not create Dear ImGui's render pass";
      return false;
    }
    if (!createImGuiDescriptorPool()) {
      return false;
    }
  }
  if (!m_leftPanel->createPipelines()) {
    m_errorMessage = m_leftPanel->getErrorMessage();
    return false;
  }
  if (!m_rightPanel->createPipelines()) {
    m_errorMessage = m_rightPanel->getErrorMessage();
    return false;
  }
  if (!m_graphics->createFramebuffers()) {
    m_errorMessage = m_graphics->getErrorMessage();
    return false;
  }
  if (!m_graphics->createCommandBuffers()) {
    m_errorMessage = m_graphics->getErrorMessage();
    return false;
  }
  return true;
}

auto VulkanLib::recreateSwapChain(uint32_t width, uint32_t height) -> bool {
  const auto size = m_graphics->getExtent2D();
  const auto sameSize = (size.width == width) && (size.height == height);
  m_logicalDevice->waitIdle();
  m_graphics->cleanupSwapChain(true, sameSize);

  assert(m_graphics->m_stdRenderPass);
  if (!buildSwapChain(true, width, height))
    return false;
  return recordStdCommandBuffers(SIZE_MAX);
}

void VulkanLib::setMousePos(double x, double y) {
  m_mousePosX = x;
  m_mousePosY = y;
}

ErrorValue<std::vector<char>> VulkanLib::readFile(const std::string& filename) {
  std::ifstream file(filename, std::ios::ate | std::ios::binary);

  if (!file.is_open()) {
    std::vector<char> buffer(0);
    return ErrorValue("Failed to open file " + filename, buffer);
  }
  const size_t fileSize = file.tellg();
  std::vector<char> buffer(fileSize);
  file.seekg(0);
  file.read(buffer.data(), static_cast<std::streamsize>(fileSize));
  file.close();

  return ErrorValue(buffer);
}

bool VulkanLib::createImGuiDescriptorPool() {
  vk::DescriptorPoolSize pool_sizes[] = {
    { vk::DescriptorType::eSampler, 1000 },
    { vk::DescriptorType::eCombinedImageSampler, 1000 },
    { vk::DescriptorType::eSampledImage, 1000 },
    { vk::DescriptorType::eStorageImage, 1000 },
    { vk::DescriptorType::eUniformTexelBuffer, 1000 },
    { vk::DescriptorType::eStorageTexelBuffer, 1000 },
    { vk::DescriptorType::eUniformBuffer, 1000 },
    { vk::DescriptorType::eStorageBuffer, 1000 },
    { vk::DescriptorType::eUniformBufferDynamic, 1000 },
    { vk::DescriptorType::eStorageBufferDynamic, 1000 },
    { vk::DescriptorType::eInputAttachment, 1000 }
  };
  vk::DescriptorPoolCreateInfo pool_info{};
  pool_info.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
  pool_info.maxSets = 1000 * IM_ARRAYSIZE(pool_sizes);
  pool_info.poolSizeCount = static_cast<uint32_t>(IM_ARRAYSIZE(pool_sizes));
  pool_info.pPoolSizes = pool_sizes;
  try {
    m_imGuiDescriptorPool = m_logicalDevice->getDevice().createDescriptorPool(pool_info);
    return true;
  } catch (const std::exception& e) {
    m_errorMessage = e.what();
    return false;
  }
}

auto VulkanLib::pickPhysicalDevice() -> bool {
  m_physicalDevice = std::make_unique<PhysicalDevice>(m_instance, m_graphics.get()->m_surface.get(), true);
  if (!m_physicalDevice->m_errorMessage.empty()) {
    m_errorMessage = std::move(m_physicalDevice->m_errorMessage);
    return false;
  } else
    return true;
}

auto VulkanLib::drawFrame (size_t iImage) -> vk::Result {
  assert(m_leftPanel->m_canDraw || m_rightPanel->m_canDraw);
  if (m_leftPanel->m_canDraw) {
    bool redraw = false;
    if (m_leftPanel->m_type == PanelType::MESH || m_leftPanel->m_type == PanelType::SOLID)
      redraw = true;
    else if (m_leftPanel->m_type == PanelType::IMAGE_COMP) {
      redraw = m_leftPanel->getSeedOscillation();
    }
    if (redraw && !m_leftPanel->copyUniformBuffer(iImage)) {
      m_errorMessage = m_leftPanel->getErrorMessage();
      return vk::Result::eErrorUnknown;
    }
  }
  if (m_rightPanel->m_canDraw && (m_rightPanel->m_type == PanelType::MESH || m_rightPanel->m_type == PanelType::SOLID) && !m_rightPanel->copyUniformBuffer(iImage)) {
    m_errorMessage = m_rightPanel->getErrorMessage();
    return vk::Result::eErrorUnknown;
  }
  if (m_graphics->m_imGuiRenderPass) {
    ImDrawData *data = ImGui::GetDrawData();
    if (data) {
      const auto& imageResource = m_graphics->m_swapChainResource->getImageResource(iImage);
      const auto extent = m_graphics->getExtent2D();
      imageResource.resetCommandPool(m_logicalDevice->getDevice());
      RenderPassCommands commands(imageResource.m_imGuiCommandBuffer, vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
      if (!commands.m_errorMessage.empty()) {
        m_errorMessage = std::move(commands.m_errorMessage);
        return vk::Result::eErrorUnknown;
      }
      vk::ClearValue imGuiClearColor = {m_graphics->getBackgroundColor()};
      vk::RenderPassBeginInfo info{};
      info.renderPass = m_graphics->m_imGuiRenderPass.get();
      info.framebuffer = imageResource.m_imGuiFrameBuffer.get();
      info.renderArea.extent = extent;
      info.clearValueCount = 1;
      info.pClearValues = &imGuiClearColor;
      commands.beginRenderPass(info);
      // Record Imgui Draw Data and draw funcs into command buffer
      ImGui_ImplVulkan_RenderDrawData(data, imageResource.m_imGuiCommandBuffer);
      // destructor of <commands> will end renderpass
    }
  }
  return m_graphics->drawFrame (iImage);
}
// Save the current setup
void VulkanLib::putConfig() {
  Config c{};
  pt::ptree root = c.readFile();
  m_leftPanel->putConfig(root, ptree_leftPanel);
  m_rightPanel->putConfig(root, ptree_rightPanel);
  c.writeVulkanLibSettings(root, m_graphics->getBackgroundColor());
  c.writeFile(root);
}

auto VulkanLib::rebuildCommandBuffers(bool doWait) -> bool {
  if (doWait)
    m_logicalDevice->waitIdle();
  // First: Free old textures
  m_leftPanel->freeTextures();
  m_rightPanel->freeTextures();
  // For a mesh-panel
  if (m_leftPanel->m_prepared) {
    if (m_leftPanel->m_type == PanelType::MESH) {
      if (m_leftPanel->m_gltfModelNext) {
        // A new glTF model is loaded.
        // Update the pipeline
        m_leftPanel->disableRotation();
        m_leftPanel->fillMeshPipelineBuffers();
      }
    } else
      m_leftPanel->m_gltfModel.reset(nullptr);
  }
  if (m_rightPanel->m_prepared) {
    if (m_rightPanel->m_type == PanelType::MESH) {
      if (m_rightPanel->m_gltfModelNext) {
        // A new glTF model is loaded.
        // Update the pipeline
        m_rightPanel->disableRotation();
        m_rightPanel->fillMeshPipelineBuffers();
      }
    } else
      m_rightPanel->m_gltfModel.reset(nullptr);
  }
  // Second: Reset command buffers
  if (!m_graphics->resetStdCommandBuffers()) {
    m_errorMessage = m_graphics->getErrorMessage();
    return false;
  }
  // Third: Update pipelines
  if (m_leftPanel->m_prepared && m_leftPanel->m_type != PanelType::NONE) {
    if (!m_leftPanel->createPipelineBuffers()) {
      m_errorMessage = m_leftPanel->getErrorMessage();
      return false;
    }
  }
  if (m_rightPanel->m_prepared && m_rightPanel->m_type != PanelType::NONE) {
    if (!m_rightPanel->createPipelineBuffers()) {
      m_errorMessage = m_rightPanel->getErrorMessage();
      return false;
    }
  }
  return recordStdCommandBuffers(SIZE_MAX);
}

auto VulkanLib::recordStdCommandBuffers(size_t currentImage) -> bool {
  // Start recording
  std::vector<vk::ClearValue> stdClearValues{};
  stdClearValues.resize(2);
  stdClearValues[0].color = m_graphics->getBackgroundColor();
  stdClearValues[1].depthStencil = vk::ClearDepthStencilValue(1.0f, 0);
  vk::RenderPassBeginInfo info{};
  info.renderPass = m_graphics->m_stdRenderPass.get();
  info.renderArea.offset = vk::Offset2D(0, 0);
  info.renderArea.extent = m_graphics->getExtent2D();
  info.clearValueCount = static_cast<uint32_t>(stdClearValues.size());
  info.pClearValues = stdClearValues.data();
  const auto nImages = m_graphics->m_swapChainResource->getImageCount();
  for (size_t iImage = 0; iImage < nImages; iImage++) {
    if (currentImage < nImages && iImage != currentImage)
      continue;
    const auto& imageResource = m_graphics->m_swapChainResource->getImageResource(iImage);
    RenderPassCommands commands(imageResource.m_stdCommandBuffer, vk::CommandBufferUsageFlags(0));
    if (!commands.m_errorMessage.empty()) {
      m_errorMessage = std::move(commands.m_errorMessage);
      return false;
    }
    info.framebuffer = imageResource.m_stdFrameBuffer.get();
    commands.beginRenderPass(info);
    if (m_leftPanel->m_prepared && m_leftPanel->m_type != PanelType::NONE)
      m_leftPanel->draw(imageResource, iImage);
    if (m_rightPanel->m_prepared && m_rightPanel->m_type != PanelType::NONE)
      m_rightPanel->draw(imageResource, iImage);
  }
  return true;
}
// Split the panel if <panel> is the only one being displayed
// m_type == PanelType::NONE means that panel is not displayed
// Hide the other panel when both are currently displayed
auto VulkanLib::splitPanel(Panel* panel) -> bool {
  if (panel->m_type != PanelType::NONE && ImGui::Button("Split")) {
    if (panel == m_leftPanel.get() && m_rightPanel->m_type == PanelType::NONE) {
      // Copy the left panel to the right panel
      if (!m_rightPanel->copyPanel(*panel, true)) {
        m_errorMessage = m_rightPanel->getErrorMessage();
        return false;
      }
      panel->m_viewport.width /= 2;
      m_rightPanel->m_prepared = true;
    } else if (panel == m_rightPanel.get() && m_leftPanel->m_type == PanelType::NONE) {
      // Copy the right panel to the left panel
      if (!m_leftPanel->copyPanel(*panel, false)) {
        m_errorMessage = m_leftPanel->getErrorMessage();
        return false;
      }
      panel->m_viewport.width /= 2;
      panel->m_viewport.x = panel->m_viewport.width;
    } else if (panel == m_leftPanel.get()) {
      // Both panels are displayed and the left panel has the mouse pointer
      // Deactivate the right panel
      m_rightPanel->clearPanel();
      panel->m_viewport.width *= 2;
    } else if (panel == m_rightPanel.get()) {
      // Both panels are displayed and the right panel has the mouse pointer
      // Deactivate the left panel
      m_leftPanel->clearPanel();
      panel->m_viewport.x = 0;
      panel->m_viewport.width *= 2;
    }
    if (m_leftPanel->m_type == PanelType::IMAGE_COMP) {
      if (!m_leftPanel->updateComputeImage()) {
        m_errorMessage = m_leftPanel->getErrorMessage();
        return false;
      }
    }
    if (m_rightPanel->m_type == PanelType::IMAGE_COMP) {
      if (!m_rightPanel->updateComputeImage()) {
        m_errorMessage = m_rightPanel->getErrorMessage();
        return false;
      }
    }
    rebuildCommandBuffers(true);
    ImGui::CloseCurrentPopup();
  }
  return true;
}

auto VulkanLib::updateTextureResource(Panel* panel) -> bool {
  m_logicalDevice->waitIdle();
  if (!panel->updateTextureResource()) {
    m_errorMessage = panel->getErrorMessage();
    return false;
  }
  panel->updateTextureUniformBuffers();
  return rebuildCommandBuffers(false);
}
