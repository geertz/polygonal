
#include "graphicspipelineresource.h"
#include "graphicsresource.h"
#include "shadermodule.h"
#include <assert.h>
#include <string.h>
#include <iostream>
#include "common.h"

GraphicsPipelineResource::GraphicsPipelineResource(GraphicsResource *pGraphics) {
  m_graphics = pGraphics;
  m_device   = m_graphics->getDevice()->getDevice();
}

std::vector<vk::DescriptorSet> GraphicsPipelineResource::allocatedDescriptorSets(uint32_t descriptorSetCount, const vk::UniqueDescriptorSetLayout& layout) {
  assert(m_descriptorPool);
  std::vector<vk::DescriptorSetLayout> layouts(descriptorSetCount, layout.get());
  vk::DescriptorSetAllocateInfo allocInfo{};
  allocInfo.descriptorPool = m_descriptorPool.get();
  allocInfo.descriptorSetCount = descriptorSetCount;
  allocInfo.pSetLayouts = layouts.data();
  return m_device.allocateDescriptorSets(allocInfo);
}

void GraphicsPipelineResource::bindCommandBuffer(const SwapchainImageResource& imageResource, size_t iImage) {
  imageResource.bindGraphicsPipeline(m_pipeline);
  if (m_imageUniformResources.size() > 0) {
    uint32_t setCount = 0;
    if (m_descriptorSetLayoutUB) {
      imageResource.m_stdCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout.get(), setCount, 1, &m_imageUniformResources.at(iImage).descriptorSetUB, 0, nullptr);
      setCount++;
    }
    if (m_descriptorSetLayoutCIS)
      imageResource.m_stdCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout.get(), setCount, 1, &m_imageUniformResources.at(iImage).descriptorSetCIS, 0, nullptr);
  }
  imageResource.m_stdCommandBuffer.bindVertexBuffers(0, *m_bufferPairs[m_currentBuffer].vertexBufferResource->m_buffer, {0});
  if (m_bufferPairs[m_currentBuffer].indexBufferResource)
    imageResource.bindIndexBuffer(m_bufferPairs[m_currentBuffer].indexBufferResource->m_buffer);
}

void GraphicsPipelineResource::bindCommandBuffer (const SwapchainImageResource& imageResource) {
  imageResource.bindGraphicsPipeline(m_pipeline);
  imageResource.m_stdCommandBuffer.bindVertexBuffers(0, *m_bufferPairs[m_currentBuffer].vertexBufferResource->m_buffer, {0});
  if (m_bufferPairs[m_currentBuffer].indexBufferResource)
    imageResource.bindIndexBuffer(m_bufferPairs[m_currentBuffer].indexBufferResource->m_buffer);
}

auto GraphicsPipelineResource::copyUBLights(uint32_t iImage, void* source, vk::DeviceSize size) -> bool {
  auto buffer = m_imageUniformResources.at(iImage).bufferLights.get();
  m_errorMessage = buffer->copyData(source, size);
  return (m_errorMessage.empty());
}

auto GraphicsPipelineResource::copyUBMatrices(uint32_t iImage, void* source, vk::DeviceSize size) -> bool {
  auto buffer = m_imageUniformResources.at(iImage).bufferMatrices.get();
  m_errorMessage = buffer->copyData(source, size);
  return (m_errorMessage.empty());
}
// Create the layout for the image sampler
auto GraphicsPipelineResource::createDescriptorSetLayoutCIS() -> void {
  vk::DescriptorSetLayoutBinding binding{};
  binding.binding = 0;
  binding.descriptorCount = 1;
  binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
  binding.pImmutableSamplers = nullptr;
  binding.stageFlags = vk::ShaderStageFlagBits::eFragment;
  vk::DescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.bindingCount = 1;
  layoutInfo.pBindings = &binding;
  m_descriptorSetLayoutCIS = m_device.createDescriptorSetLayoutUnique(layoutInfo);
}
// Create the layout for the storage buffer
auto GraphicsPipelineResource::createDescriptorSetLayoutSB() -> void {
  vk::DescriptorSetLayoutBinding binding{};
  binding.binding = 0;
  binding.descriptorCount = 1;
  binding.descriptorType = vk::DescriptorType::eStorageBuffer;
  binding.pImmutableSamplers = nullptr;
  binding.stageFlags = vk::ShaderStageFlagBits::eVertex;
  vk::DescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.bindingCount = 1;
  layoutInfo.pBindings = &binding;
  m_descriptorSetLayoutSB = m_device.createDescriptorSetLayoutUnique(layoutInfo);
}
// Create the layout for the uniform buffer
void GraphicsPipelineResource::createDescriptorSetLayoutUB(const std::vector<UniformBinding>& setup) {
  std::vector<vk::DescriptorSetLayoutBinding> bindings;
  for (const auto& e : setup) {
    vk::DescriptorSetLayoutBinding binding{};
    binding.binding = e.binding;
    binding.descriptorType = vk::DescriptorType::eUniformBuffer;
    binding.descriptorCount = 1;
    binding.stageFlags = e.stageFlags;
    binding.pImmutableSamplers = nullptr;
    bindings.push_back(binding);
  }
  vk::DescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.bindingCount = bindings.size();
  layoutInfo.pBindings = bindings.data();
  m_descriptorSetLayoutUB = m_device.createDescriptorSetLayoutUnique(layoutInfo);
}
auto GraphicsPipelineResource::createFragmentShader(const std::string& aFileName) -> bool {
  assert(!m_fragmentShaderModule);
  auto res = m_graphics->getDevice()->createShaderModule(aFileName);
  if(!res.error.empty()) {
    m_errorMessage = "Failed to create fragment shader module: " + std::move(res.error);
    return false;
  }
  m_fragmentShaderModule = std::move(res.value);
  return true;
}

bool GraphicsPipelineResource::createIndexBuffer(size_t iBuffer, vk::DeviceSize indexSize, size_t nIndices, const void *indexData) {
  assert(nIndices > 0);
  BufferResource stagingBufferResource(m_graphics->getDevice()->getDevice());
  m_graphics->createBuffer(indexSize, nIndices, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, &stagingBufferResource);

  const auto bufferSize = indexSize * nIndices;
  auto data = stagingBufferResource.mapMemory(bufferSize);
  memcpy(data, indexData, static_cast<size_t>(bufferSize));
  stagingBufferResource.unmapMemory();

  auto &bufferResource = m_bufferPairs[iBuffer].indexBufferResource;
  bufferResource.reset(new BufferResource(m_device));
  m_graphics->createBuffer(indexSize, nIndices, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, bufferResource.get());
  auto result = m_graphics->copyBuffer(stagingBufferResource.m_buffer.get(), bufferResource->m_buffer.get(), bufferSize);
  if (!result) {
    m_errorMessage = m_graphics->getErrorMessage();
  }
  return result;
}

bool GraphicsPipelineResource::createPipeline(vk::PrimitiveTopology aTopology, vk::PolygonMode aPolygonMode, const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange) {
  assert(!m_pipelineLayout);
  assert(m_descriptorSetLayoutUB || m_descriptorSetLayoutCIS);
  try {
    std::vector<vk::DescriptorSetLayout> layouts;
    if (m_descriptorSetLayoutUB)
      layouts.push_back(m_descriptorSetLayoutUB.get());
    if (m_descriptorSetLayoutSB)
      layouts.push_back(m_descriptorSetLayoutSB.get());
    if (m_descriptorSetLayoutCIS)
      layouts.push_back(m_descriptorSetLayoutCIS.get());
    m_pipelineLayout = m_graphics->createPipelineLayout(layouts, pPushConstantRange);
    if (pPushConstantRange)
      m_pushStageFlags = pPushConstantRange->stageFlags;
  } catch (...) {
    m_errorMessage = "Failed to create pipeline layout!";
    return false;
  }
  if (createPipelineBase(aTopology, aPolygonMode, &pVertexInputInfo) != vk::Result::eSuccess) {
    m_errorMessage = "Failed to create graphics pipeline!";
    return false;
  }
  return true;
}

vk::Result GraphicsPipelineResource::createPipelineBase(vk::PrimitiveTopology aTopology, vk::PolygonMode aPolygonMode, const vk::PipelineVertexInputStateCreateInfo* pVertexInputInfo) {
  assert(m_graphics->m_stdRenderPass);
  vk::PipelineShaderStageCreateInfo vertShaderStageInfo{};
  vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
  vertShaderStageInfo.module = m_vertexShaderModule.get();
  vertShaderStageInfo.pName = "main";

  vk::PipelineShaderStageCreateInfo fragShaderStageInfo{};
  fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
  fragShaderStageInfo.module = m_fragmentShaderModule.get();
  fragShaderStageInfo.pName = "main";

  m_topology = aTopology;
  vk::PipelineInputAssemblyStateCreateInfo inputAssembly{};
  inputAssembly.topology = m_topology;
  inputAssembly.primitiveRestartEnable = VK_FALSE;
  vk::PipelineViewportStateCreateInfo viewportState{};
  viewportState.viewportCount = 1;
  viewportState.scissorCount = 1;
  m_polygonMode = aPolygonMode;
  vk::PipelineRasterizationStateCreateInfo rasterizer{};
  rasterizer.depthClampEnable = VK_FALSE;
  rasterizer.rasterizerDiscardEnable = VK_FALSE;
  rasterizer.polygonMode = m_polygonMode;
  rasterizer.lineWidth = 1.0f;
  rasterizer.cullMode = vk::CullModeFlagBits::eNone;
  rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
  rasterizer.depthBiasEnable = VK_FALSE;
  rasterizer.depthBiasConstantFactor = 0.0f; // Optional
  rasterizer.depthBiasClamp = 0.0f; // Optional
  rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

  vk::PipelineMultisampleStateCreateInfo multisampling{};
  multisampling.sampleShadingEnable = VK_FALSE;
  multisampling.rasterizationSamples = m_graphics->getMsaaSamples();
  multisampling.minSampleShading = 1.0f; // Optional
  multisampling.pSampleMask = nullptr; // Optional
  multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
  multisampling.alphaToOneEnable = VK_FALSE; // Optional

  vk::PipelineColorBlendAttachmentState colorBlendAttachment{};
  colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
  colorBlendAttachment.blendEnable = VK_TRUE;
  colorBlendAttachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
  colorBlendAttachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
  colorBlendAttachment.colorBlendOp = vk::BlendOp::eAdd;
  colorBlendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
  colorBlendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
  colorBlendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

  vk::PipelineColorBlendStateCreateInfo colorBlending{};
  colorBlending.logicOpEnable = VK_FALSE;
  colorBlending.logicOp = vk::LogicOp::eCopy; // Optional
  colorBlending.attachmentCount = 1;
  colorBlending.pAttachments = &colorBlendAttachment;
  colorBlending.blendConstants[0] = 0.0f; // Optional
  colorBlending.blendConstants[1] = 0.0f; // Optional
  colorBlending.blendConstants[2] = 0.0f; // Optional
  colorBlending.blendConstants[3] = 0.0f; // Optional

  vk::PipelineShaderStageCreateInfo stages[] = {vertShaderStageInfo, fragShaderStageInfo};

  vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eScissor };
  vk::PipelineDynamicStateCreateInfo dynamicStateInfo{};
  dynamicStateInfo.dynamicStateCount = 2;
  dynamicStateInfo.pDynamicStates = dynamicStates;

  vk::GraphicsPipelineCreateInfo pipelineInfo{};
  pipelineInfo.stageCount = 2;
  pipelineInfo.pStages = stages;
  pipelineInfo.pVertexInputState = pVertexInputInfo;
  pipelineInfo.pInputAssemblyState = &inputAssembly;
  pipelineInfo.pViewportState = &viewportState;
  pipelineInfo.pRasterizationState = &rasterizer;
  pipelineInfo.pMultisampleState = &multisampling;
  pipelineInfo.pColorBlendState = &colorBlending;
  pipelineInfo.pDynamicState = &dynamicStateInfo; // Optional
  pipelineInfo.layout        = m_pipelineLayout.get();
  pipelineInfo.renderPass    = m_graphics->m_stdRenderPass.get();
  pipelineInfo.subpass = 0;
  pipelineInfo.basePipelineHandle = nullptr; // Optional

  vk::PipelineDepthStencilStateCreateInfo depthStencil{};
  depthStencil.depthTestEnable = true;
  depthStencil.depthWriteEnable = true;
  depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;
  depthStencil.depthBoundsTestEnable = false;
  depthStencil.minDepthBounds = 0.0f; // Optional
  depthStencil.maxDepthBounds = 1.0f; // Optional
  depthStencil.stencilTestEnable = VK_FALSE;
  depthStencil.back.compareOp = vk::CompareOp::eAlways;
  pipelineInfo.pDepthStencilState = &depthStencil; // Optional
  auto res = m_device.createGraphicsPipelinesUnique(nullptr, {pipelineInfo}, nullptr);
  if (res.result == vk::Result::eSuccess) {
    m_pipeline = std::move(res.value.front());
  }
  return res.result;
}

bool GraphicsPipelineResource::createVertexBuffer(vk::DeviceSize vertexSize, size_t nVertices, const void *vertexData) {
  if (m_bufferPairs[m_currentBuffer].vertexBufferResource) {
    return false;
  } else {
    updateVertexBuffer(m_currentBuffer, vertexSize, nVertices, vertexData);
    return true;
  }
}

auto GraphicsPipelineResource::createVertexShader(const std::string& aFileName) -> bool {
  assert(!m_vertexShaderModule);
  auto res = m_graphics->getDevice()->createShaderModule(aFileName);
  if(!res.error.empty()) {
    m_errorMessage = "Failed to create vertex shader module: " + std::move(res.error);
    return false;
  }
  m_vertexShaderModule = std::move(res.value);
  return true;
}

auto GraphicsPipelineResource::addDescriptorSetsCIS() -> bool {
  assert(m_descriptorSetLayoutCIS);
  const auto imageCount = m_graphics->m_swapChainResource->getImageCount();
  assert(imageCount > 0);
  if (!m_descriptorPool)
    m_descriptorPool = m_graphics->createDescriptorPoolCIS();
  std::vector<vk::DescriptorSetLayout> layouts(imageCount, m_descriptorSetLayoutCIS.get());
  vk::DescriptorSetAllocateInfo allocInfo{};
  allocInfo.descriptorPool = m_descriptorPool.get();
  allocInfo.descriptorSetCount = imageCount;
  allocInfo.pSetLayouts = layouts.data();

  try {
    auto descriptorSets = m_device.allocateDescriptorSets(allocInfo);
    m_imageUniformResources.clear();
    for (size_t i = 0; i < imageCount; ++i) {
      auto uniformResource = new BufferResource(m_device);
      m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(uniformResource), nullptr, nullptr, descriptorSets[i]});
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    m_errorMessage = "Failed to allocate descriptor sets!";
    return false;
  }
  return true;
}

auto GraphicsPipelineResource::createUniformBuffers(vk::DeviceSize bufferSize) -> bool {
  assert(m_descriptorSetLayoutUB);
  const auto imageCount = m_graphics->m_swapChainResource->getImageCount();
  assert(imageCount > 0);
  if (!m_descriptorPool)
    m_descriptorPool = m_graphics->createDescriptorPool(imageCount, 0, 0);
  std::vector<vk::DescriptorSetLayout> layouts(imageCount, m_descriptorSetLayoutUB.get());
  vk::DescriptorSetAllocateInfo allocInfo{};
  allocInfo.descriptorPool = m_descriptorPool.get();
  allocInfo.descriptorSetCount = imageCount;
  allocInfo.pSetLayouts = layouts.data();

  try {
    auto descriptorSets = m_device.allocateDescriptorSets(allocInfo);
    clearUniformResources();
    for (size_t i = 0; i < imageCount; ++i) {
      auto uniformResource = new BufferResource(m_device);
      if (bufferSize > 0)
        m_graphics->createBuffer(bufferSize, 1, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, uniformResource);
      m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(uniformResource), nullptr, descriptorSets[i], nullptr});
    }
    if (bufferSize > 0)
      updateDescriptorSetsUB(bufferSize, 0);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    m_errorMessage = "Failed to allocate descriptor sets!";
    return false;
  }
  return true;
}

auto GraphicsPipelineResource::createUniformBuffers2(vk::DeviceSize bufferSize) -> bool {
  try {
    const auto imageCount = m_graphics->m_swapChainResource->getImageCount();
    assert(imageCount > 0);
    if (!m_descriptorPool) {
      if (m_descriptorSetLayoutCIS)
        m_descriptorPool = m_graphics->createGraphicsDescriptorPool(1);
      else
        m_descriptorPool = m_graphics->createDescriptorPool(imageCount, 0, 0);
    }
    auto descriptorSetsUB = allocatedDescriptorSets(imageCount, m_descriptorSetLayoutUB);
    std::vector<vk::DescriptorSet> descriptorSetsCIS;
    if (m_descriptorSetLayoutCIS)
      descriptorSetsCIS = allocatedDescriptorSets(imageCount, m_descriptorSetLayoutCIS);

    clearUniformResources();
    for (size_t i = 0; i < imageCount; ++i) {
      auto uniformResource = new BufferResource(m_device);
      if (bufferSize > 0)
        m_graphics->createBuffer(bufferSize, 1, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, uniformResource);
      if (descriptorSetsCIS.empty())
        m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(uniformResource), nullptr, descriptorSetsUB[i], nullptr});
      else
        m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(uniformResource), nullptr, descriptorSetsUB[i], descriptorSetsCIS[i]});
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    m_errorMessage = "Failed to allocate descriptor sets";
    return false;
  }
  return true;
}

auto GraphicsPipelineResource::createUniformBuffers(const std::vector<UniformBinding>& bindings, std::vector<Mesh>& meshes, std::vector<Skin>& skins) -> bool {
  try {
    const auto imageCount = m_graphics->m_swapChainResource->getImageCount();
    size_t nPrimitives = 0;
    for (const auto& mesh : meshes) {
      nPrimitives += mesh.primitives.size();
    }
    assert(imageCount > 0);
    auto nBindings = bindings.size();
    auto nSkins = skins.size();
    // First create the pool
    m_descriptorPool = m_graphics->createDescriptorPool(imageCount * nBindings, nPrimitives, nSkins);
    // Allocate UniformBuffer descriptorsets unsing this pool
    auto descriptorSetsUB = allocatedDescriptorSets(imageCount, m_descriptorSetLayoutUB);

    m_imageUniformResources.clear();
    for (size_t i = 0; i < imageCount; ++i) {
      auto bufferMatrices = new BufferResource(m_device);
      m_graphics->createBuffer(bindings.at(0).bufferSize, 1, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bufferMatrices);
      if (bindings.size() > 1) {
        auto bufferLights = new BufferResource(m_device);
        m_graphics->createBuffer(bindings.at(1).bufferSize, 1, vk::BufferUsageFlagBits::eUniformBuffer, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, bufferLights);
        m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(bufferMatrices), std::unique_ptr<BufferResource>(bufferLights), descriptorSetsUB[i], nullptr});
      } else
        m_imageUniformResources.push_back({std::unique_ptr<BufferResource>(bufferMatrices), nullptr, descriptorSetsUB[i], nullptr});
    }
    if (m_descriptorSetLayoutCIS) {
      std::vector<vk::DescriptorSet> descriptorSetsCIS;
      descriptorSetsCIS = allocatedDescriptorSets(nPrimitives, m_descriptorSetLayoutCIS);
      size_t iPrimitive = 0;
      for (auto& mesh : meshes) {
        for (auto& p : mesh.primitives) {
          p.descriptionSet = descriptorSetsCIS[iPrimitive];
          iPrimitive++;
        }
      }
    }
    if (m_descriptorSetLayoutSB && !skins.empty()) {
      auto descriptorSetsSB = allocatedDescriptorSets(skins.size(), m_descriptorSetLayoutSB);
      size_t iSkin = 0;
      for (auto& skin : skins) {
        skin.descriptorSet = descriptorSetsSB[iSkin];
      }
    }
    for (const auto& binding : bindings) {
      updateDescriptorSetsUB(binding.bufferSize, binding.binding);
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    m_errorMessage = "Failed to allocate descriptor sets";
    return false;
  }
  return true;
}

auto GraphicsPipelineResource::createUniformBuffers(vk::DeviceSize bufferSize, const vk::UniqueImageView& textureImageView, const vk::UniqueSampler& textureSampler) -> bool {
  try {
    if (!createUniformBuffers2(bufferSize))
      return false;
    updateDescriptorSets(bufferSize, textureImageView, textureSampler);
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    m_errorMessage = "Failed to allocate descriptor sets";
    return false;
  }
  return true;
}

auto GraphicsPipelineResource::drawIndexed(const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes, const std::vector<Skin>& skins) const -> void {
  bool hasSkins = !skins.empty();
  for (const auto& mesh : meshes) {
    if (hasSkins) {
      assert(!mesh.skins.empty());
      auto& skinID = mesh.skins.front();
      auto& skin = skins.at(skinID);
      imageResource.m_stdCommandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout.get(), 1, 1, &skin.descriptorSet, 0, nullptr);
    }
    for (const auto& p : mesh.primitives) {
      if (m_descriptorSetLayoutCIS && p.descriptionSet)
        bindDescriptorSets(imageResource, p, hasSkins ? 2 : 1);
      imageResource.m_stdCommandBuffer.drawIndexed(p.indexCount, 1, p.firstIndex, 0, 0);
    }
  }
}

auto GraphicsPipelineResource::drawIndexed(const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes, const std::vector<Material>& materials) const -> void {
  for (const auto& mesh : meshes) {
    auto transforms = mesh.transformMatrices;
    if (transforms.empty())
      transforms.push_back(glm::mat4x4(1.0f));
    for (const auto& transform : transforms) {
      for (const auto& primitive : mesh.primitives) {
        PushPBR push{};
        push.transform = transform;
        if (primitive.materialIndex > -1) {
          const auto& material = materials.at(primitive.materialIndex);
          push.roughness = material.roughnessFactor;
          push.metallic = material.metallicFactor;
          push.r = material.baseColorFactor.r;
          push.b = material.baseColorFactor.b;
          push.g = material.baseColorFactor.g;
        }
        imageResource.m_stdCommandBuffer.pushConstants(m_pipelineLayout.get(), m_pushStageFlags, 0, sizeof(PushPBR), &push);
        if (m_descriptorSetLayoutCIS && primitive.descriptionSet)
          bindDescriptorSets(imageResource, primitive, 1);
        imageResource.m_stdCommandBuffer.drawIndexed(primitive.indexCount, 1, primitive.firstIndex, 0, 0);
      }
    }
  }
}

void GraphicsPipelineResource::updateDescriptorSetsUB(vk::DeviceSize bufferSize, uint32_t dstBinding) const {
  assert(bufferSize > 0);
  std::vector<vk::WriteDescriptorSet> descriptorWrites;
  for (const auto& imageResource : m_imageUniformResources) {
    assert(imageResource.descriptorSetUB);
    vk::DescriptorBufferInfo bufferInfo{};
    bufferInfo.buffer = imageResource.bufferMatrices->m_buffer.get();
    bufferInfo.offset = 0;
    bufferInfo.range = bufferSize;
    WDS_UniformBuffer(descriptorWrites, imageResource, bufferInfo, dstBinding);
  }
  m_device.updateDescriptorSets(descriptorWrites, {});
}

void GraphicsPipelineResource::updateDescriptorSetsCIS(const vk::DescriptorSet& descriptorSet, const vk::UniqueImageView& imageView, const vk::UniqueSampler& textureSampler) const {
  std::vector<vk::WriteDescriptorSet> descriptorWrites;
  vk::DescriptorImageInfo imageInfo{};
  imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
  imageInfo.imageView = imageView.get();
  imageInfo.sampler = textureSampler.get();
  WDS_ImageSampler(descriptorWrites, descriptorSet, imageInfo);
  m_device.updateDescriptorSets(descriptorWrites, {});
}

void GraphicsPipelineResource::updateDescriptorSetsSB(const vk::DescriptorSet& descriptorSet, vk::Buffer buffer, vk::DeviceSize bufferSize) const {
  std::vector<vk::WriteDescriptorSet> descriptorWrites;
  vk::DescriptorBufferInfo bufferInfo{};
  bufferInfo.buffer = buffer;
  bufferInfo.offset = 0;
  bufferInfo.range = bufferSize;
  WDS_StorageBuffer(descriptorWrites, descriptorSet, bufferInfo, 0);
  m_device.updateDescriptorSets(descriptorWrites, {});
}

void GraphicsPipelineResource::updateDescriptorSets(vk::DeviceSize bufferSize, const vk::UniqueImageView& imageView, const vk::UniqueSampler& textureSampler) const {
  assert(imageView);
  std::vector<vk::WriteDescriptorSet> descriptorWrites;
  for (auto& imageResource : m_imageUniformResources) {
    if (bufferSize > 0) {
      assert(imageResource.descriptorSetUB);
      vk::DescriptorBufferInfo bufferInfo{};
      bufferInfo.buffer = imageResource.bufferMatrices->m_buffer.get();
      bufferInfo.offset = 0;
      bufferInfo.range = bufferSize;
      WDS_UniformBuffer(descriptorWrites, imageResource, bufferInfo, 0);
    }
    assert(imageResource.descriptorSetCIS);
    vk::DescriptorImageInfo imageInfo{};
    imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    imageInfo.imageView = imageView.get();
    imageInfo.sampler = textureSampler.get();
    WDS_ImageSampler(descriptorWrites, imageResource.descriptorSetCIS, imageInfo);
  }
  m_device.updateDescriptorSets(descriptorWrites, {});
}

// Update the vertex buffer
void GraphicsPipelineResource::updateVertexBuffer(size_t iMorph, vk::DeviceSize vertexSize, size_t nVertices, const void *vertexData) {
  assert(nVertices > 0);
  const auto bufferSize = vertexSize * nVertices;
  BufferResource stagingBufferResource(m_device);
  m_graphics->createBuffer(vertexSize, nVertices, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, &stagingBufferResource);

  auto data = stagingBufferResource.mapMemory(bufferSize);
  memcpy(data, vertexData, static_cast<size_t>(bufferSize));
  stagingBufferResource.unmapMemory();

  auto &bufferResource = m_bufferPairs[iMorph].vertexBufferResource;
  bufferResource.reset(new BufferResource(m_device));
  m_graphics->createBuffer(vertexSize, nVertices, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer, vk::MemoryPropertyFlagBits::eDeviceLocal, bufferResource.get());
  m_graphics->copyBuffer(stagingBufferResource.m_buffer.get(), bufferResource->m_buffer.get(), bufferSize);
}

void GraphicsPipelineResource::WDS_ImageSampler(std::vector<vk::WriteDescriptorSet>& set, const vk::DescriptorSet& descriptorSet, const vk::DescriptorImageInfo& imageInfo) const {
  vk::WriteDescriptorSet descriptorWrite{};
  descriptorWrite.dstSet = descriptorSet;
  descriptorWrite.dstBinding = 0;
  descriptorWrite.dstArrayElement = 0;
  descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
  descriptorWrite.descriptorCount = 1;
  descriptorWrite.pBufferInfo = nullptr;
  descriptorWrite.pImageInfo = &imageInfo;
  set.emplace_back(descriptorWrite);
}

void GraphicsPipelineResource::WDS_UniformBuffer(std::vector<vk::WriteDescriptorSet>& set, const UniformDescriptor& resource, const vk::DescriptorBufferInfo& bufferInfo, uint32_t dstBinding) const {
  vk::WriteDescriptorSet descriptorWrite{};
  descriptorWrite.dstSet = resource.descriptorSetUB;
  descriptorWrite.dstBinding = dstBinding;
  descriptorWrite.dstArrayElement = 0;
  descriptorWrite.descriptorType = vk::DescriptorType::eUniformBuffer;
  descriptorWrite.descriptorCount = 1;
  descriptorWrite.pBufferInfo = &bufferInfo;
  descriptorWrite.pImageInfo = nullptr;
  descriptorWrite.pTexelBufferView = nullptr;
  set.emplace_back(descriptorWrite);
}

void GraphicsPipelineResource::WDS_StorageBuffer(std::vector<vk::WriteDescriptorSet>& set, const vk::DescriptorSet& descriptorSet, const vk::DescriptorBufferInfo& bufferInfo, uint32_t dstBinding) const {
  vk::WriteDescriptorSet descriptorWrite{};
  descriptorWrite.dstSet = descriptorSet;
  descriptorWrite.dstBinding = dstBinding;
  descriptorWrite.dstArrayElement = 0;
  descriptorWrite.descriptorType = vk::DescriptorType::eStorageBuffer;
  descriptorWrite.descriptorCount = 1;
  descriptorWrite.pBufferInfo = &bufferInfo;
  descriptorWrite.pImageInfo = nullptr;
  descriptorWrite.pTexelBufferView = nullptr;
  set.emplace_back(descriptorWrite);
}
