#include "truncatedpolyhedron.h"

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>
#include <forward_list>

TruncatedPolyhedron::TruncatedPolyhedron(const std::unique_ptr<Polyhedron>& source) {
  const auto& originalVertices = source->getVertices();
  const auto& originalFaces = source->getFaces();
  const auto& originalEdges = source->getEdges();
  const glm::dvec3 originalCenter = {0,0,0};
  ObjectType newType = ObjectType::NONE;
  switch (source->getType()) {
  case ObjectType::CUBE:
    newType = ObjectType::TRUNCATED_CUBE;
    break;
  case ObjectType::CUBOCTAHEDRON:
    newType = ObjectType::TRUNCATED_CUBOCTAHEDRON;
    break;
  case ObjectType::DODECAHEDRON:
    newType = ObjectType::TRUNCATED_DODECAHEDRON;
    break;
  case ObjectType::ICOSAHEDRON:
    newType = ObjectType::TRUNCATED_ICOSAHEDRON;
    break;
  case ObjectType::OCTAHEDRON:
    newType = ObjectType::TRUNCATED_OCTAHEDRON;
    break;
  case ObjectType::TETRAHEDRON:
    newType = ObjectType::TRUNCATED_TETRAHEDRON;
    break;
  default:
    newType = ObjectType::NONE;
  }
  m_target = std::make_unique<Polyhedron>(newType, source->getColorList(), true);
  m_target->m_isConvex = source->m_isConvex;
  m_target->m_isPlatonic = false;
  // Find the vertices which should not be truncated
  // These are vertices that are closer to the center than their neighbor vertices
  // This applies to non-convex solids
  if (!source->m_isConvex) {
    const auto nVertices = originalVertices.size();
    for (size_t iVertex = 0; iVertex < nVertices; iVertex++) {
      const auto &originalVertex = originalVertices.at(iVertex);
      // Find all edges linked to <originalVertex>
      for (const auto& originalEdge : originalEdges) {
        if (originalEdge.iVertexBegin != iVertex && originalEdge.iVertexEnd != iVertex)
          continue;
        // Found an edge linked to <originalVertex>
        const auto v1 = glm::normalize(originalCenter - originalVertex);
        glm::dvec3 v2;
        if (originalEdge.iVertexBegin == iVertex)
          v2 = glm::normalize(originalVertices.at(originalEdge.iVertexEnd) - originalVertex);
        else
          v2 = glm::normalize(originalVertices.at(originalEdge.iVertexBegin) - originalVertex);
        // Exclude vertices with angles bigger than 90 degree
        const auto dp = glm::dot(v1, v2);
        if (dp > 0.001)
          continue;
        m_excludeVertices.push_back(iVertex);
        m_target->appendVertex(originalVertex);
        break;
      }
    }
  }
  // Group the original faces by number of corners
  // The faces with more corners first
  std::map<size_t, std::vector<size_t>, std::greater<double>> cornerMap = makeCornerMap(originalFaces);
  // Create a new face at each vertex
  ushort lastColor = 0;
  // The new faces need twice as many edges/vertices than original
  for (const auto& cornerFaces : cornerMap) {
    // Process the faces with the same number of corners
    for (const auto iFace : cornerFaces.second) {
      const auto& originalFace = originalFaces.at(iFace);
      const auto originalFaceCenter = originalFace.getCenter(originalVertices);
      const auto nCorners = originalFace.getCorners();
      const auto iColor = originalFace.getColorIndex();
      if (iColor > lastColor)
        lastColor = iColor;
      // Create a new face for each original face
      Face newFace(iColor);
      for (size_t i1 = 0; i1 < nCorners; i1++) {
        const auto iOrigVertex1 = originalFace.at(i1);
        const auto i2 = (i1 + 1) % nCorners;
        const auto iOrigVertex2 = originalFace.at(i2);
        // Check whether the edge was created for a previous face
        bool edgeFound = false;
        bool inverted = false;
        for (const auto &edge : m_truncateEdges) {
          if (edge.original1 == iOrigVertex1 && edge.original2 == iOrigVertex2)
            edgeFound = true;
          else if (edge.original1 == iOrigVertex2 && edge.original2 == iOrigVertex1) {
            edgeFound = true;
            inverted = true;
          } else
            continue;

          // Add existing edge to this face
          auto iNewVertex = inverted ? edge.iNewVertex2 : edge.iNewVertex1;
          if (!newFace.contains(iNewVertex))
            newFace.push_back_unique(iNewVertex);
          iNewVertex = inverted ? edge.iNewVertex1 : edge.iNewVertex2;
          if (!newFace.contains(iNewVertex))
            newFace.push_back_unique(iNewVertex);
          break;
        }
        if (!edgeFound)
          addNewEdge2(originalVertices, originalFaceCenter, iOrigVertex1, iOrigVertex2, newFace);
      }
      m_target->appendFace(newFace);
    }
  }
  lastColor++;
  // Add the extra faces which resulted from the truncation
  for (size_t iVertex = 0; iVertex < originalVertices.size(); iVertex++) {
    if (excludeContains(iVertex))
      continue;
    Face newFace(lastColor);
    bool skipFace = false;
    const auto& originalVertex = originalVertices.at(iVertex);
    std::vector<size_t> vertexEdges;
    // Store the neighbors of <iVertex>
    std::vector<size_t> originalNeighborVertices;
    // Get all edges linked to <iVertex>
    for (size_t iEdge = 0; iEdge < originalEdges.size(); ++iEdge) {
      const auto& edge = originalEdges.at(iEdge);
      if (edge.iVertexBegin == iVertex || edge.iVertexEnd == iVertex) {
        if (edge.iVertexBegin == iVertex)
          originalNeighborVertices.push_back(edge.iVertexEnd);
        else
          originalNeighborVertices.push_back(edge.iVertexBegin);
        vertexEdges.push_back(iEdge);
      }
    }
    assert(vertexEdges.size() > 2);
    if (vertexEdges.size() == 3) {
      // Just a simple triangle
      for (const auto iEdge : vertexEdges) {
        const auto& edge = originalEdges.at(iEdge);
        addToTruncatedFace(edge, originalVertex, newFace);
      }
    } else {
      // Add the vertices to the face in the right order
      auto iOriFace = SIZE_MAX;
      while (!vertexEdges.empty()) {
        Edge edge;
        if (iOriFace == SIZE_MAX) {
          // Pick the last one
          auto iEdge = vertexEdges.back();
          vertexEdges.pop_back();
          edge = originalEdges.at(iEdge);
          iOriFace = edge.iFace1;
        } else {
          bool found = false;
          for (auto it = vertexEdges.begin(); it < vertexEdges.end(); ++it) {
            edge = originalEdges.at(*it);
            if (edge.iFace1 == iOriFace || edge.iFace2 == iOriFace) {
              iOriFace = edge.iFace1 == iOriFace ? edge.iFace2 : edge.iFace1;
              vertexEdges.erase(it);
              found = true;
              break;
            }
          }
          assert(found);
        }
        addToTruncatedFace(edge, originalVertex, newFace);
      }
      // The vertices of this face do not always lay in one plane!
      // This should fix that
      Plane facePlane;
      facePlane.point = newFace.getCenter(m_target->getVertices());
      facePlane.normal = glm::normalize(originalVertex - facePlane.point);
      Line line1;
      line1.point = originalVertex;
      size_t iterations = 0;
      assert(originalNeighborVertices.size() == newFace.getCorners());
      while (true) {
        // Avoid moving a vertex more than half the distance to a neighbor of <originalVertex>
        // So <maxFraction may never exceed 0.5!
        double maxFraction = 0;
        for (const auto iNewVertex : newFace) {
          // Move <m_target->getVertex(iVertex)> to <newvertex>
          line1.direction = glm::normalize(m_target->getVertex(iNewVertex) - originalVertex);
          size_t iNeighborVertex = SIZE_MAX;
          double minDistance = 0;
          for (const auto iVertex : originalNeighborVertices) {
            const auto& neighborVertex = originalVertices.at(iVertex);
            const auto distance = Polyhedron::getDistanceToLineDir(neighborVertex, line1.point, line1.direction);
            if (iNeighborVertex == SIZE_MAX || distance < minDistance) {
              iNeighborVertex = iVertex;
              minDistance = distance;
            }
          }
          assert(iNeighborVertex < originalVertices.size());
          const auto neighborVertex = originalVertices.at(iNeighborVertex);
          const auto newVertex = Polyhedron::getPlaneLineIntersection(facePlane, line1);
          const auto fraction = glm::distance(newVertex, originalVertex) / glm::distance(neighborVertex, originalVertex);
          if (fraction > maxFraction)
            maxFraction = fraction;
          m_target->setVertex(iNewVertex, newVertex);
        }
        if (maxFraction < 0.45)
          break;
        facePlane.point = originalVertex + (facePlane.point - originalVertex) / (maxFraction * 3.0);
        iterations++;
        assert(iterations < 3);
      }
      if (iterations > 0)
        skipFace = true;
    }
    if (!skipFace)
      m_target->appendFace(newFace);
  }
  // Clean up
  m_truncateEdges.clear();
}

void TruncatedPolyhedron::addToTruncatedFace(const Edge& originalEdge, const glm::dvec3& faceCenter, Face& face) {
  bool found = false;
  for (const auto& newEdge : m_truncateEdges) {
    if ((newEdge.original1 == originalEdge.iVertexBegin && newEdge.original2 == originalEdge.iVertexEnd) ||
        (newEdge.original1 == originalEdge.iVertexEnd   && newEdge.original2 == originalEdge.iVertexBegin)) {
      const auto vertex1 = m_target->getVertex(newEdge.iNewVertex1);
      const auto vertex2 = m_target->getVertex(newEdge.iNewVertex2);
      const auto d1 = glm::distance(faceCenter, vertex1);
      const auto d2 = glm::distance(faceCenter, vertex2);
      const auto iNewVertex = (d1 < d2) ? newEdge.iNewVertex1 : newEdge.iNewVertex2;
      face.push_back_unique(iNewVertex);
      found = true;
      break;
    }
  }
  assert(found);
}

double TruncatedPolyhedron::findLargestEntry(const glm::dmat3 &m){
  double result = 0.0;
  for(auto i=0;i<3;i++){
    for(auto j=0;j<3;j++){
      auto entry = abs(m[i][j]);
      result=std::max(entry,result);
    }
  }
  return result;
}

glm::dvec3 TruncatedPolyhedron::findEigenVectorAssociatedWithLargestEigenValue(const glm::dmat3 &m) {
  //pre-condition
  const auto scale = findLargestEntry(m);
  auto mc = m / scale;
  mc=mc*mc;
  mc=mc*mc;
  mc=mc*mc;
  glm::dvec3 v(1,1,1);
  auto lastV = v;
  for (int i=0;i<100;i++){
    v = glm::normalize(mc*v);
    const glm::dvec3 delta(v.x - lastV.x, v.x - lastV.x, v.x - lastV.x);
    if (glm::dot(delta,delta) < 1e-16){
      break;
    }
    lastV=v;
  }
  return v;
}

bool TruncatedPolyhedron::findPlane(const Face& face, const std::vector<glm::dvec3>& vertices, glm::dvec3& center, glm::dvec3& planeNormal) {
  std::vector<glm::dvec3> points;
  center = glm::dvec3(0,0,0);
  for (const auto iVertex : face) {
    points.push_back(vertices.at(iVertex));
    center += vertices.at(iVertex);
  }
  const auto nCorners = face.getCorners();
  center /= nCorners;
  double sumXX=0.0, sumXY=0.0, sumXZ=0.0;
  double sumYY=0.0, sumYZ=0.0;
  double sumZZ=0.0;
  for (const auto& point : points) {
    const auto diffX = point.x - center.x;
    const auto diffY = point.y - center.y;
    const auto diffZ = point.z - center.z;
    sumXX+=diffX*diffX;
    sumXY+=diffX*diffY;
    sumXZ+=diffX*diffZ;
    sumYY+=diffY*diffY;
    sumYZ+=diffY*diffZ;
    sumZZ+=diffZ*diffZ;
  }
  glm::dmat3 m(sumXX,sumXY,sumXZ,\
          sumXY,sumYY,sumYZ,\
          sumXZ,sumYZ,sumZZ);
  if (glm::determinant(m) == 0.0)
    return false;
  auto mInv = glm::inverse(m);
  planeNormal = findEigenVectorAssociatedWithLargestEigenValue(mInv);
  return true;
}

void TruncatedPolyhedron::addNewEdgeSimple(const std::vector<glm::dvec3>& originalVertices, const uint16_t iOrigVertex1, const uint16_t iOrigVertex2, Face& newFace) {
  assert(iOrigVertex1 < originalVertices.size());
  assert(iOrigVertex2 < originalVertices.size());
  const auto& originalVertex1 = originalVertices[iOrigVertex1];
  const auto& originalVertex2 = originalVertices[iOrigVertex2];
  const auto edgeCenter = (originalVertex1 + originalVertex2) / 2.0;
  const auto exclude1 = excludeContains(iOrigVertex1);
  const auto exclude2 = excludeContains(iOrigVertex2);
  glm::dvec3 newVertex(0);
  // <newVertex> will be between <originalVertex1> and <edgeCenter>
  // if <originalVertex1> should not be truncated use <originalVertex1> as <newVertex>
  TruncateEdge newEdge;
  newEdge.original1 = iOrigVertex1;
  newEdge.original2 = iOrigVertex2;
  if (exclude1) {
    const auto nCorners = newFace.getCorners();
    if (nCorners > 0) {
      newEdge.iNewVertex1 = newFace.at(nCorners - 1);
    } else {
      newEdge.iNewVertex1 = m_target->appendFaceWithVertex0(newFace, originalVertex1);
    }
  } else {
    newVertex = edgeCenter + (originalVertex1 - edgeCenter) / 2.0;
    newEdge.iNewVertex1 = m_target->appendFaceWithUniqueVertex(newFace, newVertex);
  }
  if (exclude2)
    newVertex = originalVertex2;
  else
    newVertex = edgeCenter + (originalVertex2 - edgeCenter) / 2.0;
  newEdge.iNewVertex2 = m_target->appendFaceWithVertex0(newFace, newVertex);
  m_truncateEdges.push_back(newEdge);
}

void TruncatedPolyhedron::addNewEdge2(const std::vector<glm::dvec3>& originalVertices, const glm::dvec3& faceCenter, uint16_t iOrigVertex1, uint16_t iOrigVertex2, Face& newFace) {
  assert(iOrigVertex1 < originalVertices.size());
  assert(iOrigVertex2 < originalVertices.size());
  const auto& originalVertex1 = originalVertices[iOrigVertex1];
  const auto& originalVertex2 = originalVertices[iOrigVertex2];
  Line edgeLine;
  edgeLine.point = originalVertex1;
  edgeLine.direction = glm::normalize(originalVertex2 - originalVertex1);
  const auto exclude1 = excludeContains(iOrigVertex1);
  const auto exclude2 = excludeContains(iOrigVertex2);
  TruncateEdge newEdge;
  newEdge.original1 = iOrigVertex1;
  newEdge.original2 = iOrigVertex2;
  assert((!exclude1 && !exclude2));
  if (!exclude1 && !exclude2) {
    const auto direction1 = glm::normalize(originalVertex1 - faceCenter);
    const auto direction2 = glm::normalize(originalVertex2 - faceCenter);
    const auto directionCenter = glm::normalize(direction1 + direction2);
    if (!exclude1) {
      Line lineA;
      lineA.point = faceCenter;
      lineA.direction = glm::normalize(direction1 + directionCenter);
      const auto newVertex = Polyhedron::getLinesIntersect(edgeLine, lineA);
      newEdge.iNewVertex1 = m_target->appendFaceWithUniqueVertex(newFace, newVertex);
    }
    if (!exclude2) {
      Line lineB;
      lineB.point = faceCenter;
      lineB.direction = glm::normalize(directionCenter + direction2);
      const auto newVertex = Polyhedron::getLinesIntersect(edgeLine, lineB);
      newEdge.iNewVertex2 = m_target->appendFaceWithUniqueVertex(newFace, newVertex);
    }
  } else {
    if (exclude1) {
      const auto nCorners = newFace.getCorners();
      if (nCorners > 0) {
        newEdge.iNewVertex1 = newFace.at(nCorners - 1);
      } else {
        newEdge.iNewVertex1 = m_target->appendFaceWithVertex0(newFace, originalVertex1);
      }
    }
    if (exclude2)
      newEdge.iNewVertex2 = m_target->appendFaceWithVertex0(newFace, originalVertex2);
  }
  m_truncateEdges.push_back(newEdge);
}

// Calculate the new vertices using the angle between iOrigVertex1 - faceCenter - iOrigVertex2
// Does not work for a truncated cuboctahedron
void TruncatedPolyhedron::addNewEdge(const std::vector<glm::dvec3>& originalVertices, const glm::dvec3& faceCenter, uint16_t iOrigVertex1, uint16_t iOrigVertex2, Face& newFace) {
  assert(iOrigVertex1 < originalVertices.size());
  assert(iOrigVertex2 < originalVertices.size());
  const auto& originalVertex1 = originalVertices[iOrigVertex1];
  const auto& originalVertex2 = originalVertices[iOrigVertex2];
  const auto edgeCenter = (originalVertex1 + originalVertex2) / 2.0;
  const auto exclude1 = excludeContains(iOrigVertex1);
  const auto exclude2 = excludeContains(iOrigVertex2);
  glm::dvec3 delta;
  if (!exclude1 && !exclude2) {
    // Angle originalVertex1 - faceCenter - originalVertex2
    const auto direction1 = glm::normalize(originalVertex1 - faceCenter);
    const auto direction2 = glm::normalize(originalVertex2 - faceCenter);
    const auto dotp = glm::dot(direction1, direction2);
    const auto angle = acos(dotp);
    // The segment between each corner can be divided in 4 equal parts if all corners are truncated
    // This creates edges with length = 2 * <delta>
    const auto edgeDir = glm::normalize(originalVertex2 - originalVertex1);
    delta = tan(angle / 4) * glm::distance(edgeCenter, faceCenter) * edgeDir;
  }
  glm::dvec3 newVertex(0);
  // <newVertex> will be between <originalVertex1> and <edgeCenter>
  // if <originalVertex1> should not be truncated use <originalVertex1> as <newVertex>
  TruncateEdge newEdge;
  newEdge.original1 = iOrigVertex1;
  newEdge.original2 = iOrigVertex2;
  if (exclude1) {
    const auto nCorners = newFace.getCorners();
    if (nCorners > 0) {
      newEdge.iNewVertex1 = newFace.at(nCorners - 1);
    } else {
      newEdge.iNewVertex1 = m_target->appendFaceWithVertex0(newFace, originalVertex1);
    }
  } else {
    newVertex = edgeCenter - delta;
    newEdge.iNewVertex1 = m_target->appendFaceWithUniqueVertex(newFace, newVertex);
  }
  if (exclude2)
    newVertex = originalVertex2;
  else
    newVertex = edgeCenter + delta;
  newEdge.iNewVertex2 = m_target->appendFaceWithVertex0(newFace, newVertex);
  m_truncateEdges.push_back(newEdge);
}

std::map<size_t, std::vector<size_t>, std::greater<double> > TruncatedPolyhedron::makeCornerMap(const std::vector<Face>& originalFaces) {
  std::map<size_t, std::vector<size_t>, std::greater<double>> cornerMap;
  for (size_t iFace = 0; iFace < originalFaces.size(); iFace++) {
    const auto nCorners = originalFaces.at(iFace).getCorners();
    const auto it = cornerMap.find(nCorners);
    if (it == cornerMap.end()) {
      std::vector<size_t> vEmpty{iFace};
      cornerMap.insert(std::make_pair(nCorners, vEmpty));
    } else {
      it->second.push_back(iFace);
    }
  }
  return cornerMap;
}
