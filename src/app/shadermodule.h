#pragma once

#include <vector>
#include <vulkan/vulkan.hpp>

class ShaderModule {
private:
  vk::Device m_device;
public:
  vk::UniqueShaderModule m_shaderModule;

  ShaderModule(const vk::Device &aDevice);
  ShaderModule(const ShaderModule&) = delete; // No copy needed
  auto create(const std::vector<char>& code) -> bool;
};
