#pragma once

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <chrono>
#include <array>
#include <cstddef>
#include <cstdlib>
//#include <experimental/optional>
#include <memory>
#include <vector>
#include <vulkan/vulkan.hpp>
#include <boost/property_tree/ptree.hpp>

#include "physicaldevice.h"
#include "logicaldevice.h"
#include "computeresource.h"
#include "graphicsresource.h"
#include "graphicspipelineresource.h"
#include "polyhedron.h"
#include "panel.h"
#include "common.h"

namespace pt = boost::property_tree;

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif
const std::vector<const char*> validationLayers = {
  "VK_LAYER_KHRONOS_validation"
};

class VulkanLib {
public:
  vk::UniqueInstance                m_instance;
  std::unique_ptr<LogicalDevice>    m_logicalDevice;
  std::unique_ptr<GraphicsResource> m_graphics;
  std::unique_ptr<Panel>            m_leftPanel, m_rightPanel;

  VulkanLib();
  ~VulkanLib();

  auto acquireNextImage(uint32_t* pImageIndex) -> vk::Result;
  auto buildSwapChain(bool recreate, uint32_t width, uint32_t height) -> bool;
  auto createInstance(const std::vector<const char *>& extensions) -> bool;
  auto createLogicalDevice() -> bool;
  void cleanup();
  auto drawFrame(size_t iImage) -> vk::Result;
  void getConfig(pt::ptree& root);
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  auto getPanel() const -> Panel*;
  auto initImGui() -> bool;
  static void check_vk_result(VkResult err);
  static ErrorValue<std::vector<char>> readFile(const std::string& filename);
  auto pickPhysicalDevice() -> bool;
  void putConfig();
  auto rebuildCommandBuffers(bool doWait) -> bool;
  auto recreateSwapChain(uint32_t width, uint32_t height) -> bool;
  inline void setBackgroundColor(const vk::ClearColorValue& aColor) {
    m_graphics->setBackgroundColor(aColor);
  }
  void setMousePos(double x, double y);
  auto splitPanel(Panel* panel) -> bool;
  auto updateTextureResource(Panel* panel) -> bool;

private:
  VkDebugUtilsMessengerEXT debugMessenger;

  std::unique_ptr<PhysicalDevice> m_physicalDevice;
  std::string                     m_errorMessage;
  vk::DescriptorPool              m_imGuiDescriptorPool;
  double                          m_mousePosX, m_mousePosY;

  void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
  auto setupDebugMessenger() -> bool;
  auto createImGuiDescriptorPool() -> bool;
#ifdef PRESENT_MODE_MAILBOX
  static VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes);
#endif
  void logExtensions();
  auto recordStdCommandBuffers(size_t currentImage) -> bool;
};
