#include "face.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/normal.hpp>

Polygon::Polygon() {
}

Plane Polygon::getPlane() const {
  Plane result;
  result.normal = glm::triangleNormal(m_vertices.at(0), m_vertices.at(1), m_vertices.at(2));
  result.point = m_vertices.at(0);
  return result;
}

glm::dvec3 Polygon::takeLast() {
  auto vertex = m_vertices.back();
  m_vertices.pop_back();
  return vertex;
}

Face::Face() {
  m_colorIndex = 0;
  m_indices = {};
}

Face::Face(const uint16_t aColorIndex) {
  m_colorIndex = aColorIndex;
  m_indices = {};
}

Face::Face(const std::vector<uint16_t>& someIndices) {
  m_colorIndex = 0;
  m_indices = someIndices;
}

Face::Face(const std::vector<uint16_t>& someIndices, const uint16_t aColorIndex) {
  m_colorIndex = aColorIndex;
  m_indices = someIndices;
}

bool Face::areNeighbors(const Face& face) const {
  const auto nCorners = m_indices.size();
  assert(nCorners > 2);
  for (auto i = 0ul; i < nCorners; i++) {
    const auto iVertex = m_indices[i];
    for (auto j = 0ul; j < nCorners; j++) {
      if (face[uint16_t(j)] == iVertex)
        return true;
    }
  }
  return false;
}

glm::dvec3 Face::getCenter(const std::vector<glm::dvec3>& someVertices) const {
  glm::dvec3 center(0,0,0);
  for (const auto iVertex : m_indices)
    center += someVertices[iVertex];
  center /= m_indices.size();
  return center;
}

Plane Face::getPlane(const std::vector<glm::dvec3>& someVertices) const {
  Plane result;
  result.normal = calculateNormal(someVertices);
  result.point = someVertices[m_indices[0]];
  return result;
}

Polygon Face::getPolygon(const std::vector<glm::dvec3>& someVertices) const {
  Polygon result;
  for (const auto iVertex : m_indices)
    result.push_back(someVertices[iVertex]);
  return result;
}

glm::dvec3 Face::calculateNormal(const std::vector<glm::dvec3>& vertices) const {
  const auto nCorners = m_indices.size();
  assert(nCorners > 2);
  const auto vertex1 = vertices[m_indices[0]];
  ushort i = 1;
  auto vertex2 = vertices[m_indices[i]];
  while (glm::distance(vertex1, vertex2) < 0.1  && i < nCorners - 2) {
    i++;
    vertex2 = vertices[m_indices[i]];
  }
  i++;
  const auto vertex3 = vertices[m_indices[i]];
  const auto v = glm::triangleNormal(vertex1, vertex2, vertex3);
  if (glm::dot(v, vertex1) < 0)
    return -v;
  else {
    return v;
  }
}

long Face::indexOf(uint16_t index) const {
  const auto it = std::find(m_indices.cbegin(), m_indices.cend(), index);
  if (it == m_indices.cend())
    return -1;
  else
    return std::distance(m_indices.cbegin(), it);
}

std::vector<glm::dvec3> Face::getVertices(const std::vector<glm::dvec3>& objVertices) const {
  std::vector<glm::dvec3> result{};
  for (const auto iVertex : m_indices) {
    assert(iVertex < objVertices.size());
    result.push_back(objVertices[iVertex]);
  }
  return result;
}
