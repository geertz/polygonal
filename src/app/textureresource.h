#pragma once

#include "imageresource.h"
#include "graphicsresource.h"
#include "computeresource.h"
#include <tiny_gltf.h>

class TextureResource : public ImageResource {
public:
  explicit TextureResource(GraphicsResource* aResource) : ImageResource(aResource->getDevice(), vk::Format::eR8G8B8A8Srgb) {
    m_graphics = aResource;
  }
  explicit TextureResource(GraphicsResource* pGraphics, const char *filename, bool createMipmaps);
  explicit TextureResource(GraphicsResource* pGraphics, tinygltf::Image& anImage, bool createMipmaps);
  explicit TextureResource(GraphicsResource* pGraphics, std::unique_ptr<ComputeResource>& pCompute);
  auto copyBufferToImage(const vk::UniqueBuffer& buffer, uint32_t width, uint32_t height) -> bool;
  auto generateMipmaps() -> bool;
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  inline auto getExtent() const -> VkExtent2D {
    return m_extent;
  }
  inline auto hasError() -> bool {
    return !m_errorMessage.empty();
  }
private:
  struct Pixel {
    float r, g, b, a;
  };
  GraphicsResource *m_graphics = nullptr;
  std::string       m_errorMessage;
  vk::Extent2D      m_extent;
  uint32_t          m_mipLevels;

  auto createTextureImage(const vk::Extent2D anExtent) -> bool;
  inline auto createTextureImageView() -> void {
    createImageView(vk::ImageAspectFlagBits::eColor, m_mipLevels);
  }
  auto transitionImageLayout(vk::ImageLayout oldLayout, vk::ImageLayout newLayout) -> bool;
};
