#include "singletimecommands.h"

SingleTimeCommands::SingleTimeCommands(vk::Device aDevice, const vk::CommandPool &aPool) {
  vk::CommandBufferAllocateInfo allocInfo{};
  allocInfo.level = vk::CommandBufferLevel::ePrimary;
  allocInfo.commandPool = aPool;
  allocInfo.commandBufferCount = 1;
  m_commandBuffer = std::move(aDevice.allocateCommandBuffersUnique(allocInfo).front());
  vk::CommandBufferBeginInfo info(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
  m_commandBuffer->begin(info);
  m_started = true;
}

SingleTimeCommands::~SingleTimeCommands() {
  if (m_started)
    m_commandBuffer->end();
}

auto SingleTimeCommands::submit(const std::unique_ptr<QueueThread> &queue) -> bool {
  vk::SubmitInfo submitInfo{};
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &m_commandBuffer.get();
  m_commandBuffer->end();

  m_started = false;
  try {
    queue->submit(submitInfo, {});
    queue->waitIdle();
    return true;
  } catch (...) {
    m_errorMessage = "Failed to submit singe time commands";
    return false;
  }
}
