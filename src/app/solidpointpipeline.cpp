#include "solidpointpipeline.h"

SolidPointPipeline::SolidPointPipeline(GraphicsResource* pGraphics) : PipelineBase(pGraphics, "shaders/vert_solid_line.spv", "shaders/frag_object.spv", "Solid point pipeline") {
  if (!m_pipelineResource || hasError())
    return;
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(glm::vec3));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  if (!createPipeline(vk::PrimitiveTopology::ePointList, vk::PolygonMode::ePoint, vertexInputInfo)) {
    setResourceError();
    return;
  }
  m_ubo.alpha = 1.0f;
  m_ubo.color = {1.0f, 1.0f, 0.0f};
  m_ubo.pointSize = 10.0f;
  if (!m_pipelineResource->createUniformBuffers(sizeof(UniformBufferObject)))
    setResourceError();
}
// Matching 'layout(location = 0) in vec3 inPosition;' in shader solid_line.vert
std::vector<vk::VertexInputAttributeDescription> SolidPointPipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, 0}
  };
  return attributeDescriptions;
}

// Copy Uniform buffer object to image <currentImage>
auto SolidPointPipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

auto SolidPointPipeline::fillBuffers(const std::unique_ptr<Polyhedron>& solid) -> void {
  m_vertices.clear();
  for (const auto& vertex : solid->getVertices()) {
    m_vertices.push_back(vertex);
  }
}
