#pragma once

#include "graphicsresource.h"
#include "solidfillbasepipeline.h"

class SolidFillPipeline : public SolidFillBasePipeline {
public:
  explicit SolidFillPipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient);
  void clearAll();
  void draw(const SwapchainImageResource& imageResource, size_t iImage);
  void fillBuffers(const std::unique_ptr<Polyhedron>& solid);
  inline auto getNumberOfVertices() const -> std::size_t {
    return m_vertices.size();
  }
  void updateColor(const std::vector<glm::vec3>& oldColorList, const std::vector<glm::vec3>& newColorList);
  void updateVertexIndexBuffers();
private:
  // To be used with shader <solid.vert>
  struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 color;
  };
  std::vector<Vertex> m_vertices;
  std::vector<Mesh>   m_meshes;

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
  void appendVertex(uint16_t iVertex, const Vertex& row);
};
