#pragma once

#include "graphicsresource.h"
#include "graphicspipelineresource.h"

class PipelineBase {
public:
  explicit PipelineBase(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel);
  inline auto bindCommandBuffer(const SwapchainImageResource& imageResource, size_t iImage) -> void {
    assert(m_pipelineResource);
    m_pipelineResource->bindCommandBuffer(imageResource, iImage);
  }
  inline auto bindCommandBuffer(const SwapchainImageResource& imageResource) -> void {
    assert(m_pipelineResource);
    m_pipelineResource->bindCommandBuffer(imageResource);
  }
  inline auto createPipeline(vk::PrimitiveTopology aTopology, vk::PolygonMode aPolygonMode, const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange = nullptr) -> bool {
    assert(m_pipelineResource);
    return m_pipelineResource->createPipeline(aTopology, aPolygonMode, pVertexInputInfo, pPushConstantRange);
  }
  auto createPipeline(const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange = nullptr) -> bool;
  void draw(const SwapchainImageResource& imageResource, size_t iImage);
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  inline auto hasError() const -> bool {
    return !m_errorMessage.empty();
  }
protected:
  std::unique_ptr<GraphicsPipelineResource> m_pipelineResource;

  inline auto drawIndexed(const vk::CommandBuffer& cmdBuffer) -> void {
    assert(m_pipelineResource);
    m_pipelineResource->drawIndexed(cmdBuffer);
  }
  inline auto drawIndexed (const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes) -> void {
    assert(m_pipelineResource);
    m_pipelineResource->drawIndexed(imageResource, meshes);
  }
  inline auto drawIndexed (const SwapchainImageResource& imageResource, const std::vector<Mesh>& meshes, const std::vector<Skin>& skins) -> void {
    assert(m_pipelineResource);
    m_pipelineResource->drawIndexed(imageResource, meshes, skins);
  }
  static vk::VertexInputBindingDescription getBindingDescription(uint32_t stride);
  inline void setError(const std::string& anError) {
    m_errorMessage = m_errorLabel + ": " + anError;
  }
  inline void setResourceError() {
    assert(m_pipelineResource);
    m_errorMessage = m_errorLabel + ": " + m_pipelineResource->getErrorMessage();
    m_pipelineResource.reset(nullptr);
  }
private:
  std::string m_errorLabel;
  std::string m_errorMessage;
};

