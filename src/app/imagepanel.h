#pragma once

#include <string>
#include <tuple>
#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include "graphicspipelineresource.h"
#include "bufferresource.h"
#include "imageresource.h"
#include "textureresource.h"

class ImagePanel {
public:
  std::string                               m_errorMessage;
  vk::Viewport                              m_viewport;
  std::unique_ptr<GraphicsPipelineResource> m_pipelineResource = nullptr;

  ImagePanel(GraphicsResource *pGraphics);
  void bindCommandBuffer(const SwapchainImageResource &imageResource, size_t iImage);
  bool createImage(const std::string &fileName);
  auto createBuffers() -> void;
  void updateViewPort(uint32_t x, uint32_t width, uint32_t height);

private:
  struct VertexInput {
    glm::vec2 position;
    glm::vec2 texCoord;
  };

  GraphicsResource                *m_graphics;
  std::unique_ptr<TextureResource> m_textureResource;
  vk::UniqueSampler                m_textureSampler;
  std::vector<VertexInput>         m_vertices;

  auto fillVertexBuffer() -> void;
  std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
  vk::VertexInputBindingDescription getBindingDescription();
};

