#pragma once

#include <glm/glm.hpp>
#include "pipelinebase.h"

class AxisPipeline : public PipelineBase {
public:
  explicit AxisPipeline(GraphicsResource* pGraphics);
  auto copyUBO(uint32_t iImage) -> bool;
  auto fillVertexBuffer() -> void;
  inline void setMVP (glm::dmat4 mvp) {
      m_ubo.mvp = mvp;
  }
private:
  struct VertexInput {
    glm::vec4 lines;

    static vk::VertexInputBindingDescription getBindingDescription() {
      vk::VertexInputBindingDescription bindingDescription{};
      bindingDescription.binding = 0;
      bindingDescription.stride = sizeof(VertexInput);
      return bindingDescription;
    }
    static std::array<vk::VertexInputAttributeDescription, 1> getAttributeDescriptions() {
      std::array<vk::VertexInputAttributeDescription, 1> attributeDescriptions{};
      // inPosition1
      attributeDescriptions[0].binding = 0;
      attributeDescriptions[0].location = 0;
      attributeDescriptions[0].format = vk::Format::eR32G32B32A32Sfloat; // glm::vec4
      attributeDescriptions[0].offset = offsetof(VertexInput, lines);

      return attributeDescriptions;
    }
  };
  struct UniformBufferObject {
    glm::mat4 mvp;
  };

  UniformBufferObject      m_ubo{};
  std::vector<VertexInput> m_vertices;
};

