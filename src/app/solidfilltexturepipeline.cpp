#include <string.h>
#include "solidfilltexturepipeline.h"

SolidFillTexturePipeline::SolidFillTexturePipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient, const std::unique_ptr<TextureResource>& textureResource) : SolidFillBasePipeline(pGraphics, "shaders/vert_solid_fill_texture.spv", "shaders/frag_solid_fill_texture.spv", "Solid fill texture pipeline") {
  if (!m_pipelineResource)
    return;
  // Create the descriptor layout
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  m_pipelineResource->createDescriptorSetLayoutCIS();
  // Create the pipeline
  assert(textureResource);
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  if (!m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vertexInputInfo)) {
    setResourceError();
    return;
  }
  setColorAlpha(anAlpha);
  setColorAmbient(anAmbient);
  m_textureSampler = pGraphics->getDevice()->createSampler();
  m_pipelineResource->createDefaultDescriptorPool();
  if (!m_pipelineResource->createUniformBuffers(sizeof(UniformBufferObject), textureResource->m_imageView, m_textureSampler)) {
    setResourceError();
  }
}

void SolidFillTexturePipeline::appendVertex(uint16_t iVertex, const Vertex& row) {
  m_vertices.emplace_back(row);
  m_listOfIndices.push_back({iVertex});
}

void SolidFillTexturePipeline::clearAll() {
  m_indices.clear();
  m_vertices.clear();
  m_listOfIndices.clear();
  m_meshes.clear();
}

void SolidFillTexturePipeline::createBuffers() {
  assert(!m_vertices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
  Mesh mesh("");
  DrawPrimitive primitive(m_indices.size());
  mesh.primitives.push_back(primitive);
  m_meshes.push_back(mesh);
}

void SolidFillTexturePipeline::fillBuffers(const std::unique_ptr<Polyhedron>& solid) {
  assert(solid);
  if (!m_vertices.empty() && !m_indices.empty() && m_solidType == solid->getType() && m_isDualCompound == solid->getIsDualCompound()) {
    return;
  }
  clearAll();
  uint8_t iFace = 0;
  uint16_t pos = 0;
  for (const auto &face : solid->getFaces()) {
    const auto normal = face.calculateNormal(solid->getVertices());
    auto indices = face.copyIndices();
    glm::dmat4 transform(1.0);
    const auto axis2D = solid->transform2D2(face, transform);
    std::vector<glm::dvec3> faceVertices;
    for (const auto iVertex : face) {
      faceVertices.emplace_back(solid->getVertex(iVertex));
    }
    const auto ring1 = Polyhedron::createRing2D(faceVertices, transform, axis2D);
    double xMin = 0, xMax = 0;
    double yMin = 0, yMax = 0;
    bool first = false;
    for (const auto &point : ring1) {
      if (!first) {
        first = true;
        xMin = point.x;
        xMax = point.x;
        yMin = point.y;
        yMax = point.y;
      } else {
        if (point.x < xMin)
          xMin = point.x;
        if (point.x > xMax)
          xMax = point.x;
        if (point.y < yMin)
          yMin = point.y;
        if (point.y > yMax)
          yMax = point.y;
      }
    }
    const auto deltaX = xMax - xMin;
    const auto xCenter = (xMin + xMax) / 2.0;
    const auto deltaY = yMax - yMin;
    const auto yCenter = (yMin + yMax) / 2.0;
    const auto factor = deltaX * m_texExtent.height / (deltaY * m_texExtent.width);
    auto offset = pos;
    for (size_t i = 0; i < indices.size(); i++) {
      const auto iVertex = indices[i];
      Vertex v{};
      v.position = solid->getVertex(iVertex);
      v.normal = normal;
      if (factor > 1.0) {
        v.texCoord.x = 0.5f + static_cast<float>((ring1[i].x - xCenter) * factor / deltaX);
        v.texCoord.y = static_cast<float>((ring1[i].y - yMin) / deltaY);
      } else {
        v.texCoord.x = static_cast<float>((ring1[i].x - xMin) / deltaX);
        v.texCoord.y = 0.5f + static_cast<float>((ring1[i].y - yCenter) / (factor * deltaY));
      }
      appendVertex(iVertex, v);
      pos++;
    }
    makeTriangles(solid, face, indices, offset);
    m_listOfIndices.clear();
    iFace++;
  }
  m_solidType = solid->getType();
  m_isDualCompound = solid->getIsDualCompound();
}

std::vector<vk::VertexInputAttributeDescription> SolidFillTexturePipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)},
    {2, 0, vk::Format::eR32G32Sfloat, offsetof(Vertex, texCoord)}
  };
  return attributeDescriptions;
}
