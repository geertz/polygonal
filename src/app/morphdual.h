#pragma once

#include "morphbase.h"
#include "dualpolyhedron.h"

class MorphDual : public MorphBase {
public:
  MorphDual(const std::unique_ptr<Polyhedron>& origin);
private:
  void findNextDual(const std::unique_ptr<Polyhedron>& origin, const std::vector<DualObject>& duals, const std::vector<size_t>& vertexDuals, size_t iPrevDual, size_t& iNextDual, const std::vector<size_t>& excludeDuals);
  void makeMorphEdges(const std::unique_ptr<Polyhedron>& origin, const std::vector<DualObject>& duals, ushort iColor);
  ushort contractToDualPoint(const std::vector<DualObject>& duals, const std::vector<Face>& originalFaces, ushort iColor);
};
