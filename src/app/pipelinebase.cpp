#include "pipelinebase.h"

PipelineBase::PipelineBase(GraphicsResource* pGraphics, const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& aLabel) {
  m_pipelineResource = std::make_unique<GraphicsPipelineResource>(pGraphics);
  m_errorLabel = aLabel;
  if (!m_pipelineResource->createVertexShader(vertexShaderFile)) {
    setResourceError();
    return;
  }
  if (!m_pipelineResource->createFragmentShader(fragmentShaderFile)) {
    setResourceError();
    return;
  }
}

auto PipelineBase::createPipeline(const vk::PipelineVertexInputStateCreateInfo& pVertexInputInfo, vk::PushConstantRange* pPushConstantRange) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, pVertexInputInfo, pPushConstantRange)) {
    setResourceError();
    return false;
  }
  return true;
}

void PipelineBase::draw(const SwapchainImageResource& imageResource, size_t iImage) {
  bindCommandBuffer(imageResource, iImage);
  drawIndexed(imageResource.m_stdCommandBuffer);
}

vk::VertexInputBindingDescription PipelineBase::getBindingDescription(uint32_t stride) {
  vk::VertexInputBindingDescription bindingDescription{};
  bindingDescription.binding = 0;
  bindingDescription.stride = stride;
  bindingDescription.inputRate = vk::VertexInputRate::eVertex;
  return bindingDescription;
}

