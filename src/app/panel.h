#pragma once

#include <memory>
#include <boost/optional.hpp>
#include "config.h"
#include "axispipeline.h"
#include "imagepipeline.h"
#include "solidfillpipeline.h"
#include "solidfilltexturepipeline.h"
#include "solidlinepipeline.h"
#include "solidpointpipeline.h"
#include "morphpipeline.h"
#include "meshpipeline.h"
#include "meshmaterialpipeline.h"
#include "meshtexturepipeline.h"
#include "meshtexskinpipeline.h"
#include "transformsolid.h"
#include "computeresource.h"
#include "gltfmodel.h"

#define ptree_leftPanel "leftPanel"
#define ptree_rightPanel "rightPanel"

#define pPanelInfoName "popupSolidInfo"
#define pErrorName "popupError"
#define pZoomName "popupZoom"

enum class PanelType {NONE, IMAGE_COMP, IMAGE_FILE, SOLID, MESH};

enum class ShaderType {NONE, SOLID_FILL, SOLID_FILL_TEXTURE, SOLID_LINE, MORPH};

static std::map<PanelType, std::string> panelTypes {
  {PanelType::NONE, "NONE"},
  {PanelType::IMAGE_COMP, "IMAGE_COMP"},
  {PanelType::IMAGE_FILE, "IMAGE_FILE"},
  {PanelType::SOLID, "SOLID"},
  {PanelType::MESH, "MESH"}
};

static std::map<FractalType, std::string> fractalTypes {
  {FractalType::JULIA, "JULIA"},
  {FractalType::MANDELBROT, "MANDELBROT"}
};

struct PanelSettings;

class Panel {
public:
  PanelType                                 m_type = PanelType::NONE;
  PanelType                                 m_nextType = PanelType::NONE;
  vk::Viewport                              m_viewport;
  // Solid pipelines
  std::unique_ptr<SolidFillPipeline>        m_solidFillPipeline;
  std::unique_ptr<SolidFillTexturePipeline> m_solidFillTexturePipeline;
  std::unique_ptr<SolidLinePipeline>        m_solidLinePipeline;
  // Polyhedron
  std::unique_ptr<Polyhedron>               m_solid;
  std::unique_ptr<GltfModel>                m_gltfModel, m_gltfModelNext;
  std::string                               m_textureFile, m_meshFile;
  bool                                      m_canDraw = false;
  bool                                      m_prepared = false;
  // Currently for stellations only
  size_t actionCount = 1;

  Panel(GraphicsResource* pGraphics);
  auto addEyeAzimuthInclination(double deltaAzimuth, double deltaInclination) -> void;
  auto buildWindowTitle(MorphType morphType) const -> std::string;
  inline auto buildWindowTitle() const -> std::string {
    return buildWindowTitle(m_morph);
  }
  inline auto canRecompute() const -> bool {
    if (m_computeResource) {
      return (m_computeResource->m_status == ComputeState::INITIAL || m_computeResource->m_status == ComputeState::FINISHED);
    } else
      return false;
  }
  auto clearPanel() -> void;
  // Submit the recorded commands for a compute image
  auto computeImage() -> bool;
  // Duplicate panel <panel>
  auto copyPanel(const Panel& panel, bool right) -> bool;
  auto copyUniformBuffer(uint32_t iImage) -> bool;
  auto createPipelines() -> bool;
  auto createPipelineBuffers() -> bool;
  inline void disableRotation() {
    m_enableRotation = false;
  }
  void draw(const SwapchainImageResource& imageResource, size_t iImage);
  void fillMeshPipelineBuffers();
  auto fillSolidPipelineBuffers(bool changedObject, bool drawLines, bool showPoints, bool useTexture, MorphType morphType) -> bool;
  inline void fillSolidPipelineBuffers() {
    fillSolidPipelineBuffers(true, m_drawLines, m_showPoints, m_useTexture, m_morph);
  }
  inline void fillSolidFillTexturePipelineBuffer() {
    assert(m_solidFillTexturePipeline);
    assert(m_solid);
    m_solidFillTexturePipeline->fillBuffers(m_solid);
  }
  auto freeTextures() -> void;
  auto getConfig(pt::ptree parent, const char *tag, bool isDefault) -> bool;
  inline auto getDrawLines() const -> bool {
    return m_drawLines;
  }
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  inline auto getModel() const -> glm::mat4 {
    if (m_enableRotation)
      return glm::rotate(glm::dmat4(1.0), m_angleOffset + m_duration * M_PI_2 * m_rotationSpeed, m_rotationAxis);
    else
      return glm::mat4(1.0f);
  }
  inline auto getProjection() const -> glm::mat4 {
    glm::mat4 proj = glm::perspective(M_PIf32 / 3.0f, static_cast<float>(m_viewport.width) / static_cast<float>(m_viewport.height), 0.1f, 100.0f);
    // Vulkan clip space has inverted Y and half Z.
    static glm::mat4 clip(1.0f,  0.0f, 0.0f, 0.0f,
                          0.0f,  1.0f, 0.0f, 0.0f,
                          0.0f,  0.0f, 0.5f, 0.0f,
                          0.0f,  0.0f, 0.5f, 1.0f);
    if (m_flipY)
      clip[1][1] = -1.0f;
    return clip * proj;
  }
  inline auto hasError() const -> bool {
    return !m_errorMessage.empty();
  }
  inline auto hasMesh() const -> bool {
    return (m_meshPipeline || m_meshTexturePipeline || m_meshMaterialPipeline || m_meshTexSkinPipeline);
  }
  inline auto getEye() const -> glm::dvec3 {
    return m_eye;
  }
  inline auto getFractalType() const -> boost::optional<FractalType> {
    return m_fractalType;
  }
  inline auto getMorphType() const -> MorphType {
    return m_morph;
  }
  inline auto getShowPoints() const -> bool {
    return m_showPoints;
  }
  inline auto getUseTexture() const -> bool {
    return m_useTexture;
  }
  inline auto getNextObjectType() const -> ObjectType {
    return m_nextObject;
  }
  inline auto getSeedOscillation() {
    if (m_type == PanelType::IMAGE_COMP)
      return m_computeResource->getSeedOscillation();
    else
      return false;
  }
  inline void initUBO () {
    if (m_type == PanelType::SOLID)
      m_axisPipeline->setMVP(buildObjectVP());
  }
  void inwardEye();
  void outwardEye();
  auto prepareCompute(FractalType newFractalType, const ComputeSetup &aSetup) -> bool;
  void prepareDrawingSolid();
  void putConfig(pt::ptree &parent, const char *tag);
  void releasePipelines();
  void resetSolid();
  auto resetTextureResource() -> bool;
  inline void saveRenderedImage(const std::string& fileName) const {
    assert(m_computeResource);
    m_computeResource->saveRenderedImage(fileName);
  }
  void setColorAlpha();
  void setColorAmbient();
  inline void setDrawLines(bool aValue) {
    m_drawLines = aValue;
  }
  void setDuration();
  inline void setEyeDistance(double aValue) {
    m_eyeDistance = aValue;
    m_viewChanged = true;
  }
  void setMorphFraction(const glm::vec3& lightDirection, double cosine);
  inline void setMorphType(MorphType aValue) {
    m_morph = aValue;
  }
  inline void setNextObjectType(ObjectType aValue) {
    m_nextObject = aValue;
  }
  void setRotationAxisAzimuth(double aValue, bool updateAxis);
  void setRotationAxisInclination(double aValue, bool updateAxis);
  inline bool setShowPoints(bool aValue) {
    if (m_showPoints != aValue) {
      m_showPoints = aValue;
      return true;
    } else
      return false;
  }
  inline void setTransform(PolyhedronTransformation aTransformation, size_t count) {
    m_transform = aTransformation;
    actionCount = count;
  }
  bool setTypeImage();
  auto inline setXYoffsets(double deltaX, double deltaY) -> ComputeState {
    assert(m_type == PanelType::IMAGE_COMP);
    assert(m_computeResource);
    return m_computeResource->setXYoffsets(deltaX, deltaY);
  }
  auto showComputeSetup() -> bool;
  void showControls();
  void showError();
  void showInfo();
  void showSolidActions(PanelSettings& setting);
  static bool sliderAngle(const char* label, double* angleRadian, double v_degrees_min, double v_degrees_max);
  void toggleRotate();
  void updateColor(const std::vector<glm::vec3> &newColorList);
  auto updateComputeImage() -> bool;
  inline void updateComputeTexture() {
    assert(m_computeResource->m_status == ComputeState::FINISHED);
    updateTextureResource(new TextureResource(m_graphics, m_computeResource));
  }
  auto updateGltfModel() -> void;
  auto updateSolid(ObjectType aType, const std::vector<glm::vec3>& aColorList) -> void;
  auto updateTextureResource(const char *filename) -> bool;
  bool updateTextureResource();
  void updateTextureUniformBuffers();
  inline void updateImageUniformBuffers() {
    assert(m_imagePipeline);
    m_imagePipeline->updateUniformBuffers(m_textureResource->m_imageView);
  }
  void updateViewPort(uint32_t x, uint32_t width, uint32_t height);
  inline auto updatingTexture() -> bool {
    return (m_textureResourceOld.get() != nullptr);
  }
  inline auto zoomIn() const -> ComputeState {
    assert(m_type == PanelType::IMAGE_COMP);
    assert(m_computeResource);
    return m_computeResource->zoomIn();
  }
  inline auto zoomOut() const -> ComputeState {
    assert(m_type == PanelType::IMAGE_COMP);
    assert(m_computeResource);
    return m_computeResource->zoomOut();
  }

private:
  GraphicsResource *m_graphics;
  std::string       m_errorMessage;
  bool              m_enableRotation = false;
  bool              m_flipY = true;
  bool              m_drawLines = false;
  MorphType         m_morph = MorphType::NONE;
  bool              m_showPoints = false;
  bool              m_useTexture = false;
  ShaderType        m_shader = ShaderType::NONE;
  float             m_colorAlpha = 0.7f, m_colorAmbient = 0.2f, m_metallic = 1.0f;
  double            m_eyeAzimuth = 0.0, m_eyeInclination = 1.0, m_eyeDistance = 5.0;
  bool              m_viewChanged = false;
  glm::dvec3        m_eye;
  /* Azimuth and inclination in radians */
  double            m_lightInclination = -1.5, m_lightAzimuth = 0.0;
  double            m_rotationAxisAzimuth = 0.0, m_rotationAxisInclination = 0.0;
  glm::dvec3        m_rotationAxis;
  double            m_rotationSpeed = 1.0;
  double            m_morphSpeed = 1.0;
  double                                     m_angleOffset = 0.0, m_morphOffset = 0.0;
  std::chrono::_V2::system_clock::time_point m_startTime;
  double                                     m_duration;
  ObjectType                                 m_nextObject = ObjectType::NONE;
  PolyhedronTransformation                   m_transform = PolyhedronTransformation::NONE;
  // Is the object rotating?
  bool                                       m_rotate = false;
  // Does the glTF model have materials?
  bool                                       m_meshUsesMaterial = false;
  // Does the glTF model have textures?
  bool                                       m_meshUsesTexture = false;
  // Does the glTF model have skins?
  bool                                       m_meshUsesSkin = false;
  // Axis pipeline
  std::unique_ptr<AxisPipeline>              m_axisPipeline;
  // Image pipeline
  std::unique_ptr<ImagePipeline>             m_imagePipeline;
  // Morph pipeline
  std::unique_ptr<MorphPipeline>             m_morphPipeline;
  // Solid points pipeline
  std::unique_ptr<SolidPointPipeline>        m_solidPointPipeline;
  // Mesh pipeline
  std::unique_ptr<MeshPipeline>              m_meshPipeline;
  std::unique_ptr<MeshMaterialPipeline>      m_meshMaterialPipeline;
  std::unique_ptr<MeshTexturePipeline>       m_meshTexturePipeline;
  std::unique_ptr<MeshTexSkinPipeline>       m_meshTexSkinPipeline;

  std::unique_ptr<TextureResource>           m_textureResourceOld;
  std::unique_ptr<TextureResource>           m_textureResource;
  std::unique_ptr<ComputeResource>           m_computeResource;
  boost::optional<FractalType>               m_fractalType;
  boost::optional<ComputeSetup>              m_juliaSetup, m_mandelbrotSetup;

  auto buildObjectMVP() const -> glm::mat4;
  auto buildObjectVP() const -> glm::mat4;
  auto copyPipelineUBO(size_t iImage) -> bool;
  auto createMorphPipeline(MorphType morphType) -> bool;
  inline auto createMorphPipeline() -> void {
    createMorphPipeline(m_morph);
  }
  auto createObjectPipeline() -> bool;
  auto createSolidFillPipeline() -> bool;
  auto createSolidLinePipeline() -> bool;
  auto createSolidPointPipeline() -> bool;
  inline auto fillSolidFillPipelineBuffer() -> void {
    assert(m_solidFillPipeline);
    assert(m_solid);
    m_solidFillPipeline->fillBuffers(m_solid);
  }
  void getJuliaConfig(boost::optional<pt::ptree&> root);
  void getMandelbrotConfig(boost::optional<pt::ptree&> root);
  inline auto getMorphTime() const -> double {
    return m_morphOffset + m_duration * M_PI * m_morphSpeed;
  }
  auto prepareCompute() -> bool;
  auto prepareCompute(const ComputeSetup& setup) -> bool;
  auto prepareImage() -> bool;
  auto putFractalConfig(boost::optional<FractalType> optFractalType, FractalType setupType, const ComputeSetup& setup) const -> pt::ptree;
  inline void resetSolid(Polyhedron* solid) {
    m_solid.reset(solid);
    m_nextObject = ObjectType::NONE;
    m_transform = PolyhedronTransformation::NONE;
  }
  void resetTime();
  void setEye();
  void setMetallic();
  void setRotationAxis();
  void sliderColorAlpha();
  void sliderColorAmbient();
  auto sliderFractalPower() -> bool;
  void sliderLightDirection();
  auto sliderMetallic() -> void;
  void sliderMorphSpeed();
  void sliderRotation();
  bool updateMorphPipeline(MorphType aType);
  auto updateMVP() -> void;
  auto updateTextureResource(TextureResource *resource) -> void;
  auto updateUniformBuffer(bool full) -> void;
};

struct PanelSettings {
  Panel    *panel = nullptr;
  bool      drawLines = false;
  bool      showPoints = false;
  bool      useTexture = false;
  MorphType morphType = MorphType::NONE;
  PolyhedronTransformation transform = PolyhedronTransformation::NONE;
  // Currently for stellations only
  size_t                   actionCount = 1;
  bool   changedDrawLines = false;
  bool   changedShowpoints = false;
  bool   changedUseTexture = false;
  bool   changedMorph = false;
};

