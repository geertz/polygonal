#pragma once

#include <tiny_gltf.h>
#include <glm/glm.hpp>
#include "textureresource.h"
#include "graphicspipelineresource.h"

/*
    Animation related structures
  */

struct AnimationSampler {
  std::string            interpolation;
  std::vector<float>     inputs;
  std::vector<glm::vec4> outputsVec4;
};

struct AnimationChannel {
  std::string path;
  GltfNode*   node;
  uint32_t    samplerIndex;
};

struct Animation {
  std::string                   name;
  std::vector<AnimationSampler> samplers;
  std::vector<AnimationChannel> channels;
  float                         start       = std::numeric_limits<float>::max();
  float                         end         = std::numeric_limits<float>::min();
  float                         currentTime = 0.0f;
};

class GltfModel {
public:
  std::vector<std::unique_ptr<GltfNode>> m_nodes;
  glm::mat4x4                            m_transform = glm::mat4x4(1.0f);

  GltfModel(const std::string& fileName);
  auto getFirstVertex(glm::vec3& position, glm::vec3& normal) -> bool;
  auto getFirstVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv) -> bool;
  auto getFirstVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv, glm::vec4& jointIndices, glm::vec4& jointWeights) -> bool;
  inline auto getName() const -> std::string {
    return m_name;
  }
  auto getNextVertex(glm::vec3& position, glm::vec3& normal) -> bool;
  auto getNextVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv) -> bool;
  auto getNextVertex(glm::vec3& position, glm::vec3& normal, glm::vec2& uv, glm::vec4& jointIndices, glm::vec4& jointWeights) -> bool;
  inline auto hasMaterials() const -> bool {
    return !m_model.materials.empty();
  }
  inline auto hasSkins() const -> bool {
    return !m_model.skins.empty();
  }
  inline auto hasTextures() const -> bool {
    return !m_model.textures.empty();
  }
  void loadAnimations(std::vector<Animation>& animations);
  auto loadMeshes(std::vector<Mesh>& meshes, std::vector<uint16_t>& indices, std::vector<Material>& materials, std::vector<Texture>& textures) -> void;
  void loadNodes();
  void loadSkins(GraphicsResource* pGraphics, std::vector<Mesh>& meshes, std::vector<Skin>& skins);
  inline void makeTextureResource(GraphicsResource* pGraphics, size_t anIndex, std::unique_ptr<TextureResource>& resource) {
    resource = std::make_unique<TextureResource>(pGraphics, m_model.images.at(anIndex), false);
  }
  void showInfo();
private:
  size_t                 m_currentPos;
  std::string            m_name;
  tinygltf::Model        m_model;
  std::vector<glm::vec3> m_vertexPositions;
  std::vector<glm::vec3> m_vertexNormals;
  std::vector<glm::vec2> m_vertexUVs;
  std::vector<glm::vec4> m_vertexJointIndices;
  std::vector<glm::vec4> m_vertexJointWeights;

  void loadNode(const tinygltf::Node& inputNode, GltfNode* parent, uint32_t nodeIndex);
  GltfNode* nodeFromIndex(uint32_t index);
  void readIndices(const tinygltf::Primitive& glTFPrimitive, uint32_t vertexStart, uint32_t& indexCount, std::vector<uint16_t>& indices);
  void readMaterials(std::vector<Material>& materials);
  void readTextures(std::vector<Texture>& textures);
  void readVertices(const tinygltf::Primitive& glTFPrimitive);
};
