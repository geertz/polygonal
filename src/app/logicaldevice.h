#pragma once

#include <vulkan/vulkan.hpp>
#include "physicaldevice.h"
#include <vector>
#include "common.h"

class LogicalDevice {
public:
  PhysicalDevice  *m_physicalDevice = nullptr;
  std::string      m_errorMessage;

  LogicalDevice(PhysicalDevice *aPhysicalDevice, vk::DeviceCreateInfo &info);

  auto createCommandPool(uint32_t queueFamilyIndex) const -> vk::UniqueCommandPool;
  auto createDescriptorPool(const std::vector<vk::DescriptorPoolSize>& poolSizes) -> vk::UniqueDescriptorPool;
  auto createFence(vk::FenceCreateFlagBits flags) const -> vk::UniqueFence;
  auto createFrameBuffer(vk::FramebufferCreateInfo& info, const vk::RenderPass& renderPass, vk::ImageView* pAttachments, uint32_t count) const -> vk::UniqueFramebuffer;
  inline auto createRenderPass(vk::RenderPassCreateInfo& info) -> vk::UniqueRenderPass {
    return m_device->createRenderPassUnique(info);
  }
  auto createPipelineLayout(const std::vector<vk::DescriptorSetLayout>& layouts, vk::PushConstantRange* pPushConstantRange = nullptr) const -> vk::UniquePipelineLayout;
  auto createSampler(vk::SamplerAddressMode mode) const -> vk::UniqueSampler;
  inline vk::UniqueSampler createSampler() {
    return createSampler(vk::SamplerAddressMode::eRepeat);
  }
  auto createSemaphore() const -> vk::UniqueSemaphore;
  ErrorValue<vk::UniqueShaderModule> createShaderModule(const std::string& aFileName);
  inline auto createSwapchain(const vk::SwapchainCreateInfoKHR& info) -> vk::SwapchainKHR {
    return m_device->createSwapchainKHR(info);
  }
  inline auto destroySwapchain(vk::SwapchainKHR& swapChain) -> void {
    m_device->destroySwapchainKHR(swapChain);
  }
  inline auto findMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties) -> uint32_t {
     const auto memType = m_physicalDevice->findMemoryType(typeFilter, properties);
     if (memType == UINT32_MAX) {
       m_errorMessage = m_physicalDevice->m_errorMessage;
     }
     return memType;
  }
  inline auto getComputeFamily() const -> uint32_t {
    return m_physicalDevice->getComputeFamily();
  }
  inline auto getComputeQueue() const -> vk::Queue {
    return m_device->getQueue(m_physicalDevice->getComputeFamily(), 0);
  }
  inline auto getDevice() const -> vk::Device {
    return m_device.get();
  }
  inline auto getGraphicsFamily() const -> uint32_t {
    return m_physicalDevice->getGraphicsFamily();
  }
  inline auto getGraphicsQueue() const -> vk::Queue {
    return m_device->getQueue(getGraphicsFamily(), 0);
  }
  inline auto getMsaaSamples() const -> vk::SampleCountFlagBits {
    return m_physicalDevice->m_msaaSamples;
  }
  inline auto getPresentQueue() const -> vk::Queue {
    return m_device->getQueue(m_physicalDevice->getPresentFamily(), 0);
  }
  inline auto getSwapchainImages(vk::SwapchainKHR& swapChain) -> std::vector<vk::Image> {
    return m_device->getSwapchainImagesKHR(swapChain);
  }
  inline auto resetFence(const vk::Fence& fence) -> void {
    m_device->resetFences({fence});
  }
  inline auto waitForFence(const vk::Fence& fence) -> vk::Result {
    return m_device->waitForFences({fence}, VK_TRUE, UINT64_MAX);
  }
  inline auto waitIdle() const -> void {
    m_device->waitIdle();
  }
private:
  vk::UniqueDevice m_device;
};
