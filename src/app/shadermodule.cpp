#include "shadermodule.h"

ShaderModule::ShaderModule(const vk::Device &aDevice) {
  m_device = aDevice;
}

auto ShaderModule::create(const std::vector<char>& code) -> bool {
  assert(code.size() > 0);
  vk::ShaderModuleCreateInfo createInfo{};
  createInfo.codeSize = code.size();
  createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());
  try {
    m_shaderModule = m_device.createShaderModuleUnique(createInfo);
    return true;
  } catch (...) {
    return false;
  }
}
