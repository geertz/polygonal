#pragma once

#include <vector>
#include <glm/glm.hpp>
#include "pipelinebase.h"
#include "polyhedron.h"
#include <vulkan/vulkan.hpp>

class SolidPointPipeline : public PipelineBase {
  struct UniformBufferObject {
    glm::mat4 mvp;
    glm::vec3 color;
    float     alpha;
    float     pointSize;
  };
public:
  explicit SolidPointPipeline(GraphicsResource* pGraphics);
  auto copyUBO(uint32_t iImage) -> bool;
  inline void updateVertexBuffer() {
    assert(!m_vertices.empty());
    m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  }
  inline void draw(const SwapchainImageResource& imageResource, size_t iImage) {
    assert(!m_vertices.empty());
    bindCommandBuffer(imageResource, iImage);
    imageResource.m_stdCommandBuffer.draw(static_cast<uint32_t>(m_vertices.size()), 1, 0, 0);
  }
  void fillBuffers(const std::unique_ptr<Polyhedron>& solid);
  inline void setMVP(const glm::mat4x4& aValue) {
    m_ubo.mvp = aValue;
  }
private:
  UniformBufferObject    m_ubo;
  std::vector<glm::vec3> m_vertices{};

  static std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions();
};
