#pragma once

#include <glm/gtx/closest_point.hpp>

#include "polyhedron.h"

class StellationFace2 {
public:
  glm::dvec3        normal;
  std::vector<Edge> edges;
};

class StellationFace : public Face {
public:
  std::vector<Line> m_lines;

  StellationFace();
  StellationFace(const Face& aFace);
  StellationFace(const glm::dvec3& aNormal);
  void appendNoPairs(ushort iVertex);
  void appendLine(const glm::dvec3& aPoint, const glm::dvec3& aDirection);
};

class StellatedPolyhedron {
  struct LineVertex {
    ushort iLine, iVertex;
  };
public:
  std::unique_ptr<Polyhedron> m_target;

  StellatedPolyhedron(const std::unique_ptr<Polyhedron>&);
  void createStellation(const std::unique_ptr<Polyhedron>&);
private:
  std::vector<glm::dvec3>     m_stellationVertices;
  std::vector<StellationFace> m_stellationFaces;

  void createNextStellation(const Polyhedron* source, std::unique_ptr<Polyhedron>& target);
  void createNextStellation2(const std::unique_ptr<Polyhedron>& source, std::unique_ptr<Polyhedron>& target);
  void calculateStellationDiagram();
  auto fillLines(const StellationFace& face, ushort iVertex1, ushort iVertex2) -> std::vector<StellatedPolyhedron::LineVertex>;
  void getNewVertexUsingLines(const StellationFace& face, const std::vector<LineVertex>& lines, ushort iElem1, ushort iDelta, short& iNewVertex, glm::dvec3& newVertex);
};

