#include "solidfillpipeline.h"
#include <glm/gtc/matrix_transform.hpp>

SolidFillPipeline::SolidFillPipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient) : SolidFillBasePipeline(pGraphics, "shaders/vert_solid_fill.spv", "shaders/frag_solid_fill.spv", "Solid fill pipeline") {
  if (!m_pipelineResource || hasError())
    return;
  // Create the descriptor layout
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  // Create the pipeline
  auto attributeDescriptions = getAttributeDescriptions();
  auto bindingDescription    = PipelineBase::getBindingDescription(sizeof(Vertex));
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  m_pipelineResource->createPipeline(vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vertexInputInfo);
  setColorAlpha(anAlpha);
  setColorAmbient(anAmbient);
  if (!m_pipelineResource->createUniformBuffers(bindings, m_meshes))
    setResourceError();
}

void SolidFillPipeline::appendVertex(uint16_t iVertex, const Vertex& row) {
  m_vertices.push_back(row);
  m_listOfIndices.push_back({iVertex});
}

void SolidFillPipeline::clearAll() {
  m_indices.clear();
  m_vertices.clear();
  m_listOfIndices.clear();
  m_meshes.clear();
}

void SolidFillPipeline::draw(const SwapchainImageResource& imageResource, size_t iImage) {
  assert(m_pipelineResource);
  m_pipelineResource->setCurrentBuffer(0);
  bindCommandBuffer(imageResource);
  m_pipelineResource->bindDescriptorSets(imageResource, iImage);
  drawIndexed(imageResource, m_meshes);
}

auto SolidFillPipeline::updateVertexIndexBuffers() -> void {
  assert(!m_vertices.empty());
  assert(!m_indices.empty());
  m_pipelineResource->updateVertexBuffer(0, sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()));
  m_pipelineResource->createIndexBuffer(0, sizeof(m_indices[0]), m_indices.size(), reinterpret_cast<const void *>(m_indices.data()));
  Mesh mesh("");
  DrawPrimitive primitive(m_indices.size());
  mesh.primitives.push_back(primitive);
  m_meshes.push_back(mesh);
}

auto SolidFillPipeline::fillBuffers(const std::unique_ptr<Polyhedron>& solid) -> void {
  if (!m_vertices.empty() && !m_indices.empty() && m_solidType == solid->getType() && m_isDualCompound == solid->getIsDualCompound()) {
    return;
  }
  clearAll();
  uint8_t iFace = 0;
  uint16_t pos = 0;
  const auto colorList = solid->getColorList();
  const auto nColors = colorList.size();
  assert(solid->getNumberOfFaces() > 0);
  for (const auto& face : solid->getFaces()) {
    const auto normal = face.normal;
    auto indices = face.copyIndices();
    std::vector<glm::dvec3> faceVertices;
    for (const auto iVertex : face) {
      faceVertices.push_back(solid->getVertex(iVertex));
    }
    auto offset = pos;
    const auto faceColor = colorList[face.getColorIndex() % nColors];
    for (const auto iVertex : indices) {
      Vertex vi{};
      vi.position = solid->getVertex(iVertex);
      vi.normal = normal;
      if (nColors > 1)
        vi.color = faceColor;
      else {
        if ((iFace % 2) == 0)
          vi.color = {1.0f, 0.0f, 0.0f};
        else
          vi.color = {0.0f, 0.0f, 1.0f};
      }
      appendVertex(iVertex, vi);
      pos++;
    }
    makeTriangles(solid, face, indices, offset);
    m_listOfIndices.clear();
    iFace++;
  }
  m_solidType = solid->getType();
  m_isDualCompound = solid->getIsDualCompound();
}

std::vector<vk::VertexInputAttributeDescription> SolidFillPipeline::getAttributeDescriptions() {
  std::vector<vk::VertexInputAttributeDescription> attributeDescriptions = {
    {0, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, position)},
    {1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, normal)},
    {2, 0, vk::Format::eR32G32B32Sfloat, offsetof(Vertex, color)}
  };
  return attributeDescriptions;
}

// Update the colors of the shader vertices
auto SolidFillPipeline::updateColor(const std::vector<glm::vec3>& oldColorList, const std::vector<glm::vec3>& newColorList) -> void {
  for (auto& vertex : m_vertices) {
    for (size_t i = 0; i < oldColorList.size(); i++) {
      if (vertex.color == oldColorList.at(i))
        vertex.color = newColorList.at(i);
    }
  }
}
