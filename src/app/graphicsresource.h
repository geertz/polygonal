#pragma once

#include "logicaldevice.h"
#include "bufferresource.h"
#include "swapchainimageresource.h"
#include "queuethread.h"

#include <string>
#include <thread>

const int MAX_FRAMES_IN_FLIGHT = 3;

class FrameResource {
public:
  FrameResource(const LogicalDevice *aDevice);
  FrameResource(const FrameResource&) = delete; // No copy needed

  auto getInFlightFence() const -> vk::Fence;
  auto getImageAvailableSemaphore() const -> vk::Semaphore;
  auto getRenderFinishedSemaphore() const -> vk::Semaphore;
private:
  vk::UniqueFence     m_inFlightFence;
  vk::UniqueSemaphore m_imageAvailableSemaphore;
  vk::UniqueSemaphore m_renderFinishedSemaphore;
};
struct SwapChainSupportDetails2 {
  std::vector<VkSurfaceFormatKHR> formats;
#ifdef PRESENT_MODE_MAILBOX
  std::vector<VkPresentModeKHR> presentModes;
#endif
  VkSurfaceCapabilitiesKHR capabilities;
};

class SwapchainResource {
  friend class GraphicsResource;
public:
  std::string                         m_errorMessage;
  std::vector<SwapchainImageResource> m_swapChainImageResources{};

  SwapchainResource(LogicalDevice *device, vk::SurfaceKHR surface, vk::SwapchainKHR oldOne, uint32_t width, uint32_t height);
  ~SwapchainResource();

  inline auto getExtent() const -> vk::Extent2D {
    return m_swapChainExtent;
  }
  inline auto getImageCount() const -> uint32_t {
    return m_imageCount;
  }
  inline auto getImageResource(size_t index) const -> const SwapchainImageResource& {
    return m_swapChainImageResources.at(index);
  }
  auto redrawNeeded() -> void;
  auto updateImages() -> bool;
private:
  LogicalDevice             *m_device = nullptr;
  vk::Extent2D               m_swapChainExtent;
  vk::Format                 m_swapChainImageFormat;
  vk::SwapchainKHR           m_swapChain;
  uint32_t                   m_imageCount;

  static auto chooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR> &availableFormats) -> vk::SurfaceFormatKHR;
};

class GraphicsResource {
public:
  vk::UniqueSurfaceKHR                m_surface;
  vk::Queue                           m_graphicsQueue;
  std::unique_ptr<SwapchainResource>  m_swapChainResource;
  vk::UniqueRenderPass                m_stdRenderPass;
  vk::UniqueRenderPass                m_imGuiRenderPass;
  std::unique_ptr<ImageResource>      m_colorImageResource;
  std::unique_ptr<ImageResource>      m_depthImageResource;
  vk::UniqueCommandPool               m_commandPool;
  std::unique_ptr<QueueThread>        m_graphicsQueue2;

  GraphicsResource();
  ~GraphicsResource();
  GraphicsResource(const GraphicsResource&) = delete; // No copy needed

  auto acquireNextImage(uint32_t* pImageIndex) -> vk::Result;
  auto cleanupSwapChain(bool recreate, bool sameSize) -> void;
  auto copyBuffer(const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, vk::DeviceSize size) -> bool;
  auto copyBufferToImage(const vk::UniqueBuffer& buffer, vk::Image& image, const vk::Extent2D extent) -> bool;
  inline auto createBuffer(vk::DeviceSize bufferSize, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, BufferResource* resource) -> bool {
    return createBuffer(bufferSize, 1, usage, properties, resource);
  }
  auto createBuffer(vk::DeviceSize size, std::size_t nElements, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, BufferResource* resource) -> bool;
  auto createColorResource() -> bool;
  auto createCommandBuffers() -> bool;
  auto createDepthResource() -> bool;
  auto createDescriptorPool(uint32_t imageCount, uint32_t textureCount, uint32_t storageBufferCount) -> vk::UniqueDescriptorPool;
  auto createDescriptorPoolCIS() -> vk::UniqueDescriptorPool;
  auto createGraphicsDescriptorPool(uint32_t textureCount) -> vk::UniqueDescriptorPool;
  auto createFramebuffers() -> bool;
  auto createImGuiRenderPass() -> void;
  inline auto createPipelineLayout(const std::vector<vk::DescriptorSetLayout>& layouts, vk::PushConstantRange* pPushConstantRange = nullptr) -> vk::UniquePipelineLayout {
    return m_device->createPipelineLayout(layouts, pPushConstantRange);
  }
  auto createStdCommandPool() -> bool;
  auto createStdRenderPass() -> void;
  auto createSwapChain(uint32_t width, uint32_t height) -> bool;
  auto createSyncObjects() -> bool;
  auto drawFrame(uint32_t imageIndex) -> vk::Result;
  auto findDepthFormat() -> vk::Format;
  inline auto getBackgroundColor() const -> vk::ClearColorValue {
    return m_backgroundColor;
  }
  inline auto getDevice() const -> LogicalDevice* {
    return m_device;
  }
  inline auto getErrorMessage() -> std::string {
    return std::move(m_errorMessage);
  }
  inline auto getExtent2D() const -> vk::Extent2D {
    return m_swapChainResource->m_swapChainExtent;
  }
  inline auto getMsaaSamples() const -> vk::SampleCountFlagBits {
    return m_device->m_physicalDevice->m_msaaSamples;
  }
  auto resetStdCommandBuffers() -> bool;
  auto setBackgroundColor(const vk::ClearColorValue aColor) -> void;
  inline auto setDevice(LogicalDevice* aDevice) -> void {
    m_device = aDevice;
  }
  auto setQueues() -> void;
  inline auto updateCurrentFrame() -> void {
    m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
  }
private:
  LogicalDevice                              *m_device = nullptr;
  std::string                                 m_errorMessage;
  vk::Queue                                   m_presentQueue;
  vk::ClearColorValue                         m_backgroundColor = vk::ClearColorValue(std::array<float, 4>({{0.0f, 0.0f, 0.0f, 0.0f}}));
  std::vector<std::unique_ptr<FrameResource>> m_frameResources;
  size_t                                      m_currentFrame = 0;
  std::mutex                                  m_submit_mutex;
};
