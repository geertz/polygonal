#pragma once

#include "morphbase.h"

class MorphRegular : public MorphBase {
public:
  MorphRegular(const std::unique_ptr<Polyhedron>&);
};
