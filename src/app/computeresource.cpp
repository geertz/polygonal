#include "computeresource.h"
#include "lodepng.h"
#include "vulkanlib.h"
#include "shadermodule.h"

ComputeResource::ComputeResource(LogicalDevice* aDevice, uint32_t width, uint32_t height, FractalType type, const ComputeSetup& setup) {
  m_device = aDevice;
  if (type == FractalType::JULIA)
    m_push.fractal_type = 1;
  else
    m_push.fractal_type = 0;
  m_push.juliaPower = setup.fractalPower;
  m_push.width = width;
  m_push.height = height;
  m_push.xOffset = setup.xOffset;
  m_push.yOffset = setup.yOffset;
  m_push.zoom = setup.zoom;
  m_push.colorA = setup.colorA;
  m_push.colorB = setup.colorB;
  m_seedOscillate = false;
  m_juliaSeed1 = setup.juliaSeed1;
  m_juliaSeed2 = setup.juliaSeed2;
  m_status = ComputeState::INITIAL;
}

auto ComputeResource::changeSize(const std::unique_ptr<QueueThread>& queue, uint32_t width, uint32_t height, double duration) -> bool {
  const auto minWH_new = fmin(width, height);
  const auto minWH_old = fmin(m_push.width, m_push.height);
  m_push.xOffset *=  minWH_new * m_push.width / (minWH_old * width);
  m_push.width = width;
  m_push.height = height;
  m_bufferResource.reset(nullptr);
  m_descriptorSet.reset(nullptr);
  const auto bufferSize = m_pixelSize * width * height;
  if (!createBuffer(bufferSize))
    return false;
  if (!createDescriptorSet(bufferSize))
    return false;
  return submit(queue, duration);
}
auto ComputeResource::createBuffer(vk::DeviceSize bufferSize) -> bool {
  /*
    We will now create a buffer. We will render the mandelbrot set into this buffer
    in a computer shader later.
    */

  vk::BufferCreateInfo bufferCreateInfo{};
  bufferCreateInfo.size = bufferSize; // buffer size in bytes.
  bufferCreateInfo.usage = vk::BufferUsageFlagBits::eStorageBuffer; // buffer is used as a storage buffer.
  bufferCreateInfo.sharingMode = vk::SharingMode::eExclusive; // buffer is exclusive to a single queue family at a time.

  m_bufferResource = std::make_unique<BufferResource>(m_device->getDevice());
  try {
    m_bufferResource->createBuffer(bufferCreateInfo);
  } catch (...) {
    delete m_bufferResource.release();
    m_errorMessage = "Failed to create compute buffer!";
    return false;
  }

  /*
    But the buffer doesn't allocate memory for itself, so we must do that manually.
    */

  /*
    First, we find the memory requirements for the buffer.
    */
  auto memoryRequirements = m_bufferResource->getBufferMemoryRequirements();

  /*
    Now use obtained memory requirements info to allocate the memory for the buffer.
    */
  vk::MemoryAllocateInfo allocateInfo{};
  allocateInfo.allocationSize = memoryRequirements.size; // specify required memory.
  /*
    There are several types of memory that can be allocated, and we must choose a memory type that:

    1) Satisfies the memory requirements(memoryRequirements.memoryTypeBits).
    2) Satifies our own usage requirements. We want to be able to read the buffer memory from the GPU to the CPU
       with vkMapMemory, so we set VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT.
    Also, by setting VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, memory written by the device(GPU) will be easily
    visible to the host(CPU), without having to call any extra flushing commands. So mainly for convenience, we set
    this flag.
    */
  allocateInfo.memoryTypeIndex = m_device->findMemoryType(memoryRequirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eHostCoherent | vk::MemoryPropertyFlagBits::eHostVisible);

  m_bufferResource->allocateMemory(allocateInfo);
  m_bufferResource->bindBufferMemory();
  return true;
}

auto ComputeResource::createDescriptorSetLayout() -> bool  {
  /*
    Here we specify a descriptor set layout. This allows us to bind our descriptors to
    resources in the shader.

    */

  /*
    Here we specify a binding of type VK_DESCRIPTOR_TYPE_STORAGE_BUFFER to the binding point
    0. This binds to

      layout(std140, binding = 0) buffer buf

    in the compute shader.
    */
  vk::DescriptorSetLayoutBinding layoutBinding{};
  layoutBinding.binding = 0;
  layoutBinding.descriptorType = vk::DescriptorType::eStorageBuffer;
  layoutBinding.descriptorCount = 1;
  layoutBinding.stageFlags = vk::ShaderStageFlagBits::eCompute;

  std::array<vk::DescriptorSetLayoutBinding, 1> bindings = {layoutBinding};
  vk::DescriptorSetLayoutCreateInfo layoutInfo{};
  layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
  layoutInfo.pBindings = bindings.data();

  // Create the descriptor set layout.
  try {
    m_descriptorSetLayout = m_device->getDevice().createDescriptorSetLayoutUnique(layoutInfo);
    return true;
  } catch (...) {
    m_errorMessage = "Failed to create compute descriptor set layout!";
    return false;
  }
}

auto ComputeResource::createDescriptorPool() -> bool {
  // Our descriptor pool can only allocate a single storage buffer.
  const std::vector<vk::DescriptorPoolSize> poolSize = {
    {vk::DescriptorType::eStorageBuffer, 1}
  };
  // create descriptor pool.
  try {
    m_descriptorPool = m_device->createDescriptorPool(poolSize);
    return true;
  } catch (...) {
    m_errorMessage = "failed to create compute descriptor pool";
    return false;
  }
}

auto ComputeResource::createDescriptorSet(vk::DeviceSize bufferSize) -> bool {
  /*
  With the pool allocated, we can now allocate the descriptor set.
  */
  std::vector<vk::DescriptorSetLayout> layouts(1, m_descriptorSetLayout.get());
  vk::DescriptorSetAllocateInfo allocInfo{};
  allocInfo.descriptorPool = m_descriptorPool.get(); // pool to allocate from.
  allocInfo.descriptorSetCount = 1; // allocate a single descriptor set.
  allocInfo.pSetLayouts = layouts.data();
  const auto &device = m_device->getDevice();
  // allocate descriptor set.
  try {
    auto set = device.allocateDescriptorSetsUnique(allocInfo);
    assert(!set.empty());
    m_descriptorSet = std::move(set.front());
  } catch (...) {
    m_errorMessage = "Failed to allocate compute description set!";
    return false;
  }
  /*
  Next, we need to connect our actual storage buffer with the descrptor.
  We use vkUpdateDescriptorSets() to update the descriptor set.
  */

  // Specify the buffer to bind to the descriptor.
  vk::DescriptorBufferInfo bufferInfo{};
  bufferInfo.buffer = m_bufferResource->m_buffer.get();
  bufferInfo.offset = 0;
  bufferInfo.range = bufferSize;

  vk::WriteDescriptorSet descriptorWrite{};
  descriptorWrite.dstSet = m_descriptorSet.get(); // write to this descriptor set.
  descriptorWrite.dstBinding = 0; // write to the first, and only binding.
  descriptorWrite.descriptorCount = 1; // update a single descriptor.
  descriptorWrite.descriptorType = vk::DescriptorType::eStorageBuffer; // storage buffer.
  descriptorWrite.pBufferInfo = &bufferInfo;

  // perform the update of the descriptor set.
  device.updateDescriptorSets({descriptorWrite}, {});
  return true;
}

auto ComputeResource::createPipeline(const std::string &aShaderFileName) -> bool {
  const auto shaderCode = VulkanLib::readFile(aShaderFileName);
  if (!shaderCode.error.empty()) {
    m_errorMessage = std::move(shaderCode.error);
    return false;
  }
  const auto &device = m_device->getDevice();
  auto shader = ShaderModule(device);
  if (!shader.create(shaderCode.value)) {
    m_errorMessage = "Failed to create compute shader module!";
    return false;
  }
  /*
  Now let us actually create the compute pipeline.
  A compute pipeline is very simple compared to a graphics pipeline.
  It only consists of a single stage with a compute shader.

  So first we specify the compute shader stage, and it's entry point(main).
  */
  vk::PipelineShaderStageCreateInfo shaderStageCreateInfo{};
  shaderStageCreateInfo.stage = vk::ShaderStageFlagBits::eCompute;
  shaderStageCreateInfo.module = shader.m_shaderModule.get();
  shaderStageCreateInfo.pName = "main";

  /*
  The pipeline layout allows the pipeline to access descriptor sets.
  So we just specify the descriptor set layout we created earlier.
  */
  try {
    vk::PushConstantRange pushRange;
    pushRange.offset = 0;
    pushRange.size = sizeof(PushConstants);
    pushRange.stageFlags = vk::ShaderStageFlagBits::eCompute;
    m_pipelineLayout = m_device->createPipelineLayout({m_descriptorSetLayout.get()}, &pushRange);
} catch (...) {
    m_errorMessage = "Failed to create compute pipeline";
    return false;
  }
  vk::ComputePipelineCreateInfo pipelineInfo{};
  pipelineInfo.stage = shaderStageCreateInfo;
  pipelineInfo.layout = m_pipelineLayout.get();
  /*
  Now, we finally create the compute pipeline.
  */
  try {
    auto res = device.createComputePipelineUnique(nullptr, pipelineInfo);
    if (res.result == vk::Result::eSuccess) {
      m_pipeline = std::move(res.value);
    } else {
      m_errorMessage = "Failed to create compute pipeline";
      return false;
    }
  } catch (...) {
    m_errorMessage = "Failed to create compute pipeline";
    return false;
  }
  return true;
}

auto ComputeResource::createCommandPool() -> bool {
  try {
    m_commandPool = m_device->createCommandPool(m_device->getComputeFamily());
  } catch (...) {
    m_errorMessage = "Failed to create compute command pool";
    return false;
  }
  return true;
}

auto ComputeResource::createCommandBuffer() -> bool {
  /*
  Now allocate a command buffer from the command pool.
  */
  vk::CommandBufferAllocateInfo allocInfo{};
  allocInfo.commandPool = m_commandPool.get(); // specify the command pool to allocate from.
  // if the command buffer is primary, it can be directly submitted to queues.
  // A secondary buffer has to be called from some primary command buffer, and cannot be directly
  // submitted to a queue. To keep things simple, we use a primary command buffer.
  allocInfo.level = vk::CommandBufferLevel::ePrimary;
  allocInfo.commandBufferCount = 1; // allocate a single command buffer.
  try {
    m_commandBuffer = std::move(m_device->getDevice().allocateCommandBuffers(allocInfo).front());
  } catch (...) {
    m_errorMessage = "Failed to create compute command buffer";
    return false;
  }
  return true;
}

auto ComputeResource::init() -> bool {
  m_pixelSize = sizeof(Pixel);
  assert(m_push.width > 0);
  assert(m_push.height > 0);
  auto bufferSize = m_pixelSize * m_push.width * m_push.height;
  if (!createBuffer(bufferSize))
    return false;
  if (!createDescriptorSetLayout())
    return false;

  if (!createDescriptorPool())
    return false;

  if (!createDescriptorSet(bufferSize))
    return false;
  if (!createPipeline("shaders/comp.spv"))
    return false;
  if (!createCommandPool())
    return false;

  return createCommandBuffer();
}

bool ComputeResource::recordCommandBuffer(double duration) {
  /*
  Now we shall start recording commands into the newly allocated command buffer.
  */
  vk::CommandBufferBeginInfo beginInfo{};
  beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit; // the buffer is only submitted and used once in this application.
  try {
    m_commandBuffer.begin(beginInfo);
  } catch (...) {
    m_status = ComputeState::ERROR;
    m_errorMessage = "Failed to begin recording compute buffer!";
    return false;
  }
  /*
  We need to bind a pipeline, AND a descriptor set before we dispatch.

  The validation layer will NOT give warnings if you forget these, so be very careful not to forget them.
  */
  m_commandBuffer.bindPipeline(vk::PipelineBindPoint::eCompute, m_pipeline.get());
  m_commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eCompute, m_pipelineLayout.get(), 0, 1, &m_descriptorSet.get(), 0, {});
  if (m_seedOscillate)
    setJuliaSeed(duration);
  else
    m_push.juliaSeed = m_juliaSeed1;
  m_commandBuffer.pushConstants(m_pipelineLayout.get(), vk::ShaderStageFlagBits::eCompute, 0, sizeof(PushConstants), &m_push);
  /*
  Calling vkCmdDispatch basically starts the compute pipeline, and executes the compute shader.
  The number of workgroups is specified in the arguments.
  If you are already familiar with compute shaders from OpenGL, this should be nothing new to you.
  */
  m_commandBuffer.dispatch(uint32_t(ceil(m_push.width / double(WORKGROUP_SIZE))), uint32_t(ceil(m_push.height / double(WORKGROUP_SIZE))), 1);
  m_commandBuffer.end();
  m_status = ComputeState::READY_FOR_SUBMIT;
  return true;
}

auto ComputeResource::setXYoffsets(double deltaX, double deltaY) -> ComputeState {
  m_push.xOffset -= m_push.zoom * static_cast<float>(deltaX) / m_push.width;
  m_push.yOffset -= m_push.zoom * static_cast<float>(deltaY) / m_push.height;
  return m_status;
}

auto ComputeResource::getXoffetResized(uint32_t width, uint32_t height) const -> float {
  auto minWH_new = fmin(width, height);
  auto minWH_old = fmin(m_push.width, m_push.height);
  return m_push.xOffset * minWH_new * m_push.width / (minWH_old * width);
}

auto ComputeResource::submit(const std::unique_ptr<QueueThread>& queue, double duration) -> bool {
  assert(m_status == ComputeState::INITIAL || m_status == ComputeState::FINISHED);
  m_commandBuffer.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
  if (!recordCommandBuffer(duration)) {
    m_status = ComputeState::ERROR;
    return false;
  }
  assert(m_status == ComputeState::READY_FOR_SUBMIT);
  m_status = ComputeState::BUSY;
  /*
  Now we shall finally submit the recorded command buffer to a queue.
  */

  vk::SubmitInfo submitInfo{};
  submitInfo.commandBufferCount = 1; // submit a single command buffer
  submitInfo.pCommandBuffers = &m_commandBuffer; // the command buffer to submit.

  /*
    We create a fence.
  */
  try {
    auto fence = m_device->createFence(vk::FenceCreateFlagBits(0));
    /*
    We submit the command buffer on the queue, at the same time giving a fence.
    */
    queue->submit(submitInfo, fence.get());
    /*
    The command will not have finished executing until the fence is signalled.
    So we wait here.
    We will directly after this read our buffer from the GPU,
    and we will not be sure that the command has finished executing unless we wait for the fence.
    Hence, we use a fence here.
    */
    if (m_device->waitForFence(fence.get()) != vk::Result::eSuccess) {
      m_errorMessage = "Failed to wait for a compute";
      m_status = ComputeState::ERROR;
      return false;
    }
  } catch (...) {
    m_errorMessage = "Failed to create a fence for submitting a compute!";
    m_status = ComputeState::ERROR;
    return false;
  }
  // Ready for next compute
  m_status = ComputeState::FINISHED;
  return true;
}

void ComputeResource::saveRenderedImage(const std::string& fileName) {
  auto size = m_push.width * m_push.height;
  const auto bufferSize = m_pixelSize * size;
  // Map the buffer memory, so that we can read from it on the CPU.
  auto mappedMemory = m_bufferResource->mapMemory(bufferSize);
  Pixel* pmappedMemory =  reinterpret_cast<Pixel *>(mappedMemory);

  // Get the color data from the buffer, and cast it to bytes.
  // We save the data to a vector.
  std::vector<unsigned char> image;
  image.reserve(size * 4);
  for (uint32_t i = 0; i < size; i++) {
    image.push_back(u_char(255.0f * pmappedMemory[i].r));
    image.push_back(u_char(255.0f * pmappedMemory[i].g));
    image.push_back(u_char(255.0f * pmappedMemory[i].b));
    image.push_back(u_char(255.0f * pmappedMemory[i].a));
  }
  // Done reading, so unmap.
  m_bufferResource->unmapMemory();

  // Now we save the acquired color data to a .png.
  unsigned error = lodepng::encode(fileName, image, m_push.width, m_push.height);
  if (error) printf("encoder error %d: %s", error, lodepng_error_text(error));
}
