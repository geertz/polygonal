#pragma once

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <string>
#include <vector>
#include "polyhedron.h"
#include "morphbase.h"
#include "transformsolid.h"

namespace pt = boost::property_tree;

#define NODE_BACKGROUND_COLOR "backgroundColor"
#define NODE_OBJECT_COLORS "objectColors"
#define NODE_AMBIENT "ambientLight"
#define NODE_ALPHA "alpha"
#define NODE_EYE_AZIMUTH "eye.Azimuth"
#define NODE_EYE_INCLINATION "eye.Inclination"
#define NODE_EYE_DISTANCE "eye.Distance"
#define NODE_LIGHT_AZIMUTH "light.Azimuth"
#define NODE_LIGHT_INCLINATION "light.Inclination"
#define NODE_SOLID_OBJECT_TYPE "solid.typeOfObject"
#define NODE_SOLID_MORPH_TYPE "solid.typeOfMorph"
#define NODE_SOLID_DUAL_COMPOUND "solid.dualCompound"
#define NODE_SOLID_DRAW_LINES "solid.drawLines"
#define NODE_SOLID_SHOW_POINTS "solid.showPoints"
#define NODE_SOLID_USE_TEXTURE "solid.useTexture"
#define NODE_SPEED_MORPH "speed.morph"
#define NODE_SPEED_ROTATION "speed.rotation"
#define NODE_WINDOW_WIDTH "window.width"
#define NODE_WINDOW_HEIGHT "window.height"
#define NODE_ROTATION_AZIMUTH "rotationAxis.Azimuth"
#define NODE_ROTATION_INCLINATION "rotationAxis.Inclination"
#define NODE_MESH_FILE "meshFile"
#define NODE_TEXTURE_FILE "textureFile"
#define NODE_PANEL_TYPE "panel.type"
#define NODE_PANEL_ENABLE_ROTATION "panel.enableRotation"
#define NODE_FRACTAL_TYPE "fractal.type"
#define NODE_JULIA "julia"
#define NODE_MANDELBROT "mandelbrot"
// Fractal settings
#define NODE_FRACTAL_COLOR_A "colorA"
#define NODE_FRACTAL_COLOR_B "colorB"
#define NODE_FRACTAL_POWER "power"
#define NODE_FRACTAL_SEED1 "seed1"
#define NODE_FRACTAL_SEED2 "seed2"
#define NODE_FRACTAL_XOFFSET "xOffset"
#define NODE_FRACTAL_YOFFSET "yOffset"
#define NODE_FRACTAL_ZOOM "zoom"

class Config {
public:
  explicit Config();
  explicit Config(std::string filename);
  Config(const Config&) = delete; // No copy needed
  auto fileExists() -> bool;
  static auto readBackgroundColor(pt::ptree root, vk::ClearColorValue& aColor) -> void;
  static auto readColor(pt::ptree& root, const std::string& tag) -> glm::vec3;
  static auto readFractalPower(pt::ptree& root) -> uint32_t;
  static auto readJuliaSeed1(pt::ptree& root) -> glm::vec2;
  static auto readJuliaSeed2(pt::ptree& root) -> glm::vec2;
  auto readObjectColors(pt::ptree& root) -> std::vector<glm::vec3>;
  static void readWindowExtent(pt::ptree root, int& width, int& height);
  auto readFile() const -> pt::ptree;
  static auto writeColor(pt::ptree& root, const std::string& tag, const glm::vec3& aColor) -> bool;
  pt::ptree writeDefaultColors();
  inline void writeFile(const pt::ptree& root) const {
    write_json(m_filename, root);
  }
  auto writeFile() -> bool;
  static auto writeFractalPower(pt::ptree &root, const uint32_t& aValue) -> bool;
  static auto writeJuliaSeed(pt::ptree &root, const glm::vec2& seed1, const glm::vec2& seed2) -> bool;
  auto writeObjectColors(const std::vector<glm::vec3>& colorList) -> bool;
  auto writeVulkanLibSettings(pt::ptree& root, const vk::ClearColorValue& bgColor) -> bool;
  auto writeWindowSize(int width, int height) const -> bool;
private:
  std::string m_filename;

  static char *getHomeDir();
  static void putValue(pt::ptree& root, const std::string& tag, float aValue);
};
