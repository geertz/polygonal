#pragma once

#include <glm/glm.hpp>
#include "pipelinebase.h"
#include "morphbase.h"

class MorphPipeline : public PipelineBase {
  // To be used with shader <morph.vert>
  struct Vertex {
    glm::vec3 pos1;
    glm::vec3 normal1;
    glm::vec3 color1;
    glm::vec3 pos2;
  };
  struct UniformBufferObject {
    glm::mat4 model;
    glm::mat4 projView;
    glm::vec3 lightDir;
    float     ambient;
    float     alpha;
    float     fraction;
  };
public:
  MorphPipeline(GraphicsResource* pGraphics, double anAlpha, double anAmbient);
  MorphPipeline(const MorphPipeline&) = delete; // No copy needed
  auto copyUBO(uint32_t iImage) -> bool;
  void createBuffers();
  void createIndexedVertices(const MorphBase* morphObject, size_t iMorph);
  inline MorphType getMorphType() const {
    return m_type;
  }
  inline void draw(const SwapchainImageResource& imageResource, size_t iImage) {
    assert(m_pipelineResource);
    m_pipelineResource->bindCommandBuffer(imageResource, iImage);
    drawIndexed(imageResource.m_stdCommandBuffer);
  }
  inline void setColorAlpha(double aValue) {
    m_ubo.alpha = static_cast<float>(aValue);
  }
  inline void setColorAmbient(double aValue) {
    m_ubo.ambient = static_cast<float>(aValue);
  }
  auto setFraction(double aValue) -> bool;
  inline void setLightDirection(const glm::vec3& aDirection) {
    m_ubo.lightDir = aDirection;
  }
  inline void setModel(const glm::mat4x4& aValue) {
    m_ubo.model = aValue;
  }
  inline void setMorphType(MorphType aValue) {
    m_type = aValue;
    m_pipelineResource->setCurrentBuffer(0);
  }
  inline void setProjectionView(const glm::mat4x4& aValue) {
    m_ubo.projView = aValue;
  }
  auto updateColor(const std::vector<glm::vec3>& oldColorList, const std::vector<glm::vec3>& newColorList) -> void;
private:
  MorphType                                                m_type;
  UniformBufferObject                                      m_ubo;
  std::array<std::vector<Vertex>, 2>                       m_vertices;
  std::array<std::vector<uint16_t>, 2>                     m_indices;
  std::array<std::vector<std::pair<uint16_t,uint16_t>>, 2> m_listOfIndexPairs;
  float                                                    m_fraction = 0;

  static std::array<vk::VertexInputAttributeDescription, 4> getAttributeDescriptions();
  void appendIndex(size_t iMorph, uint16_t iStart, uint16_t iEnd, uint16_t offset);
  void appendVertex(size_t iMorph, uint16_t iStart, uint16_t iEnd, const Vertex& row);
  void clearAll(size_t iMorph);
};
