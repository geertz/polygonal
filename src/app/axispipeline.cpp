#include "axispipeline.h"

#define AXIS_RED 1
#define AXIS_GREEN 2
#define AXIS_BLUE 3

AxisPipeline::AxisPipeline(GraphicsResource* pGraphics) : PipelineBase(pGraphics, "shaders/vert_axis.spv", "shaders/frag_axis.spv", "Axis pipeline") {
  if (!m_pipelineResource || hasError())
    return;
  // Create the descriptor layout
  std::vector<GraphicsPipelineResource::UniformBinding> bindings = {
    {0, vk::ShaderStageFlagBits::eVertex, sizeof(UniformBufferObject)}
  };
  m_pipelineResource->createDescriptorSetLayoutUB(bindings);
  // Create the pipeline
  vk::PipelineVertexInputStateCreateInfo vertexInputInfo{};
  vertexInputInfo.vertexBindingDescriptionCount = 1;
  auto attributeDescriptions = VertexInput::getAttributeDescriptions();
  auto bindingDescription = VertexInput::getBindingDescription();
  vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
  vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
  vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
  if (!m_pipelineResource->createPipeline(vk::PrimitiveTopology::eLineList, vk::PolygonMode::eFill, vertexInputInfo)) {
    setResourceError();
    return;
  }
  fillVertexBuffer();
  if (m_pipelineResource->createVertexBuffer(sizeof(m_vertices[0]), m_vertices.size(), reinterpret_cast<const void *>(m_vertices.data()))) {
    m_pipelineResource->createUniformBuffers(sizeof(UniformBufferObject));
  }
}

auto AxisPipeline::fillVertexBuffer() -> void {
  static glm::vec4 axisLines[] = {
  // X-axis
  {0.0f, 0.0f, 0.0f, AXIS_RED},
  {8.0f, 0.0f, 0.0f, AXIS_RED},
  // Y-axis
  {0.0f, 0.0f, 0.0f, AXIS_GREEN},
  {0.0f, 8.0f, 0.0f, AXIS_GREEN},
  // Z-axis
  {0.0f, 0.0f, 0.0f, AXIS_BLUE},
  {0.0f, 0.0f, -8.0f, AXIS_BLUE}
  };

  VertexInput vi{};
  for (const auto& point : axisLines) {
    vi.lines = point;
    m_vertices.push_back(vi);
  }
}

auto AxisPipeline::copyUBO(uint32_t iImage) -> bool {
  assert(m_pipelineResource);
  if (!m_pipelineResource->copyUBMatrices(iImage, &m_ubo, sizeof(m_ubo))) {
    setResourceError();
    return false;
  }
  return true;
}

