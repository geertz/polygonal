#version 450

layout (location = 0) in vec3 inWorldPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inColor;

layout (set = 0, binding = 0) uniform UBO {
  mat4  model;
  mat4  projView;
  vec4  eye;
  vec4  lightDir;
  float ambient;
  float alpha;
  float metallic;
  float roughness;
} ubo;

layout (location = 0) out vec4 outColor;

const float PI = 3.14159265359;
const vec3 dielectricSpecular = vec3(0.04);

// Normal Distribution function --------------------------------------
float D_GGX(float dotNH, float roughness) {
  float alpha = roughness * roughness;
  float alpha2 = alpha * alpha;
  float denom = dotNH * dotNH * (alpha2 - 1.0) + 1.0;
  return (alpha2)/(PI * denom*denom);
}

// Geometric Shadowing function --------------------------------------
float G_SchlicksmithGGX(float dotNL, float dotNV, float roughness) {
  float r = (roughness + 1.0);
  float k = (r*r) / 8.0;
  float GL = dotNL / (dotNL * (1.0 - k) + k);
  float GV = dotNV / (dotNV * (1.0 - k) + k);
  return GL * GV;
}

// Fresnel function ----------------------------------------------------
vec3 F_Schlick(float cosTheta, float metallic) {
  vec3 F0 = mix(dielectricSpecular, inColor, metallic);
  vec3 F = F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
  return F;
}
// bidirectional reflectance distribution function
// Specular BRDF composition --------------------------------------------
vec3 BRDF(vec3 L, vec3 V, vec3 N, float metallic, float roughness) {


  vec3 color = vec3(0.0);

  float dotNL = clamp(dot(N, L), 0.0, 1.0);
  if (dotNL > 0.0) {
    float rroughness = max(0.05, roughness);
    // Precalculate vectors and dot products
    vec3 H = normalize (V + L);
    float dotNV = clamp(dot(N, V), 0.0, 1.0);
    float dotLH = clamp(dot(L, H), 0.0, 1.0);
    float dotNH = clamp(dot(N, H), 0.0, 1.0);
    // D = Normal distribution (Distribution of the microfacets)
    float D = D_GGX(dotNH, roughness);
    // G = Geometric shadowing term (Microfacets shadowing)
    float G = G_SchlicksmithGGX(dotNL, dotNV, roughness);
    // F = Fresnel factor (Reflectance depending on angle of incidence)
    vec3 F = F_Schlick(dotNV, metallic);

    vec3 spec = D * F * G / (4.0 * dotNL * dotNV);

    // Light color fixed
    vec3 lightColor = vec3(1.0);
    color += spec * dotNL * lightColor;
  }
  return color;
}

void main() {
  vec3 N = normalize(inNormal);
  vec3 V = normalize(vec3(ubo.eye) - inWorldPos);
  // Specular contribution
  vec3 L = -normalize(ubo.lightDir.xyz);

  vec3 color = inColor * ubo.ambient + (1.0f - ubo.ambient) * BRDF(L, V, N, ubo.metallic, ubo.roughness);
  // Gamma correct
  color = pow(color, vec3(0.4545));

  outColor = vec4(color, ubo.alpha);
}
