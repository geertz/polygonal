#version 450

layout(set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec4  eye;
  vec4  lightDir;
  float ambient;
  float alpha;
  float metallic;
  float roughness;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec2 inUV;

layout (location = 0) out vec3 outWorldPos;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec2 outUV;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  outWorldPos = vec3(ubo.model * vec4(inPosition, 1.0));
  outNormal = mat3(ubo.model) * inNormal;
  gl_Position =  ubo.projView * vec4(outWorldPos, 1.0);
  outUV = inUV;
}
