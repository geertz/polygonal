#version 450

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;
layout (location = 3) in vec3 inColor;

layout (location = 0) out vec3 outNormal;
layout (location = 1) out vec3 outColor;
layout (location = 2) out vec2 outUV;

layout(set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  lightDir;
  float ambient;
  float alpha;
} ubo;

layout(push_constant) uniform PushConsts {
  mat4  transform;
  float roughness;
  float metallic;
  float r;
  float g;
  float b;
} object;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  vec3 outWorldPos = vec3(ubo.model * object.transform * vec4(inPosition, 1.0));
  outNormal = mat3(ubo.model) * inNormal;
  gl_Position =  ubo.projView * vec4(outWorldPos, 1.0);
  outColor = inColor;
  outUV = inUV;
}
