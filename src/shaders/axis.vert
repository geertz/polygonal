#version 450

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
} ubo;

layout(location = 0) in vec4 inData;

layout(location = 0) out vec3 fragColor;

void main(void)
{
  gl_Position = ubo.mvp * vec4(inData[0], inData[1], inData[2], 1.0);
  if (inData[3] == 1)
    fragColor = vec3(1,0,0);
  else if (inData[3] == 2)
    fragColor = vec3(0,1,0);
  else
    fragColor = vec3(0,0,1);
}
