#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 mvp;
    vec3 color;
    float alpha;
    float pointSize;
} ubo;

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec4 fragColor;

void main() {
  gl_PointSize = ubo.pointSize;
  gl_Position = ubo.mvp * vec4(inPosition, 1.0);
  fragColor = vec4(ubo.color, ubo.alpha);
}
