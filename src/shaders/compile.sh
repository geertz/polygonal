rm *.spv
glslc solid_fill.vert -o vert_solid_fill.spv
glslc solid_fill_texture.vert -o vert_solid_fill_texture.spv
glslc solid_line.vert -o vert_solid_line.spv
glslc morph.vert -o vert_morph.spv
glslc morph2.vert -o vert_morph2.spv
glslc axis.vert -o vert_axis.spv
glslc object.frag -o frag_object.spv
glslc solid_fill.frag -o frag_solid_fill.spv
glslc solid_fill_texture.frag -o frag_solid_fill_texture.spv
glslc axis.frag -o frag_axis.spv
glslc shader.comp -o comp.spv
# Image shaders
glslc image.vert -o vert_image.spv
glslc image.frag -o frag_image.spv
# Mesh shaders
glslc mesh.vert -o vert_mesh.spv
glslc mesh.frag -o frag_mesh.spv
glslc mesh2.vert -o vert_mesh2.spv
glslc mesh2.frag -o frag_mesh2.spv
glslc mesh1.vert -o vert_mesh1.spv
glslc mesh1.frag -o frag_mesh1.spv
glslc mesh_skin.vert -o vert_mesh_skin.spv
glslc mesh_skin.frag -o frag_mesh_skin.spv
