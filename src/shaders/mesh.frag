#version 450

layout (set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  lightDir;
  float ambient;
  float alpha;
} ubo;

layout (set = 1, binding = 0) uniform sampler2D texSampler;

layout (location = 0) in vec3 inNormal;
layout (location = 1) in vec3 inColor;
layout (location = 2) in vec2 inUV;

layout (location = 0) out vec4 outColor;

void main() {
  vec4 color = texture(texSampler, inUV);
  vec3 N = normalize(inNormal);
  vec3 L = normalize(-ubo.lightDir);
  vec3 diffuse = max(dot(N, L), 0.15f) * inColor;
  outColor = vec4(diffuse * color.rgb, 1.0);;
}
