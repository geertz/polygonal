#version 450

layout(binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  lightDir;
  float ambient;
  float alpha;
  float fraction;
} ubo;

layout(location = 0) in vec3 inPosition1;
layout(location = 1) in vec3 inNormal1;
layout(location = 2) in vec3 inColor1;
layout(location = 3) in vec3 inPosition2;

layout(location = 0) out vec4 outColor;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  vec3 vPos = (1.0f - ubo.fraction) * inPosition1 + ubo.fraction * inPosition2;
  vec3 worldPos = vec3(ubo.model * vec4(vPos, 1.0));
  vec3 outNormal = normalize(mat3(ubo.model) * inNormal1);
  gl_Position = ubo.projView * vec4(vPos, 1.0f);
  outColor = vec4(inColor1 * (ubo.ambient + (1.0f - ubo.ambient) * max(dot(outNormal, normalize(-ubo.lightDir)), 0.0f)), ubo.alpha);
}
