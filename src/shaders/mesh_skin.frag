#version 450

layout (set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  eye;
  float ambient;
  vec3  lightDir;
  float alpha;
} ubo;

layout (set = 2, binding = 0) uniform sampler2D texSampler;

layout (location = 0) in vec3 inWorldPos;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inColor;
layout (location = 3) in vec2 inUV;

layout (location = 0) out vec4 outColor;

void main() {
  vec4 color = texture(texSampler, inUV);
  vec3 N = normalize(inNormal);
  vec3 L = normalize(-ubo.lightDir);
  vec3 R = reflect(-L, N);
  vec3 V = normalize(ubo.eye - inWorldPos);
  vec3 diffuse = max(dot(N, L), ubo.ambient) * inColor;
  vec3 specular = pow(max(dot(R, V), 0.0), 16.0) * vec3(0.75);
  //outColor = vec4(diffuse * color.rgb, 1.0);;
  outColor = vec4(diffuse * color.rgb + specular, ubo.alpha);
}
