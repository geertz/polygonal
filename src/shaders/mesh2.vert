#version 450

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec3 inColor;

layout (location = 0) out vec4 outColor;

layout (set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  lightDir;
  float ambient;
  float alpha;
} ubo;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  vec3 outWorldPos = vec3(ubo.model * vec4(inPosition, 1.0));
  vec3 normal = mat3(ubo.model) * inNormal;
  gl_Position =  ubo.projView * vec4(outWorldPos, 1.0);
  outColor = vec4(inColor * (ubo.ambient + (1.0 - ubo.ambient) * clamp(dot(normal, ubo.lightDir), 0.0f, 1.0f)), ubo.alpha);
}
