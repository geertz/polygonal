#version 450

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;
layout (location = 3) in vec3 inColor;
layout (location = 4) in vec4 inJointIndices;
layout (location = 5) in vec4 inJointWeights;

layout (location = 0) out vec3 outWorldPos;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec3 outColor;
layout (location = 3) out vec2 outUV;
//layout (location = 3) out vec3 outViewVec;
//layout (location = 4) out vec3 outLightVec;

layout(set = 0, binding = 0) uniform UniformBufferObject {
  mat4  model;
  mat4  projView;
  vec3  eye;
  float ambient;
  vec3  lightDir;
  float alpha;
} ubo;

layout(std430, set = 1, binding = 0) readonly buffer JointMatrices {
  mat4 jointMatrices[];
};

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  // Calculate skinned matrix from weights and joint indices of the current vertex
  mat4 skinMat =
    inJointWeights.x * jointMatrices[int(inJointIndices.x)] +
    inJointWeights.y * jointMatrices[int(inJointIndices.y)] +
    inJointWeights.z * jointMatrices[int(inJointIndices.z)] +
    inJointWeights.w * jointMatrices[int(inJointIndices.w)];

  outWorldPos = vec3(ubo.model * skinMat * vec4(inPosition, 1.0));
  gl_Position = ubo.projView * ubo.model * skinMat * vec4(inPosition, 1.0);
  outNormal = normalize(transpose(inverse(mat3(ubo.model * skinMat))) * inNormal);

//  outNormal = mat3(ubo.model) * inNormal;
//  gl_Position =  ubo.projView * vec4(outWorldPos, 1.0);
  outColor = inColor;
  outUV = inUV;
}
