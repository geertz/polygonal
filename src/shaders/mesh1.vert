#version 450

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;

layout (location = 0) out vec3 outWorldPos;
layout (location = 1) out vec3 outNormal;

layout (set = 0, binding = 0) uniform UniformBufferObject {
  mat4 model;
  mat4 projView;
  vec3 eye;
} ubo;

layout(push_constant) uniform PushConsts {
  mat4  transform;
  float roughness;
  float metallic;
  float r;
  float g;
  float b;
} material;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  outWorldPos = vec3(ubo.model * material.transform * vec4(inPosition, 1.0));
  outNormal = mat3(ubo.model) * inNormal;
  gl_Position =  ubo.projView * vec4(outWorldPos, 1.0);
}
