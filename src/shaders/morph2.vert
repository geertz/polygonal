#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4  mvp;
    vec3  lightDir;
    float ambient;
    float alpha;
    float fraction;
} ubo;

layout(location = 0) in vec3 inPosition1;
layout(location = 1) in vec3 inNormal1;
layout(location = 2) in vec3 inColor1;
layout(location = 3) in vec3 inPosition2;
layout(location = 4) in vec3 inPosition3;
layout(location = 5) in vec3 inNormal3;
layout(location = 6) in vec3 inColor3;
layout(location = 7) in vec3 inPosition4;

layout(location = 0) out vec4 fragColor;

void main() {
  vec3 vPos;
  if (ubo.fraction <= 1.0f) {
    // 0 - 1
    vPos = (1.0f - ubo.fraction) * inPosition1 + ubo.fraction * inPosition2;
    vec4 outNormal1 = ubo.mvp * vec4(inNormal1, 0.0f);
    fragColor = vec4(inColor1 * (ubo.ambient + (1.0f - ubo.ambient) * max(dot(vec3(outNormal1), ubo.lightDir), 0.0f)), ubo.alpha);
  } else {
    // 1 - 2
    vPos = (2.0f - ubo.fraction) * inPosition3 + (ubo.fraction - 1.0f) * inPosition4;
    vec4 outNormal3 = ubo.mvp * vec4(inNormal3, 0.0f);
    fragColor = vec4(inColor3 * (ubo.ambient + (1.0f - ubo.ambient) * max(dot(vec3(outNormal3), ubo.lightDir), 0.0f)), ubo.alpha);
  }
  gl_Position = ubo.mvp * vec4(vPos, 1.0f);
}
