TARGET = myapp
TEMPLATE = lib
#CONFIG -= app_bundle
CONFIG += c++1z
QT-=core
LIBS   += -lglfw -lvulkan  -ltinygltf

VPATH += ./src
DEPENDPATH += ./src
INCLUDEPATH += $$PWD/include
SOURCES += \
    app/axispipeline.cpp \
    app/gltfmodel.cpp \
    app/gltfnode.cpp \
    app/imagepipeline.cpp \
    app/imageresource.cpp \
    app/logicaldevice.cpp \
    app/meshpipelinebase.cpp \
    app/meshpipeline.cpp \
    app/meshmaterialpipeline.cpp \
    app/meshtexturepipeline.cpp \
    app/meshtexskinpipeline.cpp \
    app/morphexpanddual.cpp \
    app/panel.cpp \
    app/physicaldevice.cpp \
    app/pipelinebase.cpp \
    app/queuethread.cpp \
    app/textureresource.cpp \
    imgui/imgui.cpp \
    imgui/imgui_draw.cpp \
    imgui/imgui_tables.cpp \
    imgui/imgui_widgets.cpp \
    imgui/imgui_impl_glfw.cpp \
    imgui/imgui_impl_vulkan.cpp \
    imgui/imgui_demo.cpp \
    imgui_fd/ImGuiFileDialog.cpp \
    app/bufferresource.cpp \
    app/config.cpp \
    app/dualcompound.cpp \
    app/dualpolyhedron.cpp \
    app/expandedpolyhedron.cpp \
    app/face.cpp \
    app/lodepng.cpp \
    app/morphbase.cpp \
    app/morphdual.cpp \
    app/morphregular.cpp \
    app/polyhedron.cpp \
    app/regularpolyhedron.cpp \
    app/shadermodule.cpp \
    app/swapchainimageresource.cpp \
    app/truncatedpolyhedron.cpp \
    app/vulkanlib.cpp \
    app/vulkanwindow.cpp \
    app/zonohedron.cpp \
    app/computeresource.cpp \
    app/graphicsresource.cpp \
    app/graphicspipelineresource.cpp \
    app/transformsolid.cpp \
    app/solidfillpipeline.cpp \
    app/solidlinepipeline.cpp \
    app/morphpipeline.cpp \
    app/solidpointpipeline.cpp \
    app/morphedge.cpp \
    app/compoundpolyhedron.cpp \
    app/singletimecommands.cpp \
    app/renderpasscommands.cpp \
    app/solidfilltexturepipeline.cpp \
    app/solidfillbasepipeline.cpp \
    app/stellatedpolyhedron.cpp

DISTFILES += \
    shaders/axis.frag \
    shaders/compile.sh \
    shaders/image.frag \
    shaders/image.vert \
    shaders/mesh.frag \
    shaders/mesh.vert \
    shaders/mesh1.frag \
    shaders/mesh1.vert \
    shaders/mesh2.frag \
    shaders/mesh2.vert \
    shaders/morph.vert \
    shaders/axis.vert \
    shaders/object.frag \
    shaders/solid_fill.vert \
    shaders/solid_fill_texture.frag \
    shaders/solid_line.vert \
    shaders/solid_fill.frag \
    shaders/solid_fill_texture.vert \
    shaders/shader.comp

HEADERS += \
    app/axispipeline.h \
    app/common.h \
    app/gltfmodel.h \
    app/gltfnode.h \
    app/imagepipeline.h \
    app/imageresource.h \
    app/logicaldevice.h \
    app/meshpipelinebase.h \
    app/meshpipeline.h \
    app/meshmaterialpipeline.h \
    app/meshtexturepipeline.h \
    app/meshtexskinpipeline.h \
    app/morphexpanddual.h \
    app/panel.h \
    app/physicaldevice.h \
    app/pipelinebase.h \
    app/queuethread.h \
    app/textureresource.h \
    imgui/imgui.h \
    imgui/imconfig.h \
    imgui/imstb_rectpack.h \
    imgui/imstb_truetype.h \
    imgui/imgui_internal.h \
    imgui/imgui_impl_glfw.h \
    imgui/imgui_impl_vulkan.h \
    imgui_fd/ImGuiFileDialog.h \
    imgui_fd/ImGuiFileDialogConfig.h \
    app/bufferresource.h \
    app/config.h \
    app/dualcompound.h \
    app/dualpolyhedron.h \
    app/expandedpolyhedron.h \
    app/face.h \
    app/lodepng.h \
    app/morphbase.h \
    app/morphdual.h \
    app/morphregular.h \
    app/polyhedron.h \
    app/regularpolyhedron.h \
    app/shadermodule.h \
    app/swapchainimageresource.h \
    app/truncatedpolyhedron.h \
    app/vulkanlib.h \
    app/vulkanwindow.h \
    app/zonohedron.h \
    app/computeresource.h \
    app/graphicsresource.h \
    app/graphicspipelineresource.h \
    app/transformsolid.h \
    app/solidfillpipeline.h \
    app/solidlinepipeline.h \
    app/morphpipeline.h \
    app/solidpointpipeline.h \
    app/morphedge.h \
    app/compoundpolyhedron.h \
    app/singletimecommands.h \
    app/renderpasscommands.h \
    app/solidfilltexturepipeline.h \
    app/solidfillbasepipeline.h \
    app/stellatedpolyhedron.h
