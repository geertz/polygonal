TEMPLATE = subdirs
CONFIG+=ordered
CONFIG+=c++17
QMAKE_CXXFLAGS+=c++17
SUBDIRS = \
    src \
    app \
    tests

app.depends = src
tests.depends = src
